/*
 * trace.h
 *
 *  Created on: 17 ���. 2016 �.
 *      Author: Yuriy
 */

#ifndef SRC_TRACE_H_
#define SRC_TRACE_H_


#ifdef DEBUG
#include "SEGGER_RTT.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#ifdef DEBUG
	#define TRACE_INIT()				SEGGER_RTT_Init()
	#define TRACE(x)					SEGGER_RTT_WriteString(0,x)
	#define TRACE_PRINTF(fmt, ...)		SEGGER_RTT_printf(0, fmt, ## __VA_ARGS__)

	#define TRACE_WHITE(str);			TRACE(RTT_CTRL_TEXT_BRIGHT_WHITE); 	TRACE(str);
	#define TRACE_GREEN(str);			TRACE(RTT_CTRL_TEXT_BRIGHT_GREEN); 	TRACE(str);
	#define TRACE_RED(str);				TRACE(RTT_CTRL_TEXT_BRIGHT_RED); 	TRACE(str);
	#define TRACE_BLUE(str);			TRACE(RTT_CTRL_TEXT_BRIGHT_BLUE); 	TRACE(str);
#else
	#define TRACE_INIT(x)
	#define TRACE(x)
	#define TRACE_PRINTF(fmt, ...)		SEGGER_RTT_printf(0, fmt, ## __VA_ARGS__)
#endif

#ifdef __cplusplus
}
#endif

#endif /* SRC_TRACE_H_ */
