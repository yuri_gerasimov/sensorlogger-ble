/*
 * rtc.cpp
 *
 *  Created on: 06.08.2015
 *      Author: Yuriy
 */
#include <string.h>
#include <str.h>
#include <em_assert.h>
#include <em_cmu.h>
#include <em_rtc.h>

#include "rtc_cal.h"

static const uint8_t month_days[12] = { 31,28,31,30,31,30,31,31,30,31,30,31 };

RealTimeClock::RealTimeClock(void)
{
	m_ulRtcTicks = 0;

	m_dt.day = 1;
	m_dt.month = 10;
	m_dt.year = 16;
	m_dt.hours = 0;
	m_dt.minutes = 0;
	m_dt.seconds = 0;
	m_dt.day_of_week = 6;
}

void RealTimeClock::initHW(void)
{
	RTC_Init_TypeDef rtcInit = RTC_INIT_DEFAULT;

	/* Enable LE domain registers */
	CMU_ClockEnable(cmuClock_CORELE, true);

	/* Enable LFXO as LFACLK in CMU. This will also start LFXO */
	CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFXO);

	/* Set a clock divisor of 32 to reduce power consumption. */
	CMU_ClockDivSet(cmuClock_RTC, cmuClkDiv_32);
	// CMU_ClockDivSet(cmuClock_RTC, cmuClkDiv_32768);

	/* Enable RTC clock */
	CMU_ClockEnable(cmuClock_RTC, true);

	/* Initialize RTC */
	rtcInit.enable = false; 	/* Do not start RTC after initialization is complete. */
	rtcInit.debugRun = false; 	/* Halt RTC when debugging. */
	rtcInit.comp0Top = true; 	/* Wrap around on COMP0 match. */
	RTC_Init(&rtcInit);

	/* Interrupt every minute */
	RTC_CompareSet(0, ((RTC_FREQ / 32) * 1) - 1);
	//RTC_CompareSet(0, 0UL);

	/* Enable interrupt */
	NVIC_EnableIRQ(RTC_IRQn);
	RTC_IntEnable(RTC_IEN_COMP0);

	NVIC_SetPriority(RTC_IRQn, 6);

	/* Start Counter */
	RTC_Enable(true);
}

void RealTimeClock::read(date_time_t *pDestDateTime)
{
	EFM_ASSERT(pDestDateTime);
	memcpy(pDestDateTime,&m_dt,sizeof(m_dt));
}

date_time_t* RealTimeClock::get(void)
{
	return &m_dt;
}

void RealTimeClock::set(date_time_t *pSrcDateTime)
{
	EFM_ASSERT(pSrcDateTime);
	if (pSrcDateTime->day > 0 && pSrcDateTime->day < 32	&& pSrcDateTime->month > 0	&& pSrcDateTime->month < 13
			&& pSrcDateTime->year < 100	&& pSrcDateTime->hours < 24	&& pSrcDateTime->minutes < 60	&& pSrcDateTime->seconds < 60)
		memcpy(&m_dt, pSrcDateTime, sizeof(m_dt));
}

void RealTimeClock::handleTick(void)
{
	// Increase time on one second
	m_ulRtcTicks++;
//	bIsRtcEvent = true;

	m_dt.seconds++;
	if (m_dt.seconds > 59)
	{
		m_dt.seconds = 0;
		m_dt.minutes++;
		if (m_dt.minutes > 59)
		{
			m_dt.minutes = 0;
			m_dt.hours++;
			if (m_dt.hours > 23)
			{
				m_dt.hours = 0;
				m_dt.day++;

				if ((m_dt.month != 2 && m_dt.day > month_days[m_dt.month]) || (m_dt.month == 2 && m_dt.year % 4 && m_dt.day > 29) || (m_dt.month == 2 && m_dt.year % 4 > 0 && m_dt.day > 28))
				{
					m_dt.day = 1;
					m_dt.month++;
					if (m_dt.month > 12)
					{
						m_dt.month = 1;
						m_dt.year++;
					}
				}
			}
		}
	}
}

datetime_t RealTimeClock::getDateTime(void)
{
	/* Pack date and time into the uint32_t variable */
	return (datetime_t)(((uint32_t)(m_dt.year + 20) << 25) | ((uint32_t)m_dt.month << 21) | ((uint32_t)m_dt.day << 16) | ((uint32_t)m_dt.hours << 11) | ((uint32_t)m_dt.minutes << 5) | ((uint32_t)m_dt.seconds >> 1));
}

void RealTimeClock::format(char *str, date_time_t* pSrcDateTime)
{
	char buffer[16];
	*str = 0;
	strCat(str,strutoa(m_dt.year + 2000, buffer));
	strCat(str,"-");
	strCat(str,strutoaz(m_dt.month, buffer,2));
	strCat(str,"-");
	strCat(str,strutoaz(m_dt.day, buffer,2));
	strCat(str," ");
	strCat(str,strutoaz(m_dt.hours, buffer,2));
	strCat(str,":");
	strCat(str,strutoaz(m_dt.minutes, buffer,2));
	strCat(str,":");
	strCat(str,strutoaz(m_dt.seconds, buffer,2));
}

void RealTimeClock::formatTime(char *str, date_time_t* pSrcDateTime)
{
	char buffer[16];
	*str = 0;
	strCat(str,strutoaz(m_dt.hours, buffer,2));
	strCat(str,":");
	strCat(str,strutoaz(m_dt.minutes, buffer,2));
	strCat(str,":");
	strCat(str,strutoaz(m_dt.seconds, buffer,2));
}

void RealTimeClock::formatDate(char *str, date_time_t* pSrcDateTime)
{
	char buffer[16];
	*str = 0;
	strCat(str,strutoa(m_dt.year + 2000, buffer));
	strCat(str,"-");
	strCat(str,strutoaz(m_dt.month, buffer,2));
	strCat(str,"-");
	strCat(str,strutoaz(m_dt.day, buffer,2));
}

#if 0
static bool bIsRtcEvent = false;
static uint32_t ulRtcTicks = 0;
static const uint8_t month_days[12] = { 31,28,31,30,31,30,31,31,30,31,30,31};
static date_time dt =
{
		.hours = 0,
		.minutes = 0,
		.seconds = 0,
		.day = 1,
		.month = 1,
		.year = 0,
		.day_of_week = 6	//	Saturday
};

/**************************************************************************//**
 * @brief RTC Interrupt Handler.
 *        Updates minutes and hours.
 *****************************************************************************/
void rtcTickHandler(void)
{
	// Increase time by one second
	ulRtcTicks++;
	bIsRtcEvent = true;

	dt.seconds++;
	if (dt.seconds > 59) {
		dt.seconds = 0;
		dt.minutes++;
		if (dt.minutes > 59) {
			dt.minutes = 0;
			dt.hours++;
			if (dt.hours > 23) {
				dt.hours = 0;
				dt.day++;

				if ((dt.month != 2 && dt.day > month_days[dt.month])
						|| (dt.month == 2 && dt.year % 4 && dt.day > 29)
						|| (dt.month == 2 && dt.year % 4 > 0 && dt.day > 28)) {
					dt.day = 1;
					dt.month++;
					if (dt.month > 12) {
						dt.month = 1;
						dt.year++;
					}
				}
			}
		}
	}
}

/**************************************************************************//**
 * @brief Enables LFACLK and selects LFXO as clock source for RTC
 *        Sets up the RTC to generate an interrupt every minute.
 *****************************************************************************/
void rtcSetup(void)
{
  RTC_Init_TypeDef rtcInit = RTC_INIT_DEFAULT;

  /* Enable LE domain registers */
  CMU_ClockEnable(cmuClock_CORELE, true);

  /* Enable LFXO as LFACLK in CMU. This will also start LFXO */
  CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFXO);

  /* Set a clock divisor of 32 to reduce power consumption. */
  CMU_ClockDivSet(cmuClock_RTC, cmuClkDiv_32);
 // CMU_ClockDivSet(cmuClock_RTC, cmuClkDiv_32768);

  /* Enable RTC clock */
  CMU_ClockEnable(cmuClock_RTC, true);

  /* Initialize RTC */
  rtcInit.enable   = false;  /* Do not start RTC after initialization is complete. */
  rtcInit.debugRun = false;  /* Halt RTC when debugging. */
  rtcInit.comp0Top = true;   /* Wrap around on COMP0 match. */
  RTC_Init(&rtcInit);

  /* Interrupt every minute */
  RTC_CompareSet(0, ((RTC_FREQ / 32) * 1 ) - 1 );
  //RTC_CompareSet(0, 0UL);

  /* Enable interrupt */
  NVIC_EnableIRQ(RTC_IRQn);
  RTC_IntEnable(RTC_IEN_COMP0);

  NVIC_SetPriority(RTC_IRQn,6);

  /* Start Counter */
  RTC_Enable(true);
}

date_time* rtcGetTime(void)
{
	return &dt;
}

void rtsSetTime(date_time* pDateTime)
{
	memcpy(&dt,pDateTime,sizeof(dt));
}

void rtcFormatDateTime(char *str, date_time* t)
{
//	sprintf(str,"%04d.%02d.%02d %02d:%02d:%02d",2000 + t->year, t->month, t->day, t->hours,t->minutes,t->seconds);
}

void rtcFormatTime(char *str, date_time* t)
{
//	sprintf(str,"%02d:%02d:%02d",t->hours,t->minutes,t->seconds);
}

void rtcFormatDate(char *str, date_time* t)
{
//	sprintf(str,"%02d.%02d.%02d",t->year,t->month,t->day);
}

bool rtcIsEvent(void)
{
	if(bIsRtcEvent == false) return false;
	bIsRtcEvent = false;
	return true;
}

datetime_t rtcGetDateTime(void)
{
//	date_time *rtc = rtcGetTime();
	/* Pack date and time into a uint32_t variable */
	return (datetime_t)(((uint32_t)(dt.year +2000 - 1980) << 25) | ((uint32_t)dt.month << 21) | ((uint32_t)dt.day << 16) | ((uint32_t)dt.hours << 11) | ((uint32_t)dt.minutes << 5) | ((uint32_t)dt.seconds >> 1));
}
#endif
