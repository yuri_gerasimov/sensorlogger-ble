/*
 * rtc.h
 *
 *  Created on: 06.08.2015
 *      Author: Yuriy
 */

#ifndef RTC_CAL_H_
#define RTC_CAL_H_

#include <stdint.h>
#include <stddef.h>

#define RTC_FREQ    32768

typedef uint32_t datetime_t;

/* Initial setup to 2000-01-01 00:00 */
typedef struct
{
	uint8_t hours;
	uint8_t minutes;
	uint8_t seconds;
	uint8_t day;
	uint8_t month;
	uint8_t year;
	uint8_t day_of_week;	//	Saturday
	uint8_t uReserved1;
} date_time_t;

class RealTimeClock
{
private:
	uint32_t m_ulRtcTicks;
	date_time_t m_dt;

public:
	RealTimeClock(void);

	static void initHW(void);
	void handleTick(void);

	date_time_t* get(void);
	void read(date_time_t *pDestDateTime);
	void set(date_time_t *pSrcDateTime);
	datetime_t getDateTime(void);

	void format(char *str, date_time_t* pSrcDateTime);
	void formatTime(char *str, date_time_t* pSrcDateTime);
	void formatDate(char *str, date_time_t* pSrcDateTime);

	uint32_t getTicks(void)
	{
		return m_ulRtcTicks;
	}
};

/*s
#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#define RTC_FREQ    32768

//typedef uint32_t datetime_t;

void rtcSetup(void);
void rtcTickHandler(void);
bool rtcIsEvent(void);
date_time * rtcGetTime(void);
void rtsSetTime(date_time* pDateTime);
void rtcFormatTime(char *str, date_time* t);
void rtcFormatDate(char *str, date_time* t);
void rtcFormatDateTime(char *str, date_time* t);
datetime_t rtcGetDateTime(void);

#ifdef __cplusplus
}
#endif
*/
#endif /* RTC_H_ */
