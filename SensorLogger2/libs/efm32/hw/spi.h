/*
 * spi.h
 *
 *  Created on: 22.07.2015
 *      Author: Yuriy
 */

#ifndef SPI_H_
#define SPI_H_

#include "em_assert.h"
#include "em_usart.h"
#include "em_gpio.h"
#include "em_cmu.h"

//#include "hw_config.h"

#define SPI_ClearCS(port,pin)	GPIO_PinOutClear(port,pin)
#define SPI_SetCS(port,pin)		GPIO_PinOutSet(port,pin)

#define SPI_LowCS(port,pin)		GPIO_PinOutClear(port,pin)
#define SPI_HighCS(port,pin)	GPIO_PinOutSet(port,pin)

class SPI
{
private:
	uint32_t m_uLocation;

public:
	USART_TypeDef *m_port;
	SPI(USART_TypeDef *port, uint32_t location);
	bool Init(uint32_t uBaudrate = 1000000, bool isMaster = true, bool bIsMSB = true, bool isAutoCS = false, bool bUseUsartCSpin = false);

	//	bool Init(USART_TypeDef *port, uint8_t location);
	//	void Init(uint32_t baudrate, bool master);

	USART_TypeDef *GetUSART(void)
	{
		return m_port;
	}

	void Enable(bool bIsEnabled);
	void SetDatabits(USART_Databits_TypeDef bits);
	void WriteBuffer(uint8_t *pData, int length);
	void BaudrateSyncSet(uint32_t boudrate)
	{
		USART_BaudrateSyncSet(m_port, 0, boudrate);
	}

	void WriteByteFast(uint8_t data)
	{
		/* Write SPI address */
		USART_Tx(m_port, data);

		/* Wait for transmission to finish */
		while (!(m_port->STATUS & USART_STATUS_TXC)) ;
	}

	uint8_t WriteByte(uint8_t data)
	{
		/* Write SPI address */
		USART_Tx(m_port, data);

		/* Wait for transmission to finish */
		while (!(m_port->STATUS & USART_STATUS_TXC)) ;

		//	TODO: check for FAST WRITE operations -> transformed to WriteByteFast
		/* Just ignore data read back */
		return USART_Rx(m_port);
	}

	uint8_t ReadByte(void)
	{
	  /* Write SPI address */
	  USART_Tx(m_port, 0x00);

	  /* Wait for transmission to finish */
	  while (!(m_port->STATUS & USART_STATUS_TXC)) ;

	  /* Return the data read from SPI buffer */
	  return(USART_Rx(m_port));
	}

	void WriteWord(uint16_t data)
	{
		/* Write SPI address */
		USART_TxDouble(m_port, data);

		/* Wait for transmission to finish */
		while (!(m_port->STATUS & USART_STATUS_TXC)) ;

		/* Just ignore data read back */
		USART_Rx(m_port);
	}

	static void ClearCS(GPIO_Port_TypeDef port, uint8_t pin)
	{
		GPIO_PinOutClear(port,pin);
	}

	static void SetCS(GPIO_Port_TypeDef port, uint8_t pin)
	{
		GPIO_PinOutSet(port,pin);
	}
};


#endif /* SPI_H_ */
