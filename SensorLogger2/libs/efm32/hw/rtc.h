/*
 * rtc.h
 *
 *  Created on: 06.08.2015
 *      Author: Yuriy
 */

#ifndef RTC_H_
#define RTC_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#define RTC_FREQ    32768

/* Initial setup to 2000-01-01 00:00 */
typedef struct
{
	uint8_t hours;
	uint8_t minutes;
	uint8_t seconds;
	uint8_t day;
	uint8_t month;
	uint8_t year;
	uint8_t day_of_week;	//	Saturday
} date_time;

typedef uint32_t datetime_t;

void rtcSetup(void);
void rtcTickHandler(void);
bool rtcIsEvent(void);
date_time * rtcGetTime(void);
void rtsSetTime(date_time* pDateTime);
void rtcFormatTime(char *str, date_time* t);
void rtcFormatDate(char *str, date_time* t);
void rtcFormatDateTime(char *str, date_time* t);
datetime_t rtcGetDateTime(void);

#ifdef __cplusplus
}
#endif

#endif /* RTC_H_ */
