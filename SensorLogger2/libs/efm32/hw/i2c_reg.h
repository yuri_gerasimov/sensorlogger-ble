/*
 * i2c_reg.h
 *
 *  Created on: 03.12.2015
 *      Author: Yuriy
 */

#ifndef I2C_REG_H_
#define I2C_REG_H_

#include <stdint.h>
#include <stdbool.h>

#include "i2c_reg.h"
#include "i2cspm.h"

class I2CReg
{
private:
	I2C_TypeDef * m_i2c;
	uint8_t m_i2cAddr;

public:
	I2CReg();
	void Init(I2C_TypeDef *i2c, uint8_t uI2Caddr);

	bool Test(uint8_t uRegAddr);

	bool WriteU8(uint8_t uRegAddr, uint8_t uValue);
	bool Write(uint8_t uRegAddr, void *pBuffer, uint8_t uLenBytes);

	uint8_t ReadU8(uint8_t uRegAddr);
	int8_t ReadS8(uint8_t uRegAddr);

	uint16_t ReadU16(uint8_t uRegAddr);
	int16_t ReadS16(uint8_t uRegAddr);
	uint16_t ReadU16LE(uint8_t uRegAddr);
	int16_t ReadS16LE(uint8_t uRegAddr);

	bool Read(uint8_t uRegAddr, void *pBuffer, uint8_t uLenBytes);

};

/*
bool I2C_RegWrite(I2C_TypeDef *i2c, uint8_t uI2Caddr, uint8_t uRegAddr, uint8_t uValue);
bool I2C_RegRead(I2C_TypeDef *i2c, uint8_t uI2Caddr, uint8_t uRegAddr, void *pBuffer, uint8_t uLenBytes);
*/

#endif /* I2C_REG_H_ */
