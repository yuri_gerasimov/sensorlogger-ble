/*
 * rtc.cpp
 *
 *  Created on: 06.08.2015
 *      Author: Yuriy
 */

#include <stdio.h>
#include <string.h>

#include "em_cmu.h"
#include "em_rtc.h"
#include "rtc.h"

static bool bIsRtcEvent = false;
static uint32_t ulRtcTicks = 0;
static const uint8_t month_days[12] = { 31,28,31,30,31,30,31,31,30,31,30,31};
static date_time dt =
{
		.hours = 0,
		.minutes = 0,
		.seconds = 0,
		.day = 1,
		.month = 1,
		.year = 0,
		.day_of_week = 6	//	Saturday
};

/**************************************************************************//**
 * @brief RTC Interrupt Handler.
 *        Updates minutes and hours.
 *****************************************************************************/
void rtcTickHandler(void)
{
	// Increase time by one second
	ulRtcTicks++;
	bIsRtcEvent = true;

	dt.seconds++;
	if (dt.seconds > 59) {
		dt.seconds = 0;
		dt.minutes++;
		if (dt.minutes > 59) {
			dt.minutes = 0;
			dt.hours++;
			if (dt.hours > 23) {
				dt.hours = 0;
				dt.day++;

				if ((dt.month != 2 && dt.day > month_days[dt.month])
						|| (dt.month == 2 && dt.year % 4 && dt.day > 29)
						|| (dt.month == 2 && dt.year % 4 > 0 && dt.day > 28)) {
					dt.day = 1;
					dt.month++;
					if (dt.month > 12) {
						dt.month = 1;
						dt.year++;
					}
				}
			}
		}
	}
}

/**************************************************************************//**
 * @brief Enables LFACLK and selects LFXO as clock source for RTC
 *        Sets up the RTC to generate an interrupt every minute.
 *****************************************************************************/
void rtcSetup(void)
{
  RTC_Init_TypeDef rtcInit = RTC_INIT_DEFAULT;

  /* Enable LE domain registers */
  CMU_ClockEnable(cmuClock_CORELE, true);

  /* Enable LFXO as LFACLK in CMU. This will also start LFXO */
  CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFXO);

  /* Set a clock divisor of 32 to reduce power consumption. */
  CMU_ClockDivSet(cmuClock_RTC, cmuClkDiv_32);
 // CMU_ClockDivSet(cmuClock_RTC, cmuClkDiv_32768);

  /* Enable RTC clock */
  CMU_ClockEnable(cmuClock_RTC, true);

  /* Initialize RTC */
  rtcInit.enable   = false;  /* Do not start RTC after initialization is complete. */
  rtcInit.debugRun = false;  /* Halt RTC when debugging. */
  rtcInit.comp0Top = true;   /* Wrap around on COMP0 match. */
  RTC_Init(&rtcInit);

  /* Interrupt every minute */
  RTC_CompareSet(0, ((RTC_FREQ / 32) * 1 ) - 1 );
  //RTC_CompareSet(0, 0UL);

  /* Enable interrupt */
  NVIC_EnableIRQ(RTC_IRQn);
  RTC_IntEnable(RTC_IEN_COMP0);

  NVIC_SetPriority(RTC_IRQn,6);

  /* Start Counter */
  RTC_Enable(true);
}

date_time* rtcGetTime(void)
{
	return &dt;
}

void rtsSetTime(date_time* pDateTime)
{
	memcpy(&dt,pDateTime,sizeof(dt));
}

void rtcFormatDateTime(char *str, date_time* t)
{
	sprintf(str,"%04d.%02d.%02d %02d:%02d:%02d",2000 + t->year, t->month, t->day, t->hours,t->minutes,t->seconds);
}

void rtcFormatTime(char *str, date_time* t)
{
	sprintf(str,"%02d:%02d:%02d",t->hours,t->minutes,t->seconds);
}

void rtcFormatDate(char *str, date_time* t)
{
	sprintf(str,"%02d.%02d.%02d",t->year,t->month,t->day);
}

bool rtcIsEvent(void)
{
	if(bIsRtcEvent == false) return false;
	bIsRtcEvent = false;
	return true;
}

datetime_t rtcGetDateTime(void)
{
//	date_time *rtc = rtcGetTime();
	/* Pack date and time into a uint32_t variable */
	return (datetime_t)(((uint32_t)(dt.year +2000 - 1980) << 25) | ((uint32_t)dt.month << 21) | ((uint32_t)dt.day << 16) | ((uint32_t)dt.hours << 11) | ((uint32_t)dt.minutes << 5) | ((uint32_t)dt.seconds >> 1));
}
