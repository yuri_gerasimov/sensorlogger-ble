/*
 * i2c_reg.cpp
 *
 *  Created on: 03.12.2015
 *      Author: Yuriy
 */

#include <stdint.h>
#include <stdbool.h>

#include "em_assert.h"

#include "i2c_reg.h"
#include "i2cspm.h"

I2CReg::I2CReg()
{
	m_i2c = 0UL;
	m_i2cAddr = 0;
}

void I2CReg::Init(I2C_TypeDef *i2c, uint8_t uI2Caddr)
{
	EFM_ASSERT(i2c);
	EFM_ASSERT(uI2Caddr);

	m_i2c = i2c;
	m_i2cAddr = uI2Caddr;
}

//bool I2CReg::Test(uint8_t uRegAddr);

bool I2CReg::WriteU8(uint8_t uRegAddr, uint8_t uValue)
{
	uint8_t data[2] = { uRegAddr, uValue};
	/* Transfer structure */
	I2C_TransferSeq_TypeDef i2cTransfer;

	/* Initializing I2C transfer */
	i2cTransfer.addr = m_i2cAddr;
	/* Master write */
	i2cTransfer.flags = I2C_FLAG_WRITE;

	/* Transmit buffer, 2 bytes to send */
	i2cTransfer.buf[0].data = data;
	i2cTransfer.buf[0].len = 2;

	i2cTransfer.buf[1].data = 0UL;
	i2cTransfer.buf[1].len = 0;

	if(I2CSPM_Transfer(m_i2c, &i2cTransfer) == i2cTransferDone)	return true;
	return false;
}

bool I2CReg::Write(uint8_t uRegAddr, void *pBuffer, uint8_t uLenBytes)
{
	/* Transfer structure */
	I2C_TransferSeq_TypeDef i2cTransfer;

	/* Initializing I2C transfer */
	i2cTransfer.addr = m_i2cAddr;
	/* Master write */
	i2cTransfer.flags = I2C_FLAG_WRITE_WRITE;

	/* Transmit buffer, no data to send */
	i2cTransfer.buf[0].data = &uRegAddr;
	i2cTransfer.buf[0].len = 1;

	/* Receive buffer, two bytes to receive */
	i2cTransfer.buf[1].data = (uint8_t  *)pBuffer;
	i2cTransfer.buf[1].len = uLenBytes;

	if(I2CSPM_Transfer(m_i2c, &i2cTransfer) == i2cTransferDone)	return true;
	return false;
}

//----------------------------------------------------------------------------
//	READ operations
//----------------------------------------------------------------------------

bool I2CReg::Read(uint8_t uRegAddr, void *pBuffer, uint8_t uLenBytes)
{
	/* Transfer structure */
		I2C_TransferSeq_TypeDef i2cTransfer;

		/* Initializing I2C transfer */
		i2cTransfer.addr = m_i2cAddr;
		/* Master write */
		i2cTransfer.flags = I2C_FLAG_WRITE_READ;

		/* Transmit buffer, no data to send */
		i2cTransfer.buf[0].data = &uRegAddr;
		i2cTransfer.buf[0].len = 1;

		/* Receive buffer, two bytes to receive */
		i2cTransfer.buf[1].data = (uint8_t  *)pBuffer;
		i2cTransfer.buf[1].len = uLenBytes;

		if(I2CSPM_Transfer(m_i2c, &i2cTransfer) == i2cTransferDone)	return true;
		return false;
}

uint8_t I2CReg::ReadU8(uint8_t uRegAddr)
{
	uint8_t data = 0;
	/* Transfer structure */
	I2C_TransferSeq_TypeDef i2cTransfer;

	/* Initializing I2C transfer */
	i2cTransfer.addr = m_i2cAddr;
	/* Master write */
	i2cTransfer.flags = I2C_FLAG_WRITE_READ;

	/* Transmit buffer, no data to send */
	i2cTransfer.buf[0].data = &uRegAddr;
	i2cTransfer.buf[0].len = 1;

	/* Receive buffer, two bytes to receive */
	i2cTransfer.buf[1].data = &data;
	i2cTransfer.buf[1].len = 1;

	if(I2CSPM_Transfer(m_i2c, &i2cTransfer) != i2cTransferDone)	return 0;
	return data;
}

int8_t I2CReg::ReadS8(uint8_t uRegAddr)
{
	return (int8_t)ReadU8(uRegAddr);
}

uint16_t I2CReg::ReadU16(uint8_t uRegAddr)
{
	uint16_t data = 0;

	/* Transfer structure */
	I2C_TransferSeq_TypeDef i2cTransfer;

	/* Initializing I2C transfer */
	i2cTransfer.addr = m_i2cAddr;
	/* Master write */
	i2cTransfer.flags = I2C_FLAG_WRITE_READ;

	/* Transmit buffer, no data to send */
	i2cTransfer.buf[0].data = &uRegAddr;
	i2cTransfer.buf[0].len = 1;

	/* Receive buffer, two bytes to receive */
	i2cTransfer.buf[1].data = (uint8_t *)&data;
	i2cTransfer.buf[1].len = 2;

	if(I2CSPM_Transfer(m_i2c, &i2cTransfer) != i2cTransferDone)	return 0;
	return data;
}

int16_t I2CReg::ReadS16(uint8_t uRegAddr)
{
	return (int16_t)ReadU16(uRegAddr);
}

uint16_t I2CReg::ReadU16LE(uint8_t uRegAddr)
{
	uint16_t temp = ReadU16(uRegAddr);
	return (temp >> 8) | (temp << 8);
}

int16_t I2CReg::ReadS16LE(uint8_t uRegAddr)
{
	return (int16_t)ReadU16LE(uRegAddr);
}
#if 0
bool I2C_RegWrite(I2C_TypeDef *i2c, uint8_t uI2Caddr, uint8_t uRegAddr, uint8_t uValue)
{
	uint8_t data[2] = { uRegAddr, uValue};
	/* Transfer structure */
	I2C_TransferSeq_TypeDef i2cTransfer;

	/* Initializing I2C transfer */
	i2cTransfer.addr = uI2Caddr;
	/* Master write */
	i2cTransfer.flags = I2C_FLAG_WRITE;

	/* Transmit buffer, 2 bytes to send */
	i2cTransfer.buf[0].data = data;
	i2cTransfer.buf[0].len = 2;

	i2cTransfer.buf[1].data = 0UL;
	i2cTransfer.buf[1].len = 0;

	if(I2CSPM_Transfer(i2c, &i2cTransfer) == i2cTransferDone)	return true;
	return false;
}

bool I2C_RegRead(I2C_TypeDef *i2c, uint8_t uI2Caddr, uint8_t uRegAddr, void *pBuffer, uint8_t uLenBytes)
{
	/* Transfer structure */
	I2C_TransferSeq_TypeDef i2cTransfer;

	/* Initializing I2C transfer */
	i2cTransfer.addr = uI2Caddr;
	/* Master write */
	i2cTransfer.flags = I2C_FLAG_WRITE_READ;

	/* Transmit buffer, no data to send */
	i2cTransfer.buf[0].data = &uRegAddr;
	i2cTransfer.buf[0].len = 1;

	/* Receive buffer, two bytes to receive */
	i2cTransfer.buf[1].data = (uint8_t  *)pBuffer;
	i2cTransfer.buf[1].len = uLenBytes;

	if(I2CSPM_Transfer(i2c, &i2cTransfer) == i2cTransferDone)	return true;
	return false;
}
#endif
