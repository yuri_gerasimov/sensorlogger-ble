/*
 * spi.cpp
 *
 *  Created on: 22.07.2015
 *      Author: Yuriy
 */
#include "SPI.h"
#include "em_cmu.h"
#include "em_assert.h"

#include "config/hw_config.h"

SPI::SPI(USART_TypeDef *port, uint32_t location)
{
	EFM_ASSERT(port);
	m_port = port;
	m_uLocation = location;
}

bool SPI::Init(uint32_t uBaudrate, bool isMaster, bool bIsMSB, bool isAutoCS, bool bUseUsartCSpin)
{
	CMU_Clock_TypeDef m_clock;
	GPIO_Port_TypeDef m_portCS, m_portMOSI, m_portMISO, m_portCLK;
	uint8_t m_pinCS, m_pinMOSI, m_pinMISO, m_pinCLK;

	USART_InitSync_TypeDef usartConfig = USART_INITSYNC_DEFAULT;

#if defined( USART0 )
	if(m_port==USART0)
	{
//		port_index = 0;
	}
#endif

#if defined( USART1 )
	if(m_port==USART1)
	{
			m_clock = cmuClock_USART1;

			if(m_uLocation==USART_ROUTE_LOCATION_LOC1)
			{
				m_portCS = m_portMOSI = m_portMISO = m_portCLK = gpioPortD;

				m_pinCS = 3;
				m_pinCLK = 2;
				m_pinMOSI = 0;
				m_pinMISO = 1;
			}
			else if(m_uLocation==USART_ROUTE_LOCATION_LOC2)
			{
				m_portCS = gpioPortF;
				m_portCLK = gpioPortF;
				m_portMOSI = m_portMISO = gpioPortD;

				m_pinCS = 1;
				m_pinCLK = 0;
				m_pinMOSI = 7;
				m_pinMISO = 6;
			}
			else
				return false;
		}
#endif

#if defined( USART2 )
	if(m_port==USART2)
	{
			m_clock = cmuClock_USART2;
			if(m_uLocation==USART_ROUTE_LOCATION_LOC0)
			{
				m_portCS = m_portMOSI = m_portMISO = m_portCLK = gpioPortC;

				m_pinCS = 1;
				m_pinCLK = 4;
				m_pinMISO = 3;
				m_pinMOSI = 2;
			}
			else
				if(m_uLocation==USART_ROUTE_LOCATION_LOC1)
			{
				m_portCS = m_portMOSI = m_portMISO = m_portCLK = gpioPortB;

				m_pinCS = 6;
				m_pinCLK = 5;
				m_pinMISO = 4;
				m_pinMOSI = 3;
			}
	}
#endif

	GPIO_PinModeSet(m_portMOSI, m_pinMOSI, gpioModePushPull, 0);	// Data Out pin
	GPIO_PinModeSet(m_portMISO, m_pinMISO, gpioModeInput, 0);		// Data In pin
	GPIO_PinModeSet(m_portCLK, m_pinCLK, gpioModePushPull, 0);		// Clock pin

	if(bUseUsartCSpin)
		GPIO_PinModeSet(m_portCS, m_pinCS, gpioModePushPull, 1);		// Chip-Select pin

	CMU_ClockEnable(m_clock, true);

	usartConfig.baudrate = uBaudrate;
	usartConfig.msbf = bIsMSB;
	usartConfig.master = isMaster;
	usartConfig.clockMode = usartClockMode0;

	USART_InitSync(m_port, &usartConfig);

	// Enable USART SPI pins (3/4 wires) on location.
	m_port->ROUTE = USART_ROUTE_CLKPEN | USART_ROUTE_TXPEN  | USART_ROUTE_RXPEN | m_uLocation;
	if(isAutoCS && bUseUsartCSpin)
	{
		m_port->CTRL |= USART_CTRL_AUTOCS;	//	enable auto CS pin operation
		m_port->ROUTE |= USART_ROUTE_CSPEN;	//	make the route for CS pin
	}
	return true;
}
/*
void SPI::Init(uint32_t baudrate, bool master)
{
	USART_InitSync_TypeDef usartConfig = USART_INITSYNC_DEFAULT;

	EFM_ASSERT(SPI_USART);

	// Configure GPIOs for SPI pins
	GPIO_PinModeSet(MOSI_PORT, MOSI_PIN, gpioModePushPull, 0);	// Data Out
	GPIO_PinModeSet(MISO_PORT, MISO_PIN, gpioModeInput, 0);		// Data In
	GPIO_PinModeSet(CLK_PORT, CLK_PIN, gpioModePushPull, 0);	// Clock
	GPIO_PinModeSet(CS_PORT, CS_PIN, gpioModePushPull, 1);		// CS

	CMU_ClockEnable(SPI_USART_CLOCK, true);

	usartConfig.baudrate = baudrate;
	usartConfig.msbf = true;
	usartConfig.master = master;
//	usartConfig.clockMode = usartClockMode0;

	USART_InitSync(SPI_USART, &usartConfig);

//	SPI_USART->CTRL |= USART_CTRL_AUTOCS;

	// Enable USART SPI pins (3 wire) on location SPI_LOCATION.
	SPI_USART->ROUTE = USART_ROUTE_CLKPEN | USART_ROUTE_TXPEN  | USART_ROUTE_RXPEN | SPI_LOCATION;
}
*/

void SPI::Enable(bool bIsEnabled)
{
	if(bIsEnabled)
		USART_Enable(m_port,usartEnable);
	else
		USART_Enable(m_port,usartDisable);
}

void SPI::WriteBuffer(uint8_t *txBuffer, int length)
{
	int ii;

	/* Sending the data */
	for (ii = 0; ii < length;  ii++)
	{
	  /* Waiting for the USART to be ready */
	  while (!(m_port->STATUS & USART_STATUS_TXBL)) ;

	  if (txBuffer != 0)
	  {
		/* Writing next byte to USART */
		  m_port->TXDATA = *txBuffer;
		  txBuffer++;
	  }
	  else
	  {
		  m_port->TXDATA = 0;
	  }
	}
	/*Waiting for transmission of last byte */
	while (!(m_port->STATUS & USART_STATUS_TXC)) ;
}


void SPI::SetDatabits(USART_Databits_TypeDef bits)
{
	m_port->FRAME = ((uint32_t) (bits)) | (USART_FRAME_STOPBITS_DEFAULT) | (USART_FRAME_PARITY_DEFAULT);
}

