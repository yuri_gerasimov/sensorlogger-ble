/*
 * defines.h
 *
 *  Created on: 19 июля 2013 г.
 *      Author: Yuriy
 */

#ifndef DEFINES_H_
#define DEFINES_H_

#ifndef NULL
	#define NULL 0L
#endif
#define LOW 0
#define HIGH 1

#define FALSE 0
#define TRUE 1

#define bit(b) (1 << (b))
#define bitRead(value, bit) (((value) >> (bit)) & 0x01)
#define bitSet(value, bit) ((value) |= (1 << (bit)))
#define bitClear(value, bit) ((value) &= ~(1 << (bit)))
#define bitWrite(value, bit, bitvalue) (bitvalue ? bitSet(value, bit) : bitClear(value, bit))

#define bit32(b) (1UL << (b))
#define bitSet32(value, bit) ((value) |= (1UL << (bit)))
#define bitClear32(value, bit) ((value) &= ~(1UL << (bit)))

#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))

#define configOutputLow(ddr,port,pin)		do {	bitSet(ddr,pin); 	\
													bitClear(port,pin);	\
												}	while(0)
#define configOutputHigh(ddr,port,pin)		do {	bitSet(ddr,pin); 	\
													bitSet(port,pin);	\
												}	while(0)
#define configInputLow(ddr,port,pin)		do {	bitClear(ddr,pin); 	\
													bitClear(port,pin);	\
												}	while(0)
#define configInputHigh(ddr,port,pin)		do {	bitClear(ddr,pin); 	\
													bitSet(port,pin);	\
												}	while(0)

#define MAKEWORD(hi_byte,low_byte) (uint16_t)((uint16_t)(hi_byte<<8)|(uint16_t)low_byte)
#define HIBYTE(word) (uint8_t)(word>>8)
#define LOWBYTE(word) (uint8_t)(word)

typedef volatile uint8_t register8_t;
typedef volatile uint16_t register16_t;
typedef volatile uint32_t register32_t;

#define UNUSED_VARIABLE(X)  ((void)(X))
#define UNUSED_PARAMETER(X) UNUSED_VARIABLE(X)
#define UNUSED_RETURN_VALUE(X) UNUSED_VARIABLE(X)

#endif /* DEFINES_H_ */
