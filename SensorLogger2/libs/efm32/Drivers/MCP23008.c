#include "MCP23008.h"

static uint8_t m_Addr;
static I2C_TypeDef *m_i2c;

bool mcp23008_init(I2C_TypeDef *i2c, uint8_t addr)
{
	m_i2c = i2c;
	m_Addr = addr;
	return true;
}

static bool write_register(uint8_t uRegAddr, uint8_t uValue)
{
	uint8_t data[2] = { uRegAddr, uValue};
	/* Transfer structure */
	I2C_TransferSeq_TypeDef i2cTransfer;

	/* Initializing I2C transfer */
	i2cTransfer.addr = m_Addr;
	/* Master write */
	i2cTransfer.flags = I2C_FLAG_WRITE;

	/* Transmit buffer, 2 bytes to send */
	i2cTransfer.buf[0].data = data;
	i2cTransfer.buf[0].len = 2;

	i2cTransfer.buf[1].data = 0UL;
	i2cTransfer.buf[1].len = 0;

	if(I2CSPM_Transfer(m_i2c, &i2cTransfer) == i2cTransferDone)	return true;
	return false;
}

static bool read_register(uint8_t uRegAddr, void *pBuffer, uint8_t uLenBytes)
{
	/* Transfer structure */
	I2C_TransferSeq_TypeDef i2cTransfer;

	/* Initializing I2C transfer */
	i2cTransfer.addr = m_Addr;
	/* Master write */
	i2cTransfer.flags = I2C_FLAG_WRITE_READ;

	/* Transmit buffer, no data to send */
	i2cTransfer.buf[0].data = &uRegAddr;
	i2cTransfer.buf[0].len = 1;

	/* Receive buffer, two bytes to receive */
	i2cTransfer.buf[1].data = (uint8_t  *)pBuffer;
	i2cTransfer.buf[1].len = uLenBytes;

	if(I2CSPM_Transfer(m_i2c, &i2cTransfer) == i2cTransferDone)	return true;
	return false;
}

void mcp23008_io_direction_set(uint8_t nIoPin, bool bIsInput, bool bIsPullUp)
{
	uint8_t data = 0;
	uint8_t data1 = 0;

	read_register(MCP23008_REG_IODIR,data,1);
	read_register(MCP23008_REG_GPPU,data1,1);
	if(bIsInput)
		{
			data|=(1<<nIoPin);
			if(bIsPullUp)	data1|=(1<<nIoPin);
			else data1&=(~(1<<nIoPin));
		}
	else data&=(~(1<<nIoPin));

	write_register(MCP23008_REG_GPPU,data);
	write_register(MCP23008_REG_IODIR,data);
}

void mcp23008_input_polarity_set(uint8_t nIoPin, bool bIsInverted)
{
	uint8_t data = 0;

	read_register(MCP23008_REG_IPOL,data,1);
	if(bIsInverted)	data|=(1<<nIoPin);
	else data&=(~(1<<nIoPin));
	write_register(MCP23008_REG_IPOL,data);
}

void mcp23008_output_set(uint8_t nIoPin)
{

}

void mcp23008_output_clear(uint8_t nIoPin)
{

}
