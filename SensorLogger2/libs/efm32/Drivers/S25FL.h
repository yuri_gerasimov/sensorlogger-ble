/*
 * S25FL1-K.h
 *
 *  Created on: 22.07.2015
 *      Author: Yuriy Gerasimov
 */

#ifndef S25FL1K_H_
#define S25FL1K_H_

#include <stdint.h>
#include <stdbool.h>

#include "spi.h"

// 	http://www.spansion.com/Products/memory/Serial-Flash/Pages/Spansion%20FL.aspx
//	https://github.com/BleepLabs/S25FLx

// S25FL1-K Instruction codes
//	--------- Command Set (Configuration, Status, Erase, Program Commands) ----------
#define S25FL_READ_STATUS_REG1		0x05	//	Read status register-1
#define S25FL_READ_STATUS_REG2		0x35	//	Read status register-2
#define S25FL_READ_STATUS_REG3		0x33	//	Read status register-2
#define S25FL_WRITE_ENABLE			0x06	//	Write Enable
#define S25FL_WRITE_ENABLE_VSR		0x50	//	Write Enable for Volatile Status Register
#define S25FL_WRITE_DISABLE			0x04	//	Write Disable
#define S25FL_WRITE_STATUS_REGS		0x01	//	Write Status Registers
#define S25FL_SET_BURST_WRAP		0x77	//	Set Burst with Wrap
#define S25FL_SET_BP_PROTECTION		0x39	//	Set Block / Pointer Protection
#define S25FL_PAGE_PROGRAM			0x02	//	Page Program
#define S25FL_SECTOR_ERASE			0x20	//	Sector Erase (4 kB)
#define S25FL_BLOCK_ERASE			0xD8	//	Block Erase (64 kB)
#define S25FL_CHIP_ERASE			0xC7	//	Chip Erase
#define S25FL_EP_SUSPEND			0x75	//	Erase / Program Suspend
#define S25FL_EP_RESUME				0x7A	//	Erase / Program Resume

//	---------------------- Command Set (Read Commands) ----------------------------
#define S25FL_READ_DATA				0x03	//	Read data
#define S25FL_FAST_READ				0x0B	//	Fast Read data
#define S25FL_FAST_READ_DO			0x3B	//	Fast Read Dual Output
#define S25FL_FAST_READ_QO			0x6B	//	Fast Read Quad Output
#define S25FL_FAST_READ_DIO			0xBB	//	Fast Read Dual I/O
#define S25FL_FAST_READ_QIO			0xEB	//	Fast Read Quad I/O
#define S25FL_READ_RESET			0xFF	//	Continuous Read Mode Reset

//	---------------------- Command Set (Reset Commands) ----------------------------
#define S25FL_SW_RESET_ENABLE		0x66	//	Software Reset Enable
#define S25FL_SW_RESET				0x99	//	Software Reset

//	---------------------- Command Set (ID, Security Commands) ---------------------
#define S25FL_POWER_DOWN			0xB9	//	Deep Power-down
#define S25FL_POWER_DOWN_RELEASE	0xAB	//	Release Power down / Device ID
#define S25FL_MAN_DEVID				0x90	//	Manufacturer / Device ID
#define S25FL_JEDEC_ID				0x9F	//	JEDEC ID
#define S25FL_READ_SFDP_UID			0x5A	//	Read SFDP Register / Read Unique ID Number
#define S25FL_READ_SEC_REG			0x48	//	Read Security Registers
#define S25FL_ERASE_SEC_REG			0x44	//	Erase Security Registers
#define S25FL_PROGRAM_SEC_REG		0x42	//	Program Security Registers

class FlashMem
{
public:
	FlashMem(SPI *pSPI, GPIO_Port_TypeDef portCS, uint8_t pinCS);

private:
	SPI *m_pSPI;
	GPIO_Port_TypeDef m_portCS;
	uint8_t m_pinCS;

	void WriteStatusReg(uint8_t w);

public:
	void Read_JEDEC_ID(uint8_t *pManufactureID, uint8_t *pDeviceType, uint8_t *pCapacityCode);
	void Init(void);
	uint8_t GetStatus(void);
	bool IsBusy(void);
	void Wait(void);
	void WriteEnable(void);

	void Read(uint32_t uAddress, void *pBuffer, uint32_t uLenght);
	void Write(uint32_t uAddress, void *pBuffer, uint32_t uLenght);

	void ReadReg(uint8_t uRegCmd, void *pBufferSend, uint32_t uLenghtSend, void *pBufferRcv, uint32_t uLenghtRcv);

	void EraseSector(uint32_t uAddress);	// 	4K size sector
	void EraseBlock(uint32_t uAddress);		//	64K size block
	void EraseChip(void);
};

#endif
