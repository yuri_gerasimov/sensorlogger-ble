/*
 * MAX44009.cpp
 *
 *  Created on: 03.12.2015
 *      Author: Yuriy
 */

#include "MAX44009.h"
#include "i2cspm.h"
#include "i2c_reg.h"


MAX44009::MAX44009()
{
//	m_Addr = 0x00;
//	m_i2c = 0UL;
}

bool MAX44009::Init(I2C_TypeDef *i2c, uint8_t addr)
{
	m_i2cReg.Init(i2c, addr);
//	m_i2c = i2c;
//	m_Addr = addr;
	return true;
}

void MAX44009::EnableInterrupt(bool bIsEnabled)
{
	MAX44009_INT_ENABLE_Register reg;
	reg.mBits.bitINTE = bIsEnabled;

	m_i2cReg.WriteU8(MAX44009_IntEnable_Reg,reg.mByte);
//	I2C_RegWrite(m_i2c,m_Addr,MAX44009_IntEnable_Reg,reg.mByte);
}

bool MAX44009::GetInterruptFlag(void)
{
	MAX44009_INT_STATUS_Register reg;
	reg.mByte = m_i2cReg.ReadU8(MAX44009_IntStatus_Reg);
	return reg.mByte;
//	return I2C_RegRead(m_i2c,m_Addr,MAX44009_IntStatus_Reg,&reg.mByte,sizeof(reg.mByte));
}

//TODO:	check MAX44009 resolution & sensitivity more
bool MAX44009::Read(uint32_t *pValue)
{
	uint8_t lux[2], lux_exponent;

	if(m_i2cReg.Read(MAX44009_LuxHigh_Reg,lux,2))
//	if(I2C_RegRead(m_i2c,m_Addr,MAX44009_LuxHigh_Reg,lux,2))
	{
		lux_exponent = ((lux[0] >> 4) & 0x0F);
		lux[0] = ((lux[0] << 4) & 0xF0);
		lux[1] &= 0x0F;
		*pValue = (uint32_t)45 * ( lux[0] | lux[1] ) * (1<<lux_exponent);
	}
	else
		return false;

	return true;
}

void MAX44009::SetContiniousMode(bool bIsEnabled)
{
	MAX44009_CONFIG_Register reg;

	reg.mByte = m_i2cReg.ReadU8(MAX44009_Config_Reg);
	reg.mBits.biCONT = bIsEnabled;
	m_i2cReg.WriteU8(MAX44009_Config_Reg,reg.mByte);
/*
	if(I2C_RegRead(m_i2c,m_Addr,MAX44009_Config_Reg,&reg.mByte,sizeof(reg.mByte)))
	{
		reg.mBits.biCONT = bIsEnabled;
		I2C_RegWrite(m_i2c,m_Addr,MAX44009_Config_Reg,reg.mByte);
	}
*/
}
