/*
 * APDS9250.h
 *
 *  Created on: 03.09.2016
 *      Author: Yuriy Gerasimov
 */

#ifndef APDS9250_H_
#define APDS9250_H_

#include <stdint.h>
#include <stdbool.h>

#include "i2cspm.h"
#include "i2c_reg.h"

#define APDS9250_ADDRESS (0x52<<1)
#define APDS9250_PART_ID (0xB2)

#define APDS9250_BIT_LS_EN (1)

/*=========================================================================
 REGISTERS
 -----------------------------------------------------------------------*/
typedef enum
{
	APDS9250_REGISTER_MAIN_CTRL = 0x00,
	APDS9250_REGISTER_LS_MEAS_RATE = 0x04,
	APDS9250_REGISTER_LS_GAIN = 0x05,
	APDS9250_REGISTER_PART_ID = 0x06,
	APDS9250_REGISTER_MAIN_STATUS = 0x07,
	APDS9250_REGISTER_DATA_IR = 0x0A,
	APDS9250_REGISTER_DATA_GREEN = 0x0D,
	APDS9250_REGISTER_DATA_BLUE = 0x10,
	APDS9250_REGISTER_DATA_RED = 0x13,
	APDS9250_REGISTER_INT_CFG = 0x19,
	APDS9250_REGISTER_INT_PERSISTENCE = 0x1A,
	APDS9250_REGISTER_LS_THRES_UP = 0x21,
	APDS9250_REGISTER_LS_THRES_LOW = 0x24,
	APDS9250_REGISTER_LS_THRES_VAR = 0x27
} APDS9250_Register_t;


#define APDS9250_MODE_ALS 0
#define APDS9250_MODE_CS 1

union APDS9250_MAIN_CTRL_Register
{
	struct
	{
		uint8_t bitRsvd1:1;
		uint8_t bitLS_EN:1;
		uint8_t bitCS_Mode:1;
		uint8_t bitRsvd2:1;
		uint8_t bitSwReset:1;
		uint8_t bitRsvd3:3;
	} mBits;
	uint8_t mByte;
};

union APDS9250_LS_MEAS_RATE_Register
{
	struct
	{
		uint8_t bitLS_Rate:3;
		uint8_t bitRsvd1:1;
		uint8_t bitLS_Res:3;
		uint8_t bitRsvd2:1;
	} mBits;
	uint8_t mByte;
};

union APDS9250_LS_GAIN_Register
{
	struct
	{
		uint8_t bitLS_Gain:3;
		uint8_t bitRsvd1:5;
	} mBits;
	uint8_t mByte;
};

typedef enum
{
	APDS9250_LS_ResolutionBits_20 = 0,
	APDS9250_LS_ResolutionBits_19 = 1,
	APDS9250_LS_ResolutionBits_18 = 2,
	APDS9250_LS_ResolutionBits_17 = 3,
	APDS9250_LS_ResolutionBits_16 = 4,
	APDS9250_LS_ResolutionBits_13 = 5,
} APDS9250_LS_Resolution_t;

typedef enum
{
	APDS9250_LS_Resolution_25mS = 0,
	APDS9250_LS_Resolution_50mS = 1,
	APDS9250_LS_Resolution_100mS = 2,
	APDS9250_LS_Resolution_200mS = 3,
	APDS9250_LS_Resolution_500mS = 4,
	APDS9250_LS_Resolution_1000mS = 5,
	APDS9250_LS_Resolution_2000mS = 6,
	APDS9250_LS_Resolution_2S = 7,
} APDS9250_LS_Rate_t;

typedef enum
{
	APDS9250_LS_Gain_1 = 0,
	APDS9250_LS_Gain_3 = 1,
	APDS9250_LS_Gain_6 = 2,
	APDS9250_LS_Gain_9 = 3,
	APDS9250_LS_Gain_18 = 4,
} APDS9250_LS_Gain_t;

class APDS9250
{
private:
	I2CReg m_i2cReg;

public:
	bool init(I2C_TypeDef *i2c, uint8_t addr);
	void enable(bool bIsEnabled, uint8_t uMode);
	void setResolution(APDS9250_LS_Resolution_t eRes);
	void setRate(APDS9250_LS_Rate_t eRate);
	void setGain(APDS9250_LS_Gain_t eGain);

	uint32_t readRed(void);
	uint32_t readGreen(void);
	uint32_t readBlue(void);
	uint32_t readIR(void);
};

#endif /* BME280_H_ */
