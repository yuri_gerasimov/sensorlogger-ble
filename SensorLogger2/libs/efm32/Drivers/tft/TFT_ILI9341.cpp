/*
 * TFT_ILI9341.cpp
 *
 *  Created on: 24.07.2015
 * 	Author: Yuriy Gerasimov
 */
//#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>

#ifdef SUPPORT_ANTIALIASING
#include <math.h>
#endif

#include <em_gpio.h>

#include "tft/tft_driver.h"
#include "rgb.h"
#include "dmactrl.h"

#include "delay.h"

//#include "gui/tft_driver.h"
//#include "config/hw_config.h"
//#include "rgb.h"

#ifdef TFT_INTERFACE_8080_16BIT_GPIO
#include "em_gpio.h"
#endif

#ifdef TFT_INTERFACE_8080_16BIT_EBI
#include "em_ebi.h"
#endif

#ifdef TFT_USE_FRAME_BUFFER_DMA
#include "dmactrl.h"
#endif


#ifdef TFT_USE_FRAME_BUFFER
TFT::TFT(uint16_t uWidth, uint16_t uHeight, uint16_t uBitsPerPixel, Color_TypeDef* pFrameBuffer, uint8_t nFrameBufferNum)
#else
TFT::TFT(uint16_t uWidth, uint16_t uHeight, uint16_t uBitsPerPixel)
#endif
{
	m_uOrientation = 0;
	m_xOffset = 0;
	m_yOffset = 0;

	m_uWidth = uWidth;
	m_uHeight = uHeight;
	m_uRotatedWidth = uWidth;
	m_uRotatedHeight = uHeight;
	m_uBitsPerPixel = uBitsPerPixel;
	m_uPixelsNumber = uWidth * uHeight;

	m_pFontInfo = NULL;
	m_TextAlignmentH = StringAlign_Left;
	m_TextAlignmentV = StringAlign_Top;

#ifdef TFT_USE_FRAME_BUFFER
	m_nFrameBufferNumber = nFrameBufferNum;

	for(uint8_t i = 0; i<nFrameBufferNum; i++)
		m_pFrameBuffer[i] = pFrameBuffer + i * m_uPixelsNumber;

	m_uFbCurrentIndex = 0;
#endif

	m_ptrCmd = (uint16_t *) (EBI_BankAddress(config_TFT_EBI_BANK));
	m_ptrData = (uint16_t *) (EBI_BankAddress(config_TFT_EBI_BANK) | (1<<config_TFT_DC_ADDR_BIT));

	m_colorPen = RGB(255,255,255);
	m_colorFill = RGB(0,0,0);
	m_colorText = RGB(0,255,0);
}

void TFT::init(void)
{
#ifdef TFT_INTERFACE_8080_16BIT_GPIO
	initInterface(true);
#endif

	GPIO_PinOutSet(config_LCD_CS_PIN);
	delay_us(5000);

	GPIO_PinOutClear(config_LCD_RESET_PIN);
	delay_us(1000);
	GPIO_PinOutSet(config_LCD_RESET_PIN);

	delay_us(120000);

	writeCmd(0x11,0UL,0);
	delay_us(120000);	//	works good without this delay ?!

//	uint8_t d[4] = {0,0,0,0};
//	TFT_ReadCmd(0x04,d,4);

	const uint8_t cmdPwrControlAData[] = { 0x39, 0x2C, 0x00, 0x34, 0x02};
	writeCmd(0xCB,cmdPwrControlAData,5);

	const uint8_t cmdPwrControlBData[] = { 0x00,0xc3,0x30 };
	writeCmd(0xCF,cmdPwrControlBData,3);

	const uint8_t cmdDriverTimingControlAData[] = { 0x85,0x10,0x79 };
	writeCmd(0xE8,cmdDriverTimingControlAData,3);

	const uint8_t cmdDriverTimingControlBData[] = { 0x00, 0x00 };
	writeCmd(0xEA, cmdDriverTimingControlBData, 2);

	const uint8_t cmdPwrOnSeqControlData[] = { 0x64,0x03,0x12,0x81 };
	writeCmd(0xED,cmdPwrOnSeqControlData,4);

	const uint8_t cmdPumpRatioCtrl[] = { 0x20};
	writeCmd(0xF7,cmdPumpRatioCtrl,1);

	const uint8_t cmdPowerCtrl1[] = { 0x22 };
	writeCmd(0xC0, cmdPowerCtrl1, 1);

	const uint8_t cmdPowerCtrl2[] = { 0x11 };
	writeCmd(0xC1, cmdPowerCtrl2, 1);

	const uint8_t cmdVCOMCtrl1[] = { 0x3d, 0x20 };
	writeCmd(0xC5, cmdVCOMCtrl1, 2);

	const uint8_t cmdVCOMCtrl2[] = { 0xAA };
	writeCmd(0xC7, cmdVCOMCtrl2, 1);

	const uint8_t cmdMemAccCtrl[] = { 0x08 };
	writeCmd(0x36, cmdMemAccCtrl, 1);

	const uint8_t cmdPixDormatSet[] = { 0x55 };
	writeCmd(0x3A, cmdPixDormatSet, 1);

	const uint8_t cmdBrightness[] = { 0x80 };
	writeCmd(0x51, cmdBrightness, 1);

//	const uint8_t cmdFrameRateCtrl[] = { 0x00, 0x1B };	//	FrameRate = 70Hz
//	const uint8_t cmdFrameRateCtrl[] = { 0x00, 0x13 };	//	FrameRate = 100Hz
//	const uint8_t cmdFrameRateCtrl[] = { 0x00, 0x18 };	//	FrameRate = 79Hz
	const uint8_t cmdFrameRateCtrl[] = { 0x00, 0x1F };	//	FrameRate = 61Hz
	writeCmd(0xB1, cmdFrameRateCtrl, 2);

	const uint8_t cmdDisplayCtrlFunc[] = { 0x08, 0x82, 0x27, 0x00 };
	writeCmd(0xB6, cmdDisplayCtrlFunc, 4);

	const uint8_t cmdInterfaceCtrl[] = { 0x01, 0x30 };
	writeCmd(0xF6, cmdInterfaceCtrl, 2);

	const uint8_t cmdEnable3G[] = { 0x00 };
	writeCmd(0xF2, cmdEnable3G, 1);

	const uint8_t cmdGammaSet[] = { 0x01 };
	writeCmd(0x26, cmdGammaSet, 1);

	const uint8_t cmdGammaPosCorr[] = { 0x0F, 0x3F, 0x2F, 0x0C, 0x10, 0x0A, 0x53, 0xD5, 0x40, 0x0A, 0x13, 0x03, 0x08, 0x03, 0x00 };
	writeCmd(0xE0, cmdGammaPosCorr, 15);

	const uint8_t cmdGammaNegCorr[] = { 0x00, 0x00, 0x10, 0x03, 0x0F, 0x05, 0x2C, 0xA2, 0x3F, 0x05, 0x0E, 0x0C, 0x37, 0x3C, 0x0F };
	writeCmd(0xE1, cmdGammaNegCorr, 15);

	writeCmd(0x11,0UL,0);
	delay_us(120000);		//	works good without this delay ?!

	writeCmd(0x29,0UL,0);
	delay_us(50000);		//	works good without this delay ?!
}

void TFT::enterSleep(void)
{
	writeCmd(0x28,0UL,0);	// Display off
	writeCmd(0x10,0UL,0);	// Enter Sleep mode
}

void TFT::exitSleep(void)
{
	writeCmd(0x11,0UL,0);	// Sleep out
	delay_us(120000);
	writeCmd(0x29,0UL,0);	// Display on
}

void TFT::setOrientation(uint8_t o)
{
	uint8_t d = 0;
    switch (o)
    {
        case 0:    	d = 0x48;	break;
        case 1:		d = 0x28;	break;
        case 2:		d = 0x88;	break;
        case 3:
        default:	d = 0xe8;	break;
    }
    writeCmd(0x36,&d,1);
    m_uOrientation = o;

    if (m_uOrientation == 0 || m_uOrientation == 2)
    {
    	m_uRotatedWidth = m_uWidth;
    	m_uRotatedHeight = m_uHeight;
    }
    else
    	{
    		m_uRotatedWidth = m_uHeight;
    		m_uRotatedHeight = m_uWidth;
    	}

    setWindow(0, 0, getWidth(),  getHeight());
}

void TFT::setScroll(int16_t x, int16_t y)
{
	m_xOffset = x;
	m_yOffset = y;
}

void TFT::setWindow(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2)
{
	uint8_t x[4];
	uint8_t y[4];

	x[0]=x1>>8;
	x[1]=x1;
	x[2]=x2>>8;
	x[3]=x2;

	y[0]=y1>>8;
	y[1]=y1;
	y[2]=y2>>8;
	y[3]=y2;

	writeCmd(0x2A, x, 4);
	writeCmd(0x2B, y, 4);
}

#ifdef TFT_USE_FRAME_BUFFER

void TFT::setFrameBufferIndex(uint8_t nIndex)
{
	if(nIndex < m_nFrameBufferNumber)
		m_uFbCurrentIndex = nIndex;
}

Color_TypeDef* TFT::getFrameBuffer(uint8_t nIndex)
{
	return m_pFrameBuffer[nIndex];
}

void TFT::updateFrameBuffer(bool bWaitForComplete)
{
//	uint16_t *ptrTftData = (uint16_t *) (EBI_BankAddress(TFT_EBI_BANK) | (1<<TFT_DC_ADDR_BIT));

#ifdef TFT_USE_FRAME_BUFFER_DMA
	startWriteGRAM();
	// Start DMA transfer
	startDmaTransfer(m_pFrameBuffer[m_uFbCurrentIndex] + m_yOffset * getWidth(), m_ptrData, m_uPixelsNumber);

	// Sleep until transfer is finished. This is done to prevent software from overwriting the frame buffer
	// before it is copied to the display (would cause flickering)
	if(bWaitForComplete)
	{
		while(isDmaBusy())
		{
			//	EMU_EnterEM1();
		}
	}
#else
	copyToGRAM(m_pFrameBuffer, m_uPixelsNumber);
#endif
}

#endif

void TFT::fill(void)
{
#ifdef TFT_USE_FRAME_BUFFER
	Color_TypeDef *ptrTftData = m_pFrameBuffer[m_uFbCurrentIndex];
	uint32_t uCount = m_uPixelsNumber;
	while(uCount--)
		*ptrTftData++ = m_colorFill;
#else
	setWindow(0, 0, TFT_getWidth()-1, TFT_getHeight()-1);
	fillGRAM(color, TFT_getWidth() * TFT_getHeight());
#endif
}

void TFT::drawPixel(uint16_t x, uint16_t y)
{
#ifdef TFT_USE_FRAME_BUFFER
	*((Color_TypeDef *)(m_pFrameBuffer[m_uFbCurrentIndex] + getWidth() * y + x)) = m_colorPen;
#else
	setWindow(x, y, x, y);
	fillGRAM(color, 1);
#endif
}

void TFT::drawVLine(uint16_t x, uint16_t y, uint16_t h)
{
#ifdef TFT_USE_FRAME_BUFFER
	uint16_t w = getWidth();
	Color_TypeDef *ptrTftData = m_pFrameBuffer[m_uFbCurrentIndex] + w * y + x;
	while(h--)
	{
		*ptrTftData = m_colorPen;
		ptrTftData+=w;
	}

#else
	setWindow(x, y, x, y+h-1);
	fillGRAM(color, h);
#endif
}

void TFT::fillVLine(uint16_t x, uint16_t y, uint16_t h)
{
#ifdef TFT_USE_FRAME_BUFFER
	uint16_t w = getWidth();
	Color_TypeDef *ptrTftData = m_pFrameBuffer[m_uFbCurrentIndex] + w * y + x;
	while (h--)
	{
		*ptrTftData = m_colorFill;
		ptrTftData += w;
	}

#else
	setWindow(x, y, x, y + h - 1);
	fillGRAM(color, h);
#endif
}


void TFT::drawLine(int x0, int y0, int x1, int y1)
{
	 int dx = abs(x1-x0), sx = x0<x1 ? 1 : -1;
	 int dy = abs(y1-y0), sy = y0<y1 ? 1 : -1;
	 int err = (dx>dy ? dx : -dy)/2, e2;

	  for(;;)
	  {
	    drawPixel(x0,y0);
	    if (x0==x1 && y0==y1) break;
	    e2 = err;
	    if (e2 >-dx) { err -= dy; x0 += sx; }
	    if (e2 < dy) { err += dx; y0 += sy; }
	  }
}

void TFT::drawHLine(uint16_t x, uint16_t y, uint16_t w)
{
#ifdef TFT_USE_FRAME_BUFFER
	Color_TypeDef *ptrTftData = m_pFrameBuffer[m_uFbCurrentIndex] + getWidth() * y + x;
	while(w--)
		*ptrTftData++ = m_colorPen;
#else
	 setWindow(x, y, x+w-1, y+1);
	 fillGRAM(m_colorPen, w);
#endif
}

void TFT::fillHLine(uint16_t x, uint16_t y, uint16_t w)
{
#ifdef TFT_USE_FRAME_BUFFER
	Color_TypeDef *ptrTftData = m_pFrameBuffer[m_uFbCurrentIndex] + getWidth() * y + x;
	while (w--)
		*ptrTftData++ = m_colorFill;
#else
	setWindow(x, y, x + w - 1, y + 1);
	fillGRAM(m_colorFill, w);
#endif
}


void TFT::drawRect(uint16_t x, uint16_t y, uint16_t w, uint16_t h)
{
	drawHLine(x,y,w+1);
	drawHLine(x,y+h,w+1);
	drawVLine(x,y+1,h-1);
	drawVLine(x+w,y+1,h-1);
}

void TFT::fillRect(uint16_t x, uint16_t y, uint16_t w, uint16_t h)
{
#ifdef TFT_USE_FRAME_BUFFER
	uint16_t w_save = w;
	Color_TypeDef *ptrTftData = m_pFrameBuffer[m_uFbCurrentIndex] + getWidth() * y + x;
	uint16_t dw = getWidth() - w;
	while(h--)
	{
		w = w_save;
		while(w--)	*ptrTftData++ = m_colorFill;
		ptrTftData+=dw;
	}
#else
	 setWindow(x, y, x+w-1, y+h-1);
	 fillGRAM(color, w*h);
#endif
}

void TFT::drawCircle(uint16_t x0, uint16_t y0, uint16_t r)
{
 int x = -r, y = 0, err = 2-2*r, e2;
    do {
    	drawPixel(x0-x, y0+y);
    	drawPixel(x0+x, y0+y);
    	drawPixel(x0+x, y0-y);
    	drawPixel(x0-x, y0-y);
        e2 = err;
        if (e2 <= y)
        {
            err += ++y*2+1;
            if (-x == y && e2 <= x) e2 = 0;
        }
        if (e2 > x) err += ++x*2+1;
    } while (x <= 0);
}

void TFT::fillCircle(uint16_t x0, uint16_t y0, uint16_t r)
{
	int x = -r, y = 0, err = 2-2*r, e2;
    do
    {
    	fillHLine(x0+x, y0+y, -2*x);
    	fillHLine(x0+x, y0-y, -2*x);
        e2 = err;
        if (e2 <= y)
        {
            err += ++y*2+1;
            if (-x == y && e2 <= x) e2 = 0;
        }
        if (e2 > x) err += ++x*2+1;
    } while (x <= 0);
}

void TFT::fillRoundHLine(uint16_t x, uint16_t y, uint16_t w, uint16_t h)
{
	uint16_t r = h>>1;
	fillCircle(x+r, y, r);
	fillCircle(x+w-r, y, r);
	fillRect(x+r, y-r, w-h, h);
}

void TFT::fillRoundVLine(uint16_t x, uint16_t y, uint16_t w, uint16_t h)
{
	uint16_t r = w>>1;
	fillCircle(x, y+r, r);
	fillCircle(x, y+h-r, r);
	fillRect(x-r, y+r, w, h-w);
}


void TFT::drawBitmap(uint16_t x, uint16_t y, uint16_t w, uint16_t h, Color_TypeDef *pSrcBitmap)
{

#ifdef TFT_USE_FRAME_BUFFER
	uint16_t w_save = w;
	Color_TypeDef *ptrTftData = m_pFrameBuffer[m_uFbCurrentIndex] + getWidth() * y + x;
	uint16_t dw =  getWidth() - w;
	while(h--)
	{
		w = w_save;
		while(w--)	*ptrTftData++ = *pSrcBitmap++;
		ptrTftData+=dw;
	}
#else

	TFT_setWindow(x, y, x + w -1, y + h -1);

#ifdef TFT_INTERFACE_8080_16BIT_EBI
//	Color_TypeDef *ptrTftCmd = (uint16_t *) EBI_BankAddress(TFT_EBI_BANK);
//	Color_TypeDef *ptrTftData = (uint16_t *) (EBI_BankAddress(TFT_EBI_BANK) | (1<<TFT_DC_ADDR_BIT));

	*ptrTftCmd = (uint16_t)ILI9341_MEMORYWRITE_REG;
	uint32_t size = w * h;
	while(size--)
		*ptrTftData =  *pSrcBitmap++;
#endif	// TFT_INTERFACE_8080_16BIT_EBI

#endif	//	TFT_USE_FRAME_BUFFER
}

void TFT::setFont(const FONT_INFO *pFontInfo)
{
	EFM_ASSERT(pFontInfo);
	m_pFontInfo = (FONT_INFO *)pFontInfo;
}

void TFT::drawChar(char c, uint16_t x, uint16_t y)
{
	uint8_t i;
	uint8_t j;

	EFM_ASSERT(m_pFontInfo);

	int index = c - m_pFontInfo->m_uStartChar;
	uint8_t char_width = m_pFontInfo->m_CharInfo[index].m_uCharWidth;
	uint8_t* pData = (uint8_t*)(m_pFontInfo->m_pFontBitmap + m_pFontInfo->m_CharInfo[index].m_uCharDataOffset);
	uint8_t bit_mask = 0x80;

	for(j=0;j<m_pFontInfo->m_uCharHeight;j++)
	{
		for (i=0; i<char_width; i++)
		{
			if (*pData & bit_mask)
			{
		//		drawPixel(x + i, y + j, color);
				//TODO: optimize address calculation
				*((Color_TypeDef *)(m_pFrameBuffer[m_uFbCurrentIndex] + getWidth() * (y+j) + (x+i))) = m_colorText;
			}

			bit_mask = bit_mask>>1;
			if(!bit_mask)
			{
				bit_mask = 0x80;
				pData++;
			}
		}
		bit_mask = 0x80;
		pData++;
	}
}

void TFT::drawString(const char *pStr, uint16_t x, uint16_t y)
{
	uint8_t i;
	uint8_t j;
	char *pstr = (char *) pStr;
	while(*pstr)
	{
		uint8_t char_width = 0;
		if(*pstr >= m_pFontInfo->m_uStartChar && *pstr <= m_pFontInfo->m_uEndChar)		//	font's characters boundary check
		{
			int index = *pstr - m_pFontInfo->m_uStartChar;
			char_width = m_pFontInfo->m_CharInfo[index].m_uCharWidth;
			uint8_t* pData = (uint8_t*)(m_pFontInfo->m_pFontBitmap + m_pFontInfo->m_CharInfo[index].m_uCharDataOffset);
			uint8_t bit_mask = 0x80;

			for(j=0;j<m_pFontInfo->m_uCharHeight;j++)
			{
				for (i=0; i<char_width; i++)
				{
					if (*pData & bit_mask)
					{
					//	drawPixel(x + i, y + j, color);
						//TODO: optimize address calculation
						*((Color_TypeDef *)(m_pFrameBuffer[m_uFbCurrentIndex] + getWidth() * (y + j) + (x + i))) = m_colorText;
					}

					bit_mask = bit_mask>>1;
					if(!bit_mask)
					{
						bit_mask = 0x80;
						pData++;
					}
				}
				if(char_width & 0x07)
				{
					bit_mask = 0x80;
					pData++;
				}
			}
		}
		x += (char_width + m_pFontInfo->m_uSpaceWidth);
		pstr++;
	}
}

uint8_t TFT::getFontHeight(void)
{
	EFM_ASSERT(m_pFontInfo);
	return m_pFontInfo->m_uCharHeight;
}

void TFT::setTextAlignemt(TFT_StringAlign_t alignH, TFT_StringAlign_t alignV)
{
	m_TextAlignmentH = alignH;
	m_TextAlignmentV = alignV;
}

void TFT::drawStringInRect(const char *pStr, uint16_t x, uint16_t y, uint16_t w, uint16_t h)
{
	uint16_t start_x = x;
	uint16_t start_y = y;
	uint16_t string_width = 0;
	uint16_t string_height = 0;
	getStringDimension(pStr, &string_width, &string_height);

	switch(m_TextAlignmentH)
	{
		case StringAlign_Center:
		{
			start_x = x + (w/2 - string_width/2);
			break;
		}
		case StringAlign_Right:
		{
			start_x = x + w -  string_width;
			break;
		}
		case StringAlign_Left:
		default:
			break;
	}

	switch(m_TextAlignmentV)
	{
		case StringAlign_Middle:
		{
			start_y = y + h/2 - string_height/2;
			break;
		}
		case StringAlign_Bottom:
		{
			start_y = y + h - string_height;
			break;
		}
		case StringAlign_Top:
		default:
			break;
	}
	drawString(pStr,start_x,start_y);
}

void TFT::getStringDimension(const char *pStr, uint16_t *pWidth, uint16_t *pHeight)
{
	EFM_ASSERT(m_pFontInfo);

	*pHeight = getFontHeight();

	char *pstr = (char *) pStr;
	while(*pstr)
	{
		if(*pstr >= m_pFontInfo->m_uStartChar && *pstr <= m_pFontInfo->m_uEndChar)		//	font's characters boundary check
		{
			int index = *pstr - m_pFontInfo->m_uStartChar;
			*pWidth += (m_pFontInfo->m_CharInfo[index].m_uCharWidth + m_pFontInfo->m_uSpaceWidth);
		}
		pstr++;
	}
}

void TFT::drawArc(uint16_t x0, uint16_t y0, uint16_t r, int16_t a1, int16_t a2)
{
	int16_t first_sector = 0;
	int16_t last_sector = 0;

	a1 = -a1 + 90;
	a2 = -a2 + 90;

	int16_t first_x = (int16_t)round((double)r * cos((double)a1 * M_PI / 180));
	int16_t first_y = (int16_t)round((double)r * sin((double)a1 * M_PI / 180));

	int16_t last_x = (int16_t)round((double)r * cos((double)a2 * M_PI / 180));
	int16_t last_y = (int16_t)round((double)r * sin((double)a2 * M_PI / 180));


	if (last_x >= 0 && last_y >= 0)			last_sector = 0;
	else if (last_x < 0 && last_y >= 0)		last_sector = 3;
	else if (last_x < 0 && last_y < 0)		last_sector = 2;
	else if (last_x >= 0 && last_y < 0)		last_sector = 1;

	if (first_x >= 0 && first_y >= 0)		first_sector = 0;
	else if (first_x < 0 && first_y >= 0)	first_sector = 3;
	else if (first_x < 0 && first_y < 0)	first_sector = 2;
	else if (first_x >= 0 && first_y < 0)	first_sector = 1;

	int x = -r, y = 0;					/* II. quadrant from bottom left to top right */
	int i, x2, e2, err = 2 - 2 * r;		/* error of 1.step */
	r = 1 - err;

	do {
		bool bFillMiddleFlag = true;
		uint16_t flags = 0;
	//	i = 255 * abs(err - 2 * (x + y) - 2) / r;               /* get blend value of pixel */

		if (first_sector == last_sector)
		{
			if (a1 < a2)
			{
				switch (first_sector)
				{
				case 0:		if ((y >= first_x || y <= last_x) && (-x <= first_y || -x >= last_y))		flags |= (1 << 0);	break;
				case 1:		if ((-x <= first_x || -x >= last_x) && (-y <= first_y || -y >= last_y))		flags |= (1 << 1);	break;
				case 2:		if ((-y <= first_x || -y >= last_x) && (x >= first_y || x <= last_y))		flags |= (1 << 2);	break;
				case 3:		if ((x >= first_x || x <= last_x) && (y >= first_y || y <= last_y))			flags |= (1 << 3);	break;
				default:	break;
				}
			}
			else
			{
				bFillMiddleFlag = false;
				switch (first_sector)
				{
				case 0:		if (y >= first_x && y <= last_x && -x <= first_y && -x >= last_y)		flags |= (1 << 0);	break;
				case 1:		if (-x <= first_x && -x >= last_x && -y <= first_y && -y >= last_y)		flags |= (1 << 1);	break;
				case 2:		if (-y <= first_x && -y >= last_x && x >= first_y && x <= last_y)		flags |= (1 << 2);	break;
				case 3:		if (x >= first_x && x <= last_x && y >= first_y && y <= last_y)			flags |= (1 << 3);	break;
				default:	break;
				}
			}
		}
		else
		{
			switch (first_sector)
			{
			case 0:		if (y >= first_x && -x <= first_y)		flags |= (1 << 0);	break;
			case 1:		if (-x <= first_x && -y <= first_y)		flags |= (1 << 1);	break;
			case 2:		if (-y <= first_x && x >= first_y)		flags |= (1 << 2);	break;
			case 3:		if (x >= first_x && y >= first_y)		flags |= (1 << 3);	break;
			default:	break;
			}

			switch (last_sector)
			{
			case 0:		if (y <= last_x && -x >= last_y)		flags |= (1 << 0);	else flags &= ~(1 << 0);	break;
			case 1:		if (-x >= last_x && -y >= last_y)		flags |= (1 << 1);	else flags &= ~(1 << 1);	break;
			case 2:		if (-y >= last_x && x <= last_y)		flags |= (1 << 2);	else flags &= ~(1 << 2);	break;
			case 3:		if (x <= last_x && y <= last_y)			flags |= (1 << 3);	else flags &= ~(1 << 3);	break;
			default:	break;
			}
		}

		//	fill middle sectors
		if (bFillMiddleFlag)
		{
			int16_t cs = first_sector;
			for (int16_t ii = 0; ii < 4; ii++)
			{
				if (cs < 3)	cs++;	else cs = 0;
				if (cs == last_sector)	break;
				flags |= (1 << cs);
			}
		}

		if (flags & (1 << 0))	drawPixel(x0 + y, y0 + x);	//	pixel in sector #0
		if (flags & (1 << 1))	drawPixel(x0 - x, y0 + y);	//	pixel in sector #1
		if (flags & (1 << 2))	drawPixel(x0 - y, y0 - x);	//	pixel in sector #2
		if (flags & (1 << 3))	drawPixel(x0 + x, y0 - y);	//	pixel in sector #3

		e2 = err; x2 = x;		/* remember values */
		if (err + y > 0)
		{										/* x step */
			i = 255 * (err - 2 * x - 1) / r;    /* outward pixel */
			if (i < 256)
			{
//				if (flags & (1 << 0))	drawPixelAA(x0 + y + 1, y0 + x, color, i);	//	pixel in sector #0
//				if (flags & (1 << 1))	drawPixelAA(x0 - x, y0 + y + 1, color, i);	//	pixel in sector #1
//				if (flags & (1 << 2))	drawPixelAA(x0 - y - 1, y0 - x, color, i);	//	pixel in sector #2
//				if (flags & (1 << 3))	drawPixelAA(x0 + x, y0 - y - 1, color, i);	//	pixel in sector #3
			}
			err += ++x * 2 + 1;
		}
		if (e2 + x2 <= 0)
		{										/* y step */
			i = 255 * (2 * y + 3 - e2) / r;     /* inward pixel */
			if (i < 256)
			{
//				if (flags & (1 << 0))	drawPixelAA(x0 + y, y0 + x2 + 1, color, i);	//	pixel in sector #0
//				if (flags & (1 << 1))	drawPixelAA(x0 - x2 - 1, y0 + y, color, i);	//	pixel in sector #1
//				if (flags & (1 << 2))	drawPixelAA(x0 - y, y0 - x2 - 1, color, i);	//	pixel in sector #2
//				if (flags & (1 << 3))	drawPixelAA(x0 + x2 + 1, y0 - y, color, i);	//	pixel in sector #3
			}
			err += ++y * 2 + 1;
		}

		//		drawPixel(x0 + first_x, y0 - first_y, RGB(0, 255, 0));
		//		drawPixel(x0 + last_x, y0 - last_y, RGB(255, 0, 0));
	} while (x < 0);
}

#ifdef SUPPORT_ANTIALIASING

#define AA(S,D,A) ((((uint16_t)S*A)+((uint16_t)D*(255-A)))>>8)
//#define AA(S,D,A) (((int16_t)D)+(A*((int16_t)S-(int16_t)D))>>8)

/*
void TFT::drawArc(uint16_t x0, uint16_t y0, uint16_t r, int16_t a1, int16_t a2, Color_TypeDef color)
{
	double start_angle = a1 * M_PI / 180;
	double end_angle = a2 * M_PI / 180;
	double step = 0.2;
	double alfa = start_angle;
	double s = sin(alfa);
	double c = cos(alfa);
	int16_t last_x = (int16_t)round((double)r * c);
	int16_t last_y = (int16_t)round((double)r * s);

	do
	{
		alfa += step;
		if (alfa > end_angle)	alfa = end_angle;

		s = sin(alfa);
		c = cos(alfa);
		int16_t x = (int16_t)round((double)r * c);
		int16_t y = (int16_t)round((double)r * s);

		drawLine(x0 - x, y0 - y, x0 - last_x, y0 - last_y, color);

		last_x = x;
		last_y = y;
	} while (alfa < end_angle);
}
*/

void TFT::drawArcAA(uint16_t x0, uint16_t y0, uint16_t r, int16_t a1, int16_t a2)
{
	int16_t first_sector = 0;
	int16_t last_sector = 0;

	a1 = - a1 + 90;
	a2 = - a2 + 90;

	int16_t first_x = (int16_t)round((double)r * cos((double)a1 * M_PI / 180));
	int16_t first_y = (int16_t)round((double)r * sin((double)a1 * M_PI / 180));

	int16_t last_x = (int16_t)round((double)r * cos((double)a2 * M_PI / 180));
	int16_t last_y = (int16_t)round((double)r * sin((double)a2 * M_PI / 180));


	if (last_x >= 0 && last_y >= 0)			last_sector = 0;
	else if (last_x < 0 && last_y >= 0)		last_sector = 3;
	else if (last_x < 0 && last_y < 0)		last_sector = 2;
	else if (last_x >= 0 && last_y < 0)		last_sector = 1;

	if (first_x >= 0 && first_y >= 0)		first_sector = 0;
	else if (first_x < 0 && first_y >= 0)	first_sector = 3;
	else if (first_x < 0 && first_y < 0)	first_sector = 2;
	else if (first_x >= 0 && first_y < 0)	first_sector = 1;
		
	int x = -r, y = 0;					/* II. quadrant from bottom left to top right */
	int i, x2, e2, err = 2 - 2 * r;		/* error of 1.step */
	r = 1 - err;

	do {
		bool bFillMiddleFlag = true;
		uint16_t flags = 0;
		i = 255 * abs(err - 2 * (x + y) - 2) / r;               /* get blend value of pixel */

		if (first_sector == last_sector)
		{
			if (a1 < a2)
			{
				switch (first_sector)
				{
					case 0:		if ((y >= first_x || y <= last_x)  && (-x <= first_y || -x >= last_y))		flags |= (1 << 0);	break;
					case 1:		if ((-x <= first_x || -x >= last_x) && (-y <= first_y || -y >= last_y))		flags |= (1 << 1);	break;
					case 2:		if ((-y <= first_x || -y >= last_x) && (x >= first_y || x <= last_y))		flags |= (1 << 2);	break;
					case 3:		if ((x >= first_x || x <= last_x) && (y >= first_y || y <= last_y))			flags |= (1 << 3);	break;
					default:	break;
				}
			}
			else
			{
				bFillMiddleFlag = false;
				switch (first_sector)
				{
					case 0:		if (y >= first_x && y <= last_x && -x <= first_y && -x >= last_y)		flags |= (1 << 0);	break;
					case 1:		if (-x <= first_x && -x >= last_x && -y <= first_y && -y >= last_y)		flags |= (1 << 1);	break;
					case 2:		if (-y <= first_x && -y >= last_x && x >= first_y && x <= last_y)		flags |= (1 << 2);	break;
					case 3:		if (x >= first_x && x <= last_x && y >= first_y && y <= last_y)			flags |= (1 << 3);	break;
					default:	break;
				}
			}
		}
		else
		{
			switch (first_sector)
			{
				case 0:		if (y >= first_x && -x <= first_y)		flags |= (1 << 0);	break;
				case 1:		if (-x <= first_x && -y <= first_y)		flags |= (1 << 1);	break;
				case 2:		if (-y <= first_x && x >= first_y)		flags |= (1 << 2);	break;
				case 3:		if (x >= first_x && y >= first_y)		flags |= (1 << 3);	break;
				default:	break;
			}

			switch (last_sector)
			{
				case 0:		if (y <= last_x && -x >= last_y)		flags |= (1 << 0);	else flags &= ~(1 << 0);	break;
				case 1:		if (-x >= last_x && -y >= last_y)		flags |= (1 << 1);	else flags &= ~(1 << 1);	break;
				case 2:		if (-y >= last_x && x <= last_y)		flags |= (1 << 2);	else flags &= ~(1 << 2);	break;
				case 3:		if (x <= last_x && y <= last_y)			flags |= (1 << 3);	else flags &= ~(1 << 3);	break;
				default:	break;
			}
		}

		//	fill middle sectors
		if (bFillMiddleFlag)
		{
			int16_t cs = first_sector;
			for (int16_t ii = 0; ii < 4; ii++)
			{
				if (cs < 3)	cs++;	else cs = 0;
				if (cs == last_sector)	break;
				flags |= (1 << cs);
			}
		}

		if (flags & (1<<0))	drawPixelAA(x0 + y, y0 + x, i);	//	pixel in sector #0
		if (flags & (1<<1))	drawPixelAA(x0 - x, y0 + y, i);	//	pixel in sector #1
		if (flags & (1<<2))	drawPixelAA(x0 - y, y0 - x, i);	//	pixel in sector #2
		if (flags & (1<<3))	drawPixelAA(x0 + x, y0 - y, i);	//	pixel in sector #3

		e2 = err; x2 = x;		/* remember values */
		if (err + y > 0)
		{										/* x step */
			i = 255 * (err - 2 * x - 1) / r;    /* outward pixel */
			if (i < 256)
			{
				if (flags & (1 << 0))	drawPixelAA(x0 + y + 1, y0 + x, i);	//	pixel in sector #0
				if (flags & (1 << 1))	drawPixelAA(x0 - x, y0 + y + 1, i);	//	pixel in sector #1
				if (flags & (1 << 2))	drawPixelAA(x0 - y - 1, y0 - x, i);	//	pixel in sector #2
				if (flags & (1 << 3))	drawPixelAA(x0 + x, y0 - y - 1, i);	//	pixel in sector #3
			}
			err += ++x * 2 + 1;
		}
		if (e2 + x2 <= 0)
		{										/* y step */
			i = 255 * (2 * y + 3 - e2) / r;     /* inward pixel */
			if (i < 256)
			{
				if (flags & (1 << 0))	drawPixelAA(x0 + y, y0 + x2 + 1, i);	//	pixel in sector #0
				if (flags & (1 << 1))	drawPixelAA(x0 - x2 - 1, y0 + y, i);	//	pixel in sector #1
				if (flags & (1 << 2))	drawPixelAA(x0 - y, y0 - x2 - 1, i);	//	pixel in sector #2
				if (flags & (1 << 3))	drawPixelAA(x0 + x2 + 1, y0 - y, i);	//	pixel in sector #3
			}
			err += ++y * 2 + 1;
		}

//		drawPixel(x0 + first_x, y0 - first_y, RGB(0, 255, 0));
//		drawPixel(x0 + last_x, y0 - last_y, RGB(255, 0, 0));
	} while (x < 0);
}

//	TODO: optimize for SIMD instructions
void TFT::drawPixelAA(uint16_t x, uint16_t y, uint8_t alfa)
{
#ifdef TFT_USE_FRAME_BUFFER
	Color_TypeDef *ptrTftData = m_pFrameBuffer[m_uFbCurrentIndex] + getWidth() * y + x;
	Color_TypeDef d = *ptrTftData;
	uint8_t r = (uint8_t)AA(RED(d), RED(m_colorPen), alfa);
	uint8_t g = (uint8_t)AA(GREEN(d), GREEN(m_colorPen), alfa);
	uint8_t b = (uint8_t)AA(BLUE(d), BLUE(m_colorPen), alfa);
	*ptrTftData = RGB(r, g, b);
#else
	//TODO: implement anti-aliasing support without frame buffer
	setWindow(x, y, x, y);
	fillGRAM(color, 1);
#endif
}

void TFT::fillRectAA(uint16_t x, uint16_t y, uint16_t w, uint16_t h)
{

}


void TFT::drawCircleAA(uint16_t xm, uint16_t ym, uint16_t r)
{
		int x = -r, y = 0;           /* II. quadrant from bottom left to top right */
		int i, x2, e2, err = 2 - 2 * r;                             /* error of 1.step */
		r = 1 - err;
		do {
			i = 255 * abs(err - 2 * (x + y) - 2) / r;               /* get blend value of pixel */
			drawPixelAA(xm - x, ym + y, i);                             /*   I. Quadrant */
			drawPixelAA(xm - y, ym - x, i);                             /*  II. Quadrant */
			drawPixelAA(xm + x, ym - y, i);                             /* III. Quadrant */
			drawPixelAA(xm + y, ym + x, i);                             /*  IV. Quadrant */
			e2 = err; x2 = x;											/* remember values */
			if (err + y > 0) 
			{																/* x step */
				i = 255 * (err - 2 * x - 1) / r;                              /* outward pixel */
				if (i < 256) 
				{
					drawPixelAA(xm - x, ym + y + 1, i);
					drawPixelAA(xm - y - 1, ym - x, i);
					drawPixelAA(xm + x, ym - y - 1, i);
					drawPixelAA(xm + y + 1, ym + x, i);
				}
				err += ++x * 2 + 1;
			}
			if (e2 + x2 <= 0) 
			{																/* y step */
				i = 255 * (2 * y + 3 - e2) / r;                                /* inward pixel */
				if (i < 256) 
				{
					drawPixelAA(xm - x2 - 1, ym + y, i);
					drawPixelAA(xm - y, ym - x2 - 1, i);
					drawPixelAA(xm + x2 + 1, ym - y, i);
					drawPixelAA(xm + y, ym + x2 + 1, i);
				}
				err += ++y * 2 + 1;
			}
		} while (x < 0);
}

void TFT::drawLineAA(int16_t x0, int16_t y0, int16_t x1, int16_t y1)
{
	int dx = abs(x1 - x0), sx = x0<x1 ? 1 : -1;
	int dy = abs(y1 - y0), sy = y0<y1 ? 1 : -1;
	int err = dx - dy, e2, x2;                       /* error value e_xy */
	int ed = dx + dy == 0 ? 1 : sqrt((float)dx*dx + (float)dy*dy);

	for (;;) 
	{                                         /* pixel loop */
		drawPixelAA(x0, y0, 255 * abs(err - dx + dy) / ed);
		e2 = err; x2 = x0;
		if (2 * e2 >= -dx) 
		{                                    /* x step */
			if (x0 == x1) break;
			if (e2 + dy < ed) drawPixelAA(x0, y0 + sy, 255 * (e2 + dy) / ed);
			err -= dy; x0 += sx;
		}
		if (2 * e2 <= dy) 
		{                                     /* y step */
			if (y0 == y1) break;
			if (dx - e2 < ed) drawPixelAA(x2 + sx, y0, 255 * (dx - e2) / ed);
			err += dx; y0 += sy;
		}
	}
}

// ----- Bezier curves routines -----
#ifdef SUPPORT_BEZIER
void TFT::drawQuadBezierSeg(int x0, int y0, int x1, int y1, int x2, int y2)
{                                  /* plot a limited quadratic Bezier segment */
	int sx = x2 - x1, sy = y2 - y1;
	long xx = x0 - x1, yy = y0 - y1, xy;              /* relative values for checks */
	double dx, dy, err, cur = xx*sy - yy*sx;                         /* curvature */

//	assert(xx*sx <= 0 && yy*sy <= 0);			 /* sign of gradient must not change */

	if (sx*(long)sx + sy*(long)sy > xx*xx + yy*yy) {      /* begin with longer part */
		x2 = x0; x0 = sx + x1; y2 = y0; y0 = sy + y1; cur = -cur;       /* swap P0 P2 */
	}
	if (cur != 0) {                                         /* no straight line */
		xx += sx; xx *= sx = x0 < x2 ? 1 : -1;                /* x step direction */
		yy += sy; yy *= sy = y0 < y2 ? 1 : -1;                /* y step direction */
		xy = 2 * xx*yy; xx *= xx; yy *= yy;               /* differences 2nd degree */
		if (cur*sx*sy < 0) {                                /* negated curvature? */
			xx = -xx; yy = -yy; xy = -xy; cur = -cur;
		}
		dx = 4.0*sy*cur*(x1 - x0) + xx - xy;                  /* differences 1st degree */
		dy = 4.0*sx*cur*(y0 - y1) + yy - xy;
		xx += xx; yy += yy; err = dx + dy + xy;                     /* error 1st step */
		do {
			drawPixel(x0, y0);                                          /* plot curve */
			if (x0 == x2 && y0 == y2) return;       /* last pixel -> curve finished */
			y1 = 2 * err < dx;                       /* save value for test of y step */
			if (2 * err > dy) { x0 += sx; dx -= xy; err += dy += yy; }      /* x step */
			if (y1) { y0 += sy; dy -= xy; err += dx += xx; }      /* y step */
		} while (dy < 0 && dx > 0);        /* gradient negates -> algorithm fails */
	}
	drawLine(x0, y0, x2, y2);                       /* plot remaining part to end */
}

void TFT::drawQuadBezier(int x0, int y0, int x1, int y1, int x2, int y2)
{                                          /* plot any quadratic Bezier curve */
	int x = x0 - x1, y = y0 - y1;
	double t = x0 - 2 * x1 + x2, r;

	if ((long)x*(x2 - x1) > 0) {                        /* horizontal cut at P4? */
		if ((long)y*(y2 - y1) > 0)                     /* vertical cut at P6 too? */
			if (fabs((y0 - 2 * y1 + y2) / t*x) > abs(y)) {               /* which first? */
				x0 = x2; x2 = x + x1; y0 = y2; y2 = y + y1;            /* swap points */
			}                            /* now horizontal cut at P4 comes first */
		t = (x0 - x1) / t;
		r = (1 - t)*((1 - t)*y0 + 2.0*t*y1) + t*t*y2;                       /* By(t=P4) */
		t = (x0*x2 - x1*x1)*t / (x0 - x1);                       /* gradient dP4/dx=0 */
		x = floor(t + 0.5); y = floor(r + 0.5);
		r = (y1 - y0)*(t - x0) / (x1 - x0) + y0;                  /* intersect P3 | P0 P1 */
		drawQuadBezierSeg(x0, y0, x, floor(r + 0.5), x, y);
		r = (y1 - y2)*(t - x2) / (x1 - x2) + y2;                  /* intersect P4 | P1 P2 */
		x0 = x1 = x; y0 = y; y1 = floor(r + 0.5);             /* P0 = P4, P1 = P8 */
	}
	if ((long)(y0 - y1)*(y2 - y1) > 0) {                    /* vertical cut at P6? */
		t = y0 - 2 * y1 + y2; t = (y0 - y1) / t;
		r = (1 - t)*((1 - t)*x0 + 2.0*t*x1) + t*t*x2;                       /* Bx(t=P6) */
		t = (y0*y2 - y1*y1)*t / (y0 - y1);                       /* gradient dP6/dy=0 */
		x = floor(r + 0.5); y = floor(t + 0.5);
		r = (x1 - x0)*(t - y0) / (y1 - y0) + x0;                  /* intersect P6 | P0 P1 */
		drawQuadBezierSeg(x0, y0, floor(r + 0.5), y, x, y);
		r = (x1 - x2)*(t - y2) / (y1 - y2) + x2;                  /* intersect P7 | P1 P2 */
		x0 = x; x1 = floor(r + 0.5); y0 = y1 = y;             /* P0 = P6, P1 = P7 */
	}
	drawQuadBezierSeg(x0, y0, x1, y1, x2, y2);                  /* remaining part */
}

void TFT::drawCubicBezierSeg(int x0, int y0, float x1, float y1,	float x2, float y2, int x3, int y3)
{                                        /* plot limited cubic Bezier segment */
	int f, fx, fy, leg = 1;
	int sx = x0 < x3 ? 1 : -1, sy = y0 < y3 ? 1 : -1;        /* step direction */
	float xc = -fabs(x0 + x1 - x2 - x3), xa = xc - 4 * sx*(x1 - x2), xb = sx*(x0 - x1 - x2 + x3);
	float yc = -fabs(y0 + y1 - y2 - y3), ya = yc - 4 * sy*(y1 - y2), yb = sy*(y0 - y1 - y2 + y3);
	double ab, ac, bc, cb, xx, xy, yy, dx, dy, ex, *pxy, EP = 0.01;
	/* check for curve restrains */
	/* slope P0-P1 == P2-P3    and  (P0-P3 == P1-P2      or   no slope change) */
//	assert((x1 - x0)*(x2 - x3) < EP && ((x3 - x0)*(x1 - x2) < EP || xb*xb < xa*xc + EP));
//	assert((y1 - y0)*(y2 - y3) < EP && ((y3 - y0)*(y1 - y2) < EP || yb*yb < ya*yc + EP));

	if (xa == 0 && ya == 0) {                              /* quadratic Bezier */
		sx = floor((3 * x1 - x0 + 1) / 2); sy = floor((3 * y1 - y0 + 1) / 2);   /* new midpoint */
		return drawQuadBezierSeg(x0, y0, sx, sy, x3, y3);
	}
	x1 = (x1 - x0)*(x1 - x0) + (y1 - y0)*(y1 - y0) + 1;                    /* line lengths */
	x2 = (x2 - x3)*(x2 - x3) + (y2 - y3)*(y2 - y3) + 1;
	do {                                                /* loop over both ends */
		ab = xa*yb - xb*ya; ac = xa*yc - xc*ya; bc = xb*yc - xc*yb;
		ex = ab*(ab + ac - 3 * bc) + ac*ac;       /* P0 part of self-intersection loop? */
		f = ex > 0 ? 1 : sqrt(1 + 1024 / x1);               /* calculate resolution */
		ab *= f; ac *= f; bc *= f; ex *= f*f;            /* increase resolution */
		xy = 9 * (ab + ac + bc) / 8; cb = 8 * (xa - ya);  /* init differences of 1st degree */
		dx = 27 * (8 * ab*(yb*yb - ya*yc) + ex*(ya + 2 * yb + yc)) / 64 - ya*ya*(xy - ya);
		dy = 27 * (8 * ab*(xb*xb - xa*xc) - ex*(xa + 2 * xb + xc)) / 64 - xa*xa*(xy + xa);
		/* init differences of 2nd degree */
		xx = 3 * (3 * ab*(3 * yb*yb - ya*ya - 2 * ya*yc) - ya*(3 * ac*(ya + yb) + ya*cb)) / 4;
		yy = 3 * (3 * ab*(3 * xb*xb - xa*xa - 2 * xa*xc) - xa*(3 * ac*(xa + xb) + xa*cb)) / 4;
		xy = xa*ya*(6 * ab + 6 * ac - 3 * bc + cb); ac = ya*ya; cb = xa*xa;
		xy = 3 * (xy + 9 * f*(cb*yb*yc - xb*xc*ac) - 18 * xb*yb*ab) / 8;

		if (ex < 0) {         /* negate values if inside self-intersection loop */
			dx = -dx; dy = -dy; xx = -xx; yy = -yy; xy = -xy; ac = -ac; cb = -cb;
		}                                     /* init differences of 3rd degree */
		ab = 6 * ya*ac; ac = -6 * xa*ac; bc = 6 * ya*cb; cb = -6 * xa*cb;
		dx += xy; ex = dx + dy; dy += xy;                    /* error of 1st step */

		for (pxy = &xy, fx = fy = f; x0 != x3 && y0 != y3; ) {
			drawPixel(x0, y0);                                       /* plot curve */
			do {                                  /* move sub-steps of one pixel */
				if (dx > *pxy || dy < *pxy) goto exit;       /* confusing values */
				y1 = 2 * ex - dy;                    /* save value for test of y step */
				if (2 * ex >= dx) {                                   /* x sub-step */
					fx--; ex += dx += xx; dy += xy += ac; yy += bc; xx += ab;
				}
				if (y1 <= 0) {                                      /* y sub-step */
					fy--; ex += dy += yy; dx += xy += bc; xx += ac; yy += cb;
				}
			} while (fx > 0 && fy > 0);                       /* pixel complete? */
			if (2 * fx <= f) { x0 += sx; fx += f; }                      /* x step */
			if (2 * fy <= f) { y0 += sy; fy += f; }                      /* y step */
			if (pxy == &xy && dx < 0 && dy > 0) pxy = &EP;  /* pixel ahead valid */
		}
	exit: xx = x0; x0 = x3; x3 = xx; sx = -sx; xb = -xb;             /* swap legs */
		yy = y0; y0 = y3; y3 = yy; sy = -sy; yb = -yb; x1 = x2;
	} while (leg--);                                          /* try other end */
	drawLine(x0, y0, x3, y3);       /* remaining part in case of cusp or crunode */
}


void TFT::drawCubicBezier(int x0, int y0, int x1, int y1, int x2, int y2, int x3, int y3)
{                                              /* plot any cubic Bezier curve */
	int n = 0, i = 0;
	long xc = x0 + x1 - x2 - x3, xa = xc - 4 * (x1 - x2);
	long xb = x0 - x1 - x2 + x3, xd = xb + 4 * (x1 + x2);
	long yc = y0 + y1 - y2 - y3, ya = yc - 4 * (y1 - y2);
	long yb = y0 - y1 - y2 + y3, yd = yb + 4 * (y1 + y2);
	float fx0 = x0, fx1, fx2, fx3, fy0 = y0, fy1, fy2, fy3;
	double t1 = xb*xb - xa*xc, t2, t[5];
	/* sub-divide curve at gradient sign changes */
	if (xa == 0) {                                               /* horizontal */
		if (abs(xc) < 2 * abs(xb)) t[n++] = xc / (2.0*xb);            /* one change */
	}
	else if (t1 > 0.0) {                                      /* two changes */
		t2 = sqrt(t1);
		t1 = (xb - t2) / xa; if (fabs(t1) < 1.0) t[n++] = t1;
		t1 = (xb + t2) / xa; if (fabs(t1) < 1.0) t[n++] = t1;
	}
	t1 = yb*yb - ya*yc;
	if (ya == 0) {                                                 /* vertical */
		if (abs(yc) < 2 * abs(yb)) t[n++] = yc / (2.0*yb);            /* one change */
	}
	else if (t1 > 0.0) {                                      /* two changes */
		t2 = sqrt(t1);
		t1 = (yb - t2) / ya; if (fabs(t1) < 1.0) t[n++] = t1;
		t1 = (yb + t2) / ya; if (fabs(t1) < 1.0) t[n++] = t1;
	}
	for (i = 1; i < n; i++)                         /* bubble sort of 4 points */
		if ((t1 = t[i - 1]) > t[i]) { t[i - 1] = t[i]; t[i] = t1; i = 0; }

	t1 = -1.0; t[n] = 1.0;                                /* begin / end point */
	for (i = 0; i <= n; i++) {                 /* plot each segment separately */
		t2 = t[i];                                /* sub-divide at t[i-1], t[i] */
		fx1 = (t1*(t1*xb - 2 * xc) - t2*(t1*(t1*xa - 2 * xb) + xc) + xd) / 8 - fx0;
		fy1 = (t1*(t1*yb - 2 * yc) - t2*(t1*(t1*ya - 2 * yb) + yc) + yd) / 8 - fy0;
		fx2 = (t2*(t2*xb - 2 * xc) - t1*(t2*(t2*xa - 2 * xb) + xc) + xd) / 8 - fx0;
		fy2 = (t2*(t2*yb - 2 * yc) - t1*(t2*(t2*ya - 2 * yb) + yc) + yd) / 8 - fy0;
		fx0 -= fx3 = (t2*(t2*(3 * xb - t2*xa) - 3 * xc) + xd) / 8;
		fy0 -= fy3 = (t2*(t2*(3 * yb - t2*ya) - 3 * yc) + yd) / 8;
		x3 = floor(fx3 + 0.5); y3 = floor(fy3 + 0.5);        /* scale bounds to int */
		if (fx0 != 0.0) { fx1 *= fx0 = (x0 - x3) / fx0; fx2 *= fx0; }
		if (fy0 != 0.0) { fy1 *= fy0 = (y0 - y3) / fy0; fy2 *= fy0; }
		if (x0 != x3 || y0 != y3)                            /* segment t1 - t2 */
			drawCubicBezierSeg(x0, y0, x0 + fx1, y0 + fy1, x0 + fx2, y0 + fy2, x3, y3);
		x0 = x3; y0 = y3; fx0 = fx3; fy0 = fy3; t1 = t2;
	}
}

void TFT::fillLine(int x0, int y0, int x1, int y1, float wd)
{
	int dx = abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
	int dy = abs(y1 - y0), sy = y0 < y1 ? 1 : -1;
	int err = dx - dy, e2, x2, y2;                          /* error value e_xy */
	float ed = dx + dy == 0 ? 1 : sqrt((float)dx*dx + (float)dy*dy);

	for (wd = (wd + 1) / 2; ; ) 
	{                                   /* pixel loop */
		drawPixelAA(x0, y0, 255 * (abs(err - dx + dy) / ed - wd + 1));
		e2 = err; x2 = x0;
		if (2 * e2 >= -dx) 
		{                                           /* x step */
			for (e2 += dy, y2 = y0; e2 < ed*wd && (y1 != y2 || dx > dy); e2 += dx)
				drawPixelAA(x0, y2 += sy, 255 * (abs(e2) / ed - wd + 1));
			if (x0 == x1) break;
			e2 = err; err -= dy; x0 += sx;
		}
		if (2 * e2 <= dy) 
		{                                            /* y step */
			for (e2 = dx - e2; e2 < ed*wd && (x1 != x2 || dx < dy); e2 += dy)
				drawPixelAA(x2 += sx, y0, 255 * (abs(e2) / ed - wd + 1));
			if (y0 == y1) break;
			err += dx; y0 += sy;
		}
	}
}

#endif // SUPPORT_BEZIER

// number representing the maximum angle (e.g. if 100, then if you pass in start=0 and end=50, you get a half circle)
// this can be changed with setArcParams function at runtime
#define DEFAULT_ARC_ANGLE_MAX 360		

// rotational offset in degrees defining position of value 0 (-90 will put it at the top of circle)
// this can be changed with setAngleOffset function at runtime
#define DEFAULT_ANGLE_OFFSET -90	

float _arcAngleMax = DEFAULT_ARC_ANGLE_MAX;
int16_t _angleOffset = DEFAULT_ANGLE_OFFSET;


// DrawArc function thanks to Jnmattern and his Arc_2.0 (https://github.com/Jnmattern)
void TFT::fillArcOffsetted(uint16_t cx, uint16_t cy, uint16_t radius, uint16_t thickness, float start, float end)
{
	int16_t xmin = 65535, xmax = -32767, ymin = 32767, ymax = -32767;
	float cosStart, sinStart, cosEnd, sinEnd;
	float r, t;
	float startAngle, endAngle;

	//Serial << "start: " << start << " end: " << end << endl;
	startAngle = (start / _arcAngleMax) * 360;	// 252
	endAngle = (end / _arcAngleMax) * 360;		// 807
												//Serial << "startAngle: " << startAngle << " endAngle: " << endAngle << endl;

	while (startAngle < 0) startAngle += 360;
	while (endAngle < 0) endAngle += 360;
	while (startAngle > 360) startAngle -= 360;
	while (endAngle > 360) endAngle -= 360;
	//if (endAngle == 0) endAngle = 360;

	if (startAngle > endAngle)
	{
		fillArcOffsetted(cx, cy, radius, thickness, ((startAngle) / (float)360) * _arcAngleMax, _arcAngleMax);
		fillArcOffsetted(cx, cy, radius, thickness, 0, ((endAngle) / (float)360) * _arcAngleMax);
	}
	else
	{
		// Calculate bounding box for the arc to be drawn
		cosStart = cos(startAngle * M_PI / 180);
		sinStart = sin(startAngle * M_PI / 180);
		cosEnd = cos(endAngle * M_PI / 180);
		sinEnd = sin(endAngle * M_PI / 180);

		r = radius;
		// Point 1: radius & startAngle
		t = r * cosStart;
		if (t < xmin) xmin = t;
		if (t > xmax) xmax = t;
		t = r * sinStart;
		if (t < ymin) ymin = t;
		if (t > ymax) ymax = t;

		// Point 2: radius & endAngle
		t = r * cosEnd;
		if (t < xmin) xmin = t;
		if (t > xmax) xmax = t;
		t = r * sinEnd;
		if (t < ymin) ymin = t;
		if (t > ymax) ymax = t;

		r = radius - thickness;
		// Point 3: radius-thickness & startAngle
		t = r * cosStart;
		if (t < xmin) xmin = t;
		if (t > xmax) xmax = t;
		t = r * sinStart;
		if (t < ymin) ymin = t;
		if (t > ymax) ymax = t;

		// Point 4: radius-thickness & endAngle
		t = r * cosEnd;
		if (t < xmin) xmin = t;
		if (t > xmax) xmax = t;
		t = r * sinEnd;
		if (t < ymin) ymin = t;
		if (t > ymax) ymax = t;

		// Corrections if arc crosses X or Y axis
		if ((startAngle < 90) && (endAngle > 90)) {
			ymax = radius;
		}

		if ((startAngle < 180) && (endAngle > 180)) {
			xmin = -radius;
		}

		if ((startAngle < 270) && (endAngle > 270)) {
			ymin = -radius;
		}

		// Slopes for the two sides of the arc
		float sslope = (float)cosStart / (float)sinStart;
		float eslope = (float)cosEnd / (float)sinEnd;

		if (endAngle == 360) eslope = -1000000;

		int ir2 = (radius - thickness) * (radius - thickness);
		int or2 = radius * radius;

//		fillScanline16(color);
		for (int x = xmin; x <= xmax; x++) 
		{
			bool y1StartFound = false, y2StartFound = false;
			bool y1EndFound = false, y2EndSearching = false;
			int y1s = 0, y1e = 0, y2s = 0;
			for (int y = ymin; y <= ymax; y++)
			{
				int x2 = x * x;
				int y2 = y * y;

				if ((x2 + y2 < or2 && x2 + y2 >= ir2) && (
					(y > 0 && startAngle < 180 && x <= y * sslope) ||
						(y < 0 && startAngle > 180 && x >= y * sslope) ||
						(y < 0 && startAngle <= 180) ||
						(y == 0 && startAngle <= 180 && x < 0) ||
						(y == 0 && startAngle == 0 && x > 0)
						) && ((y > 0 && endAngle < 180 && x >= y * eslope) ||
							(y < 0 && endAngle > 180 && x <= y * eslope) ||
							(y > 0 && endAngle >= 180) ||
							(y == 0 && endAngle >= 180 && x < 0) ||
							(y == 0 && startAngle == 0 && x > 0)))
				{
					if (!y1StartFound)	//start of the higher line found
					{
						y1StartFound = true;
						y1s = y;
					}
					else if (y1EndFound && !y2StartFound) //start of the lower line found
					{
						y2StartFound = true;
						//drawPixel_cont(cx+x, cy+y, ILI9341_BLUE);
						y2s = y;
						y += y1e - y1s - 1;	// calculate the most probable end of the lower line (in most cases the length of lower line is equal to length of upper line), in the next loop we will validate if the end of line is really there
						if (y > ymax - 1) // the most probable end of line 2 is beyond ymax so line 2 must be shorter, thus continue with pixel by pixel search
						{
							y = y2s;	// reset y and continue with pixel by pixel search
							y2EndSearching = true;
						}
					}
					else if (y2StartFound && !y2EndSearching)
					{
						// we validated that the probable end of the lower line has a pixel, continue with pixel by pixel search, in most cases next loop with confirm the end of lower line as it will not find a valid pixel
						y2EndSearching = true;
					}
					//drawPixel_cont(cx+x, cy+y, ILI9341_BLUE);
				}
				else
				{
					if (y1StartFound && !y1EndFound) //higher line end found
					{
						y1EndFound = true;
						y1e = y - 1;
					//	drawFastVLine_cont_noFill(cx + x, cy + y1s, y - y1s, color);
						//drawVLine(cx + x, cy + y1s, y - y1s, color);
						fillVLine(cx + x, cy + y1s, y - y1s);
						if (y < 0)
						{
							//Serial << x << " " << y << endl;
							y = abs(y); // skip the empty middle
						}
						else
							break;
					}
					else if (y2StartFound)
					{
						if (y2EndSearching)
						{
							// we found the end of the lower line after pixel by pixel search
						//	drawFastVLine_cont_noFill(cx + x, cy + y2s, y - y2s, color);
							//drawVLine(cx + x, cy + y2s, y - y2s, color);
							fillVLine(cx + x, cy + y2s, y - y2s);
							y2EndSearching = false;
							break;
						}
						else
						{
							// the expected end of the lower line is not there so the lower line must be shorter
							y = y2s;	// put the y back to the lower line start and go pixel by pixel to find the end
							y2EndSearching = true;
						}
					}
					//else
					//drawPixel_cont(cx+x, cy+y, ILI9341_RED);
				}
				//

				//delay(75);
			}
			if (y1StartFound && !y1EndFound)
			{
				y1e = ymax;
			//	drawFastVLine_cont_noFill(cx + x, cy + y1s, y1e - y1s + 1, color);
				//drawVLine(cx + x, cy + y1s, y1e - y1s + 1, color);
				fillVLine(cx + x, cy + y1s, y1e - y1s + 1);
			}
			else if (y2StartFound && y2EndSearching)	// we found start of lower line but we are still searching for the end
			{											// which we haven't found in the loop so the last pixel in a column must be the end
			//	drawFastVLine_cont_noFill(cx + x, cy + y2s, ymax - y2s + 1, color);
				//drawVLine(cx + x, cy + y2s, ymax - y2s + 1, color);
				fillVLine(cx + x, cy + y2s, ymax - y2s + 1);
			}
		}
	//	TODO: draw AA line by fill color
	//	drawLineAA(cx + round(cosEnd * (float)radius), cy + round(sinEnd * (float)radius), cx + round(cosEnd * (float)r), cy + round(sinEnd * (float)r));
	}

//	drawLineAA(cx + round(cosStart * radius), cy + round(sinStart * radius), cx + round(cosStart * r), cy + round(sinStart * r), color);
//	drawLineAA(cx + round(cosEnd * radius), cy + round(sinEnd * radius), cx + round(cosEnd * (radius - thickness)), cy + round(sinEnd * (radius - thickness)), color);

}

void TFT::fillArc(uint16_t x, uint16_t y, uint16_t radius, uint16_t thickness, float start, float end)
{
	if (start == 0 && end == _arcAngleMax)
		fillArcOffsetted(x, y, radius, thickness, 0, _arcAngleMax);
	else
		fillArcOffsetted(x, y, radius, thickness, start + (_angleOffset / (float)360)*_arcAngleMax, end + (_angleOffset / (float)360)*_arcAngleMax);
}

#endif

/*
void TFT::drawFillRectGradient(uint16_t x, uint16_t y, uint16_t w, uint16_t h, Color_TypeDef color1, Color_TypeDef color2)
{
	uint16_t i;
	uint8_t red1 = RED(color1);
	uint8_t green1 = GREEN(color1);
	uint8_t blue1 = BLUE(color1);

	uint8_t red2 = RED(color2);
	uint8_t green2 = GREEN(color2);
	uint8_t blue2 = BLUE(color2);

	for(i = 0; i<h; i++)
	{
		uint8_t r = red1 + (i * (red2 - red1) / h);
		uint8_t g = green1 + (i * (green2 - green1) / h);
		uint8_t b = blue1 + (i * (blue2 - blue1) / h);
		TFT_drawHLine(x, y+i, w, RGB565(r,g,b));
	}
}
*/



#ifdef TFT_INTERFACE_8080_16BIT_GPIO

void TFT::initInterface(bool bIsOutputMode)
{
	GPIO_PinModeSet(config_LCD_WE_PIN, gpioModePushPull, 1);
	GPIO_PinModeSet(config_LCD_RE_PIN, gpioModePushPull, 1);
	GPIO_PinModeSet(config_LCD_RESET_PIN, gpioModePushPull, 1);
	GPIO_PinModeSet(config_LCD_CS_PIN, gpioModePushPull, 1);
	GPIO_PinModeSet(config_LCD_DC_PIN, gpioModePushPull, 0);

	if(bIsOutputMode)
	{
		GPIO_PinModeSet(config_LCD_D0,gpioModePushPull,0);
		GPIO_PinModeSet(config_LCD_D1,gpioModePushPull,0);
		GPIO_PinModeSet(config_LCD_D2,gpioModePushPull,0);
		GPIO_PinModeSet(config_LCD_D3,gpioModePushPull,0);
		GPIO_PinModeSet(config_LCD_D4,gpioModePushPull,0);
		GPIO_PinModeSet(config_LCD_D5,gpioModePushPull,0);
		GPIO_PinModeSet(config_LCD_D6,gpioModePushPull,0);
		GPIO_PinModeSet(config_LCD_D7,gpioModePushPull,0);

		GPIO_PinModeSet(config_LCD_D8,gpioModePushPull,0);
		GPIO_PinModeSet(config_LCD_D9,gpioModePushPull,0);
		GPIO_PinModeSet(config_LCD_D10,gpioModePushPull,0);
		GPIO_PinModeSet(config_LCD_D11,gpioModePushPull,0);
		GPIO_PinModeSet(config_LCD_D12,gpioModePushPull,0);
		GPIO_PinModeSet(config_LCD_D13,gpioModePushPull,0);
		GPIO_PinModeSet(config_LCD_D14,gpioModePushPull,0);
		GPIO_PinModeSet(config_LCD_D15,gpioModePushPull,0);
	}
	else
	{
		GPIO_PinModeSet(config_LCD_D0,gpioModeInput,0);
		GPIO_PinModeSet(config_LCD_D1,gpioModeInput,0);
		GPIO_PinModeSet(config_LCD_D2,gpioModeInput,0);
		GPIO_PinModeSet(config_LCD_D3,gpioModeInput,0);
		GPIO_PinModeSet(config_LCD_D4,gpioModeInput,0);
		GPIO_PinModeSet(config_LCD_D5,gpioModeInput,0);
		GPIO_PinModeSet(config_LCD_D6,gpioModeInput,0);
		GPIO_PinModeSet(config_LCD_D7,gpioModeInput,0);

		GPIO_PinModeSet(config_LCD_D8,gpioModeInput,0);
		GPIO_PinModeSet(config_LCD_D9,gpioModeInput,0);
		GPIO_PinModeSet(config_LCD_D10,gpioModeInput,0);
		GPIO_PinModeSet(config_LCD_D11,gpioModeInput,0);
		GPIO_PinModeSet(config_LCD_D12,gpioModeInput,0);
		GPIO_PinModeSet(config_LCD_D13,gpioModeInput,0);
		GPIO_PinModeSet(config_LCD_D14,gpioModeInput,0);
		GPIO_PinModeSet(config_LCD_D15,gpioModeInput,0);
	}
}

void TFT::writeCmd(uint8_t uRegNum, const uint8_t* pData, uint8_t nDataSize)
{
	uint8_t i;
	GPIO_PinOutClear(config_LCD_CS_PIN);
	GPIO_PinOutClear(config_LCD_DC_PIN);		//	command mode
	GPIO_PortOutSetVal(config_LCD_LB_PORT, ((uint16_t)uRegNum)<<8, config_LCD_LB_MASK);
//	for(uint8_t c = 0; c<DELAY_CMD_VALUE; c++)	 	__asm("nop");

	GPIO_PinOutClear(config_LCD_WE_PIN);		//	Write strobe
	GPIO_PinOutSet(config_LCD_WE_PIN);

	if(nDataSize && pData)
	{
		GPIO_PinOutSet(config_LCD_DC_PIN);			//	data mode
		for(i = 0; i<nDataSize; i++)
		{
			GPIO_PortOutSetVal(config_LCD_LB_PORT, ((uint16_t)pData[i])<<8, config_LCD_LB_MASK);
//			for(uint8_t c = 0; c<DELAY_CMD_VALUE; c++)	 	__asm("nop");
			GPIO_PinOutClear(config_LCD_WE_PIN);		//	Write parameter strobe
			GPIO_PinOutSet(config_LCD_WE_PIN);
		}
	}
	GPIO_PinOutSet(config_LCD_CS_PIN);
}

void TFT::readCmd(uint8_t uRegNum, uint8_t* pData, uint8_t nDataSize)
{
	uint8_t i;
	GPIO_PinOutClear(config_LCD_CS_PIN);
	GPIO_PinOutClear(config_LCD_DC_PIN);		//	command mode

	initInterface(false);

	GPIO_PortOutSetVal(config_LCD_LB_PORT, ((uint16_t)uRegNum)<<8, config_LCD_LB_MASK);

	GPIO_PinOutClear(config_LCD_WE_PIN);		//	Write command code strobe
	GPIO_PinOutSet(config_LCD_WE_PIN);

	if(nDataSize && pData)
	{
		GPIO_PinOutSet(config_LCD_DC_PIN);			//	data mode

		for(i = 0; i<nDataSize; i++)
		{
		//	GPIO_PinOutClear(RE_PORT, RE_PIN);		//	Read parameter strobe
//			Delay(1);
		//	GPIO_PinOutSet(RE_PORT, RE_PIN);
			GPIO_PinOutClear(config_LCD_RE_PIN);							//	Write strobe
			GPIO_PinOutSet(config_LCD_RE_PIN);

			uint16_t uPortData = GPIO_PortInGet(config_LCD_LB_PORT);
			pData[i] = uPortData>>8;
		}
	}
	initInterface(true);
	GPIO_PinOutSet(config_LCD_CS_PIN);
}

void TFT::copyToGRAM(uint16_t *pSrcBuffer, uint32_t uCount)
{
	GPIO_PinOutClear(config_LCD_CS_PIN);
	GPIO_PinOutClear(config_LCD_DC_PIN);							//	command mode
	GPIO_PortOutSetVal(config_LCD_LB_PORT, ((uint16_t)0x2C)<<8, config_LCD_LB_MASK);

	GPIO_PinOutClear(config_LCD_WE_PIN);							//	Write strobe
	GPIO_PinOutSet(config_LCD_WE_PIN);

	GPIO_PinOutSet(config_LCD_DC_PIN);			//	data mode
	while(uCount--)
	{
		GPIO_PortOutSetVal(config_LCD_LB_PORT, *pSrcBuffer<<8, config_LCD_LB_MASK);
		GPIO_PortOutSetVal(config_LCD_HB_PORT, *pSrcBuffer>>8, config_LCD_HB_MASK);
		if(*pSrcBuffer&(1<<8))	GPIO_PinOutSet(config_LCD_HB_PORT,15);	else GPIO_PinOutClear(config_LCD_HB_PORT,15);
		pSrcBuffer++;

//		GPIO->P[WE_PORT].DOUTCLR = 1 << WE_PIN;	//	Write parameter strobe
//		GPIO->P[WE_PORT].DOUTSET = 1 << WE_PIN;
		GPIO_PinOutClear(config_LCD_WE_PIN);							//	Write strobe
		GPIO_PinOutSet(config_LCD_WE_PIN);
	}
	GPIO_PinOutSet(config_LCD_CS_PIN);
}

void TFT::fillGRAM(uint16_t uColor, uint32_t uCount)
{
	GPIO_PinOutClear(config_LCD_CS_PIN);
	GPIO_PinOutClear(config_LCD_DC_PIN);							//	command mode
	GPIO_PortOutSetVal(config_LCD_LB_PORT, ((uint16_t)ILI9341_MEMORYWRITE_REG)<<8, config_LCD_LB_MASK);
//	for(uint8_t c = 0; c<DELAY_CMD_VALUE; c++)	 	__asm("nop");

	GPIO_PinOutClear(config_LCD_WE_PIN);							//	Write strobe
	GPIO_PinOutSet(config_LCD_WE_PIN);

	GPIO_PinOutSet(config_LCD_DC_PIN);			//	data mode

	GPIO_PortOutSetVal(config_LCD_LB_PORT, uColor<<8, config_LCD_LB_MASK);
	GPIO_PortOutSetVal(config_LCD_HB_PORT, uColor>>9, config_LCD_HB_MASK);
	if(uColor&(1<<8))	GPIO_PinOutSet(config_LCD_HB_PORT,15);	else GPIO_PinOutClear(config_LCD_HB_PORT,15);
//	for(uint8_t c = 0; c<2*DELAY_CMD_VALUE; c++)	 	__asm("nop");
	while(uCount--)
	{
//		GPIO->P[WE_PORT].DOUTCLR = 1 << WE_PIN;	//	Write parameter strobe
//		GPIO->P[WE_PORT].DOUTSET = 1 << WE_PIN;
		GPIO_PinOutClear(config_LCD_WE_PIN);							//	Write strobe
		GPIO_PinOutSet(config_LCD_WE_PIN);
	}
	GPIO_PinOutSet(config_LCD_CS_PIN);
}
#endif

#ifdef TFT_INTERFACE_8080_16BIT_EBI
//	-----	use EBI interface for TFT driving

void TFT::initInterface(bool flag)
{
	GPIO_PinModeSet(config_LCD_RESET_PIN, gpioModePushPull, 1);
}

void TFT::writeCmd(uint8_t uRegNum, const uint8_t* pData, uint8_t nDataSize)
{
	//	D/C bit: 0 = Command, 1 = Data
//	uint16_t *ptrTftCmd = (uint16_t *) EBI_BankAddress(TFT_EBI_BANK);
//	uint16_t *ptrTftData = (uint16_t *) (EBI_BankAddress(TFT_EBI_BANK) | (1<<TFT_DC_ADDR_BIT));

	*m_ptrCmd = (uint16_t)uRegNum;
	while(nDataSize--)
		*m_ptrData = *pData++;
}

Color_TypeDef* TFT::getGRAM(void)
{
	return m_ptrData;
//	return ((Color_TypeDef *) (EBI_BankAddress(TFT_EBI_BANK) | (1<<TFT_DC_ADDR_BIT)));
}

void TFT::startWriteGRAM(void)
{
//	uint16_t *ptrTftCmd = (uint16_t *) EBI_BankAddress(TFT_EBI_BANK);
	*m_ptrCmd = (uint16_t)ILI9341_MEMORYWRITE_REG;
}

#ifndef TFT_USE_FRAME_BUFFER_DMA
static void TFT::copyToGRAM(uint16_t *pSrcBuffer, uint32_t uCount)
{
//	uint16_t *ptrTftCmd = (uint16_t *) EBI_BankAddress(TFT_EBI_BANK);
//	uint16_t *ptrTftData = (uint16_t *) (EBI_BankAddress(TFT_EBI_BANK) | (1<<TFT_DC_ADDR_BIT));

	*ptrTftCmd = (uint16_t)ILI9341_MEMORYWRITE_REG;
	while(uCount--)
		*ptrTftData = *pSrcBuffer++;
}
#endif	// TFT_USE_FRAME_BUFFER_DMA

#ifndef TFT_USE_FRAME_BUFFER
static void TFT::fillGRAM(Color_TypeDef uColor, uint32_t uCount)
{
//	uint16_t *ptrTftCmd = (uint16_t *) EBI_BankAddress(TFT_EBI_BANK);
//	uint16_t *ptrTftData = (uint16_t *) (EBI_BankAddress(TFT_EBI_BANK) | (1<<TFT_DC_ADDR_BIT));
	*ptrTftCmd = (uint16_t)ILI9341_MEMORYWRITE_REG;
	while(uCount--)
			*ptrTftData =  uColor;
}
#endif	//	TFT_USE_FRAME_BUFFER

#endif	//	TFT_INTERFACE_8080_16BIT_EBI

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------
