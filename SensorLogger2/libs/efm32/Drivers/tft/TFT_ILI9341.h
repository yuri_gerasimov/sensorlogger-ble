/*
 * TFT_ILI9341.h
 *
 *  Created on: 24.07.2015
 *      Author: Yuriy
 */

#ifndef TFT_ILI9341_H_
#define TFT_ILI9341_H_

#include <stddef.h>
#include <stdint.h>
#include "hw_config.h"

#include "fonts/font_common.h"

/*
#define TFT_USE_FRAME_BUFFER
#define TFT_WIDTH 320
#define TFT_HEIGHT 240
*/

#define TFT_USE_FRAME_BUFFER_MAX 2

#if config_TFT_BPS==8
typedef uint8_t Color_TypeDef;
#elif config_TFT_BPS==16
typedef uint16_t Color_TypeDef;
#define RGB(r,g,b)  	(((r&0xF8)<<8)|((g&0xFC)<<3)|((b&0xF8)>>3)) 	//5 red | 6 green | 5 blue
#define RGB565(r,g,b)  	(((r&0xF8)<<8)|((g&0xFC)<<3)|((b&0xF8)>>3)) 	//5 red | 6 green | 5 blue
#elif config_TFT_BPS==32
typedef uint32_t Color_TypeDef;
#endif

#define ILI9341_MEMORYWRITE_REG 0x2C

class TFT
{
private:
	uint8_t m_uOrientation;
	uint16_t m_uWidth;
	uint16_t m_uHeight;

	uint16_t m_uRotatedWidth;
	uint16_t m_uRotatedHeight;

	uint16_t m_xOffset;
	uint16_t m_yOffset;
	uint16_t m_uBitsPerPixel;
	uint32_t m_uPixelsNumber;

	Color_TypeDef m_colorPen;
	Color_TypeDef m_colorFill;
	Color_TypeDef m_colorText;

	Color_TypeDef* m_ptrData;
	Color_TypeDef* m_ptrCmd;

	FONT_INFO *m_pFontInfo;
	TFT_StringAlign_t m_TextAlignmentH;
	TFT_StringAlign_t m_TextAlignmentV;

	void initInterface(bool bIsOutputMode);
	void startWriteGRAM(void);

	void writeCmd(uint8_t uRegNum, const uint8_t* pData, uint8_t nDataSize);
	void readCmd(uint8_t uRegNum, const uint8_t* pData, uint8_t nDataSize);

protected:

public:

#ifdef TFT_USE_FRAME_BUFFER
	uint8_t m_uFbCurrentIndex;
	uint16_t m_nFrameBufferNumber;
	Color_TypeDef* m_pFrameBuffer[TFT_USE_FRAME_BUFFER_MAX];

	TFT(uint16_t uWidth, uint16_t uHeight, uint16_t uBitsPerPixel, Color_TypeDef* pFrameBuffer, uint8_t nFrameBufferNum);
#else
	TFT(uint16_t uWidth, uint16_t uHeight, uint16_t uBitsPerPixel);
#endif
	void init(void);

#ifdef TFT_INTERFACE_8080_16BIT_EBI
	Color_TypeDef* getGRAM(void);
#endif

	void setScroll(int16_t x, int16_t y);
	void setWindow(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);
	void setOrientation(uint8_t o);
	void enterSleep(void);
	void exitSleep(void);

	void setColorPen(Color_TypeDef color) 	{		m_colorPen = color;		}
	void setColorFill(Color_TypeDef color)	{		m_colorFill = color;	}
	void setColorText(Color_TypeDef color)	{		m_colorText = color;	}

	Color_TypeDef getColorPen(void)			{		return m_colorPen;		}
	Color_TypeDef getColorFill(void)		{		return m_colorFill;		}
	Color_TypeDef getColorText(void)		{		return m_colorText;		}
	
	uint16_t getWidth(void)					{		return m_uRotatedWidth;		}
	uint16_t getHeight(void)				{		return m_uRotatedHeight;	}
	uint16_t getBitsPerPixel(void)			{		return m_uBitsPerPixel;		}

//	graphic primitives drawing functions = pen color used
	void drawPixel(uint16_t x, uint16_t y);
	void drawLine(int x0, int y0, int x1, int y1);
	void drawVLine(uint16_t x, uint16_t y, uint16_t h);
	void drawHLine(uint16_t x, uint16_t y, uint16_t w);
	void drawRect(uint16_t x, uint16_t y, uint16_t w, uint16_t h);
	void drawCircle(uint16_t x0, uint16_t y0, uint16_t r);
	void drawArc(uint16_t x0, uint16_t y0, uint16_t r, int16_t a1, int16_t a2);
	void drawBitmap(uint16_t x, uint16_t y, uint16_t w, uint16_t h, Color_TypeDef *pSrcBitmap);
#ifdef SUPPORT_ANTIALIASING
	void drawPixelAA(uint16_t x, uint16_t y, uint8_t alfa);
	void drawArcAA(uint16_t x, uint16_t y, uint16_t r, int16_t a1, int16_t a2);
	void drawLineAA(int16_t x0, int16_t y0, int16_t x1, int16_t y1);
	void drawCircleAA(uint16_t x0, uint16_t y0, uint16_t r);
#endif

//	graphic primitives filling functions = fill color ised
	void fill(void);
	void fillHLine(uint16_t x, uint16_t y, uint16_t w);
	void fillVLine(uint16_t x, uint16_t y, uint16_t h);
	void fillLine(int x0, int y0, int x1, int y1, float wd);
	void fillRect(uint16_t x, uint16_t y, uint16_t w, uint16_t h);
	void fillGradientRect(uint16_t x, uint16_t y, uint16_t w, uint16_t h, Color_TypeDef color1, Color_TypeDef color2);
	void fillCircle(uint16_t x0, uint16_t y0, uint16_t r);
	void fillArc(uint16_t x, uint16_t y, uint16_t radius, uint16_t thickness, float start, float end);
	void fillRoundHLine(uint16_t x, uint16_t y, uint16_t w, uint16_t h);
	void fillRoundVLine(uint16_t x, uint16_t y, uint16_t w, uint16_t h);
#ifdef SUPPORT_ANTIALIASING
	void fillRectAA(uint16_t x, uint16_t y, uint16_t w, uint16_t h);
	void fillArcOffsetted(uint16_t cx, uint16_t cy, uint16_t radius, uint16_t thickness, float start, float end);
#endif


#ifdef TFT_USE_FRAME_BUFFER
	void setFrameBufferIndex(uint8_t nIndex);
	void updateFrameBuffer(bool bWaitForComplete);
	Color_TypeDef* getFrameBuffer(uint8_t nIndex);
#endif

//	text primitives drawing functions - text color used
	void setFont(const FONT_INFO *pFontInfoplotCubicBezier);
	void drawChar(char c, uint16_t x, uint16_t y);
	void drawString(const char *pStr, uint16_t x, uint16_t y);
	void drawStringInRect(const char *pStr, uint16_t x, uint16_t y, uint16_t w, uint16_t h);

	void setTextAlignemt(TFT_StringAlign_t alignH, TFT_StringAlign_t alignV);
	void getStringDimension(const char *pStr, uint16_t *pWidth, uint16_t *pHeight);
	uint8_t getFontHeight(void);

#ifdef SUPPORT_BEZIER
	void drawQuadBezier(int x0, int y0, int x1, int y1, int x2, int y2);
	void drawQuadBezierSeg(int x0, int y0, int x1, int y1, int x2, int y2);
	void drawCubicBezier(int x0, int y0, int x1, int y1, int x2, int y2, int x3, int y3);
	void drawCubicBezierSeg(int x0, int y0, float x1, float y1, float x2, float y2, int x3, int y3);
#endif
};

#endif /* TFT_ILI9341_H_ */
