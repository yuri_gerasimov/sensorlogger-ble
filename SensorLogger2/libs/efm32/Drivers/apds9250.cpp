/*
 * APDS9250.cpp
 *
 *  Created on: 03.09.2016
 *      Author: Yuriy Gerasimov
 */

#include "APDS9250.h"
#include "i2cspm.h"

#include "defines.h"

bool APDS9250::init(I2C_TypeDef *i2c, uint8_t addr)
{
	m_i2cReg.Init(i2c, addr);

	if(m_i2cReg.ReadU8(APDS9250_REGISTER_PART_ID)!=APDS9250_PART_ID)
		return false;

	return true;
}

uint32_t APDS9250::readRed(void)
{
	uint32_t value = 0;
	m_i2cReg.Read(APDS9250_REGISTER_DATA_RED,&value,3);
	return value;
}

uint32_t APDS9250::readGreen(void)
{
	uint32_t value = 0;
	m_i2cReg.Read(APDS9250_REGISTER_DATA_GREEN,&value,3);
	return value;
}

uint32_t APDS9250::readBlue(void)
{
	uint32_t value = 0;
	m_i2cReg.Read(APDS9250_REGISTER_DATA_BLUE,&value,3);
	return value;
}

uint32_t APDS9250::readIR(void)
{
	uint32_t value = 0;
	m_i2cReg.Read(APDS9250_REGISTER_DATA_IR,&value,3);
	return value;
}

void APDS9250::enable(bool bIsEnabled, uint8_t uMode)
{
	APDS9250_MAIN_CTRL_Register reg;

	reg.mByte = m_i2cReg.ReadU8(APDS9250_REGISTER_MAIN_CTRL);

	if(bIsEnabled)	reg.mBits.bitLS_EN = 1;
	else reg.mBits.bitLS_EN = 0;

	reg.mBits.bitCS_Mode = uMode;

	m_i2cReg.WriteU8(APDS9250_REGISTER_MAIN_CTRL,reg.mByte);
}

void APDS9250::setResolution(APDS9250_LS_Resolution_t eRes)
{
	APDS9250_LS_MEAS_RATE_Register reg;
	reg.mByte = m_i2cReg.ReadU8(APDS9250_REGISTER_LS_MEAS_RATE);
	reg.mBits.bitLS_Res = eRes;
	m_i2cReg.WriteU8(APDS9250_REGISTER_LS_MEAS_RATE,reg.mByte);
}

void APDS9250::setRate(APDS9250_LS_Rate_t eRate)
{
	APDS9250_LS_MEAS_RATE_Register reg;
	reg.mByte = m_i2cReg.ReadU8(APDS9250_REGISTER_LS_MEAS_RATE);
	reg.mBits.bitLS_Rate = eRate;
	m_i2cReg.WriteU8(APDS9250_REGISTER_LS_MEAS_RATE,reg.mByte);

}

void APDS9250::setGain(APDS9250_LS_Gain_t eGain)
{
	APDS9250_LS_GAIN_Register reg;
	reg.mByte = m_i2cReg.ReadU8(APDS9250_REGISTER_LS_GAIN);
	reg.mBits.bitLS_Gain = eGain;
	m_i2cReg.WriteU8(APDS9250_REGISTER_LS_GAIN,reg.mByte);
}
