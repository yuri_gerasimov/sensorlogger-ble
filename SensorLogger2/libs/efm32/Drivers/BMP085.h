/*
 * BMP085.h
 *
 *  Created on: 03.08.2015
 *      Author: Yuriy
 */

#ifndef BMP085_H_
#define BMP085_H_

#include "stdint.h"
#include "stdbool.h"

#include "i2cspm.h"

#define BMP085_COEF_START_REG 	0xAA
#define BMP085_COEF_NUMBER	11

#define BMP085_CONTROL_REGISTER	0xF4
#define BMP085_TEMPDATA         0xF6
#define BMP085_PRESSUREDATA     0xF6
#define BMP085_READTEMPCMD      0x2E
#define BMP085_READPRESSURECMD  0x34

class BMP085
{
private:
	uint8_t m_Addr;

	union
	{
		uint16_t m_Raw[BMP085_COEF_NUMBER];
		struct
		{
			int16_t AC1;
			int16_t AC2;
			int16_t AC3;
			uint16_t AC4;
			uint16_t AC5;
			uint16_t AC6;
			int16_t B1;
			int16_t B2;
			int16_t MB;
			int16_t MC;
			int16_t MD;
		};
	} m_CalibCoeffs;

	int32_t B5;

	I2C_TypeDef *m_i2c;
	void WriteCmd(uint8_t cmd);
	bool ReadRegister(uint8_t uRegAddr, void *pBuffer, uint8_t uLenBytes);
	//bool ReadRegister(uint8_t uRegAddr, void *pBuffer, uint8_t uLenBytes);
	bool WriteRegister(uint8_t uRegAddr, uint8_t uValue);

public:
	BMP085(void);

public:
	bool Init(I2C_TypeDef *i2c, uint8_t addr);
	void StartMeasureT(void);
	void StartMeasureP(void);

	int16_t GetT(void);
	int32_t GetP(void);
};


#endif /* BMP085_H_ */
