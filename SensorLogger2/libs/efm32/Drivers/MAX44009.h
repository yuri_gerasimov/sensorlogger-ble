/*
 * MAX44009.h
 *
 *  Created on: 03.12.2015
 *      Author: Yuriy
 */

#ifndef MAX44009_H_
#define MAX44009_H_

#include <stdint.h>
#include <stdbool.h>

#include "i2cspm.h"
#include "i2c_reg.h"

#define MAX44009_ADDRESS_A0_LOW 	(0x4A<<1)
#define MAX44009_ADDRESS_A0_HIGH 	(0x4B<<1)

typedef enum
{
	MAX44009_IntStatus_Reg = 0x00,
	MAX44009_IntEnable_Reg = 0x01,
	MAX44009_Config_Reg = 0x02,
	MAX44009_LuxHigh_Reg = 0x03,
	MAX44009_LuxLow_Reg = 0x04,
	MAX44009_UpThreshold_Reg = 0x05,
	MAX44009_LowThreshold_Reg = 0x06,
	MAX44009_ThresholdTimer_Reg = 0x07
} MAX44009_RegistryMap_t;

union MAX44009_INT_STATUS_Register
{
	struct
	{
		uint8_t bitINTS:1;
		uint8_t bitRes:7;
	} mBits;
	uint8_t mByte;
};

union MAX44009_INT_ENABLE_Register
{
	struct
	{
		uint8_t bitINTE:1;
		uint8_t bitRes:7;
	} mBits;
	uint8_t mByte;
};

union MAX44009_CONFIG_Register
{
	struct
	{
		uint8_t bitsTIM:3;
		uint8_t bitCDR:3;
		uint8_t bitRes:2;
		uint8_t biMANUAL:1;
		uint8_t biCONT:1;
	} mBits;
	uint8_t mByte;
};


class MAX44009
{
private:
//	uint8_t m_Addr;
//	I2C_TypeDef *m_i2c;
	I2CReg m_i2cReg;

public:
	MAX44009();
	bool Init(I2C_TypeDef *i2c, uint8_t addr);
	void EnableInterrupt(bool bIsEnabled);
	bool Read(uint32_t *pValue);
	bool GetInterruptFlag(void);
	void SetContiniousMode(bool bIsEnabled);
};

#endif /* MAX44009_H_ */
