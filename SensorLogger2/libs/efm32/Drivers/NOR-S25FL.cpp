/*
 * S25FL1-K.cpp
 *
 *  Created on: 22.07.2015
 *      Author: Yuriy Gerasimov
 */

// 	http://www.spansion.com/Products/memory/Serial-Flash/Pages/Spansion%20FL.aspx
//	https://github.com/BleepLabs/S25FLx

#include "em_assert.h"
#include "em_usart.h"
#include "em_gpio.h"

#include "NOR-S25FL.h"
#include "config/hw_config.h"

#define SPI_CHIP_SELECT(port,pin)	GPIO_PinOutClear(port,pin)
#define SPI_CHIP_DESELECT(port,pin)	GPIO_PinOutSet(port,pin)

void FlashMem::Init(USART_TypeDef *pUSART, GPIO_Port_TypeDef portCS, uint8_t pinCS)
{
	m_uSectorCount = 0;

	m_pUSART = pUSART;
	m_portCS = portCS;
	m_pinCS = pinCS;

	EFM_ASSERT(m_pUSART);
	GPIO_PinModeSet(m_portCS, m_pinCS, gpioModePushPull, 1);		// configure Chip-Select pin

//	m_pSPI->ConfigCSpin(gpioPortF,3);
//	m_pSPI->Init(USART1,USART_ROUTE_LOCATION_LOC1,16000000,true,false,true);
	//m_pSPI->Init(8000000,true);
	//m_pSPI->Enable(true);
}

bool FlashMem::Read_JEDEC_ID(uint8_t *pManufactureID, uint8_t *pDeviceType, uint8_t *pCapacityCode)
{
	EFM_ASSERT(m_pUSART);

	SPI_CHIP_SELECT(m_portCS,m_pinCS);

	USART_SpiTransfer(m_pUSART,S25FL_JEDEC_ID);
	*pManufactureID = USART_SpiTransfer(m_pUSART, 0);
	*pDeviceType = USART_SpiTransfer(m_pUSART, 0);
	*pCapacityCode = USART_SpiTransfer(m_pUSART, 0);

/*	m_pSPI->WriteByte(S25FL_JEDEC_ID);
	*pManufactureID = m_pSPI->ReadByte();
	*pDeviceType = m_pSPI->ReadByte();
	*pCapacityCode = m_pSPI->ReadByte();
*/
	SPI_CHIP_DESELECT(m_portCS,m_pinCS);

	if(*pDeviceType!=S25FL_DEVICE_TYPE)	return false;

	if(*pCapacityCode == S25FL116K_CAPACITY_CODE)		m_uSectorCount = 512;
	else if(*pCapacityCode == S25FL132K_CAPACITY_CODE)	m_uSectorCount = 1024;
	else if(*pCapacityCode == S25FL164K_CAPACITY_CODE)	m_uSectorCount = 2048;
	else return false;

	return true;
}

void FlashMem::ReadReg(uint8_t uRegCmd, void *pBufferSend, uint32_t uLenghtSend, void *pBufferRcv, uint32_t uLenghtRcv)
{
	uint8_t *pBytePtrSend = (uint8_t *)pBufferSend;
	uint8_t *pBytePtrRcv = (uint8_t *)pBufferRcv;

	SPI_CHIP_SELECT(m_portCS,m_pinCS);
//	m_pSPI->WriteByte(uRegCmd);
	USART_SpiTransfer(m_pUSART,uRegCmd);

	while(uLenghtSend>0)
	{
//		m_pSPI->WriteByte(*pBytePtrSend++);
		USART_SpiTransfer(m_pUSART, *pBytePtrSend++);
		uLenghtSend--;
	}

	while(uLenghtRcv>0)
	{
	//	*pBytePtrRcv++ = m_pSPI->ReadByte();
		*pBytePtrRcv++ = USART_SpiTransfer(m_pUSART, 0);
		uLenghtRcv--;
	}
	SPI_CHIP_DESELECT(m_portCS,m_pinCS);
}

uint8_t FlashMem::GetStatus(void)
{
	SPI_CHIP_SELECT(m_portCS,m_pinCS);
	//m_pSPI->WriteByte(S25FL_READ_STATUS_REG1);
	USART_SpiTransfer(m_pUSART, S25FL_READ_STATUS_REG1);
//	uint8_t b = m_pSPI->ReadByte();
	uint8_t b = USART_SpiTransfer(m_pUSART, 0);
	SPI_CHIP_DESELECT(m_portCS,m_pinCS);
	return b;
}

bool FlashMem::IsBusy(void)
{
	uint8_t b = GetStatus();
	if(b&1)	return true;
	return false;
}

void FlashMem::Wait(void)
{
	uint8_t b;
	do
		b = GetStatus();
	while(b&1);
}

void FlashMem::WriteEnable(void)
{
	SPI_CHIP_SELECT(m_portCS,m_pinCS);
//	m_pSPI->WriteByte(S25FL_WRITE_ENABLE);
	USART_SpiTransfer(m_pUSART, S25FL_WRITE_ENABLE);
	SPI_CHIP_DESELECT(m_portCS,m_pinCS);
	Wait();
}

void FlashMem::Read(uint32_t uAddress, void *pBuffer, uint32_t uLenght)
{
	uint8_t *pBytePtr = (uint8_t *)pBuffer;

	SPI_CHIP_SELECT(m_portCS,m_pinCS);
	USART_SpiTransfer(m_pUSART, S25FL_READ_DATA);
	USART_SpiTransfer(m_pUSART, (uint8_t)(uAddress>>16));
	USART_SpiTransfer(m_pUSART, (uint8_t)(uAddress>>8));
	USART_SpiTransfer(m_pUSART, (uint8_t)(uAddress & 0xff));

	/*
	m_pSPI->WriteByte(S25FL_READ_DATA);           	//control byte follow by location bytes
	m_pSPI->WriteByte((uint8_t)(uAddress>>16));   	// convert the location integer to 3 bytes
	m_pSPI->WriteByte((uint8_t)(uAddress>>8));
	m_pSPI->WriteByte((uint8_t)(uAddress & 0xff));
	*/

	for (uint32_t i=0; i<uLenght;i++)
	//  pBytePtr[i] = m_pSPI->ReadByte();  	//read the data
		pBytePtr[i] = USART_SpiTransfer(m_pUSART, 0);

	SPI_CHIP_DESELECT(m_portCS,m_pinCS);
}

void FlashMem::Write(uint32_t uAddress, void *pBuffer, uint32_t uLenght)
{
	if(uLenght<255)
	{
		uint8_t *pBytePtr = (uint8_t *)pBuffer;
		WriteEnable(); // Must be done before writing can commence. Erase clears it.
		Wait();

		SPI_CHIP_SELECT(m_portCS,m_pinCS);

		USART_SpiTransfer(m_pUSART, S25FL_PAGE_PROGRAM);
		USART_SpiTransfer(m_pUSART, (uint8_t)(uAddress>>16));
		USART_SpiTransfer(m_pUSART, (uint8_t)(uAddress>>8));
		USART_SpiTransfer(m_pUSART, (uint8_t)(uAddress & 0xff));
/*
		m_pSPI->WriteByte(S25FL_PAGE_PROGRAM);
		m_pSPI->WriteByte((uint8_t)(uAddress>>16));
		m_pSPI->WriteByte((uint8_t)(uAddress>>8));
		m_pSPI->WriteByte((uint8_t)(uAddress & 0xff));
*/
		for (uint32_t i=0; i<uLenght;i++)
//			m_pSPI->WriteByte(pBytePtr[i]);
		USART_SpiTransfer(m_pUSART, pBytePtr[i]);

		SPI_CHIP_DESELECT(m_portCS,m_pinCS);
		Wait();
	}
}

//	erase 4K sector
void FlashMem::EraseSector(uint32_t uAddress)
{
	Wait();
	WriteEnable();
	SPI_CHIP_SELECT(m_portCS,m_pinCS);
/*
	m_pSPI->WriteByte(S25FL_SECTOR_ERASE);
	m_pSPI->WriteByte((uint8_t)(uAddress>>16));
	m_pSPI->WriteByte((uint8_t)(uAddress>>8));
	m_pSPI->WriteByte((uint8_t)(uAddress & 0xFF));
	*/
	USART_SpiTransfer(m_pUSART, S25FL_SECTOR_ERASE);
	USART_SpiTransfer(m_pUSART, (uint8_t)(uAddress>>16));
	USART_SpiTransfer(m_pUSART, (uint8_t)(uAddress>>8));
	USART_SpiTransfer(m_pUSART, (uint8_t)(uAddress & 0xff));
	SPI_CHIP_DESELECT(m_portCS,m_pinCS);
	Wait();
}

//	erase 64K block
void FlashMem::EraseBlock(uint32_t uAddress)
{
	Wait();
	WriteEnable();
	SPI_CHIP_SELECT(m_portCS,m_pinCS);
/*	m_pSPI->WriteByte(S25FL_BLOCK_ERASE);
	m_pSPI->WriteByte((uint8_t)(uAddress>>16));
	m_pSPI->WriteByte((uint8_t)(uAddress>>8));
	m_pSPI->WriteByte((uint8_t)(uAddress & 0xFF));
	*/
	USART_SpiTransfer(m_pUSART, S25FL_BLOCK_ERASE);
	USART_SpiTransfer(m_pUSART, (uint8_t)(uAddress>>16));
	USART_SpiTransfer(m_pUSART, (uint8_t)(uAddress>>8));
	USART_SpiTransfer(m_pUSART, (uint8_t)(uAddress & 0xff));
	SPI_CHIP_DESELECT(m_portCS,m_pinCS);
//	Wait();
}

//	erase whole chip
void FlashMem::EraseChip(void)
{
	Wait();
	WriteEnable();
	SPI_CHIP_SELECT(m_portCS,m_pinCS);
//	m_pSPI->WriteByte(S25FL_CHIP_ERASE);
	USART_SpiTransfer(m_pUSART, S25FL_CHIP_ERASE);
	SPI_CHIP_DESELECT(m_portCS,m_pinCS);
//	Wait();
}

void FlashMem::WriteStatusReg(uint8_t w)
{

}
