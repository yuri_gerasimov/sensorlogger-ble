/*
 * FXOS8700CQ.cpp
 *
 *  Created on: 03.12.2015
 *      Author: Yuriy
 */

#include "FXOS8700CQ.h"
#include "i2cspm.h"

bool FXOS8700CQ::Init(I2C_TypeDef *i2c, uint8_t addr)
{
	accelFSR = AFS_2g;     	// Set the scale below either 2, 4 or 8
	accelODR = AODR_200HZ; 	// In hybrid mode, accel/mag data sample rates are half of this value
	magOSR = MOSR_5;     	// Choose magnetometer oversample rate

	m_i2cReg.Init(i2c, addr);

	if(m_i2cReg.ReadU8(FXOS8700CQ_WHO_AM_I)!=0xC7)	return false;

	//Standby(true);  // Must be in standby to change registers

	m_i2cReg.WriteU8(FXOS8700CQ_CTRL_REG1,0);
	m_i2cReg.WriteU8(FXOS8700CQ_M_CTRL_REG1,0x1F);
	m_i2cReg.WriteU8(FXOS8700CQ_M_CTRL_REG2,0x20);
	m_i2cReg.WriteU8(FXOS8700CQ_XYZ_DATA_CFG,0x01);
	m_i2cReg.WriteU8(FXOS8700CQ_CTRL_REG1,0x0D);

	return true;
		// Configure the accelerometer
	m_i2cReg.WriteU8(FXOS8700CQ_XYZ_DATA_CFG, accelFSR);  // Choose the full scale range to 2, 4, or 8 g.

	//writeReg(FXOS8700CQ_CTRL_REG1, readReg(FXOS8700CQ_CTRL_REG1) & ~(0x38)); // Clear the 3 data rate bits 5:3
	if (accelODR <= 7)
		m_i2cReg.WriteU8(FXOS8700CQ_CTRL_REG1, m_i2cReg.ReadU8(FXOS8700CQ_CTRL_REG1) | (accelODR << 3));
	//writeReg(FXOS8700CQ_CTRL_REG2, readReg(FXOS8700CQ_CTRL_REG2) & ~(0x03)); // clear bits 0 and 1
	//writeReg(FXOS8700CQ_CTRL_REG2, readReg(FXOS8700CQ_CTRL_REG2) |  (0x02)); // select normal(00) or high resolution (10) mode

	// Configure the magnetometer
	m_i2cReg.WriteU8(FXOS8700CQ_M_CTRL_REG1, 0x80 | magOSR << 2 | 0x03); // Set auto-calibration, set oversampling, enable hybrid mode

	// Configure interrupts 1 and 2
	//writeReg(CTRL_REG3, readReg(CTRL_REG3) & ~(0x02)); // clear bits 0, 1
	//writeReg(CTRL_REG3, readReg(CTRL_REG3) |  (0x02)); // select ACTIVE HIGH, push-pull interrupts
	//writeReg(CTRL_REG4, readReg(CTRL_REG4) & ~(0x1D)); // clear bits 0, 3, and 4
	//writeReg(CTRL_REG4, readReg(CTRL_REG4) |  (0x1D)); // DRDY, Freefall/Motion, P/L and tap ints enabled
	//writeReg(CTRL_REG5, 0x01);  // DRDY on INT1, P/L and taps on INT2

	Standby(false);  // Set to active to start reading
	return true;
}

bool FXOS8700CQ::Standby(bool bIsStandby)
{
	uint8_t b = m_i2cReg.ReadU8(FXOS8700CQ_CTRL_REG1);
	if(bIsStandby)	b = b & ~(0x01);
			else 	b = b | (0x01);
	return m_i2cReg.WriteU8(FXOS8700CQ_CTRL_REG1, b);
}

bool FXOS8700CQ::ReadAccelData(AccelerometerData_t *pData)
{
	uint8_t rawData[6] = {0,0,0,0,0,0};  // x/y/z accel register data stored here
//	uint16_t rawData[3];

	if(!m_i2cReg.Read(FXOS8700CQ_OUT_X_MSB, &rawData[0], 6 ))	return false;  // Read the six raw data registers into data array
//	pData->x = rawData[0]>>2;
//	pData->y = rawData[1]>>2;
//	pData->z = rawData[2]>>2;
	pData->x = ((int16_t) rawData[0] << 8 | rawData[1]) >> 2;
	pData->y = ((int16_t) rawData[2] << 8 | rawData[3]) >> 2;
	pData->z = ((int16_t) rawData[4] << 8 | rawData[5]) >> 2;
	return true;
}

bool FXOS8700CQ::ReadMagnData(MagnetometerData_t *pData)
{
	uint8_t rawData[6] = {0,0,0,0,0,0};  // x/y/z magnetometer register data stored here
//	uint16_t rawData[3];

	if(!m_i2cReg.Read(FXOS8700CQ_M_OUT_X_MSB, &rawData[0], 6))	return false;  // Read the six raw data registers into data array
//	pData->x = rawData[0]>>2;
//	pData->y = rawData[1]>>2;
//	pData->z = rawData[2]>>2;

	pData->x = ((int16_t) rawData[0] << 8 | rawData[1]);
	pData->y = ((int16_t) rawData[2] << 8 | rawData[3]);
	pData->z = ((int16_t) rawData[4] << 8 | rawData[5]);
	return true;
}


uint8_t FXOS8700CQ::ReadTempData(void)
{
	return m_i2cReg.ReadU8(FXOS8700CQ_TEMP);
}
