/*
 * BMP085.cpp
 *
 *  Created on: 03.08.2015
 *      Author: Yuriy
 */

#ifndef BMP085_CPP_
#define BMP085_CPP_

#include "BMP085.h"
#include "i2cspm.h"

BMP085::BMP085()
{
	m_Addr = 0x00;
	m_i2c = 0UL;
	B5 = 0UL;
}

bool BMP085::Init(I2C_TypeDef *i2c, uint8_t addr)
{
	m_i2c = i2c;
	m_Addr = addr;

	bool result = ReadRegister(BMP085_COEF_START_REG, m_CalibCoeffs.m_Raw, sizeof(m_CalibCoeffs.m_Raw));

	//	swap bytes in words
	for(uint8_t i = 0;i<BMP085_COEF_NUMBER;i++)
	{
		uint8_t tmp = (uint8_t)m_CalibCoeffs.m_Raw[i];
		m_CalibCoeffs.m_Raw[i]>>=8;
		m_CalibCoeffs.m_Raw[i]|=(uint16_t)tmp<<8;
	}
	return result;
}


bool BMP085::WriteRegister(uint8_t uRegAddr, uint8_t uValue)
{
	uint8_t data[2] = { BMP085_CONTROL_REGISTER, uValue};
	/* Transfer structure */
	I2C_TransferSeq_TypeDef i2cTransfer;

	/* Initializing I2C transfer */
	i2cTransfer.addr = m_Addr;
	/* Master write */
	i2cTransfer.flags = I2C_FLAG_WRITE;

	/* Transmit buffer, 2 bytes to send */
	i2cTransfer.buf[0].data = data;
	i2cTransfer.buf[0].len = 2;

	i2cTransfer.buf[1].data = 0UL;
	i2cTransfer.buf[1].len = 0;

	if(I2CSPM_Transfer(m_i2c, &i2cTransfer) == i2cTransferDone)	return true;
	return false;
}

void BMP085::WriteCmd(uint8_t command)
{
	uint8_t data[2] = { BMP085_CONTROL_REGISTER, command};
	/* Transfer structure */
	I2C_TransferSeq_TypeDef i2cTransfer;

	/* Initializing I2C transfer */
	i2cTransfer.addr = m_Addr;
	/* Master write */
	i2cTransfer.flags = I2C_FLAG_WRITE;

	/* Transmit buffer, 2 bytes to send */
	i2cTransfer.buf[0].data = data;
	i2cTransfer.buf[0].len = 2;

	i2cTransfer.buf[1].data = 0UL;
	i2cTransfer.buf[1].len = 0;

	I2CSPM_Transfer(m_i2c, &i2cTransfer);
}

bool BMP085::ReadRegister(uint8_t uRegAddr, void *pBuffer, uint8_t uLenBytes)
{
	/* Transfer structure */
	I2C_TransferSeq_TypeDef i2cTransfer;

	/* Initializing I2C transfer */
	i2cTransfer.addr = m_Addr;
	/* Master write */
	i2cTransfer.flags = I2C_FLAG_WRITE_READ;

	/* Transmit buffer, no data to send */
	i2cTransfer.buf[0].data = &uRegAddr;
	i2cTransfer.buf[0].len = 1;

	/* Receive buffer, two bytes to receive */
	i2cTransfer.buf[1].data = (uint8_t  *)pBuffer;
	i2cTransfer.buf[1].len = uLenBytes;

	if(I2CSPM_Transfer(m_i2c, &i2cTransfer) == i2cTransferDone)	return true;
	return false;
}

void BMP085::StartMeasureT(void)
{
	WriteRegister(BMP085_CONTROL_REGISTER,BMP085_READTEMPCMD);
}

void BMP085::StartMeasureP(void)
{
	WriteRegister(BMP085_CONTROL_REGISTER,BMP085_READPRESSURECMD);
}

int16_t BMP085::GetT(void)
{
    int32_t val = 0;
	uint8_t data[2];

	if(ReadRegister(BMP085_TEMPDATA,data,2))
	{
		val = ((int16_t)data[0]<<8) | ((int16_t)data[1]);

		// correction
		int32_t x1 = ((val - (int32_t)m_CalibCoeffs.AC6) * (int32_t)m_CalibCoeffs.AC5) >> 15;
		int32_t x2 = ((int32_t)m_CalibCoeffs.MC << 11) / (x1 + (int32_t)m_CalibCoeffs.MD);

		B5 = x1+x2;
		val = ((B5 + 8) >> 4) * 10;
	}
	return val;
}

int32_t BMP085::GetP(void)
{
	int32_t B3, B6, X1, X2, X3, p;
	uint32_t B4, B7;

	int32_t val = 0;
	uint8_t data[3];

	if(ReadRegister(BMP085_PRESSUREDATA,data,3))
	{
		val = ((int32_t)data[0]<<16) | ((int32_t)data[1]<<8) | (int32_t)data[2];
		val>>=8;

		// do pressure calcs
		B6 = B5 - 4000;
		X1 = ((int32_t)m_CalibCoeffs.B2 * ( (B6 * B6)>>12 )) >> 11;
		X2 = ((int32_t)m_CalibCoeffs.AC2 * B6) >> 11;
		X3 = X1 + X2;
		B3 = ((((int32_t)m_CalibCoeffs.AC1*4 + X3) << 0) + 2) / 4;

		 X1 = ((int32_t)m_CalibCoeffs.AC3 * B6) >> 13;
		 X2 = ((int32_t)m_CalibCoeffs.B1 * ((B6 * B6) >> 12)) >> 16;
		 X3 = ((X1 + X2) + 2) >> 2;
		 B4 = ((uint32_t)m_CalibCoeffs.AC4 * (uint32_t)(X3 + 32768)) >> 15;
		 B7 = ((uint32_t)val- B3) * (uint32_t)( 50000UL >> 0 );

		 if (B7 < 0x80000000)
		 {
		    p = (B7 * 2) / B4;
		 }
		 else
		 {
		    p = (B7 / B4) * 2;
		 }
		  X1 = (p >> 8) * (p >> 8);
		  X1 = (X1 * 3038) >> 16;
		  X2 = (-7357 * p) >> 16;

		  p = p + ((X1 + X2 + (int32_t)3791)>>4);
		  return p;

		/*
		int32_t B6 = B5 - 4000;

		int32_t x1 = B6 * B6;
		x1 >>= 12;
		x1 *= m_CalibCoeffs.B2;
		x1 >>= 11;

		    int32_t x2 = B6 * m_CalibCoeffs.AC2;
		    x2 >>= 11;

		    int32_t x3 = x1 + x2;

		    int32_t B3 = (m_CalibCoeffs.AC1 * 4) + x3;
		    B3 <<= 0;
		    B3 += 2;
		    B3 /= 4;

		    x1 = m_CalibCoeffs.AC3 * B6;
		    x1 >>= 13;

		    x2 = B6 * B6;
		    x2 >>= 12;
		    x2 *= m_CalibCoeffs.B1;
		    x2 >>= 16;

		    x3 = x1 + x2;
		    x3 += 2;
		    x3 >>= 2;

		    uint32_t b4 = (uint32_t) (x3 + 32768) * m_CalibCoeffs.AC4;
		    b4 >>= 15;

		    uint32_t B7 = (uint32_t) (val - B3) * (50000 >> 0);

		    if (B7 < 0x80000000)
		    {
		        val = B7 * 2 / b4;
		    }
		    else
		    {
		        val = B7 * b4 / 2;
		    }

		    x1 = (p >> 8);
		    x1 *= x1 * 3038;
		    x1 >>= 16;

		    x2 = -7357 * val;
		    x2 >>= 16;

		    return ((val + (x1 + x2 + 3791) / 16) * 3) / 4;
		    */
	}
	return val;
}
#endif /* BMP085_CPP_ */
