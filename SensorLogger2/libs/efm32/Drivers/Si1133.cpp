/*
 * APDS9250.cpp
 *
 *  Created on: 03.09.2016
 *      Author: Yuriy Gerasimov
 */

#include "SI1133.h"
#include "i2cspm.h"

#include "delay.h"
#include "defines.h"

bool Si1133::init(I2C_TypeDef *i2c, uint8_t addr)
{
	m_i2cReg.Init(i2c, addr);

	if(m_i2cReg.ReadU8(SI1133_REGISTER_PART_ID)!=SI1133_PART_ID)
		return false;

	reset();

	writeParam(SI1133_PARAM_MEASRATEH,0);
	writeParam(SI1133_PARAM_MEASRATEL,1);
	writeParam(SI1133_PARAM_MEASCOUNT0,5);
	writeParam(SI1133_PARAM_MEASCOUNT1,10);

	//seleccionamos los canales 0(16bits) y 1(24bits)
	//por lo que los resultados estaran en
	//HOTSOUT[0-1]------> canal 0 , 2 registros por la resolucion de 16bits
	//HOTSOUT[2-4]------> canal 1
	writeParam(SI1133_PARAM_CHLIST,(uint8_t)0x03);

	//=======================================================
	//configuraciones para el canal 0
	//seleccionamos el rate y el photodiodo
	writeParam(SI1133_PARAM_ADCCONFIG0, RATE_NORMAL| F_UV );
	writeParam(SI1133_PARAM_ADCSENS0, 0);
	//resolucion de los datos
	writeParam(SI1133_PARAM_ADCPSOT0, BITS_16);
	writeParam(SI1133_PARAM_MEASCONFIG0, COUNT0);
	//=======================================================
	writeParam(SI1133_PARAM_ADCCONFIG1, RATE_NORMAL| F_LARGE_IR );
	writeParam(SI1133_PARAM_ADCSENS1, 0);
	writeParam(SI1133_PARAM_ADCPSOT1, BITS_24);
	writeParam(SI1133_PARAM_MEASCONFIG1, COUNT1);

	m_i2cReg.WriteU8(SI1133_REGISTER_COMMAND, SI1133_CMD_START);
	//canal0 = uv
	//canal1 = full ir
	return true;
}

uint8_t Si1133::readHwID(void)
{
	return m_i2cReg.ReadU8(SI1133_REGISTER_HW_ID);
}

uint8_t Si1133::readRevID(void)
{
	return m_i2cReg.ReadU8(SI1133_REGISTER_REV_ID);
}

void Si1133::reset(void)
{
	m_i2cReg.WriteU8(SI1133_REGISTER_COMMAND, SI1133_CMD_RESET_SW);
	delay_us(10000);
}

uint32_t Si1133::readUV(void)
{
	uint32_t temp;
	temp = m_i2cReg.ReadU8(SI1133_REGISTER_HOSTOUT0);
	temp<<=8;
	temp|=m_i2cReg.ReadU8(SI1133_REGISTER_HOSTOUT1);
 	return temp;
}

uint32_t Si1133::readIR(void)
{
	uint32_t temp;
	temp = m_i2cReg.ReadU8(SI1133_REGISTER_HOSTOUT2);
	temp<<=8;
	temp|=m_i2cReg.ReadU8(SI1133_REGISTER_HOSTOUT3);
	temp<<=8;
	temp|=m_i2cReg.ReadU8(SI1133_REGISTER_HOSTOUT4);
 	return temp;
}

uint8_t Si1133::writeParam(uint8_t p, uint8_t v)
{
	m_i2cReg.WriteU8(SI1133_REGISTER_HOSTIN0, v);
	m_i2cReg.WriteU8(SI1133_REGISTER_COMMAND, p | SI1133_CMD_PARAM_SET);
	return m_i2cReg.ReadU8(SI1133_REGISTER_RESPONSE1);
}

uint8_t Si1133::readParam(uint8_t p)
{
	m_i2cReg.WriteU8(SI1133_REGISTER_COMMAND, p | SI1133_CMD_PARAM_QUERY);
	return m_i2cReg.ReadU8(SI1133_REGISTER_RESPONSE1);
}

/*
void SI1133::enable(bool bIsEnabled, uint8_t uMode)
{
	APDS9250_MAIN_CTRL_Register reg;

	reg.mByte = m_i2cReg.ReadU8(APDS9250_REGISTER_MAIN_CTRL);

	if(bIsEnabled)	reg.mBits.bitLS_EN = 1;
	else reg.mBits.bitLS_EN = 0;

	reg.mBits.bitCS_Mode = uMode;

	m_i2cReg.WriteU8(APDS9250_REGISTER_MAIN_CTRL,reg.mByte);
}

void SI1133::setResolution(APDS9250_LS_Resolution_t eRes)
{
	APDS9250_LS_MEAS_RATE_Register reg;
	reg.mByte = m_i2cReg.ReadU8(APDS9250_REGISTER_LS_MEAS_RATE);
	reg.mBits.bitLS_Res = eRes;
	m_i2cReg.WriteU8(APDS9250_REGISTER_LS_MEAS_RATE,reg.mByte);
}

void SI1133::setRate(APDS9250_LS_Rate_t eRate)
{
	APDS9250_LS_MEAS_RATE_Register reg;
	reg.mByte = m_i2cReg.ReadU8(APDS9250_REGISTER_LS_MEAS_RATE);
	reg.mBits.bitLS_Rate = eRate;
	m_i2cReg.WriteU8(APDS9250_REGISTER_LS_MEAS_RATE,reg.mByte);

}

void APDS9250::setGain(APDS9250_LS_Gain_t eGain)
{
	APDS9250_LS_GAIN_Register reg;
	reg.mByte = m_i2cReg.ReadU8(APDS9250_REGISTER_LS_GAIN);
	reg.mBits.bitLS_Gain = eGain;
	m_i2cReg.WriteU8(APDS9250_REGISTER_LS_GAIN,reg.mByte);
}
*/
