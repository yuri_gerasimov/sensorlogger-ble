/*
 * SI1133.h
 *
 *  Created on: 03.09.2016
 *      Author: Yuriy Gerasimov
 */

#ifndef SI1133_H_
#define SI1133_H_

#include <stdint.h>
#include <stdbool.h>

#include "i2cspm.h"
#include "i2c_reg.h"

#define SI1133_ADDRESS_PULL_UP (0x55<<1)
#define SI1133_ADDRESS_PULL_DOWN (0x52<<1)

#define SI1133_PART_ID (0x33)

//============================
#define RATE_SHORT  0x60 	//24.4us
#define RATE_NORMAL 0x00 	//48.8us
#define RATE_LONG   0x20 	//97.6us
#define RATE_VLONG  0x40  	//195us
//============================
#define BITS_24 	0x00
#define BITS_16 	0x40
//============================
#define COUNT0 		0x40
#define COUNT1 		0x80
//===========================

//PHOTODIODOS
//===========================
#define F_SMALL_IR  	0x00
#define F_MEDIUM_IR 	0x01
#define F_LARGE_IR  	0x02
#define F_WHITE     	0x0B
#define F_LARGE_WHITE   0x0C
#define F_UV    		0x18
#define F_UV_DEEP   	0x19
//=============================

/*=========================================================================
 REGISTERS
 -----------------------------------------------------------------------*/
typedef enum
{
	SI1133_REGISTER_PART_ID = 0x00,
	SI1133_REGISTER_HW_ID = 0x01,
	SI1133_REGISTER_REV_ID = 0x02,

	SI1133_REGISTER_HOSTIN3 = 0x07,
	SI1133_REGISTER_HOSTIN2 = 0x08,
	SI1133_REGISTER_HOSTIN1 = 0x09,
	SI1133_REGISTER_HOSTIN0 = 0x0A,

	SI1133_REGISTER_COMMAND = 0x0B,
	SI1133_REGISTER_RESET = 0x0F,
	SI1133_REGISTER_RESPONSE1 = 0x10,
	SI1133_REGISTER_RESPONSE0 = 0x11,
	SI1133_REGISTER_IRQ_STATUS = 0x12,

	SI1133_REGISTER_HOSTOUT0 = 0x13,
	SI1133_REGISTER_HOSTOUT1 = 0x14,
	SI1133_REGISTER_HOSTOUT2 = 0x15,
	SI1133_REGISTER_HOSTOUT3 = 0x16,
	SI1133_REGISTER_HOSTOUT4 = 0x17,
	SI1133_REGISTER_HOSTOUT5 = 0x18,
	SI1133_REGISTER_HOSTOUT6 = 0x19,
	SI1133_REGISTER_HOSTOUT7 = 0x1A,
	SI1133_REGISTER_HOSTOUT8 = 0x1B,
	SI1133_REGISTER_HOSTOUT9 = 0x1C,
	SI1133_REGISTER_HOSTOUT10 = 0x1D,
	SI1133_REGISTER_HOSTOUT11 = 0x1E,
	SI1133_REGISTER_HOSTOUT12 = 0x1F,
	SI1133_REGISTER_HOSTOUT13 = 0x20,
	SI1133_REGISTER_HOSTOUT14 = 0x21,
	SI1133_REGISTER_HOSTOUT15 = 0x22,
	SI1133_REGISTER_HOSTOUT16 = 0x23,
	SI1133_REGISTER_HOSTOUT17 = 0x24,
	SI1133_REGISTER_HOSTOUT18 = 0x25,
	SI1133_REGISTER_HOSTOUT19 = 0x26,
	SI1133_REGISTER_HOSTOUT20 = 0x27,
	SI1133_REGISTER_HOSTOUT21 = 0x28,
	SI1133_REGISTER_HOSTOUT22 = 0x29,
	SI1133_REGISTER_HOSTOUT23 = 0x2A,
	SI1133_REGISTER_HOSTOUT24 = 0x2B,
	SI1133_REGISTER_HOSTOUT25 = 0x2C
} SI1133_Register_t;


/**** COMMANDS ****/
#define SI1133_CMD_RESET_CTR  	0x00
#define SI1133_CMD_RESET_SW 	0x01
#define SI1133_CMD_FORCE  		0x11
#define SI1133_CMD_PAUSE  		0x12
#define SI1133_CMD_START  		0x13
#define SI1133_CMD_PARAM_QUERY  0x40
#define SI1133_CMD_PARAM_SET  	0x80


/**** Parameters ****/
#define SI1133_PARAM_I2CADDR   		0x00
#define SI1133_PARAM_CHLIST   		0x01
#define SI1133_PARAM_ADCCONFIG0 	0x02
#define SI1133_PARAM_ADCSENS0   	0x03
#define SI1133_PARAM_ADCPSOT0   	0x04
#define SI1133_PARAM_MEASCONFIG0  	0x05
#define SI1133_PARAM_ADCCONFIG1   	0x06
#define SI1133_PARAM_ADCSENS1   	0x07
#define SI1133_PARAM_ADCPSOT1   	0x08
#define SI1133_PARAM_MEASCONFIG1  	0x09
#define SI1133_PARAM_ADCCONFIG20  	0x0A
#define SI1133_PARAM_ADCSENS2   	0x0B
#define SI1133_PARAM_ADCPSOT2   	0x0C
#define SI1133_PARAM_MEASCONFIG2  	0x0D
#define SI1133_PARAM_ADCCONFIG3 	0x0E
#define SI1133_PARAM_ADCSENS3   	0x0F
#define SI1133_PARAM_ADCPSOT3   	0x10
#define SI1133_PARAM_MEASCONFIG3  	0x11
#define SI1133_PARAM_ADCCONFIG4   	0x12
#define SI1133_PARAM_ADCSENS4  	 	0x13
#define SI1133_PARAM_ADCPSOT4   	0x14
#define SI1133_PARAM_MEASCONFIG4  	0x15
#define SI1133_PARAM_ADCCONFIG5   	0x16
#define SI1133_PARAM_ADCSENS5   	0x17
#define SI1133_PARAM_ADCPSOT5   	0x18
#define SI1133_PARAM_MEASCONFIG5  	0x19
#define SI1133_PARAM_MEASRATEH  	0x1A
#define SI1133_PARAM_MEASRATEL  	0x1B
#define SI1133_PARAM_MEASCOUNT0   	0x1C
#define SI1133_PARAM_MEASCOUNT1   	0x1D
#define SI1133_PARAM_MEASCOUNT2   	0x1E
#define SI1133_PARAM_THRESHOLD0_H   0x25
#define SI1133_PARAM_THRESHOLD0_L   0x26
#define SI1133_PARAM_THRESHOLD1_H   0x27
#define SI1133_PARAM_THRESHOLD1_L   0x28
#define SI1133_PARAM_THRESHOLD2_H   0x29
#define SI1133_PARAM_THRESHOLD2_L   0x2A
#define SI1133_PARAM_BURST   		0x2B


class Si1133
{
private:
	I2CReg m_i2cReg;

public:
	bool init(I2C_TypeDef *i2c, uint8_t addr);
	void enable(bool bIsEnabled, uint8_t uMode);
	void reset(void);

	uint8_t readHwID(void);
	uint8_t readRevID(void);

	uint32_t readUV(void);
	uint32_t readIR(void);

private:
	uint8_t writeParam(uint8_t p, uint8_t v);
	uint8_t readParam(uint8_t p);

//	void setResolution(APDS9250_LS_Resolution_t eRes);
//	void setRate(APDS9250_LS_Rate_t eRate);
//	void setGain(APDS9250_LS_Gain_t eGain);
/*
	uint32_t readRed(void);
	uint32_t readGreen(void);
	uint32_t readBlue(void);
	uint32_t readIR(void);
	*/
};

#endif /* SI1133_H_ */
