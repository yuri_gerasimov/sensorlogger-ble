#include "stdint.h"
#include "delay.h"

#include "em_cmu.h"

static volatile uint32_t msTicks; /* counts 1ms timeTicks */

#ifdef USE_DELAY_MS

/******************************************************************************
 * @brief SysTick_Handler
 * Interrupt Service Routine for system tick counter
 *****************************************************************************/
void SysTick_Handler(void)
{
	msTicks++;       /* increment counter necessary in Delay()*/
}

/******************************************************************************
 * @brief Delays number of msTick Systicks (typically 1 ms)
 * @param dlyTicks Number of ticks to delay
 *****************************************************************************/
void delay_ms(uint32_t dlyTicks)
{
  uint32_t curTicks;

  curTicks = msTicks;
  while ((msTicks - curTicks) < dlyTicks) ;
}

/******************************************************************************
 * @brief init delay clock source for milli-seconds delay function
 * @param
 *****************************************************************************/
void delay_ms_clock_init(void)
{
 /* Enable SysTick interrupt, used by GUI software timer */
  if(SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000)) while (1);
}
#endif

/******************************************************************************
 * @brief delay for microseconds
 * @param uSec - number of microseconds
 *****************************************************************************/
void delay_us(uint32_t uSec)
{
	volatile uint32_t v = 0;
	//	delay coefficients for 48MHz HF clock
	uSec = (uSec * 5328) / 1000;
	while(uSec--)
		v = v+1;

//	using debug registers for delay is unstable, causes strange TFT behavior
//	volatile uint32_t DWT_START = DWT->CYCCNT;
//	volatile uint32_t DWT_TOTAL = ((SystemCoreClock / 1000000)* uSec);
//	while ( (DWT->CYCCNT - DWT_START) < DWT_TOTAL);
}

/*
void delay_test(uint32_t ulDelayMs)
{
	while(1)
	{
		GPIO_PinOutSet(gpioPortF, 12);
		delay_us(1000);
		GPIO_PinOutClear(gpioPortF, 12);
		delay_us(1000);
	}
}
*/
