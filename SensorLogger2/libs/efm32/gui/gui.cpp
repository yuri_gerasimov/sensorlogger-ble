/*
 * gui.cpp
 *
 *  Created on: 01.02.2016
 *      Author: Yuriy
 */
//#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <limits.h>
//#include <stdlib.h>

#include "trace.h"

#ifdef SUPPORT_ANTIALIASING
#include <math.h>
#endif

#include "em_assert.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "gui.h"
#include "rtc.h"
#include "rgb.h"
#include "hsv.h"

#include "str.h"

#include "fonts/CenturyGothic_Bold_14pts.h"
#include "fonts/CenturyGothic_Bold_16pts.h"
#include "fonts/CenturyGothic_Bold_18pts.h"
#include "fonts/Impact_14pts.h"
#include "fonts/Impact_12pts.h"
#include "fonts/Impact_10pts.h"
#include "fonts/Calibri_10pts.h"
#include "fonts/Calibri_8pts.h"
#include "fonts/Consolas_8pts.h"
#include "fonts/Tahoma_8pts.h"

#include "tft/tft_driver.h"
#include "config/gui_config.h"

GWndManager wndMgr;

static inline int16_t limit(int16_t val, int16_t min, int16_t max)
{
	if(val<min)	return min;
	if(val>max)	return max;
	return val;
}


static int16_t getProportionalValue(int16_t x, int16_t x1, int16_t x2, int16_t y1, int16_t y2)
{
	return (((y2 - y1) * (x - x1)) / (x2 - x1)) + y1;
}

//------------------------------------------------------------------------

GWndBase::GWndBase()
{
	m_pTFT = NULL;

	m_style = WndType_None;
	m_x = 0;
	m_y = 0;
	m_width = 0;
	m_height = 0;
	m_pFont = (FONT_INFO *)&tahoma_8ptFontInfo;
	m_uFontHeight = m_pFont->m_uCharHeight;

	m_FrontColor = config_WND_HEADER_COLOR;
	m_TextColor = config_WND_DEFAULT_TEXT_COLOR;
	m_BgColor = config_WND_BG_COLOR;
	m_bInvalidateFlag = true;
}

bool GWndBase::create(int16_t x, int16_t y, int16_t w, int16_t h)
{
	m_x = x;
	m_y = y;
	m_width = w;
	m_height = h;

	m_pTFT = wndMgr.getTFT();
	EFM_ASSERT(m_pTFT);

	return true;
}

void GWndBase::setFont(const FONT_INFO *pFont)
{
	m_pFont = (FONT_INFO *)pFont;
	m_pTFT->setFont(m_pFont);
	m_uFontHeight = m_pTFT->getFontHeight();
}

void GWndBase::fillGradientRectH(int16_t x, int16_t y, int16_t w, int16_t h, Color_TypeDef c1, Color_TypeDef c2)
{
	RGB_t color;
	int16_t x1 = x;
	int16_t x2 = x + w;
	while(x <= x2)
	{
		setCurrentDrawColor(RGB(color.r, color.g, color.b));
		drawVLine(x, y, h);
		color.r = (uint8_t)getProportionalValue(x, x1, x2, RED(c1), RED(c2));
		color.g = (uint8_t)getProportionalValue(x, x1, x2, GREEN(c1), GREEN(c2));
		color.b = (uint8_t)getProportionalValue(x, x1, x2, BLUE(c1), BLUE(c2));
		x++;
	}
}

void GWndBase::fillGradientRectV(int16_t x, int16_t y, int16_t w, int16_t h, Color_TypeDef c1, Color_TypeDef c2)
{
	RGB_t color;
	setCurrentDrawColor(RGB(color.r, color.g, color.b));
	while (y <= h)
	{
		drawHLine(x, y++, w);
	//	color.r = getProportionalValue(RED(c1), RED(c2), w);
	//	color.g = getProportionalValue(GREEN(c1), GREEN(c2), w);
	//	color.b = getProportionalValue(BLUE(c1), BLUE(c2), w);
	}
}


//---------------------------------------------------------
/*
GWnd::GWnd()
{

}
*/
GWnd::~GWnd()
{
//	wndMgr.UnregisterWnd(this);
}

bool GWnd::create(int16_t x, int16_t y, int16_t w, int16_t h)
{
	wndMgr.RegisterWnd(this);
	return GWndBase::create(x,y,w,h);
}

void GWnd::DoModal(void)
{
	MSG_QUEUE_ITEM m;
	QueueHandle_t hMsgQueue = wndMgr.GetMsgQueue();

	m_uResult = WND_RESULT_NONE;
	do
	{
		wndMgr.RenderAll();
		if(hMsgQueue)
		{
			if(xQueueReceive(hMsgQueue, &m, portMAX_DELAY))
			{
				OnMsgReceive(m.msg, m.pMsgData);
			}
		}
	}
	while(m_uResult == WND_RESULT_NONE);
	//	clear all messages on window closing
	xQueueReset(hMsgQueue);
	wndMgr.UnregisterWnd(this);
}

void GWnd::OnMsgReceive(MSG msg, void *pMsgData)
{
	switch(msg)
	{
		case MSG_TYPE_RTC:
		case MSG_TYPE_UPDATE:
		{
			Invalidate();
			break;
		}
		default:
			break;
	}
}

//---------------------------------------------------------

GProgressCtrl::GProgressCtrl(GWndType_t style)
{
	m_style = style;
	m_max = 100;
	m_min = 0;
	m_pos = 50;
}

void GProgressCtrl::OnDraw(void)
{
	GWndBase::OnDraw();

	int pos = (m_pos * m_width) / (m_max - m_min);
	if(m_style == WndType_VerticalProgressBar)
	{
		m_pTFT->setColorFill(m_BgColor);
		m_pTFT->fillRoundVLine(m_x, m_y, m_width, m_height);
		m_pTFT->setColorFill(m_FrontColor);
		m_pTFT->fillRoundVLine(m_x, m_y, m_width, pos);
	}
	else
		if(m_style == WndType_HorizontalProgressBar)
		{
			m_pTFT->setColorFill(m_BgColor);
			m_pTFT->fillRoundHLine(m_x, m_y, m_width, m_height);
			m_pTFT->setColorFill(m_FrontColor);
			m_pTFT->fillRoundHLine(m_x, m_y, pos, m_height);
		}
}

//---------------------------------------------------------------------------------

GBatteryCtrl::GBatteryCtrl(void)
{
	m_Status = BatteryStatus_Discharging;
	m_uLevel = 12;
	m_TextColor = config_TEXT_COLOR_HEADER;
}

void GBatteryCtrl::create(int16_t x, int16_t y, int16_t w, int16_t h)
{
	w = 16;
	h = 8;
	GWndBase::create(x,y,w,h);
}

void GBatteryCtrl::OnDraw(void)
{
	GWndBase::OnDraw();

	setFont(&calibri_8ptFontInfo);

	Color_TypeDef c = m_FrontColor;
	if(m_Status == BatteryStatus_Discharging)
	{
		if(m_uLevel<=15)	c = RGB(255,0,0);
		else if(m_uLevel<=30) c = RGB(128,128,0);
		else c = m_FrontColor;
	}
	else
		if(m_Status == BatteryStatus_Charging)
		{
			if(m_uLevel>80)	c = RGB(0,255,0);
			else c = RGB(0,128,0);
		}

	setCurrentDrawColor(c);
	setCurrentFillColor(c);
	setCurrentTextColor(m_TextColor);

	drawRect(0, 0, m_width-3, m_height);
	fillRect(2, 2, m_width-6-(10-m_uLevel/10), m_height-3);
	drawVLine(m_width-1, m_height/2-2, 4);

	char str[8];
	setTextAlignment(StringAlign_Right,StringAlign_Middle);
	drawStringInRect(strcat(utoa((uint32_t)m_uLevel, str),"%"), -31, 2, 30, m_height-2 );
}

void GBleCtrl::OnDraw(void)
{
	GWndBase::OnDraw();

	if(m_bIsConnected)	
		setCurrentDrawColor(RGB(192,192,255));
	else 
		setCurrentDrawColor(RGB(64,64,64));

	drawVLine(3,0,11);
	drawLine(3,0,6,3);
	drawLine(3,10,6,7);
	drawLine(1,2,6,7);
	drawLine(1,7,6,3);
}

void GUsbCtrl::OnDraw(void)
{
	GWndBase::OnDraw();

	if (m_bIsConnected)
		setCurrentDrawColor(RGB(192, 192, 255));
	else
		setCurrentDrawColor(RGB(64, 64, 64));
	/*
	Color_TypeDef c = m_FrontColor;
	if(m_bIsConnected)	c = RGB(192,192,255);
	else c = RGB(64,64,64);
	*/

//	setCurrentDrawColor(c);

	drawVLine(3,-1,12);
	drawLine(3,6,6,3);
	drawVLine(6,0,3);

	drawLine(3,7,0,4);
	drawPixel(0,2);
	drawPixel(0,3);

	drawVLine(2,8,2);
	drawVLine(4,8,2);

	drawPixel(2,1);
	drawPixel(4,1);
}


//------------------------------------------------------------------

GRadioCtrl::GRadioCtrl(void)
{
	m_uItemWidth = 40;
	m_uItemHeight = 14;
	m_uItems = 0;
	m_uCurrentItem = 0;
}

bool GRadioCtrl::SetItemNumber(uint8_t num)
{
	if(num>=WND_RADIO_CTRL_MAX_ITEMS)	return false;
	m_uItems = num;
	return true;
}

void GRadioCtrl::RenderItem(uint8_t index)
{
	uint16_t x;
	uint16_t r = m_uItemHeight/2;
	uint16_t R = m_height/2;

	if(index == 0)
	{
		x = R;
	}
	else if(index == m_uItems-1)
	{
		x = m_width - m_uItemWidth - R;
	}
	else
	{
		x = R + ((m_width - m_uItemWidth - 2*R) * index )/ (m_uItems-1);
	}

	if(index == m_uCurrentItem)
	{
		setCurrentFillColor(m_BgColor);

		fillCircle(x, R, r);
		fillCircle(x + m_uItemWidth, R, r);
		fillRect(x, m_height/2 - m_uItemHeight/2, m_uItemWidth, m_uItemHeight+1);
		setCurrentTextColor(RGB(160,160,192));
	}
	else
		setCurrentTextColor(RGB(96,96,96));

	drawStringInRect(m_pStrItems[index], x, m_height/2 - m_uItemHeight/2, m_uItemWidth-1, m_uItemHeight);
}

void GRadioCtrl::OnDraw(void)
{
	GWndBase::OnDraw();

	uint16_t h = m_height/2;

	setFont(&calibri_10ptFontInfo);
	setTextAlignment(StringAlign_Center, StringAlign_Middle);
	setCurrentTextColor(m_TextColor);

	setCurrentFillColor(RGB(8, 40, 56));
	fillCircle(h, h, h);
	fillCircle(m_width-h, h, h);
	fillRect(h,0,m_width-m_height,m_height+1);

	for(uint8_t i = 0; i<m_uItems; i++)
		RenderItem(i);
}

//------ GProgressBar class members ---------

GProgressBar::GProgressBar()
{
	m_Orientation = GProgressBarVertical;
	m_iValue = 0;
	m_iMinValue = 0;
	m_iMaxValue = 100;
	m_uTicksNumber = 0;
}

void GProgressBar::OnDraw(void)
{
	GWndBase::OnDraw();
	char str[8];
	
	if (m_Orientation == GProgressBarVertical)
	{
		int16_t y = 0;
		if (m_uTicksNumber)
		{
			int16_t tick_step = m_height / m_uTicksNumber;
			setCurrentFillColor(RGB(64, 64, 64));
			for (int16_t tick_index = 0; tick_index <= m_uTicksNumber; tick_index++)
			{
				if (tick_index == 0 || tick_index == m_uTicksNumber)
				{
					fillRect(-6, y-1, 4, 2);
				}
				else
					fillHLine(-6, y, 4);
				y += tick_step;
			}
		}

		if (m_iValue >= m_iMinValue && m_iValue <= m_iMaxValue)
		{
			int pos = (m_height *  m_iValue) / (m_iMaxValue - m_iMinValue);
			y = m_height - pos;

			setCurrentFillColor(m_BgColor);

			fillRect(0, 0, m_width, y);
			fillGradientRectH(0, y, m_width/3, pos, RGB((RED(m_FrontColor) >> 2 ), (GREEN(m_FrontColor) >> 2), (BLUE(m_FrontColor) >> 2)), m_FrontColor);
			fillGradientRectH(m_width/3, y, m_width - m_width/3, pos, m_FrontColor, RGB(0, 0, 0));
			//FillRect(m_width / 2, y, m_width / 2, pos, m_FrontColor);

			//	put current value mark
#ifdef SUPPORT_ANTIALIASING
			setCurrentDrawColor(m_TextColor);
			drawLineAA(-7, y - 2, -2, y);
			drawLineAA(-7, y + 2, -2, y);
			drawLineAA(-7, y - 2, -7, y + 2);
#else
			drawLine(-7, y - 2, -2, y);
			drawLine(-7, y + 2, -2, y);
			drawLine(-7, y - 2, -7, y + 2);
#endif
		}
	}
	else if (m_Orientation == GProgressBarHorizontal)
	{
		int pos = (m_width *  m_iValue) / (m_iMaxValue - m_iMinValue);

		setCurrentFillColor(m_FrontColor);
		fillRect(0, 0, pos, m_height);
		setCurrentFillColor(m_BgColor);
		fillRect(pos, 0, m_width - pos, m_height);
	}

	//	draw border
	setCurrentDrawColor(RGB(64, 64, 64));
	drawRect(1, 1, m_width-2, m_height-2);
	setCurrentDrawColor(RGB(128, 128, 192));
	drawRect(0, 0, m_width, m_height);
}


//------ GProgressBar class members --------------------------------------------

GArcProgressCtrl::GArcProgressCtrl(void)
{
	m_iValue = 0;
	m_iMinValue = 0;
	m_iMaxValue = 100;
	m_uTicksNumber = 0;
	m_iStartAngle = 0;
	m_iEndAngle = 360;
	m_uThickness = 10;
	m_bIsFillCircleBorders = false;
}

void GArcProgressCtrl::OnDraw(void)
{
	GWndBase::OnDraw();
	setCurrentDrawColor(m_FrontColor);
	setCurrentFillColor(m_FrontColor);

	uint16_t r0 = m_width >> 1;
	int16_t currentAngle = (m_iValue * (m_iEndAngle - m_iStartAngle)) / (m_iMaxValue - m_iMinValue);

	fillArc(r0, r0, r0, m_uThickness, m_iStartAngle, m_iStartAngle + currentAngle);

	if (m_bIsFillCircleBorders)
	{
		drawCircleAA(r0, r0, r0);
		drawCircleAA(r0, r0, r0 - m_uThickness);
	}
	else
	{
		drawArcAA(r0, r0, r0, m_iStartAngle, m_iStartAngle + currentAngle);
		drawArcAA(r0, r0, r0 - m_uThickness, m_iStartAngle, m_iStartAngle + currentAngle);
	}
}

//------ GProgressBar class members ---------

GGaugeCtrl::GGaugeCtrl()
{
	m_iValue = 0;
	m_iValueDivider = 1;
	m_iMinValue = 0;
	m_iMaxValue = 100;

	m_iScaleStart = 0;
	m_iScaleStep = 1;
	
	m_uTicksNumber = 50;
	m_uBigTickMult = 5;

	m_TextColor = RGB(128, 128, 128);
	m_BgColor = RGB(64, 64, 64);
	m_FrontColor = RGB(128, 128, 128);
	*m_strText = 0;
	*m_strExtText = 0;

	m_iStartAngle = -45;
	m_iEndAngle = 210;
}

void GGaugeCtrl::OnDraw(void)
{
	GWndBase::OnDraw();
	char str[16];

	uint16_t r = m_width >> 1;
	
	//	drop the shadow
	setCurrentDrawColor(RGB(16, 16, 16));
	drawCircleAA(r+2, r+2, r);

	setCurrentFillColor(m_BgColor);
	setCurrentDrawColor(m_FrontColor);
	setCurrentTextColor(m_TextColor);

	// draw outer circles

	fillCircle(r, r, r - 4);
	drawCircleAA(r, r, r - 4);
	drawCircleAA(r, r, r);
	drawHLine(r >> 1, r + 20, r);

	//	setCurrentDrawColor(RGB(250, 0, 0));
	//	drawArcAA(r, r, r - 1, 90, 210);

	setTextAlignment(StringAlign_Center, StringAlign_Middle);

	if (m_iValueDivider > 1)
	{
		char str2[16];
		strCopy(str, itoa(m_iValue / m_iValueDivider, str2));
		strCat(str, ".");
		strCat(str, itoa(m_iValue%m_iValueDivider, str2));
		drawStringInRect(str, r - 9, r + 8, 20, 10);
	}
	else
		drawStringInRect(itoa(m_iValue, str), r - 9, r + 8, 20, 10);

	if (*m_strText)
		drawStringInRect(m_strText, r - 7, r - 18, 16, 10);

	setFont(&calibri_8ptFontInfo);
	if(*m_strExtText)
		drawStringInRect(m_strExtText, r - 6, r + 22, 16, 10);

	double start_angle = (double)m_iStartAngle * M_PI / 180;
	double last_angle = (double)m_iEndAngle * M_PI / 180;

	//	draw tick marks
	int16_t n = m_iScaleStart;
	int16_t i = 0;
	double step = (last_angle - start_angle) / m_uTicksNumber;
	for (double alfa = start_angle; alfa <= last_angle+0.01; alfa += step, i++)
	{
		double s = sin(alfa);
		double c = cos(alfa);

		if (!(i % m_uBigTickMult))
		{
			int16_t x1 = (int16_t)round((double)(r-6) * c);
			int16_t y1 = (int16_t)round((double)(r-6) * s);
			int16_t x2 = (int16_t)round((double)(r)* c);
			int16_t y2 = (int16_t)round((double)(r)* s);

			int16_t tx = (int16_t)round((double)(r - 12) * c);
			int16_t ty = (int16_t)round((double)(r - 12) * s);

			drawLineAA(r-x1, r-y1, r-x2, r-y2);
			drawString(itoa(n, str), r - tx - 2 - 4 * (n / 10), r - ty - 4);
			n += m_iScaleStep;
		}
		else
		{
			int16_t x = (int16_t)round((double)(r - 5) * c);
			int16_t y = (int16_t)round((double)(r - 5) * s);
			drawPixel(r - x, r - y);
		}
	}

	//	draw arrow marks
	double current_angle;
	if (m_iValue < m_iMinValue) current_angle = start_angle;
	else if (m_iValue > m_iMaxValue) current_angle = last_angle;
	else current_angle = start_angle + ((last_angle - start_angle) *  (m_iValue - m_iMinValue)) / (double)(m_iMaxValue - m_iMinValue);

	double s = sin(current_angle);
	double c = cos(current_angle);

	int16_t x = (int16_t)round((double)(r - 10) * c);
	int16_t y = (int16_t)round((double)(r - 10) * s);

	//	draw arrow 

	double a1 = current_angle + M_PI / 2;
	double a2 = current_angle - M_PI / 2;

	int16_t x1 = (int16_t)round((double)(1) * cos(a1));
	int16_t y1 = (int16_t)round((double)(1) * sin(a1));

	int16_t x2 = (int16_t)round((double)(1) * cos(a2));
	int16_t y2 = (int16_t)round((double)(1) * sin(a2));

	setCurrentDrawColor(RGB(200, 0, 0));

	drawCircle(r, r, 1);
	drawCircle(r, r, 2);
	drawCircleAA(r, r, 3);
	drawLineAA(r - x, r - y, r, r);
	
	drawLineAA(r - x, r - y, r - x1, r - y1);
	drawLineAA(r - x, r - y, r - x2, r - y2);
}


//------ GButton class members ---------

GButton::GButton()
{
	m_TextColor = RGB(200, 200, 200);
	m_FrontColor = RGB(0, 120, 120);
}

void GButton::OnDraw(void)
{
	setCurrentFillColor(m_FrontColor);
	fillRect(0, 0, m_width, m_height);
	drawStringInRect(m_strText, 0, 0, m_width, m_height);
}


//------ GGraphicWnd class members ---------

GGraphicWnd::GGraphicWnd(void)
{
	m_uXstep = 3;
	m_FrontColor = RGB(32, 32, 64);

	memset(m_DataSet, 0, sizeof(m_DataSet));
}

void GGraphicWnd::create(int16_t x, int16_t y, int16_t w, int16_t h)
{
	GWndBase::create(x, y, w, h);
	m_yAxe = h >> 1;		//	Y axe is at the middle of control
}

void GGraphicWnd::SetApperance(uint16_t nDatasetIndex, GraphicType_t type, Color_TypeDef color)
{
	m_DataSet[nDatasetIndex].m_GraphColor = color;
	m_DataSet[nDatasetIndex].m_GraphType = type;
}

void GGraphicWnd::SetData(float *pData, uint16_t uDataSize, uint16_t nDatasetIndex, float fYScaleMinimum)
{
	if (nDatasetIndex >= 4)	return;

	uint16_t uVisibleMaxNum = (m_width - 5) / m_uXstep;

	m_DataSet[nDatasetIndex].m_pData = pData;
	m_DataSet[nDatasetIndex].m_uDataSize = uDataSize;

	if (uDataSize)
	{
		if (uDataSize > uVisibleMaxNum)
			m_DataSet[nDatasetIndex].m_uStartIndex = uDataSize - uVisibleMaxNum;
		else
			m_DataSet[nDatasetIndex].m_uStartIndex = 0;

		m_DataSet[nDatasetIndex].m_uLastIndex = uDataSize - 1;

		float min = pData[m_DataSet[nDatasetIndex].m_uStartIndex];
		float max = min;

		for (uint16_t i = m_DataSet[nDatasetIndex].m_uStartIndex; i <= m_DataSet[nDatasetIndex].m_uLastIndex; i++)
		{
			if (min > pData[i])	min = pData[i];
			if (max < pData[i])	max = pData[i];
		}

		m_DataSet[nDatasetIndex].m_yMidiumValue = (max + min) / 2;
		m_DataSet[nDatasetIndex].m_yScale = (max - min) / (m_height * 8 / 10);
		if (m_DataSet[nDatasetIndex].m_yScale < fYScaleMinimum)	m_DataSet[nDatasetIndex].m_yScale = fYScaleMinimum;
	}
}


void GGraphicWnd::DrawGraphic(uint16_t nDatasetIndex)
{
	if (m_DataSet[nDatasetIndex].m_uDataSize)
	{
		int16_t x = m_uXstep;

		for (uint16_t i = m_DataSet[nDatasetIndex].m_uStartIndex; i <= m_DataSet[nDatasetIndex].m_uLastIndex; i++)
		{
			setCurrentDrawColor(m_DataSet[nDatasetIndex].m_GraphColor);
			int16_t y = m_yAxe - (m_DataSet[nDatasetIndex].m_pData[i] - m_DataSet[nDatasetIndex].m_yMidiumValue) / m_DataSet[nDatasetIndex].m_yScale;

			switch (m_DataSet[nDatasetIndex].m_GraphType)
			{
				case GraphicType_Dots:
				{
					drawPixel(x, y);
					break;
				}
				case GraphicType_Line:
				{
					static int16_t x0 = 0;
					static int16_t y0 = 0;
					if(i > m_DataSet[nDatasetIndex].m_uStartIndex)
						drawLine(x0, y0, x, y);
					x0 = x;
					y0 = y;
					break;
				}
				case GraphicType_Bars:
				{
					if(y < m_yAxe)
						drawVLine(x, y, m_yAxe - y);
					else
						drawVLine(x, m_yAxe, y - m_yAxe);

				//	DrawPixel(x, y, RGB(255,0,0));
					break;
				}
				default:
					break;
			}
			
			x += m_uXstep;
		}
	}
}

void GGraphicWnd::OnDraw(void)
{
	setCurrentDrawColor(config_WND_GRID_COLOR);
	drawVLine(0, 0, m_height);
	drawHLine(m_uXstep, m_yAxe, m_width - m_uXstep);

	for (uint16_t n = 0; n < 4; n++)
		if (m_DataSet[n].m_pData != NULL)	DrawGraphic(n);
	
	/*
	if (!m_uDataSize)	return;
	
	const int16_t y_range = m_height - m_height / 4;
	int16_t my = m_height / 2;
	int16_t range = 5;

	uint16_t start_index = 0;
	uint16_t end_index = 0;

	if (m_uDataSize > GetVisibleSamples())
	{
		start_index = m_uDataSize - GetVisibleSamples();
	}
	else
	{
		start_index = 0;
	}

	end_index = m_uDataSize - 1;

	int16_t min = m_pData[start_index];
	int16_t max = m_pData[start_index];

	if ((end_index - start_index) > 2)
	{
		for (uint16_t i = start_index; i <= end_index; i++)
		{
			if (m_pData[i] > max)	max = m_pData[i];
			if (m_pData[i] < min)	min = m_pData[i];
		}
		range = max - min;
	}

	int16_t x = m_uXstep;
	int16_t y = 0;
	int16_t middle = (max + min) / 2;
	*/

	/*
	HSV_t hsv;
	RGB_t rgb;

	hsv.s = 255;
	hsv.v = 192;
	
	for (uint16_t i = start_index; i <= end_index; i++)
	{
		int16_t h, y0;

//		char str[16];
//		TRACE("\n");
//		TRACE(itoa(m_pData[i], str));
		
		y = ((m_pData[i] - middle) * y_range) / range;

		if (y > 0)
		{
			h = y; 
			y0 = my - y;
			hsv.h = 85 - h;
		}
		else
		{
			h = y * -1;
			y0 = my;
			hsv.h = 85 + h;
		}

		HSV_ToRGB(&hsv, &rgb);
		if(h)
			DrawVLine(x, y0, h, RGB(rgb.r, rgb.g, rgb.b));
		else
			DrawPixel(x, my, RGB(rgb.r, rgb.g, rgb.b));

//		DrawPixel(x, my - y, RGB(rgb.r, rgb.g, rgb.b));
		x += m_uXstep;
	}
	*/
}

//------------------------------------------------------------------

GGraphCtrl::GGraphCtrl(void)
{
//	m_style = WndType_GraphicsDots;
	m_style = WndType_GraphicsBars;

	m_uDataSize = 0;
	m_pData = NULL;

	m_uGridSizeY = 20;
	m_uGridOffsetX = 40;

	m_uDataOffsetIndex = 0;
	m_iDataMiddle = 0;
	m_iYAxePosition = 0;

	m_StepX = 10;
	m_uGridXMult = 2;
	m_uPointRadius = 2;
	m_bIsColoredVertical = false;

	m_uVerticalScale = 1;
	m_uVerticalScaleDivider = 1;

	m_uMiddleY = 150/2;

	m_uYAxeWidth = 0;

	m_TextColor = RGB565(32, 32, 32);
	m_AxeColor = config_WND_GRID_COLOR;
	m_GridColor = config_WND_GRID_COLOR;
}

void GGraphCtrl::SetAppierance(bool bIsColoredVertical, uint8_t uPointRadius)
{
	m_bIsColoredVertical = bIsColoredVertical;
	m_uPointRadius = uPointRadius;
}

uint16_t GGraphCtrl::GetVisibleSamplesNumber(void)
{
	uint16_t num = (m_width - m_uGridOffsetX - m_StepX) / m_StepX;
	if(m_uDataSize < num) num = m_uDataSize;
	return num;
}

void GGraphCtrl::SetData(int16_t *pData, uint16_t uDataSize)
{
	m_pData = pData;
	m_uDataSize = uDataSize;
}

bool GGraphCtrl::SetHorizontalOffset(uint16_t uOffset)
{
	if(uOffset >= m_uDataSize) return false;

	uint16_t num = GetVisibleSamplesNumber();

	if(uOffset > m_uDataSize - num)
		m_uDataOffsetIndex = m_uDataSize - num;
	else
		m_uDataOffsetIndex = uOffset;

	return true;
}

void GGraphCtrl::SetHorizontalAxe(int16_t value, Color_TypeDef color, uint8_t w)
{
	m_iYAxePosition = value;
	m_AxeColor = color;
	m_uYAxeWidth = w;
}

int16_t GGraphCtrl::GetYByValue(int32_t val)
{
	return m_uMiddleY - (int16_t)((val - m_iDataMiddle) * m_uVerticalScaleDivider / m_uVerticalScale);
}

void GGraphCtrl::DrawGridHorizontalLines(void)
{
	char str[16];

	if(m_uGridSizeY<1)	return;

	uint8_t i = 1;
	uint16_t y_offset = m_uGridSizeY;

	setFont(&calibri_8ptFontInfo);
	setTextAlignment(StringAlign_Right,StringAlign_Middle);
	setCurrentTextColor(RGB565(79,99,112));
	setCurrentDrawColor(m_GridColor);

	drawStringInRect(itoa(m_iDataMiddle, str), 0, m_uMiddleY - 4, m_uGridOffsetX, 8);
	drawHLine(m_uGridOffsetX, m_uMiddleY, m_width - m_uGridOffsetX);

	uint16_t uGridCellVal = m_uGridSizeY * m_uVerticalScale / m_uVerticalScaleDivider;

	while(m_uMiddleY >= y_offset)
	{
		drawStringInRect(itoa(m_iDataMiddle + uGridCellVal * i, str), 0, m_uMiddleY - y_offset - 4, m_uGridOffsetX, 8);
		drawStringInRect(itoa(m_iDataMiddle - uGridCellVal * i, str), 0, m_uMiddleY + y_offset - 4, m_uGridOffsetX, 8);

		drawHLine(m_uGridOffsetX, m_uMiddleY - y_offset, m_width - m_uGridOffsetX);
		drawHLine(m_uGridOffsetX, m_uMiddleY + y_offset, m_width - m_uGridOffsetX);
		y_offset+=m_uGridSizeY;
		i++;
	}

	setCurrentDrawColor(m_AxeColor);
	if(m_uYAxeWidth)
	{
		int y = GetYByValue(m_iYAxePosition) - m_uYAxeWidth/2;
		for(int i = 0;i<m_uYAxeWidth;i++)
			drawHLine(m_uGridOffsetX, y+i, m_width - m_uGridOffsetX);
	}
}
void GGraphCtrl::DrawGridVerticalLines(void)
{
	//	--- draw the X GRID ----
	if(m_uGridXMult>0)
	{
		uint16_t x_grid = m_uGridOffsetX + m_StepX;

		setCurrentDrawColor(m_GridColor);
		while(x_grid < m_width)
		{
			drawVLine(x_grid, 0, m_height);
			x_grid += m_StepX * m_uGridXMult;
		}
	}
}

void GGraphCtrl::SetVerticalScale(int16_t scale, int16_t divider)
{
	m_uVerticalScale = scale;
	m_uVerticalScaleDivider = divider;
}

void GGraphCtrl::SetVerticalOffset(int16_t offset_value)
{
	m_iDataMiddle = offset_value;
}

bool GGraphCtrl::SetAutoScale(uint16_t uStartSample, uint16_t divider)
{
	int16_t iDataMin = m_pData[uStartSample];
	int16_t iDataMax = m_pData[uStartSample];

	uint16_t num = GetVisibleSamplesNumber();
	if(!num)	return false;
	for(uint16_t i = uStartSample; i<num; i++)
	{
		if(iDataMin > m_pData[i])	iDataMin = m_pData[i];
		if(iDataMax < m_pData[i])	iDataMax = m_pData[i];
	}

	uint8_t y_steps = ((m_height / 2 ) / m_uGridSizeY) * 2;
	m_uVerticalScale = (iDataMax - iDataMin) * divider / (y_steps * m_uGridSizeY);
//	if(m_uVerticalScale%10)
		m_uVerticalScale = ((m_uVerticalScale/10+1)*10);
	m_uVerticalScaleDivider = divider;

	m_iDataMiddle = (iDataMax + iDataMin)>>1;	// divided by 2
	m_iDataMiddle = ((m_iDataMiddle / divider) + 1) * divider;

	return true;
}

void GGraphCtrl::OnDraw(void)
{
	GWndBase::OnDraw();

//	RGB_t rgb1;
//	HSV_t hsv1;
//	hsv1.s = hsv.s;
//	hsv1.v = 255;

	RGB_t rgb;
	HSV_t hsv;	hsv.h = 172;	hsv.s = 255;	hsv.v = 192;
	const uint8_t h_start = 172;

	setCurrentFillColor(m_BgColor);
	fillRect(0,0,m_width,m_height);

	DrawGridHorizontalLines();
	DrawGridVerticalLines();

	int16_t y0 = GetYByValue(m_iYAxePosition);
	int16_t x = m_uGridOffsetX + m_StepX;
	int16_t last_x = 0;
	int16_t last_y = 0;

	y0 = limit(y0,0,m_height);

	uint16_t num = GetVisibleSamplesNumber();

	for(uint16_t i = 0; i< num; i++)
	{
		if(x>0 && x<(m_width-m_uPointRadius))
		{
			if(m_bIsColoredVertical)
				hsv.h = h_start + m_pData[m_uDataOffsetIndex + i]/10;
			else
				hsv.h+=3;		//	shift the color

			HSV_ToRGB(&hsv,&rgb);
			setCurrentFillColor(RGB565(rgb.r, rgb.g, rgb.b));

			int16_t y = GetYByValue(m_pData[m_uDataOffsetIndex + i]);
			if(y>0 && y<m_height)
			{
			//	hsv1.h = hsv.h;
			//	HSV_ToRGB(&hsv1,&rgb1);
			//	FillCircle(x, y, m_uPointRadius, RGB565(rgb1.r,rgb1.g,rgb1.b));

				fillCircle(x, y, m_uPointRadius);
			}
			else
				y = limit(y,0,m_height);

			if(m_style == WndType_GraphicsBars)
			{
				if(y0 > y)
				{
					fillRect(x-m_uPointRadius, y, m_uPointRadius<<1, y0 - y);
				}
				else
				{
					fillRect(x-m_uPointRadius, y0, m_uPointRadius<<1, y - y0);
				}
			}

			if(i && m_style == WndType_GraphicsDotsLines)
				drawLine(last_x,last_y,x,y);

			last_x = x;		//	store coordinates for next iterations
			last_y = y;
		}
		x+=m_StepX;		//	shift to next data X position
		if(x > m_width)	break;
	}
}

//------------------------------------------------------------------

//*******************************************************************
/*
void MainWnd::UpdateDateTime(void)
{
	char str[32];
	setFont(&calibri_8ptFontInfo);
	rtcFormatDateTime(str,rtcGetTime());
	DrawString(str,150,4);
}

void MainWnd::create(int16_t x, int16_t y, int16_t w, int16_t h)
{
	GWnd::create(x,y,w,h);
	SetTextColor(config_TEXT_COLOR_HEADER);

	batCtrl.Create(300,4,16,8);
	bleCtrl.Create(262,3,8,11);
	usbCtrl.Create(254,3,8,11);
}

void MainWnd::OnDraw(void)
{
	GWnd::OnDraw();

	FillRect(0,0,m_width,m_height,config_WND_BG_COLOR);

	//	controls are not drawing through the windows manager, so we should paint them manually
	batCtrl.OnDraw();
	bleCtrl.OnDraw();
	usbCtrl.OnDraw();

	DrawHLine(10,18,300,config_WND_GRID_COLOR);

	setFont(&centuryGothic_14ptFontInfo);
	DrawString("Main",20,0);

	UpdateDateTime();
}

void MainWnd::OnMsgReceive(MSG msg, void *pMsgData)
{
	GWnd::OnMsgReceive(msg,pMsgData);

	switch(msg)
	{
		case MSG_TYPE_BATTERY:
		{
			batCtrl.SetLevel(m_uCurrectBatteryLevel);
			Invalidate();
			break;
		}
		case MSG_TYPE_USB:
		{
			usbCtrl.SetConnected(*((bool*)pMsgData));
			break;
		}
		case MSG_TYPE_BLE:
		{
			bleCtrl.SetConnected(*((bool*)pMsgData));
			break;
		}
		case MSG_TYPE_BUTTON:
		{
			DashboardWnd wnd;
			wnd.Create(0,0,m_width,m_height);
			wnd.DoModal();
			Invalidate();
			break;
		}
		case MSG_TYPE_TOUCH:	break;
		default:	break;
	}
}
*/
//*******************************************************************

GWndManager::GWndManager()
{
	memset(m_pWndList,0,sizeof(m_pWndList[0]) * WND_LIST_SIZE);
	m_hMsgQueue = NULL;
}

bool GWndManager::Init(TFT *pTFT, unsigned int uQueueLength)
{
	EFM_ASSERT(pTFT);

	m_pTFT = pTFT;
	m_hMsgQueue = xQueueCreate(uQueueLength, sizeof(MSG_QUEUE_ITEM));
	if(!m_hMsgQueue)	return false;

	return true;
}

bool GWndManager::RegisterWnd(GWnd *pWnd)
{
	for (uint8_t i = 0; i < WND_LIST_SIZE; i++)
	{
		if (m_pWndList[i] == NULL)
		{
			m_pWndList[i] = pWnd;
			return true;
		}
	}
	return false;
}

void GWndManager::UnregisterWnd(GWnd *pWnd)
{
	for(uint8_t i = 0; i<WND_LIST_SIZE; i++)
	{
		if(m_pWndList[i]==pWnd)
		{
			m_pWndList[i] = NULL;
			return;
		}
	}
}

//bool GWndManager::SendMessage(GWnd* pDestWnd, MSG msg, void *pMsgData)
bool GWndManager::SendMessage(MSG msg, void *pMsgData)
{
	if(!uxQueueSpacesAvailable(m_hMsgQueue))	return false;

	MSG_QUEUE_ITEM item;
	item.msg = msg;
	item.pMsgData = pMsgData;

	if(xQueueSend(m_hMsgQueue,&item,(TickType_t )0)!=pdTRUE)	return false;
	return true;
}

void GWndManager::RenderAll(void)
{
	uint8_t num = 0;
	EFM_ASSERT(m_pTFT);
	for(uint8_t i = 0; i<WND_LIST_SIZE; i++)
	{
		if(!m_pWndList[i])	continue;

		if(m_pWndList[i]->IsInvalidated())
		{
			m_pWndList[i]->OnDraw();
			num++;
		}
	}
	if(num)
		m_pTFT->updateFrameBuffer(false);
}

//*******************************************************************
