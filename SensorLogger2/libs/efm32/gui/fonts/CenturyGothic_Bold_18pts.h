/* Font data for Impact 14pt */

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "fonts/font_common.h"

/* Font data for Century Gothic 18pt */
extern const uint8_t centuryGothic_18ptBitmaps[];
extern const FONT_INFO centuryGothic_18ptFontInfo;
extern const FONT_CHAR_INFO centuryGothic_18ptDescriptors[];
