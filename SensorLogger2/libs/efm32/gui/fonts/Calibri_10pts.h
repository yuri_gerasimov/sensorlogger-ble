/* Font data for Impact 14pt */

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "fonts/font_common.h"

// Font data for Calibri 10pt
extern const uint8_t calibri_10ptBitmaps[];
extern const FONT_INFO calibri_10ptFontInfo;
extern const FONT_CHAR_INFO calibri_10ptDescriptors[];
