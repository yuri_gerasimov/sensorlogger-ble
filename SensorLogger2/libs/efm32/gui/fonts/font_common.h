#ifndef _FONT_COMMON_H_
#define _FONT_COMMON_H_

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

typedef enum 
{
	StringAlign_Left,
	StringAlign_Center,
	StringAlign_Right,
	StringAlign_Top,
	StringAlign_Middle,
	StringAlign_Bottom
} TFT_StringAlign_t;

typedef struct
{
	uint8_t m_uCharWidth;
	uint16_t m_uCharDataOffset;
} FONT_CHAR_INFO;

typedef struct
{
	uint8_t m_uCharHeight;
	uint8_t m_uStartChar;
	uint8_t m_uEndChar;
	uint8_t m_uSpaceWidth;
	const FONT_CHAR_INFO *m_CharInfo;
	const uint8_t* m_pFontBitmap;
} FONT_INFO;

#endif
