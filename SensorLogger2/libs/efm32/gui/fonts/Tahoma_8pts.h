/* Font data for Calibri 8pt */

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "fonts/font_common.h"

// Font data for Tahoma 8pt
extern const uint8_t tahoma_8ptBitmaps[];
extern const FONT_INFO tahoma_8ptFontInfo;
extern const FONT_CHAR_INFO tahoma_8ptDescriptors[];

