/* Font data for Impact 14pt */

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "fonts/font_common.h"

extern const uint8_t impact_14ptBitmaps[];
extern const FONT_INFO impact_14ptFontInfo;
extern const FONT_CHAR_INFO impact_14ptDescriptors[];
