/* Font data for Impact 14pt */

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "fonts/font_common.h"

/* Font data for Impact 12pt */
extern const uint8_t impact_12ptBitmaps[];
extern const FONT_INFO impact_12ptFontInfo;
extern const FONT_CHAR_INFO impact_12ptDescriptors[];
