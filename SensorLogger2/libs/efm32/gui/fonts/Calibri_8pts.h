/* Font data for Calibri 8pt */

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "fonts/font_common.h"

// Font data for Calibri 8pt
extern const uint8_t calibri_8ptBitmaps[];
extern const FONT_INFO calibri_8ptFontInfo;
extern const FONT_CHAR_INFO calibri_8ptDescriptors[];

