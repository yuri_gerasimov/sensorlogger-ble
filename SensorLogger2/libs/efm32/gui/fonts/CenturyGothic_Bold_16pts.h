/* Font data for Impact 14pt */

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "fonts/font_common.h"

/* Font data for Century Gothic 16pt */
extern const uint8_t centuryGothic_16ptBitmaps[];
extern const FONT_INFO centuryGothic_16ptFontInfo;
extern const FONT_CHAR_INFO centuryGothic_16ptDescriptors[];
