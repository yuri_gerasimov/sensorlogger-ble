/* Font data for Impact 14pt */

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "fonts/font_common.h"

/* Font data for Century Gothic 14pt */
extern const uint8_t centuryGothic_14ptBitmaps[];
extern const FONT_INFO centuryGothic_14ptFontInfo;
extern const FONT_CHAR_INFO centuryGothic_14ptDescriptors[];
