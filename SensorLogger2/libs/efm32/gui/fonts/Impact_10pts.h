/* Font data for Impact 14pt */

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "fonts/font_common.h"

/* Font data for Impact 10pt */
extern const uint8_t impact_10ptBitmaps[];
extern const FONT_INFO impact_10ptFontInfo;
extern const FONT_CHAR_INFO impact_10ptDescriptors[];
