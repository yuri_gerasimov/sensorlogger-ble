/* Font data for Calibri 8pt */

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "fonts/font_common.h"

// Font data for Consolas 8pt
extern const uint8_t consolas_8ptBitmaps[];
extern const FONT_INFO consolas_8ptFontInfo;
extern const FONT_CHAR_INFO consolas_8ptDescriptors[];

