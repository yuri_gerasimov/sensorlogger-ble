#include <stdbool.h>
#include <stdint.h>

#include "fb.h"
#include "dmactrl.h"

#include "config/hw_config.h"
#include "tft/ILI9341.h"

static uint16_t* m_pFrameBuffer = 0UL;
static uint32_t uSize = 0;
static uint32_t uPixelNumber = 0;
static uint16_t uWidth = 0;
static uint16_t uHeight = 0;

bool FB_Create(uint16_t *pBuffer, uint16_t Width, uint16_t Height, uint8_t bitsPerPixel)
{
	m_pFrameBuffer = pBuffer;
	uPixelNumber = Width * Height;
	uSize = uPixelNumber * bitsPerPixel / 8;
	uWidth = Width;
	uHeight = Height;
	return true;
}

uint16_t FB_Width(void)
{
	return uWidth;
}

uint16_t FB_Height(void)
{
	return uHeight;
}

void FB_Clear(Color_TypeDef color)
{
	uint32_t n = uPixelNumber;
	Color_TypeDef* p = m_pFrameBuffer;

	while(n--)
		*p++ = color;
}

void FB_PutPixel(int x, int y, Color_TypeDef color)
{
	uint32_t offset = y * uWidth + x;
	m_pFrameBuffer[offset] = color;
}

void FB_HLine(uint32_t x, uint32_t y, uint32_t w, Color_TypeDef color)
{
	uint32_t offset = (uint32_t)uWidth * y + x;
	Color_TypeDef* pPtr = m_pFrameBuffer + offset;

	while(w--)
		*pPtr++ = color;
}

void FB_VLine(int x, int y, int h, Color_TypeDef color)
{
	Color_TypeDef* pPtr = m_pFrameBuffer + y * uWidth + x;
	while(h--)
	{
		*pPtr = color;
		pPtr+=uWidth;
	}
}

void FB_FillRect(int x, int y, int w, int h, Color_TypeDef color)
{
	Color_TypeDef* pLine = m_pFrameBuffer + y * uWidth + x;
	Color_TypeDef* p;

	while(h--)
	{
		p = pLine;
		for(uint16_t i = 0; i< w; i++)	*p++ = color;
		pLine+=uWidth;
	}
}

void FB_Update(bool bWaitForComplete)
{
#ifndef FB_DMA_COPY
	TFT_CopyToGRAM(m_pFrameBuffer,uPixelNumber);
#else
	/* Start DMA transfer */
	startDmaTransfer(m_pFrameBuffer, uPixelNumber);

	/* Sleep until transfer is finished.
	   * This is done to prevent software from overwriting the frame buffer
	   * before it is copied to the display (would cause flickering) */
	if(bWaitForComplete)
		while(isDmaBusy())
		{
			//	EMU_EnterEM1();
		}
#endif
}

