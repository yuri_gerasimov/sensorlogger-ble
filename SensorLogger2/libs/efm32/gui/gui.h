/*
 * gui.h
 *
 *  created on: 01.02.2016
 *      Author: Yuriy
 */

#ifndef GUI_H_
#define GUI_H_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "tft/tft_driver.h"

#include "FreeRTOS.h"
#include "queue.h"

#include "rgb.h"
#include "hsv.h"

#ifdef __cplusplus
extern "C" {
#endif

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

typedef enum
{
	WndType_None,
	WndType_VerticalProgressBar,
	WndType_HorizontalProgressBar,

	// Graphic indicator styles
	WndType_GraphicsDotsLines,
	WndType_GraphicsDots,
	WndType_GraphicsBars,
	WndType_GraphicsSticks,
	WndType_GraphicsCandles,

} GWndType_t;

typedef enum
{
	MSG_TYPE_NONE,
	MSG_TYPE_TIMER,
	MSG_TYPE_RTC,
	MSG_TYPE_USB,
	MSG_TYPE_BLE,
	MSG_TYPE_BATTERY,
	MSG_TYPE_BUTTON,
	MSG_TYPE_TOUCH,
	MSG_TYPE_UPDATE,
} GWndMsg_t;

typedef enum
{
	WND_RESULT_NONE,
	WND_RESULT_OK,
	WND_RESULT_CANCEL
} GWndResult_t;

typedef enum
{
	StringAlignedLeft,
	StringAlignedCenter,
	StringAlignedRight
} GStringAlignment_t;

class GWndBase
{
public:
	GWndBase();
	virtual ~GWndBase() {};

protected:
	int16_t m_x;
	int16_t m_y;

	int16_t m_width;
	int16_t m_height;

	GWndType_t m_style;

	Color_TypeDef m_BgColor;
	Color_TypeDef m_FrontColor;
	Color_TypeDef m_TextColor;

	FONT_INFO *m_pFont;
	uint8_t m_uFontHeight;
	bool m_bInvalidateFlag;

	TFT* m_pTFT;

public:
	void Invalidate(void) { m_bInvalidateFlag = true; }
	bool IsInvalidated(void) { return m_bInvalidateFlag; }

protected:
	void setCurrentFillColor(const Color_TypeDef c)
	{
		m_pTFT->setColorFill(c);
	}

	void setCurrentDrawColor(const Color_TypeDef c)
	{
		m_pTFT->setColorPen(c);
	}

	void setCurrentTextColor(const Color_TypeDef c)
	{
		m_pTFT->setColorText(c);
	}
	
public:

	int16_t getLeft(void)
	{
		return m_x;
	}

	int16_t getTop(void)
	{
		return m_y;
	}

	int16_t getWidth(void)
	{
		return m_width;
	}
	
	int16_t getHeight(void)
	{
		return m_height;
	}

	void setBgColor(const Color_TypeDef c)
	{
		m_BgColor = c;
	}

	void setFrontColor(const Color_TypeDef c)
	{
		m_FrontColor = c;
	}

	void setTextColor(const Color_TypeDef c)
	{
		m_TextColor = c;
	}

	void drawCircle(int16_t x, int16_t y, int16_t r)
	{
		m_pTFT->drawCircle(m_x + x, m_y + y, r);
	}

	void fillCircle(int16_t x, int16_t y, int16_t r)
	{
		m_pTFT->fillCircle(m_x+x, m_y+y, r);
	}

	void fillRect(int16_t x, int16_t y, int16_t w, int16_t h)
	{
		m_pTFT->fillRect(m_x+x,m_y+y,w,h);
	}

	void drawPixel(int16_t x, int16_t y)
	{
		m_pTFT->drawPixel(m_x+x,m_y+y);
	}

	void drawRect(int16_t x, int16_t y, int16_t w, int16_t h)
	{
		m_pTFT->drawRect(m_x+x, m_y+y, w, h);
	}

	void drawString(const char *pStr, int16_t x, int16_t y)
	{
		m_pTFT->drawString(pStr,m_x + x, m_y + y);
	}

	void drawStringInRect(const char *pStr, int16_t x, int16_t y, uint16_t w, uint16_t h)
	{
		m_pTFT->drawStringInRect(pStr,m_x + x, m_y + y, w, h);
	}

	void drawHLine(int16_t x, int16_t y, uint16_t w)
	{
		m_pTFT->drawHLine(m_x+x,m_y+y,w);
	}

	void drawVLine(int16_t x, int16_t y, uint16_t h)
	{
		m_pTFT->drawVLine(m_x+x,m_y+y,h);
	}

	void fillHLine(int16_t x, int16_t y, uint16_t w)
	{
		m_pTFT->fillHLine(m_x + x, m_y + y, w);
	}

	void fillVLine(int16_t x, int16_t y, uint16_t h)
	{
		m_pTFT->fillVLine(m_x + x, m_y + y, h);
	}

	void drawLine(int x0, int y0, int x1, int y1)
	{
		m_pTFT->drawLine(m_x+x0, m_y+y0, m_x+x1, m_y+y1);
	}

	void setStyle(GWndType_t style)
	{
		m_style = style;
	}

	void setTextAlignment(TFT_StringAlign_t alignH, TFT_StringAlign_t alignV)
	{
		m_pTFT->setTextAlignemt(alignH,alignV);
	}

	bool create(int16_t x, int16_t y, int16_t w, int16_t h);
	void setFont(const FONT_INFO *pFont);
	void fillGradientRectH(int16_t x, int16_t y, int16_t w, int16_t h, Color_TypeDef c1, Color_TypeDef c2);
	void fillGradientRectV(int16_t x, int16_t y, int16_t w, int16_t h, Color_TypeDef c1, Color_TypeDef c2);

	virtual void OnDraw(void)
	{
		m_bInvalidateFlag = false;
	}

#ifdef SUPPORT_ANTIALIASING
	void drawArc(uint16_t x, uint16_t y, uint16_t r, int16_t start_angle, int16_t end_angle)
	{
		m_pTFT->drawArc(m_x + x, m_y + y, r, start_angle, end_angle);
	}
	
	void drawArcAA(uint16_t x, uint16_t y, uint16_t r, int16_t start_angle, int16_t end_angle)
	{
		m_pTFT->drawArcAA(m_x + x, m_y + y, r, start_angle, end_angle);
	}

	void drawPixelA(int16_t x, int16_t y, Color_TypeDef c, uint8_t alfa)
	{
		m_pTFT->drawPixelAA(m_x + x, m_y + y, alfa);
	}

	void drawCircleAA(uint16_t x0, uint16_t y0, uint16_t r)
	{
		m_pTFT->drawCircleAA(m_x + x0, m_y + y0, r);
	}

	void drawFillRectAA(uint16_t x, uint16_t y, uint16_t w, uint16_t h)
	{

	}

	void drawLineAA(int16_t x0, int16_t y0, int16_t x1, int16_t y1)
	{
		m_pTFT->drawLineAA(m_x + x0, m_y + y0, m_x + x1, m_y + y1);
	}

	void fillLine(int x0, int y0, int x1, int y1, float wd)
	{
		m_pTFT->fillLine(m_x + x0, m_y + y0, m_x + x1, m_y + y1, wd);
	}

	void fillArc(int16_t x, int16_t y, uint16_t radius, uint16_t thickness, float start, float end)
	{
		m_pTFT->fillArc(m_x + x, m_y + y, radius, thickness, start, end);
	}

#endif

#ifdef SUPPORT_BEZIER
	void DrawQuadBezier(int x0, int y0, int x1, int y1, int x2, int y2, Color_TypeDef color)
	{
		m_pTFT->drawQuadBezier(m_x + x0, m_y + y0, m_x + x1, m_y + y1, m_x + x2, m_y + y2, color);
	}

	void DrawCubicBezier(int x0, int y0, int x1, int y1, int x2, int y2, int x3, int y3, Color_TypeDef color)
	{
		m_pTFT->drawCubicBezier(m_x + x0, m_y + y0, m_x + x1, m_y + y1, m_x + x2, m_y + y2, m_x + x3, m_y + y3, color);
	}
#endif // SUPPORT_BEZIER
};

//---------------------------------------------------------------------

class GProgressCtrl: public GWndBase
{
private:
	int m_min;
	int m_max;
	int m_pos;

public:
	GProgressCtrl(GWndType_t style);
	virtual void OnDraw(void);

	void SetPos(int pos)
	{
		m_pos = pos;
	}
	void SetRange(int min, int max)
	{
		m_min = min;
		m_max = max;
	}
};

//********************************************************************
class GBatteryCtrl : public GWndBase
{
public:
	typedef enum
	{
		BatteryStatus_Discharging,
		BatteryStatus_Charging,
	} BatteryStatus_t;

private:
	BatteryStatus_t m_Status;
	uint8_t m_uLevel;

public:
	GBatteryCtrl(void);
	void SetLevel(uint8_t level)
	{
		m_uLevel = level;
	}
	void SetStatus(BatteryStatus_t status)
	{
		m_Status = status;
	}
	void create(int16_t x, int16_t y, int16_t w, int16_t h);
	virtual void OnDraw(void);
};

class GBleCtrl : public GWndBase
{
private:
	bool m_bIsConnected;
public:
	GBleCtrl(void)
	{
		m_bIsConnected = false;
	}
	void SetConnected(bool isConnected)
	{
		m_bIsConnected = isConnected;
	}
	virtual void OnDraw(void);
};


class GUsbCtrl : public GWndBase
{
private:
	bool m_bIsConnected;
public:
	GUsbCtrl(void)
	{
		m_bIsConnected = false;
	}
	void SetConnected(bool isConnected)
	{
		m_bIsConnected = isConnected;
	}
	virtual void OnDraw(void);
};
	
	typedef enum
	{
		GProgressBarVertical,
		GProgressBarHorizontal,
	} GProgressBarOrientation_t;

// --- PROGRESS BAR declaration -----
	class GProgressBar : public GWndBase
	{
	private:
		GProgressBarOrientation_t m_Orientation;
		int32_t m_iMinValue;
		int32_t m_iMaxValue;
		int32_t m_iValue;
		int16_t m_uTicksNumber;
	public:
		GProgressBar();
		virtual void OnDraw(void);

		void SetTicks(int16_t iTicksNumber)
		{
			m_uTicksNumber = iTicksNumber;
		}
		
		void SetOrientation(GProgressBarOrientation_t orientation)
		{
			m_Orientation = orientation;
		}

		void SetRange(int32_t iMinValue, int32_t iMaxValue)
		{
			if (m_iMaxValue > m_iMinValue)
			{
				m_iMinValue = iMinValue;
				m_iMaxValue = iMaxValue;
			}
		}
		
		void SetPos(int32_t iValue)
		{
			if(iValue>= m_iMinValue && iValue<= m_iMaxValue)
				m_iValue = iValue;
		}
	};

	class GArcProgressCtrl : public GWndBase
	{
	private:
		bool m_bIsFillCircleBorders;

		uint16_t m_uThickness;
		int16_t m_uTicksNumber;

		int16_t m_iStartAngle;
		int16_t m_iEndAngle;

		int32_t m_iValue;
		int32_t m_iMinValue;
		int32_t m_iMaxValue;

	public:
		GArcProgressCtrl();
		bool create(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t thickness)
		{
			m_uThickness = thickness;
			GWndBase::create(x, y, w, h);
		}

		virtual void OnDraw(void);

		void setBorders(bool bIsBorders)
		{
			m_bIsFillCircleBorders = bIsBorders;
		}

		void setAngles(int16_t iStartAngle, int16_t iEndAngle)
		{
			m_iStartAngle = iStartAngle;
			m_iEndAngle = iEndAngle;
		}

		void SetTicks(int16_t iTicksNumber)
		{
			m_uTicksNumber = iTicksNumber;
		}

		void SetRange(int32_t iMinValue, int32_t iMaxValue)
		{
			if (m_iMaxValue > m_iMinValue)
			{
				m_iMinValue = iMinValue;
				m_iMaxValue = iMaxValue;
			}
		}

		void SetPos(int32_t iValue)
		{
			if (iValue >= m_iMinValue && iValue <= m_iMaxValue)
				m_iValue = iValue;
		}
	};

#define WND_EXT_SIZE_MAX 16

	class GGaugeCtrl : public GWndBase
	{
	private:
		int32_t m_iMinValue;
		int32_t m_iMaxValue;
		int32_t m_iValue;
		int32_t m_iValueDivider;

		uint16_t m_uTicksNumber;
		uint16_t m_uBigTickMult;

		int16_t m_iStartAngle;
		int16_t m_iEndAngle;

		int16_t m_iScaleStart;
		int16_t m_iScaleStep;

		char m_strText[WND_EXT_SIZE_MAX];
		char m_strExtText[WND_EXT_SIZE_MAX];

	public:
		GGaugeCtrl();
		virtual void OnDraw(void);

		void SetAngles(int16_t iStartAngle, int16_t iEndAngle)
		{
			m_iStartAngle = iStartAngle;
			m_iEndAngle = iEndAngle;
		}

		void SetScaleStartValue(int16_t iStartValue)
		{
			m_iScaleStart = iStartValue;
		}

		void SetScaleStepValue(int16_t iStepValue)
		{
			m_iScaleStep = iStepValue;
		}

		void SetText(const char *strText)
		{
			strcpy(m_strText, strText);
		}

		void SetExtText(const char *strText)
		{
			strcpy(m_strExtText, strText);
		}
		
		void SetTicks(int16_t iTicksNumber)
		{
			m_uTicksNumber = iTicksNumber;
		}

		void SetBigTicks(int16_t iTicksNumber)
		{
			m_uBigTickMult = iTicksNumber;
		}

		void SetRange(int32_t iMinValue, int32_t iMaxValue)
		{
			if (m_iMaxValue > m_iMinValue)
			{
				m_iMinValue = iMinValue;
				m_iMaxValue = iMaxValue;
			}
		}

		void SetValue(int32_t iValue)
		{
			if (iValue >= m_iMinValue && iValue <= m_iMaxValue)
				m_iValue = iValue;
		}

		void SetValueDivider(int32_t iValueDivider)
		{
			m_iValueDivider = iValueDivider;
		}
	};

	// --- PROGRESS BAR declaration -----
	
	class GButton : public GWndBase
	{
	private:
		char m_strText[WND_EXT_SIZE_MAX];
		
	public:
		GButton();
		virtual void OnDraw(void);
		void SetText(const char *strText)
		{
			strcpy(m_strText, strText);
		}
	};

// --- RADIO CTRL declaration -----
	
#define WND_RADIO_CTRL_MAX_ITEMS 8

class GRadioCtrl : public GWndBase
{
private:
	uint8_t m_uItemWidth;
	uint8_t m_uItemHeight;

	uint8_t m_uItems;
	uint8_t m_uCurrentItem;
	char *m_pStrItems[WND_RADIO_CTRL_MAX_ITEMS];

	void RenderItem(uint8_t index);

public:
	GRadioCtrl(void);
	virtual void OnDraw(void);

	void SetItemsSize(uint8_t w, uint8_t h)
	{
		m_uItemWidth = w;
		m_uItemHeight = h;
	}

	bool SetItemText(uint8_t i, const char* pStrText)
	{
		if(i<WND_RADIO_CTRL_MAX_ITEMS)	m_pStrItems[i] = (char*)pStrText;
		else
			return false;
		return true;
	}

	bool SetItemNumber(uint8_t num);
	void SetCurrentItem(uint8_t index)
	{
		m_uCurrentItem = index;
	}

	uint8_t GetCurrentItem(void)
	{
		return m_uCurrentItem;
	}
};

typedef enum
{
	GraphicType_None,
	GraphicType_Dots,
	GraphicType_Line,
	GraphicType_Bars,
	GraphicType_Candles,
	GraphicType_JapCandles,
	GraphicType_Max
} GraphicType_t;

struct GraphicDataSet
{
	float *m_pData;				//	pointer to the data buffer
	float m_yMidiumValue;		//	Y middle point
	float m_yScale;				//	value per pixel
	float m_yScaleMinimum;		//	smallest Y scale value

	uint16_t m_uDataSize;
	uint16_t m_uDataOffsetIndex;
	uint16_t m_uStartIndex;
	uint16_t m_uLastIndex;

	GraphicType_t m_GraphType;
	Color_TypeDef m_GraphColor;
};

class GGraphicWnd : public GWndBase
{
private:
	//int16_t *m_pData;
	//uint16_t m_uDataSize;
	//uint16_t m_uDataOffsetIndex;

	uint16_t m_uXstep;
	uint16_t m_yAxe;

	GraphicDataSet m_DataSet[4];
	
	void DrawGraphic(uint16_t nDatasetIndex);

public:
	GGraphicWnd(void);
	virtual void OnDraw(void);
	void SetApperance(uint16_t nDatasetIndex, GraphicType_t type, Color_TypeDef color);
	void create(int16_t x, int16_t y, int16_t w, int16_t h);
	void SetData(float *pData, uint16_t uDataSize, uint16_t nDatasetIndex, float fYScaleMinimum);
};

class GGraphCtrl : public GWndBase
{
private:
	Color_TypeDef m_GridColor;	//	color of the grid
	Color_TypeDef m_AxeColor;	//	color of the axes
	Color_TypeDef m_DotsStartColor;	//	start color of the bars/dots

	uint16_t m_uGridOffsetX;	//	offset of grid & data points by X
	uint16_t m_uGridXMult;		//	how many samples X are taken for every grid X step
	uint16_t m_uGridSizeY;		//	size of grid by Y

	int16_t *m_pData;
	uint16_t m_uDataSize;
	uint16_t m_uDataOffsetIndex;

	int16_t m_iDataMiddle;
	uint16_t m_uMiddleY;

	uint16_t m_StepX;			//	how many pixels are for every data sample
	uint8_t m_uPointRadius;		//	size of data point
	bool m_bIsColoredVertical;

	//------------------------------------
	uint16_t m_uVerticalScale;
	uint16_t m_uVerticalScaleDivider;

	int16_t m_iYAxePosition;
	uint8_t m_uYAxeWidth;

private:
	void DrawGridHorizontalLines(void);
	void DrawGridVerticalLines(void);

public:
	GGraphCtrl(void);
	virtual void OnDraw(void);
	void SetData(int16_t *pData, uint16_t uDataSize);

	void SetGridSize(uint16_t x_step, uint16_t x_mult, uint16_t y_size)
	{
		m_StepX = x_step;
		m_uGridXMult = x_mult;
		m_uGridSizeY = y_size;
	}

	uint16_t GetVisibleSamplesNumber(void);

	//------------------------------------

	//	data scaling by X and Y
	bool SetAutoScale(uint16_t uOffset, uint16_t divider);		//	calculate Y scale automatically

	void SetVerticalScale(int16_t scale, int16_t divider);		//	set Y scale manually
	void SetVerticalOffset(int16_t offset_value);
	void SetVerticalMinScale(int16_t scale, int16_t divider);
	bool SetHorizontalOffset(uint16_t uOffset);
	void SetHorizontalAxe(int16_t value, Color_TypeDef color, uint8_t w);

	int16_t GetYByValue(int32_t val);
	int16_t GetValueBY(uint16_t y);

	void SetAppierance(bool bIsColoredVertical, uint8_t uPointRadius);
};


typedef uint16_t MSG;
typedef struct
{
//	GWnd *pDestWnd;		//	destination window
	MSG msg;			//	message type
	void *pMsgData;		//	message data pointer
} MSG_QUEUE_ITEM;

#define WND_LIST_SIZE 8
#define MSG_QUEUE_SIZE 8


//-----  GWnd base class ----------------------------------------------
//
// - GWnd can handle a messages
// - GWnd are used for management via GWndManager

class GWnd : public GWndBase
{
private:
//	uint16_t m_uHeaderWidth;
	volatile GWndResult_t m_uResult;

public:
//	GWnd(void);
	virtual ~GWnd();

	bool create(int16_t x, int16_t y, int16_t w, int16_t h);
	void Exit(GWndResult_t result)
	{
		m_uResult = result;
	}
	virtual void OnDraw(void)	{	GWndBase::OnDraw(); 	}
	virtual void OnMsgReceive(MSG msg, void *pMsgData);
	virtual void DoModal(void);
};

//---------------------------------------------------------------------
/*
class MainWnd : public GWnd
{
private:
	GBatteryCtrl batCtrl;
	GBleCtrl bleCtrl;
	GUsbCtrl usbCtrl;
	void UpdateDateTime(void);

public:
//	MainWnd(void);
	virtual ~MainWnd() {}
	virtual void OnDraw(void);
	virtual void OnMsgReceive(MSG msg, void *pMsgData);

	void create(int16_t x, int16_t y, int16_t w, int16_t h);
};
*/

class GWndManager
{
public:
	GWndManager();
	bool Init(TFT *pTFT, unsigned int len);

private:
	TFT *m_pTFT;
	GWnd *m_pWndList[WND_LIST_SIZE];
	QueueHandle_t m_hMsgQueue;

public:

	TFT* getTFT(void)
	{
		return m_pTFT;
	}

	QueueHandle_t GetMsgQueue(void)
	{
		return m_hMsgQueue;
	}

	bool RegisterWnd(GWnd *pWnd);
	void UnregisterWnd(GWnd *pWnd);

	void MessageLoop(void);
	void RenderAll(void);

	bool SendMessage(MSG msg, void *pMsgData);

private:
};

extern GWndManager wndMgr;

#ifdef __cplusplus
}
#endif

#endif /* GUI_H_ */
