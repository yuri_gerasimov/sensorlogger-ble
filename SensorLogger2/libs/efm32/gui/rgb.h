#ifndef _RGB_H_
#define _RGB_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct __attribute__ ((packed))
{
	uint8_t g;
	uint8_t r;
	uint8_t b;
} RGB_t;

typedef struct __attribute__ ((packed))
{
	uint32_t g;
	uint32_t r;
	uint32_t b;
} RGB32_t;

typedef union __attribute__ ((packed))
{
    uint16_t    value;              // a 16-bit value representing a color or palette entry
    struct __attribute__ ((packed))
    {
        uint16_t    r:5;            // represents the RED component
        uint16_t    g:6;            // represents the GREEN component
        uint16_t    b:5;            // represents the BLUE component
    } color;                        // color value in 5-6-5 RGB format
} RGB565_t;


#define RGB565(r,g,b)  (((r&0xF8)<<8)|((g&0xFC)<<3)|((b&0xF8)>>3)) //5 red | 6 green | 5 blue

/*
#define RED(value) ((value & 0xF800) >> 11)
#define GREEN(value) ((value & 0x7E0) >> 5)
#define BLUE(value) ((value & 0x1F))
*/

#define RED(value) (((value & 0xF800) >> 11) << 3)
#define GREEN(value) (((value & 0x7E0) >> 5) << 2)
#define BLUE(value) ((value & 0x1F)<<3)


#ifdef __cplusplus
}
#endif

#endif // _RGB_H_
