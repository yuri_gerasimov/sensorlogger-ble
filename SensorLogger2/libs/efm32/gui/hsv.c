#include "hsv.h"
#include "rgb.h"

void HSV_ToRGB(HSV_t *pHSV, RGB_t *pRGB)
{
	uint16_t region, remainder, p, q, t;

	if (!pHSV->s)
	{
		pRGB->r = pHSV->v;
		pRGB->g = pHSV->v;
		pRGB->b = pHSV->v;
		return;
	}

	region = pHSV->h / 43;
	remainder = (pHSV->h - (region * 43)) * 6;

	p = ((uint16_t)pHSV->v * (0xFF - pHSV->s)) >> 8;
	q = ((uint16_t)pHSV->v * (0xFF - ((pHSV->s * remainder) >> 8))) >> 8;
	t = ((uint16_t)pHSV->v * (0xFF - ((pHSV->s * (0xFF - remainder)) >> 8))) >> 8;

	switch (region)
	{
		case 0:		pRGB->r = pHSV->v;		pRGB->g = t;		pRGB->b = p;			break;
		case 1:		pRGB->r = q;			pRGB->g = pHSV->v;	pRGB->b = p;			break;
		case 2:		pRGB->r = p;			pRGB->g = pHSV->v;	pRGB->b = t;			break;
		case 3:		pRGB->r = p;			pRGB->g = q;		pRGB->b = pHSV->v;		break;
		case 4:		pRGB->r = t;			pRGB->g = p;		pRGB->b = pHSV->v;		break;
		default:	pRGB->r = pHSV->v;		pRGB->g = p;		pRGB->b = q;			break;
	}
}
