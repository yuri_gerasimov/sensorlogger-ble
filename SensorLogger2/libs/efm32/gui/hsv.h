#ifndef _HSV_H_
#define _HSV_H_

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "rgb.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct __attribute__ ((packed))
{
	uint8_t h;
	uint8_t s;
	uint8_t v;
} HSV_t;

void HSV_ToRGB(HSV_t *pHSV, RGB_t *pRGB);

#ifdef __cplusplus
}
#endif

#endif // _HSV_H_
