#ifndef _FB_H_
#define _FB_H_

#include "tft/ILI9341.h"

#ifdef __cplusplus
extern "C" {
#endif

uint16_t FB_Width(void);
uint16_t FB_Height(void);
bool FB_Create(uint16_t *pBuffer, uint16_t Width, uint16_t Height, uint8_t bitsPerPixel);
void FB_Clear(Color_TypeDef color);
void FB_PutPixel(int x, int y, Color_TypeDef color);
void FB_HLine(uint32_t x, uint32_t y, uint32_t w, Color_TypeDef color);
void FB_VLine(int x, int y, int h, Color_TypeDef color);
void FB_FillRect(int x, int y, int w, int h, Color_TypeDef color);
void FB_Update(bool bWaitForComplete);

#ifdef __cplusplus
}
#endif

#endif
