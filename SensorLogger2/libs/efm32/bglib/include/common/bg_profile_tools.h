#ifndef BG_PROFILE_TOOLS_H
#define BG_PROFILE_TOOLS_H
#include <stdint.h>

/**
 * Convert mantissa & exponent values to IEEE float type
 */
static inline uint32_t bg_uint32_to_float(uint32_t mantissa, int32_t exponent)
{
	return (mantissa & 0xffffff)|(uint32_t)(exponent<<24);
}

/**
 * Temperature measurement locations
 */
enum bg_thermometer_temperature_type
{
    bg_thermometer_temperature_type_armpit=1,
    bg_thermometer_temperature_type_body,
    bg_thermometer_temperature_type_ear,
    bg_thermometer_temperature_type_finger,
    bg_thermometer_temperature_type_gastro_intestinal_tract,
    bg_thermometer_temperature_type_mouth,
    bg_thermometer_temperature_type_rectum,
    bg_thermometer_temperature_type_toe,
    bg_thermometer_temperature_type_tympanum,    
};

enum bg_thermometer_temperature_measurement_flag
{
    bg_thermometer_temperature_measurement_flag_units    =0x1,
    bg_thermometer_temperature_measurement_flag_timestamp=0x2,
    bg_thermometer_temperature_measurement_flag_type     =0x4,
};

/**
 * Create temperature measurement value from IEEE float and temperature type flag
 */
static inline void bg_thermometer_create_measurement(uint8_t* buffer,uint32_t measurement,int fahrenheit)
{
	buffer[0]=fahrenheit?bg_thermometer_temperature_measurement_flag_units:0;
	buffer[1]=measurement&0xff;
	buffer[2]=(measurement>>8)&0xff;
	buffer[3]=(measurement>>16)&0xff;
	buffer[4]=(measurement>>24)&0xff;
}


#endif
