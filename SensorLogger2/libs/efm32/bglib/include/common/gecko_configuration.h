#ifndef GECKO_CONFIGURATION
#define GECKO_CONFIGURATION

#include "bg_gattdb_def.h"

/**
 * USART configuration defined by application
 */
#define USART_FLAGS_ENABLED  1
#define USART_FLAGS_BGAPI    2
#define USART_FLAGS_NORTSCTS 4//Do not enable hardware flow control in uart

typedef struct
{    
    uint32_t    baudrate;
    uint8_t     flags;
    uint8_t     rts_loc;
    uint8_t     cts_loc;
    uint8_t     rx_loc;
    uint8_t     tx_loc;
}gecko_usart_config_t;

#define I2C_FLAGS_ENABLED  1
typedef struct
{    
    uint8_t     flags;
    uint8_t     scl_loc;
    uint8_t     sda_loc;
}gecko_i2c_config_t;

#define GPIO_FLAGS_ENABLED  1

typedef struct
{
    uint32_t    CTRL;
    uint32_t    MODEL;
    uint32_t    MODEH;
    uint32_t    DOUT;
}gecko_gpio_config_t;

typedef struct
{
    uint32_t    EXTIPSELL;
    uint32_t    EXTIPSELH;
    uint32_t    EXTIRISE;
    uint32_t    EXTIFALL;
    uint32_t    IEN;
}gecko_gpio_exti_config_t;

#define ADC_FLAGS_ENABLED  1
typedef struct
{
    uint8_t     flags;
}gecko_adc_config_t;
#define SLEEP_FLAGS_ENABLE_WAKEUP_PIN  1
#define SLEEP_FLAGS_POLARITY           2
#define SLEEP_FLAGS_DEEP_SLEEP_ENABLE  4

typedef struct
{
    uint8_t     flags;
    uint8_t     port;
    uint8_t     pin;
}gecko_sleep_config_t;

typedef struct
{
    uint8_t max_connections;
}gecko_bluetooth_config_t;

#define GECKO_CONFIG_FLAG_STACK_DISABLE 2
#define GECKO_CONFIG_FLAG_LFXO_DISABLE  4

typedef struct
{
    uint32_t config_flags;
    gecko_usart_config_t  usarts[3];
    gecko_i2c_config_t  i2cs[1];
    gecko_gpio_config_t gpios[6];
    gecko_gpio_exti_config_t gpio_exti;
    gecko_adc_config_t  adc;
    gecko_sleep_config_t  sleep;
    gecko_bluetooth_config_t bluetooth;
    //

    const struct bg_gattdb_def *gattdb;
    //DCDC config, if NULL uses default values
    const void * dcdc;
    //HFXO config, if NULL uses default values
    const void * hfxo;
    //LFXO config, if NULL uses default values
    const void * lfxo;
    //PTI config, if NULL PTI is disabled
    const void * pti;
}gecko_configuration_t;


#endif
