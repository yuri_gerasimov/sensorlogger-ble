#ifndef _DELAY_H_
#define _DELAY_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef USE_DELAY_MS
void delay_ms_clock_init(void);
void delay_ms(uint32_t ulDelayMs);
#endif
void delay_us(uint32_t ulDelayUs);

#ifdef __cplusplus
}
#endif

#endif
