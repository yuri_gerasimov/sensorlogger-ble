#include "str.h"
#include "string.h"

//#include <stdlib.h>

size_t strLen(const char *s)
{
    size_t l = 0;
    while (*s++) l++;
    return l;
}

void strCopy(char *pStrDest, const char *pStrSrc)
{
	uint16_t uLen = strLen(pStrSrc);
	if(uLen)
		memcpy(pStrDest, pStrSrc, uLen+1);
}

void strCat(char *pStrDest, const char *pStrSrc)
{
	uint16_t uLenSrc = strLen(pStrSrc);
	uint16_t uLenDest = strLen(pStrDest);

	memcpy(pStrDest + uLenDest, pStrSrc, uLenSrc+1);
}

char* strUtoa(uint32_t val, uint16_t base)
{
	static char buf[16] = { 0 };
	int i = 14;

//	memset(buf,'0',16);	//	for leading zeros
	do
	{
		buf[i] = "0123456789ABCDEF"[val % base];
		val /= base;
		--i; 
	} while (val && i);
	return &buf[i + 1];
}

void strUCat(char *pStrDest, uint32_t val, uint16_t base)
{
	char* strVal = strUtoa(val,base);
	strCat(pStrDest, strVal);
}

char* strItoa(int32_t val)
{
	static char buf[16] = { 0 };
	int i = 14;
	uint8_t is_negative = 0;

	if(val<0)
	{
		is_negative = 1;
		val*=-1;
	}

	do
	{
		buf[i] = "0123456789"[val % 10];
		val /= 10;
		--i;
	} while (val && i);

	if(is_negative)
	{
		buf[i - 1] = '-';
		i--;
	}
	else
		i++;

//	return &buf[i + 1];
	return &buf[i];
}

void strICat(char *pStrDest, int32_t val)
{
	char* strVal = strItoa(val);
	strCat(pStrDest, strVal);

}

char *strutoa(uint32_t value, char *buffer)
{
	// 11 ���� ���������� ��� ����������� ������������� 32-� �������� ����� � ������������ ����
   buffer += 11;
   *--buffer = 0;
   do
   {
      *--buffer = value % 10 + '0';
      value /= 10;
   }
   while (value != 0);
   return buffer;
}

char *strutoaz(uint32_t value, char *buffer, uint16_t zeros)
{
	uint16_t z_pos = 0;
	// 11 ���� ���������� ��� ����������� ������������� 32-� �������� ����� � ������������ ����
   buffer += 11;
   *--buffer = 0;
   z_pos++;
   do
   {
      *--buffer = value % 10 + '0';
      z_pos++;
      value /= 10;
   }
   while (value != 0);

   while(z_pos<=zeros)
   {
	   *--buffer = '0';
	   z_pos++;
   }

   return buffer;
}

char *stritoa(int32_t value, char *buffer)
{
	uint8_t neg = 0;

	// 12 ���� ���������� ��� ����������� ������������� 32-� �������� ����� � ������������ ����
   buffer += 12;

   if(value < 0)
   {
	   neg = 1;
	   value*=-1;
   }

   *--buffer = 0;
   do
   {
      *--buffer = value % 10 + '0';
      value /= 10;
   }
   while (value != 0);

   if(neg)
	   *--buffer = '-';

   return buffer;
}

