#ifndef STR_H_
#define STR_H_

#include <stdint.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

size_t strLen(const char *s);
void strCopy(char *pStrDest, const char *pStrSrc);
void strCat(char *pStrDest, const char *pStrSrc);
char* strUtoa(uint32_t val, uint16_t base);
void strUCat(char *pStrDest, uint32_t val, uint16_t base);
char* strItoa(int32_t val);
void strICat(char *pStrDest, int32_t val);

char *strutoa(uint32_t value, char *buffer);
char *strutoaz(uint32_t value, char *buffer, uint16_t zeros);
char *stritoa(int32_t value, char *buffer);

#ifdef __cplusplus
}
#endif

#endif /* CDC_H_ */
