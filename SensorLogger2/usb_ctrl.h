/*
 * usb_ctrl.h
 *
 *  Created on: 20.04.2016
 *      Author: Yuriy
 */

#ifndef USB_CTRL_H_
#define USB_CTRL_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "descriptors.h"

bool initUSB(USART_TypeDef *pMicroSD_USART);
int SetupCmd(const USB_Setup_TypeDef *setup);
void StateChangeEvent(USBD_State_TypeDef oldState, USBD_State_TypeDef newState);
void HID_OnReportRecieved(uint8_t uReportId, void *pData);

#ifdef __cplusplus
}
#endif

#endif /* USB_CTRL_H_ */
