/*
 * AppGUI.cpp
 *
 *  created on: 28 ���. 2016 �.
 *      Author: Yuriy
 */

#include "AppGUI.h"
#include "gui_config.h"

#include <math.h>

#include "fonts/CenturyGothic_Bold_14pts.h"
#include "fonts/CenturyGothic_Bold_16pts.h"
#include "fonts/CenturyGothic_Bold_18pts.h"
#include "fonts/Impact_14pts.h"
#include "fonts/Impact_12pts.h"
#include "fonts/Impact_10pts.h"
#include "fonts/Calibri_10pts.h"
#include "fonts/Calibri_8pts.h"
#include "fonts/Consolas_8pts.h"
#include "fonts/Tahoma_8pts.h"

#include "SensorLogger.h"
#include "str.h"
#include "rtc_cal.h"

#include "MAX44009.h"
#include "BME280.h"
#include "FXOS8700CQ.h"

#include "rgb.h"
#include "trace.h"

#include "ble_host.h"

extern RealTimeClock rtc;
extern uint8_t m_uCurrectBatteryLevel;

void GTempGraphWnd::create(int16_t x, int16_t y, int16_t w, int16_t h)
{
	GWnd::create(x,y,w,h);
	graphCtrl.create(m_x, m_y + 50, m_width - 10, m_height - 90);
	graphCtrl.SetGridSize(8,5,20);

	radioCtrl.create(m_x + 100, m_y + 25, 320 - 110, 20);
	radioCtrl.SetItemNumber(4);
	radioCtrl.SetItemsSize(30,14);
	radioCtrl.SetItemText(0,"Day");
	radioCtrl.SetItemText(1,"Hour");
	radioCtrl.SetItemText(2,"Min");
	radioCtrl.SetItemText(3,"Sec");
	radioCtrl.SetCurrentItem(2);

	batCtrl.create(300,4,16,8);
	bleCtrl.create(262,3,8,11);
	usbCtrl.create(254,3,8,11);

	m_BgColor = config_WND_BG_COLOR;
	m_FrontColor = config_WND_GRID_COLOR;
	m_TextColor = config_TEXT_COLOR_HEADER;
}

void GTempGraphWnd::SetData(int16_t *pData, uint16_t uDataSize)
{
	graphCtrl.SetData(pData,uDataSize);
	graphCtrl.SetAutoScale(0,10);
	graphCtrl.SetVerticalScale(50,100);	// = 5 per pixel
	graphCtrl.SetVerticalOffset(20);
	graphCtrl.SetHorizontalAxe(0,config_WND_GRID_COLOR,3);
	graphCtrl.SetAppierance(false,3);
}


void GTempGraphWnd::UpdateDateTime(void)
{ 
	char str[32];
	setFont(&calibri_8ptFontInfo);
	rtc.format(str,rtc.get());
	drawString(str,145,5);
}

void GTempGraphWnd::OnDraw(void)
{
	GWnd::OnDraw();

	setCurrentFillColor(m_BgColor);
	setCurrentDrawColor(m_FrontColor);
	setCurrentTextColor(m_TextColor);
	setFont(&centuryGothic_14ptFontInfo);

	fillRect(0, 0, m_width, m_height);
	drawHLine(10,18,300);
	drawString("Temperature",20,0);
	
	bleCtrl.SetConnected(BLE_IsConnected());

	UpdateDateTime();

	//	controls are not drawing through the windows manager, so we should paint them manually
	graphCtrl.OnDraw();
	radioCtrl.OnDraw();
	batCtrl.OnDraw();
	bleCtrl.OnDraw();
	usbCtrl.OnDraw();

	setFont(&impact_12ptFontInfo);
	setCurrentTextColor(config_BIG_TEXT_COLOR);

//	char str[16];
//	sprintf(str,"T0 = %d.%u|C",m_CurrentLoggerData.m_iIemperature[0]/100, m_CurrentLoggerData.m_iIemperature[0]%100);
//	SL_Temperature_t *t = (SL_Temperature_t *)deviceGetSensorDataPtr(deviceGetSensorsData(), SL_SensorType_Temperature, config_SENSOR_ID_DEFAULT_INT);
/*
	SL_Temperature_t *t = (SL_Temperature_t *)deviceGetSensorData(SL_SensorType_Temperature, config_SENSOR_ID_DEFAULT_INT);
	if(t)
	{
		char str[16];
		sprintf(str,"T0 = %d.%u|C",*t/100, *t%100);
		drawString(str, 50, 215);
	}
	drawString("T1 = +0.0|C", 160, 215);		//	reference T0
*/

	int32_t t = logger.getTemperature(0);
	char str[16] = {"T0: "};
	strUCat(str,t/100,10);
	strCat(str,".");
	strUCat(str,t%100,10);
	strCat(str,"|C");

	drawString(str, 50, 215);
	drawString("T1 = +0.0|C", 160, 215);		//	reference T0

	setCurrentFillColor(RGB(229, 120, 40));
	fillCircle(42,223,4);

	setCurrentFillColor(RGB(0, 166, 81));
	fillCircle(152,223,4);
}

void GTempGraphWnd::OnMsgReceive(MSG msg, void *pMsgData)
{
	GWnd::OnMsgReceive(msg,pMsgData);

	switch(msg)
	{
		case MSG_TYPE_BATTERY:
		{
			batCtrl.SetLevel(m_uCurrectBatteryLevel);
			Invalidate();
			break;
		}
		case MSG_TYPE_USB:
		{
			usbCtrl.SetConnected(*((bool*)pMsgData));
			break;
		}
		case MSG_TYPE_BLE:
		{
			bleCtrl.SetConnected(*((bool*)pMsgData));
			break;
		}
		case MSG_TYPE_BUTTON:
		{
			Exit(WND_RESULT_OK);
			break;
		}
		case MSG_TYPE_TOUCH:	break;
		default:	break;
	}
}

//*******************************************************************

DashboardWnd::DashboardWnd(void)
{
//	memset(&m_accData,0,sizeof(XYZ_RAWDATA_t));
//	memset(&m_magnData,0,sizeof(XYZ_RAWDATA_t));
}

void DashboardWnd::create(int16_t x, int16_t y, int16_t w, int16_t h)
{
	GWnd::create(x,y,w,h);

	batCtrl.create(300,4,16,8);
	bleCtrl.create(262,3,8,11);
	usbCtrl.create(254,3,8,11);

//	UpdateTPHS();
//	UpdateAMS();
//	UpdateLS();
}

void DashboardWnd::UpdateDateTime(void)
{
	char str[32];
	setFont(&calibri_8ptFontInfo);
	rtc.format(str,rtc.get());
	drawString(str,145,5);
}

void DashboardWnd::OnDraw(void)
{
//	char str[16];

	const uint16_t start_y1 = 21;
	const uint16_t start_y2 = 36;
	const uint16_t dy = 73;
	const uint16_t dy2 = 100;
	const uint16_t text_x1 = 27;
	const uint16_t text_x2 = 180;

	GWnd::OnDraw();

	setCurrentFillColor(config_WND_BG_COLOR);
	fillRect(0,0,m_width,m_height);

	bleCtrl.SetConnected(BLE_IsConnected());

	//	controls are not drawing through the windows manager, so we should paint them manually
	batCtrl.OnDraw();
	bleCtrl.OnDraw();
	usbCtrl.OnDraw();

	setCurrentDrawColor(config_WND_GRID_COLOR);
	drawHLine(10,18,300);

	setFont(&centuryGothic_14ptFontInfo);
	setCurrentTextColor(RGB(255,255,255));
	drawString("Dashboard",20,0);

	UpdateDateTime();

	//**************************************************************

	setCurrentFillColor(RGB(8, 40, 56));
	fillRect(11,start_y1,148,65);
	fillRect(11,start_y1+dy,148,65);
	fillRect(11,start_y1+dy*2,148,65);
	fillRect(164,start_y2,148,85);
	fillRect(164,start_y2 + dy2,148,85);

	setCurrentFillColor(RGB(33, 135, 145));
	fillRect(11,start_y1,148,4);
	setCurrentFillColor(RGB(229, 217, 91));
	fillRect(11,start_y1+dy,148,4);
	setCurrentFillColor(RGB(240, 78, 81));
	fillRect(11,start_y1+2*dy,148,4);
	setCurrentFillColor(RGB(33, 238, 124));
	fillRect(164,start_y2,148,4);
	setCurrentFillColor(RGB(255, 131, 77));
	fillRect(164,start_y2 + dy2,148,4);

	
	setCurrentDrawColor(RGB(33, 135, 145));
	setCurrentFillColor(RGB(33, 135, 145));
	drawCircleAA(text_x1 - config_WND_CIRCLE_SIZE * 2, start_y1 + 30, config_WND_CIRCLE_SIZE);
	fillCircle(text_x1 - config_WND_CIRCLE_SIZE * 2, start_y1 + 30, config_WND_CIRCLE_SIZE);
	drawCircleAA(text_x1 - config_WND_CIRCLE_SIZE * 2, start_y1 + 50, config_WND_CIRCLE_SIZE);
	fillCircle(text_x1 - config_WND_CIRCLE_SIZE * 2, start_y1 + 50, config_WND_CIRCLE_SIZE);

	setCurrentFillColor(RGB(229, 217, 91));
	fillCircle(text_x1 - config_WND_CIRCLE_SIZE*2, start_y1+dy+30, config_WND_CIRCLE_SIZE);
	fillCircle(text_x1 - config_WND_CIRCLE_SIZE*2, start_y1+dy+50, config_WND_CIRCLE_SIZE);
	setCurrentFillColor(RGB(240, 78, 81));
	fillCircle(text_x1 - config_WND_CIRCLE_SIZE*2, start_y1+dy*2+30, config_WND_CIRCLE_SIZE);
	fillCircle(text_x1 - config_WND_CIRCLE_SIZE*2, start_y1+dy*2+50, config_WND_CIRCLE_SIZE);

	setCurrentFillColor(RGB(33, 238, 124));
	fillCircle(text_x2 - config_WND_CIRCLE_SIZE*2, start_y2+30, config_WND_CIRCLE_SIZE);
	fillCircle(text_x2 - config_WND_CIRCLE_SIZE*2, start_y2+50, config_WND_CIRCLE_SIZE);
	fillCircle(text_x2 - config_WND_CIRCLE_SIZE*2, start_y2+70, config_WND_CIRCLE_SIZE);

	setCurrentDrawColor(RGB(255, 131, 77));
	setCurrentFillColor(RGB(255, 131, 77));
	drawCircleAA(text_x2 - config_WND_CIRCLE_SIZE*2, start_y2+dy2+30, config_WND_CIRCLE_SIZE);
	fillCircle(text_x2 - config_WND_CIRCLE_SIZE * 2, start_y2 + dy2 + 30, config_WND_CIRCLE_SIZE);

	drawCircleAA(text_x2 - config_WND_CIRCLE_SIZE*2, start_y2+dy2+50, config_WND_CIRCLE_SIZE);
	fillCircle(text_x2 - config_WND_CIRCLE_SIZE * 2, start_y2 + dy2 + 50, config_WND_CIRCLE_SIZE);

	drawCircleAA(text_x2 - config_WND_CIRCLE_SIZE*2, start_y2+dy2+70, config_WND_CIRCLE_SIZE);
	fillCircle(text_x2 - config_WND_CIRCLE_SIZE * 2, start_y2 + dy2 + 70, config_WND_CIRCLE_SIZE);

	setFont(&impact_10ptFontInfo);
	setCurrentTextColor(RGB(192,192,192));
	drawString("Temperature", text_x1 - 10, start_y1+5);
	drawString("Pressure & Humidity",text_x1 - 10, start_y1+dy+5);
	drawString("Light", text_x1 - 10, start_y1+dy*2+5);
	drawString("Accelerometer", text_x2 - 10, start_y2+5);
	drawString("Magnetometer", text_x2 - 10, start_y2+dy2+5);

	setFont(&impact_12ptFontInfo);
	setCurrentTextColor(config_BIG_TEXT_COLOR);

	drawString("T0 = +0.0|C", text_x1, start_y1+22);		//	reference T0

	//	output temperature data
	int32_t value = logger.getTemperature(0);
	char str[32] = {"T0: "};
	strUCat(str,value/100,10);
	strCat(str,".");
	strUCat(str,value%100,10);
	strCat(str,"|C");
	drawString(str, text_x1, start_y1+42);

	//	output pressure data
	strCopy(str,"P: ");
	strUCat(str,logger.getPressure(0)/25600,10);
	strCat(str,".");
	strUCat(str,(logger.getPressure(0)%25600)/10,10);
	strCat(str,"hPa");
	drawString(str,text_x1, start_y1+dy+22);

	//	output humidity data
	strCopy(str,"H: ");
	strUCat(str,logger.getHumidity(0)/1024,10);
	strCat(str,".");
	strUCat(str,logger.getHumidity(0)%1024,10);
	strCat(str,"%");
	drawString(str,text_x1, start_y1+dy+42);

	//	output ALS value
//	strCopy(str,"Light: ");
	*str = 0;
	uint32_t light_val = logger.getALS(0);
	if(light_val/1000 > 100)
	{
		strUCat(str,light_val/1000,10);
	}
	else
	{
		strUCat(str,light_val/1000,10);
		strCat(str,".");
		strUCat(str,light_val%1000,10);
	}
	strCat(str," : ");
	strUCat(str,logger.getIR(0),10);
	drawString(str,text_x1, start_y1+dy*2+22);

	TRACE("UV: ");
	TRACE(utoa(logger.getUV(0),str));
	TRACE(" : ");
	TRACE(utoa(logger.getIR(0),str));
	TRACE("\n");

	AccelerometerData_t *pAccData = logger.getACC();
	strCopy(str,"X: ");
	strICat(str,pAccData->x);
	drawString(str, text_x2, start_y2+22);
	strCopy(str,"Y: ");
	strICat(str,pAccData->y);
	drawString(str, text_x2, start_y2+42);
	strCopy(str,"Z: ");
	strICat(str,pAccData->z);
	drawString(str, text_x2, start_y2+62);

	MagnetometerData_t *pMgnData = logger.getMGN();
	strCopy(str,"X: ");
	strICat(str,pMgnData->x);
	drawString(str, text_x2, start_y2+dy2+22);
	strCopy(str,"Y: ");
	strICat(str,pMgnData->y);
	drawString(str, text_x2, start_y2+dy2+42);
	strCopy(str,"Z: ");
	strICat(str,pMgnData->z);
	drawString(str, text_x2, start_y2+dy2+62);

	RGB32_t *rgb = logger.getRGB(0);
//	strCopy(str,"RGB:");
	*str = 0;
	strUCat(str,rgb->r,10);
	strCat(str,":");
	strUCat(str,rgb->g,10);
	strCat(str,":");
	strUCat(str,rgb->b,10);
	drawString(str,text_x1, start_y1+dy*2+42);
/*
	TRACE(utoa(rgb->r,str));
	TRACE(" : ");
	TRACE(utoa(rgb->g,str));
	TRACE(" : ");
	TRACE(utoa(rgb->b,str));

	TRACE(" : ");
	TRACE(itoa(-189230,str));
	TRACE("\n");
*/

//	sprintf(str,"T1 = %d.%u|C",m_CurrentLoggerData.m_iIemperature[0]/100, m_CurrentLoggerData.m_iIemperature[0]%100);
/*	SL_Temperature_t *t = (SL_Temperature_t *)deviceGetSensorData(SL_SensorType_Temperature, config_SENSOR_ID_DEFAULT_INT);
	if(t)
	{
		sprintf(str,"T1 = %d.%u|C",*t/100, *t%100);
		drawString(str, text_x1, start_y1+42);
	}
*/
	//	sprintf(str,"P = %luPa",m_CurrentLoggerData.m_uPressure/256);
/*	SL_Pressure_t *p = (SL_Pressure_t*)deviceGetSensorData(SL_SensorType_Pressure, config_SENSOR_ID_DEFAULT_INT);
	if(p)
	{
		sprintf(str,"P = %luPa",*p/256);
		drawString(str,text_x1, start_y1+dy+22);
	}
*/

	//sprintf(str,"H = %u.%02u%%", (uint16_t)m_CurrentLoggerData.m_uHumidity/1024, (uint16_t)m_CurrentLoggerData.m_uHumidity%1024);
/*	SL_Humidity_t *h = (SL_Humidity_t*)deviceGetSensorData(SL_SensorType_Humidity, config_SENSOR_ID_DEFAULT_INT);
	if(h)
	{
		sprintf(str,"H = %u.%02u%%", (uint16_t)(*h/1024), (uint16_t)(*h%1024));
		drawString(str,text_x1, start_y1+dy+42);
	}
*/

//	if(m_CurrentLoggerData.m_uLight/1000 > 100)
//		sprintf(str,"Light = %u Lux ", (uint16_t)(m_CurrentLoggerData.m_uLight/1000));
//	else
//		sprintf(str,"Light = %u.%02u Lux ", (uint16_t)(m_CurrentLoggerData.m_uLight/1000),(uint16_t)m_CurrentLoggerData.m_uLight%1000);

/*	SL_ALS_t *als = (SL_ALS_t*)deviceGetSensorData(SL_SensorType_ALS, config_SENSOR_ID_DEFAULT_INT);
	if(als)
	{
		if(*als/1000 > 100)
			sprintf(str,"Light = %u Lux ", (uint16_t)(*als/1000));
		else
			sprintf(str,"Light = %u.%02u Lux ", (uint16_t)(*als/1000),(uint16_t)*als%1000);

		drawString(str,text_x1, start_y1+dy*2+22);
		drawString("UV = 0.0",text_x1, start_y1+dy*2+42);
	}
*/

/*	sprintf(str,"X = %d", m_CurrentLoggerData.m_uAccelerometer.m_iDataX);
	drawString(str, text_x2, start_y2+22);
	sprintf(str,"Y = %d", m_CurrentLoggerData.m_uAccelerometer.m_iDataY);
	drawString(str, text_x2, start_y2+42);
	sprintf(str,"Z = %d", m_CurrentLoggerData.m_uAccelerometer.m_iDataZ);
	drawString(str, text_x2, start_y2+62);
	*/
/*
	SL_AccelerometerData_t *acc = (SL_AccelerometerData_t*)deviceGetSensorData(SL_SensorType_Accelerometer, config_SENSOR_ID_DEFAULT_INT);
	if(acc)
	{
		sprintf(str,"X = %d", acc->m_iDataX);
		drawString(str, text_x2, start_y2+22);
		sprintf(str,"Y = %d", acc->m_iDataY);
		drawString(str, text_x2, start_y2+42);
		sprintf(str,"Z = %d", acc->m_iDataZ);
		drawString(str, text_x2, start_y2+62);
	}
*/
//	sprintf(str,"X = %d", m_CurrentLoggerData.m_uMagnetometer.m_uDataX);
//	drawString(str, text_x2, start_y2+dy2+22);
//	sprintf(str,"Y = %d", m_CurrentLoggerData.m_uMagnetometer.m_uDataY);
//	drawString(str, text_x2, start_y2+dy2+42);
//	sprintf(str,"Z = %d", m_CurrentLoggerData.m_uMagnetometer.m_uDataZ);
//	drawString(str, text_x2, start_y2+dy2+62);
/*
	SL_MagnetometerData_t *mgn = (SL_MagnetometerData_t*)deviceGetSensorData(SL_SensorType_Magnetometer, config_SENSOR_ID_DEFAULT_INT);
	if(mgn)
	{
		sprintf(str,"X = %d", mgn->m_uDataX);
		drawString(str, text_x2, start_y2+dy2+22);
		sprintf(str,"Y = %d", mgn->m_uDataY);
		drawString(str, text_x2, start_y2+dy2+42);
		sprintf(str,"Z = %d", mgn->m_uDataZ);
		drawString(str, text_x2, start_y2+dy2+62);
	}
	*/
}

void DashboardWnd::OnMsgReceive(MSG msg, void *pMsgData)
{
	GWnd::OnMsgReceive(msg,pMsgData);

	switch(msg)
	{
		case MSG_TYPE_BATTERY:
		{
			batCtrl.SetLevel(m_uCurrectBatteryLevel);
			Invalidate();
			break;
		}
		case MSG_TYPE_USB:
		{
			usbCtrl.SetConnected(*((bool*)pMsgData));
			break;
		}
		case MSG_TYPE_BLE:
		{
			bleCtrl.SetConnected(*((bool*)pMsgData));
			break;
		}
		case MSG_TYPE_BUTTON:
		{
			Exit(WND_RESULT_OK);
			break;
		}
		case MSG_TYPE_TOUCH:	break;
		default:	break;
	}
}




LightWnd::LightWnd(void)
{
	//	memset(&m_accData,0,sizeof(XYZ_RAWDATA_t));
	//	memset(&m_magnData,0,sizeof(XYZ_RAWDATA_t));
	m_iLightLimit = 10000;
}

void LightWnd::create(int16_t x, int16_t y, int16_t w, int16_t h)
{
	GWnd::create(x, y, w, h);

	HSV_t hsv1 = { .h = 142, .s = 219, .v = 56 };
	RGB_t rgb;

	batCtrl.create(300, 4, 16, 8);
	bleCtrl.create(262, 3, 8, 11);
	usbCtrl.create(254, 3, 8, 11);

	redValue.create(235, 50, 15, 160);
	greenValue.create(265, 50, 15, 160);
	blueValue.create(295, 50, 15, 160);

	alsGauge.create(60, 25, 100, 100);
	irGauge.create(115, 120, 100, 100);
	uvGauge.create(5, 120, 100, 100);

//	alsGauge.SetBgColor(RGB(8, 40, 56));
	HSV_ToRGB(&hsv1, &rgb);
	alsGauge.setBgColor(RGB(rgb.r, rgb.g, rgb.b));
	alsGauge.setFrontColor(RGB(96, 96, 96));

	hsv1.h = 0;
	hsv1.s = 192;
	hsv1.v = 32;
	HSV_ToRGB(&hsv1, &rgb);
	irGauge.setBgColor(RGB(rgb.r, rgb.g, rgb.b));
	irGauge.setFrontColor(RGB(96, 96, 96));

	hsv1.h = 200;
	hsv1.s = 128;
	hsv1.v = 32;
	HSV_ToRGB(&hsv1, &rgb);
	uvGauge.setBgColor(RGB(rgb.r, rgb.g, rgb.b));
	uvGauge.setFrontColor(RGB(96, 96, 96));

	redValue.SetRange(0, 10000);
	greenValue.SetRange(0, 10000);
	blueValue.SetRange(0, 10000);

	redValue.SetTicks(10);
	greenValue.SetTicks(10);
	blueValue.SetTicks(10);

	redValue.setTextColor(RGB(224, 0, 0));
	greenValue.setTextColor(RGB(0, 192, 0));
	blueValue.setTextColor(RGB(0, 0, 255));

	redValue.setFrontColor(RGB(224, 0, 0));
	greenValue.setFrontColor(RGB(0, 192, 0));
	blueValue.setFrontColor(RGB(0, 0, 255));

	redValue.setBgColor(RGB(32, 0, 0));
	greenValue.setBgColor(RGB(0, 32, 0));
	blueValue.setBgColor(RGB(0, 0, 32));

	alsGauge.SetText("ALS");
	irGauge.SetText("IR");
	uvGauge.SetText("UV");

	alsGauge.SetExtText("x100");
	irGauge.SetExtText("x100");
	uvGauge.SetExtText("x100");

	alsGauge.SetRange(0, 1000);
	irGauge.SetRange(0, 1000);
	uvGauge.SetRange(0, 1000);

	alsGauge.SetValue(0);
	irGauge.SetValue(10);
	uvGauge.SetValue(10);
}

void LightWnd::UpdateDateTime(void)
{
	char str[32];
	setFont(&calibri_8ptFontInfo);
	rtc.format(str, rtc.get());
	drawString(str, 145, 5);
}

void LightWnd::OnDraw(void)
{
	char str[8];
	RGB32_t *pRGB = logger.getRGB(0);
	RGB32_t rgb;
	rgb.r = pRGB->r;
	rgb.g = pRGB->g;
	rgb.b = pRGB->b;

	GWnd::OnDraw();

	setCurrentFillColor(config_WND_BG_COLOR);
	fillRect(0, 0, m_width, m_height);

	bleCtrl.SetConnected(BLE_IsConnected());

	//	controls are not drawing through the windows manager, so we should paint them manually
	batCtrl.OnDraw();
	bleCtrl.OnDraw();
	usbCtrl.OnDraw();

	uint32_t max = rgb.r;
	if (rgb.g > max)	max = rgb.g;
	if (rgb.b > max)	max = rgb.b;
	
	if (max >= 100000)	m_iLightLimit = 1000000;
	else if (max < 100000 && max >= 10000)	m_iLightLimit = 100000;
	else if (max < 10000 && max >= 1000)	m_iLightLimit = 10000;
	else if (max < 1000)	m_iLightLimit = 1000;
		
	redValue.SetRange(0, m_iLightLimit);
	greenValue.SetRange(0, m_iLightLimit);
	blueValue.SetRange(0, m_iLightLimit);
	
	uint32_t als_value = logger.getALS(0);
	uint32_t als_mult = 1;
	if (als_value > 1000000)
	{
		als_mult = 10000;
		alsGauge.SetExtText("x10000");
	}
	else if (als_value > 100000)
	{
		als_mult = 1000;
		alsGauge.SetExtText("x1000");
	}
	else if (als_value > 10000)
	{
		als_mult = 100;
		alsGauge.SetExtText("x100");
	}
	else if (als_value > 1000)
	{
		als_mult = 10;
		alsGauge.SetExtText("x10");
	}
	else
	{
		alsGauge.SetExtText("x1");
	}

	alsGauge.SetValue((int32_t)(als_value / als_mult));
	
	redValue.SetPos(rgb.r);
	greenValue.SetPos(rgb.g);
	blueValue.SetPos(rgb.b);

	redValue.OnDraw();
	greenValue.OnDraw();
	blueValue.OnDraw();

	alsGauge.OnDraw();
	irGauge.OnDraw();
	uvGauge.OnDraw();

	setTextAlignment(StringAlign_Center, StringAlign_Middle);
	setFont(&calibri_8ptFontInfo);
	setCurrentTextColor(RGB(232,32,32));
	drawStringInRect(utoa(rgb.r, str), redValue.getLeft(), redValue.getTop() + redValue.getHeight() + 8, redValue.getWidth(), 8);
	setCurrentTextColor(RGB(0, 200, 0));
	drawStringInRect(utoa(rgb.g, str), greenValue.getLeft(), greenValue.getTop() + greenValue.getHeight() + 8, greenValue.getWidth(), 8);
	setCurrentTextColor(RGB(32, 32, 255));
	drawStringInRect(utoa(rgb.b, str), blueValue.getLeft(), blueValue.getTop() + blueValue.getHeight() + 8, blueValue.getWidth(), 8);

	setCurrentDrawColor(config_WND_GRID_COLOR);
	drawHLine(10, 18, 300);

	setFont(&centuryGothic_14ptFontInfo);
	setCurrentTextColor(RGB(255, 255, 255));
	drawString("Light values", 20, 0);

	UpdateDateTime();

	uint32_t sum = rgb.r + rgb.g + rgb.b;

	uint32_t m1 = rgb.r * 256 / sum;
	uint32_t m2 = rgb.g * 256 / sum;
	uint32_t m3 = rgb.b * 256 / sum;

	setCurrentTextColor(RGB(32, 32, 32));
	setCurrentDrawColor(m_FrontColor);
	setCurrentFillColor(RGB((uint8_t)m1, (uint8_t)m2, (uint8_t)m3));
	drawRect(235, 30, 75, 15);
	fillRect(237, 32, 72, 12);
	drawStringInRect("3700K", 236, 33, 73, 13);

	/*
	drawCircleAA(100, 75, 39, RGB(40, 160, 40));
	drawCircleAA(100, 75, 40, RGB(40, 160, 40));

	drawCircleAA(50, 160, 39, RGB(160, 40, 40));
	drawCircleAA(50, 160, 40, RGB(160, 40, 40));

	drawCircleAA(150, 160, 39, RGB(40, 40, 255));
	drawCircleAA(150, 160, 40, RGB(40, 40, 255));

	DrawLineAA(10, 50, 180, 150, RGB(255, 255, 255));
	DrawLineAA(20, 50, 180, 140, RGB(0, 0, 255));
	DrawLineAA(30, 50, 180, 130, RGB(255, 0, 0));
	DrawLineAA(40, 50, 180, 120, RGB(0, 255, 0));
	*/

	/*
	for (int x = 50; x < 150; x++)
	{
		for (int y = 50; y < 150; y++)
		{
			DrawPixelA(x, y, RGB(0, 0, 128), 192);
		}
	}
	*/
	//**************************************************************
	/*
	FillRect(11, start_y1, 148, 65, RGB(8, 40, 56));
	FillRect(11, start_y1 + dy, 148, 65, RGB(8, 40, 56));
	FillRect(11, start_y1 + dy * 2, 148, 65, RGB(8, 40, 56));
	FillRect(164, start_y2, 148, 85, RGB(8, 40, 56));
	FillRect(164, start_y2 + dy2, 148, 85, RGB(8, 40, 56));

	FillRect(11, start_y1, 148, 4, RGB(33, 135, 145));
	FillRect(11, start_y1 + dy, 148, 4, RGB(229, 217, 91));
	FillRect(11, start_y1 + 2 * dy, 148, 4, RGB(240, 78, 81));
	FillRect(164, start_y2, 148, 4, RGB(33, 238, 124));
	FillRect(164, start_y2 + dy2, 148, 4, RGB(255, 131, 77));

	fillCircle(text_x1 - config_WND_CIRCLE_SIZE * 2, start_y1 + 30, config_WND_CIRCLE_SIZE, RGB(33, 135, 145));
	fillCircle(text_x1 - config_WND_CIRCLE_SIZE * 2, start_y1 + 50, config_WND_CIRCLE_SIZE, RGB(33, 135, 145));
	fillCircle(text_x1 - config_WND_CIRCLE_SIZE * 2, start_y1 + dy + 30, config_WND_CIRCLE_SIZE, RGB(229, 217, 91));
	fillCircle(text_x1 - config_WND_CIRCLE_SIZE * 2, start_y1 + dy + 50, config_WND_CIRCLE_SIZE, RGB(229, 217, 91));
	fillCircle(text_x1 - config_WND_CIRCLE_SIZE * 2, start_y1 + dy * 2 + 30, config_WND_CIRCLE_SIZE, RGB(240, 78, 81));
	fillCircle(text_x1 - config_WND_CIRCLE_SIZE * 2, start_y1 + dy * 2 + 50, config_WND_CIRCLE_SIZE, RGB(240, 78, 81));

	fillCircle(text_x2 - config_WND_CIRCLE_SIZE * 2, start_y2 + 30, config_WND_CIRCLE_SIZE, RGB(33, 238, 124));
	fillCircle(text_x2 - config_WND_CIRCLE_SIZE * 2, start_y2 + 50, config_WND_CIRCLE_SIZE, RGB(33, 238, 124));
	fillCircle(text_x2 - config_WND_CIRCLE_SIZE * 2, start_y2 + 70, config_WND_CIRCLE_SIZE, RGB(33, 238, 124));

	fillCircle(text_x2 - config_WND_CIRCLE_SIZE * 2, start_y2 + dy2 + 30, config_WND_CIRCLE_SIZE, RGB(255, 131, 77));
	fillCircle(text_x2 - config_WND_CIRCLE_SIZE * 2, start_y2 + dy2 + 50, config_WND_CIRCLE_SIZE, RGB(255, 131, 77));
	fillCircle(text_x2 - config_WND_CIRCLE_SIZE * 2, start_y2 + dy2 + 70, config_WND_CIRCLE_SIZE, RGB(255, 131, 77));

	SetFont(&impact_10ptFontInfo);
	setCurrentTextColor(RGB(192, 192, 192));
	drawString("Temperature", text_x1 - 10, start_y1 + 5);
	drawString("Pressure & Humidity", text_x1 - 10, start_y1 + dy + 5);
	drawString("Light", text_x1 - 10, start_y1 + dy * 2 + 5);
	drawString("Accelerometer", text_x2 - 10, start_y2 + 5);
	drawString("Magnetometer", text_x2 - 10, start_y2 + dy2 + 5);

	SetFont(&impact_12ptFontInfo);
	setCurrentTextColor(config_BIG_TEXT_COLOR);

	drawString("T0 = +0.0|C", text_x1, start_y1 + 22);		//	reference T0

															//	output temperature data
	int32_t value = logger.getTemperature(0);
	char str[32] = { "T0: " };
	strUCat(str, value / 100, 10);
	strCat(str, ".");
	strUCat(str, value % 100, 10);
	strCat(str, "|C");
	drawString(str, text_x1, start_y1 + 42);

	//	output pressure data
	strCopy(str, "P: ");
	strUCat(str, logger.getPressure(0) / 25600, 10);
	strCat(str, ".");
	strUCat(str, (logger.getPressure(0) % 25600) / 10, 10);
	strCat(str, "hPa");
	drawString(str, text_x1, start_y1 + dy + 22);

	//	output humidity data
	strCopy(str, "H: ");
	strUCat(str, logger.getHumidity(0) / 1024, 10);
	strCat(str, ".");
	strUCat(str, logger.getHumidity(0) % 1024, 10);
	strCat(str, "%");
	drawString(str, text_x1, start_y1 + dy + 42);

	//	output ALS value
	//	strCopy(str,"Light: ");
	*str = 0;
	uint32_t light_val = logger.getALS(0);
	if (light_val / 1000 > 100)
	{
		strUCat(str, light_val / 1000, 10);
	}
	else
	{
		strUCat(str, light_val / 1000, 10);
		strCat(str, ".");
		strUCat(str, light_val % 1000, 10);
	}
	strCat(str, " : ");
	strUCat(str, logger.getIR(0), 10);
	drawString(str, text_x1, start_y1 + dy * 2 + 22);

	TRACE("UV: ");
	TRACE(utoa(logger.getUV(0), str));
	TRACE(" : ");
	TRACE(utoa(logger.getIR(0), str));
	TRACE("\n");

	AccelerometerData_t *pAccData = logger.getACC();
	strCopy(str, "X: ");
	strICat(str, pAccData->x);
	drawString(str, text_x2, start_y2 + 22);
	strCopy(str, "Y: ");
	strICat(str, pAccData->y);
	drawString(str, text_x2, start_y2 + 42);
	strCopy(str, "Z: ");
	strICat(str, pAccData->z);
	drawString(str, text_x2, start_y2 + 62);

	MagnetometerData_t *pMgnData = logger.getMGN();
	strCopy(str, "X: ");
	strICat(str, pMgnData->x);
	drawString(str, text_x2, start_y2 + dy2 + 22);
	strCopy(str, "Y: ");
	strICat(str, pMgnData->y);
	drawString(str, text_x2, start_y2 + dy2 + 42);
	strCopy(str, "Z: ");
	strICat(str, pMgnData->z);
	drawString(str, text_x2, start_y2 + dy2 + 62);

	RGB32_t *rgb = logger.getRGB(0);
	//	strCopy(str,"RGB:");
	*str = 0;
	strUCat(str, rgb->r, 10);
	strCat(str, ":");
	strUCat(str, rgb->g, 10);
	strCat(str, ":");
	strUCat(str, rgb->b, 10);
	drawString(str, text_x1, start_y1 + dy * 2 + 42);
	*/
}

void LightWnd::OnMsgReceive(MSG msg, void *pMsgData)
{
	GWnd::OnMsgReceive(msg, pMsgData);

	switch (msg)
	{
	case MSG_TYPE_BATTERY:
	{
		batCtrl.SetLevel(m_uCurrectBatteryLevel);
		Invalidate();
		break;
	}
	case MSG_TYPE_USB:
	{
		usbCtrl.SetConnected(*((bool*)pMsgData));
		break;
	}
	case MSG_TYPE_BLE:
	{
		bleCtrl.SetConnected(*((bool*)pMsgData));
		break;
	}
	case MSG_TYPE_BUTTON:
	{
		Exit(WND_RESULT_OK);
		break;
	}
	case MSG_TYPE_TOUCH:	
		break;
	default:	
		break;
	}
}

// --- Temperature, Pressure and Humidity window

float uTempBuffer[64];
float uHumidityBuffer[64];
float uPressureBuffer[64];
uint16_t uDataIndex = 0;

TphWnd::TphWnd(void)
{

}


void TphWnd::create(int16_t x, int16_t y, int16_t w, int16_t h)
{
	GWnd::create(x, y, w, h);

	batCtrl.create(300, 4, 16, 8);
	bleCtrl.create(262, 3, 8, 11);
	usbCtrl.create(254, 3, 8, 11);

	graphWnd.create(10,30,130,80);

	tGauge.create(160, 25, 100, 100);
	hGauge.create(215, 120, 100, 100);
	pGauge.create(105, 120, 100, 100);

	pGauge.SetAngles(-45, 225);

	sectorCtrl.create(1, 120, 100, 100, 10);

	tGauge.setBgColor(RGB(8, 40, 56));
	pGauge.setBgColor(RGB(32, 40, 56));
	hGauge.setBgColor(RGB(8, 48, 40));
	
	tGauge.setFrontColor(RGB(96, 96, 96));
	pGauge.setFrontColor(RGB(96, 96, 96));
	hGauge.setFrontColor(RGB(96, 96, 96));

	tGauge.SetText("T");
	pGauge.SetText("P");
	hGauge.SetText("H");

//	tGauge.SetExtText("|C");
	tGauge.SetExtText("C");
	pGauge.SetExtText("hPa");
	hGauge.SetExtText("%");

	tGauge.SetRange(0, 10000);
	pGauge.SetRange(80000, 120000);
	hGauge.SetRange(0, 10000);

	pGauge.SetScaleStartValue(8);

	pGauge.SetTicks(40);
	pGauge.SetBigTicks(10);

	tGauge.SetValueDivider(100);
	pGauge.SetValueDivider(100);
	hGauge.SetValueDivider(100);

	tGauge.SetValue(20);
	pGauge.SetValue(1000);
	hGauge.SetValue(80);

//	graphWnd.SetApperance(0, GraphicType_Line, RGB(32, 32, 200));
	graphWnd.SetApperance(0, GraphicType_Bars, RGB(255, 0, 0));	//	temperature
	graphWnd.SetApperance(1, GraphicType_Line, RGB(0, 0, 255));	//	pressure
	graphWnd.SetApperance(2, GraphicType_Line, RGB(0, 128, 0));	//	humidity
}

void TphWnd::UpdateDateTime(void)
{
	char str[32];
	setFont(&calibri_8ptFontInfo);
	rtc.format(str, rtc.get());
	drawString(str, 145, 5);
}

void TphWnd::OnDraw(void)
{
	GWnd::OnDraw();

	setCurrentFillColor(config_WND_BG_COLOR);
	setCurrentDrawColor(config_WND_GRID_COLOR);
	setCurrentTextColor(config_TEXT_COLOR_HEADER);
	setFont(&centuryGothic_14ptFontInfo);

	bleCtrl.SetConnected(BLE_IsConnected());

	fillRect(0, 0, m_width, m_height);
	drawHLine(10, 18, 300);
	drawString("TPH values", 20, 0);

	UpdateDateTime();

	tGauge.SetValue(logger.getTemperature(0));
	pGauge.SetValue(logger.getPressure(0)/256);
	hGauge.SetValue(logger.getHumidity(0)*100/1024);

	batCtrl.OnDraw();
	bleCtrl.OnDraw();
	usbCtrl.OnDraw();

	tGauge.OnDraw();
	pGauge.OnDraw();
	hGauge.OnDraw();

	if (logger.getTemperature(0))
	{
		if (uDataIndex == 63)
		{
			memmove(&uTempBuffer[0], &uTempBuffer[1], 63 * sizeof(uTempBuffer[0]));
			memmove(&uHumidityBuffer[0], &uHumidityBuffer[1], 63 * sizeof(uHumidityBuffer[0]));
			memmove(&uPressureBuffer[0], &uPressureBuffer[1], 63 * sizeof(uPressureBuffer[0]));

			uTempBuffer[63] = ((float)logger.getTemperature(0)) / 100.0f;
			uHumidityBuffer[63] = (float)logger.getHumidity(0) / 1024.0f;
			uPressureBuffer[63] = (float)logger.getPressure(0) / 25600.0f;


			graphWnd.SetData(uTempBuffer, 64, 0, 0.01f);
			graphWnd.SetData(uPressureBuffer, 64, 1, 0.01f);
			graphWnd.SetData(uHumidityBuffer, 64, 2, 0.01f);
		}
		else
		{
			uTempBuffer[uDataIndex] = ((float)logger.getTemperature(0)) / 100.0f;
			uHumidityBuffer[uDataIndex] = (float)logger.getHumidity(0) / 1024.0f;
			uPressureBuffer[uDataIndex] = (float)logger.getPressure(0) / 25600.0f;
			uDataIndex++;

			graphWnd.SetData(uTempBuffer, uDataIndex, 0, 0.01f);
			graphWnd.SetData(uPressureBuffer, uDataIndex, 1, 0.01f);
			graphWnd.SetData(uHumidityBuffer, uDataIndex, 2, 0.1f);
		}

	}

	graphWnd.OnDraw();
	sectorCtrl.OnDraw();
}

void TphWnd::OnMsgReceive(MSG msg, void *pMsgData)
{
	GWnd::OnMsgReceive(msg, pMsgData);

	switch (msg)
	{
	case MSG_TYPE_BATTERY:
	{
		batCtrl.SetLevel(m_uCurrectBatteryLevel);
		Invalidate();
		break;
	}
	case MSG_TYPE_USB:
	{
		usbCtrl.SetConnected(*((bool*)pMsgData));
		break;
	}
	case MSG_TYPE_BLE:
	{
		bleCtrl.SetConnected(*((bool*)pMsgData));
		break;
	}
	case MSG_TYPE_BUTTON:
	{
		Exit(WND_RESULT_OK);
		break;
	}
	case MSG_TYPE_TOUCH:
		break;
	default:
		break;
	}
}


// ----- TIME window -----------

TimeWnd::TimeWnd(void)
{
	cx = 120;
	cy = 130;
}


void TimeWnd::create(int16_t x, int16_t y, int16_t w, int16_t h)
{
	GWnd::create(x, y, w, h);

	batCtrl.create(300, 4, 16, 8);
	bleCtrl.create(262, 3, 8, 11);
	usbCtrl.create(254, 3, 8, 11);

	uint16_t rS = 100;
	uint16_t rM = 90;
	uint16_t rH = 70;
	
	sectorSCtrl.create(cx - rS, cy - rS, 2 * rS, 2 * rS, 6);
	sectorMCtrl.create(cx - rM, cy - rM, 2 * rM, 2 * rM, 10);
	sectorHCtrl.create(cx - rH, cy - rH, 2 * rH, 2 * rH, 14);

	tempCtrl.create(240, 50, 60, 60, 8);
	presCtrl.create(240, 180, 60, 60, 8);

	sectorHCtrl.setAngles(0, 360);
	sectorMCtrl.setAngles(0, 360);
	sectorSCtrl.setAngles(0, 360);

	tempCtrl.setAngles(270, 450);
	presCtrl.setAngles(270, 450);

	sectorHCtrl.SetRange(0, 12);
	sectorMCtrl.SetRange(0, 60);
	sectorSCtrl.SetRange(0, 60);
	sectorSCtrl.SetPos(60);

	tempCtrl.SetRange(0, 40);
	tempCtrl.SetPos(33);

	presCtrl.SetRange(0, 1100);
	presCtrl.SetPos(1000);

	tempCtrl.setFrontColor(RGB(128, 64, 32));
	presCtrl.setFrontColor(RGB(32, 128, 64));

	sectorMCtrl.setBorders(true);

	HSV_t hsv = {.h = 156, .s = 240, .v = 156};
	RGB_t rgb;
	HSV_ToRGB(&hsv, &rgb);
	sectorHCtrl.setFrontColor(RGB(rgb.r, rgb.g, rgb.b));
	hsv.s -= 48;
	hsv.v -= 32;
	HSV_ToRGB(&hsv, &rgb);
	sectorMCtrl.setFrontColor(RGB(rgb.r, rgb.g, rgb.b));
	hsv.s -= 48;
	hsv.v -= 32;
	HSV_ToRGB(&hsv, &rgb);
	sectorSCtrl.setFrontColor(RGB(rgb.r, rgb.g, rgb.b));
}

void TimeWnd::UpdateDateTime(void)
{
	char str[32];
	setFont(&calibri_8ptFontInfo);
	rtc.format(str, rtc.get());

	setCurrentTextColor(config_TEXT_COLOR_HEADER);
	drawString(str, 145, 5);

	setCurrentTextColor(RGB(96, 96, 128));

	setFont(&impact_14ptFontInfo);
	setTextAlignment(StringAlign_Center, StringAlign_Middle);
	rtc.formatTime(str, rtc.get());
	drawStringInRect(str, 70, 110, 100, 20);

	setFont(&impact_10ptFontInfo);
	rtc.formatDate(str, rtc.get());
	drawStringInRect(str, 70, 135, 100, 20);
}

void TimeWnd::OnDraw(void)
{
	GWnd::OnDraw();

	setCurrentFillColor(config_WND_BG_COLOR);
	setCurrentDrawColor(RGB(48, 56, 64));
	setCurrentTextColor(config_TEXT_COLOR_HEADER);
	setFont(&centuryGothic_14ptFontInfo);

	bleCtrl.SetConnected(BLE_IsConnected());

	fillRect(0, 0, m_width, m_height);
	drawHLine(10, 18, 300);
	drawString("Time", 20, 0);

	UpdateDateTime();

	for (int16_t a = 0; a < 360; a += 6)
	{
		double a1 = (double)a * M_PI / 180;
		int r0 = 72;
		int r1 = 78;

		if (a % 5 == 0)		r0 = 50;
	//	if (a % 15 == 0)	r0 = 40;

		int16_t x0 = (int16_t)round((double)(r0) * cos(a1));
		int16_t y0 = (int16_t)round((double)(r0) * sin(a1));
		int16_t x1 = (int16_t)round((double)(r1) * cos(a1));
		int16_t y1 = (int16_t)round((double)(r1) * sin(a1));

		drawLineAA(cx - x0, cy - y0, cx - x1, cy - y1);
	}

	batCtrl.OnDraw();
	bleCtrl.OnDraw();
	usbCtrl.OnDraw();

	date_time_t* dt = rtc.get();
	sectorHCtrl.SetPos(dt->hours % 12);
	sectorMCtrl.SetPos(dt->minutes);

	int16_t current_angle = dt->seconds * 6;

	sectorSCtrl.setAngles(current_angle - 30, current_angle);

	sectorHCtrl.OnDraw();
	sectorMCtrl.OnDraw();
	sectorSCtrl.OnDraw();

	current_angle += 90;
	
	double a1 = (double)current_angle * M_PI / 180;
	int16_t x = (int16_t)round((double)(105) * cos(a1));
	int16_t y = (int16_t)round((double)(105) * sin(a1));

	setCurrentFillColor(RGB(32,32,160));
	fillCircle(cx - x, cy - y, 2);

	tempCtrl.OnDraw();
	presCtrl.OnDraw();

	setFont(&impact_10ptFontInfo);

	int32_t t = logger.getTemperature(0);
	char str[16] = { 0 };
	strUCat(str, t / 100, 10);
//	strCat(str, ".");
//	strUCat(str, t % 100, 10);
	strCat(str, "|C");
	drawString(str, 260, 73);

	//	output pressure data
	*str = 0;
	strUCat(str, logger.getPressure(0) / 25600, 10);
//	strCat(str, ".");
//	strUCat(str, (logger.getPressure(0) % 25600) / 10, 10);
	strCat(str, "hPa");
	drawString(str, 247, 215);
}

void TimeWnd::OnMsgReceive(MSG msg, void *pMsgData)
{
	GWnd::OnMsgReceive(msg, pMsgData);

	switch (msg)
	{
	case MSG_TYPE_BATTERY:
	{
		batCtrl.SetLevel(m_uCurrectBatteryLevel);
		Invalidate();
		break;
	}
	case MSG_TYPE_USB:
	{
		usbCtrl.SetConnected(*((bool*)pMsgData));
		break;
	}
	case MSG_TYPE_BLE:
	{
		bleCtrl.SetConnected(*((bool*)pMsgData));
		break;
	}
	case MSG_TYPE_BUTTON:
	{
		Exit(WND_RESULT_OK);
		break;
	}
	case MSG_TYPE_TOUCH:
		break;
	default:
		break;
	}
}