/*
 * ble_host.h
 *
 *  Created on: 16.01.2016
 *      Author: Yuriy
 */

#ifndef BLE_HOST_H_
#define BLE_HOST_H_

#include <stdbool.h>
#include <stdint.h>

//#include "em_usart.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
	GATT_RequestTypeRead,
	GATT_RequestTypeWrite,
	GATT_RequestTypeNotify,
	GATT_RequestTypeMax
} GATT_RequestType_t;

void BLE_HostInit(void);
bool BLE_IsEvent(void);
void BLE_ProcessEvent(void);
bool BLE_IsConnected(void);
bool BLE_NotificationSend(uint16_t uCharacteristic);

void taskBleControl(void *pParmas);

typedef struct
{
	bool (*m_pRequestHandler)(uint8_t *pData, uint8_t *pDataLen, GATT_RequestType_t type);
	uint16_t m_pCharacteristic;
} BLE_CharList_t;


#ifdef __cplusplus
}
#endif

#endif /* BLE_HOST_H_ */
