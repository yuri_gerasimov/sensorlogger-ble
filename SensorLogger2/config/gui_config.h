/*
 * gui_config.h
 *
 *  Created on: 17 ���. 2016 �.
 *      Author: Yuriy
 */

#ifndef CONFIG_GUI_CONFIG_H_
#define CONFIG_GUI_CONFIG_H_

#define config_WND_HEADER_WIDTH 4

#define config_WND_BG_COLOR RGB(7,30,40)
#define config_WND_HEADER_COLOR RGB(33,135,145)

#define config_GRID_MINIMUM_SIZE 5
#define config_WND_GRID_COLOR RGB(32,48,56)

#define config_TEXT_COLOR_HEADER RGB(192,192,222)
#define config_BIG_TEXT_COLOR RGB(128,128,128)

#define config_WND_DEFAULT_TEXT_COLOR RGB(128,128,128)

#define config_WND_CIRCLE_SIZE 4


#endif /* CONFIG_GUI_CONFIG_H_ */
