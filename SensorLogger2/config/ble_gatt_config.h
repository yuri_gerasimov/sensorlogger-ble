/*
 * ble_gatt_config.h
 *
 *  Created on: 16.01.2016
 *      Author: Yuriy
 */

#ifndef BLE_GATT_CONFIG_H_
#define BLE_GATT_CONFIG_H_

//****** CONTROLLING characteristics ***************
#define gattdb_device_name                      3
#define gattdb_device_params                   17
#define gattdb_device_control                  20
#define gattdb_coeff_params                    22

//****** DATA characteristics ***************
#define gattdb_pressure_params                 25
#define gattdb_engine_params                   28
#define gattdb_acc_params                      31
#define gattdb_temp_params                     34


#define GATTDB_LIST_SIZE 8
#define GATTDB_DATA_SIZE_MAX 32


#endif /* BLE_GATT_CONFIG_H_ */

