/*
 * ble_host_config.h
 *
 *  Created on: 16.01.2016
 *      Author: Yuriy
 */

#ifndef BLE_HOST_CONFIG_H_
#define BLE_HOST_CONFIG_H_

#define BLE_USART_LOCATION 	UART_ROUTE_LOCATION_LOC3
#define BLE_USART UART1
#define BLE_CLOCK cmuClock_UART1
#define BLE_RX_IRQ UART1_RX_IRQn
#define BLE_TX_IRQ UART1_TX_IRQn
#define BLE_TX_PIN 	gpioPortE,2
#define BLE_RX_PIN 	gpioPortE,3

#define BGLIB_QUEUE_LEN 4 // reduce memory usage by BGLin for BLE

#endif /* BLE_HOST_CONFIG_H_ */

