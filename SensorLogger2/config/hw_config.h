/*
 * hw_config.h
 *
 *  Created on: 17 Aug. 2016
 * 	Author: Yuriy Gerasimov  (yuri.y.gerasimov@gmail.com)
 */

#ifndef CONFIG_HW_CONFIG_H_
#define CONFIG_HW_CONFIG_H_

// -------------------------------------------------
//	*** GPIO pins definition
// -------------------------------------------------

#define BUTTON_BIT			(7)
#define BUTTON_PIN			gpioPortA,7
#define LCD_BACKLIGHT_PIN	gpioPortA,8
#define ACCEL_INT1_PIN		gpioPortA,9
#define ACCEL_INT2_PIN		gpioPortA,10
#define CAPTOUCH_INT_PIN	gpioPortA,11

#define LIGHT_INT_PIN		gpioPortC,11

#define LCD_TE_PIN			gpioPortB,3
#define LCD_RESET_PIN		gpioPortB,4
#define RAM_ENABLE_PIN		gpioPortB,5
#define LCD_ENABLE_PIN		gpioPortB,6
#define MIC_ENABLE_PIN		gpioPortB,11
#define MICROSD_ENABLE_PIN	gpioPortB,12

#define ONEWIRE_PIN			gpioPortD,5
#define CHARGE_STATUS_PIN	gpioPortD,7

#define LED_STATUS_PIN		gpioPortF,5

#define HFXO_FREQUENCY		(48000000)

#define RAM_EBI_BANK		EBI_BANK0
#define RAM_SIZE_BYTES		(0x100000)
#define RAM_SIZE_WORDS		(0x80000)

#define LCD_EBI_BANK		EBI_BANK1

#define LCD_BACKLIGHT_PWM_TIMER	TIMER2
#define LCD_BACKLIGHT_PWM_CLOCK	cmuClock_TIMER2


// ----- LCD & Frame Buffer configuration --------------------

#define TFT_INTERFACE_8080_16BIT_EBI
//#define TFT_INTERFACE_8080_16BIT_GPIO

#ifdef TFT_INTERFACE_8080_16BIT_EBI
	#define config_TFT_DC_ADDR_BIT		(19)
	#define config_TFT_EBI_BANK 		EBI_BANK1
#endif

#define config_TFT_WIDTH			(240)	//	because of LCD portrait orientation
#define config_TFT_HEIGHT			(320)
#define config_TFT_BPS				(16)

#define TFT_USE_FRAME_BUFFER		//	use SRAM frame buffer for TFT LCD driver

#define TFT_USE_FRAME_BUFFER_DMA	//	use DMA for frame buffer copy to GRAM

//	LCD TFT interface GPIO configuration

#define config_LCD_LB_PORT 		gpioPortE
#define config_LCD_HB_PORT 		gpioPortA

#define config_LCD_LB_MASK 		0xFF00
#define config_LCD_HB_MASK 		0x007F

//	---	Control pins ---
#define config_LCD_RESET_PIN 	gpioPortB,4
#define config_LCD_CS_PIN 		gpioPortD,10
#define config_LCD_WE_PIN 		gpioPortF,8
#define config_LCD_RE_PIN 		gpioPortF,9
#define config_LCD_DC_PIN 		gpioPortB,2

//	---	Data pins ---
#define config_LCD_D0			gpioPortE,8
#define config_LCD_D1			gpioPortE,9
#define config_LCD_D2			gpioPortE,10
#define config_LCD_D3			gpioPortE,11
#define config_LCD_D4			gpioPortE,12
#define config_LCD_D5			gpioPortE,13
#define config_LCD_D6			gpioPortE,14
#define config_LCD_D7			gpioPortE,15
#define config_LCD_D8			gpioPortA,15
#define config_LCD_D9			gpioPortA,0
#define config_LCD_D10			gpioPortA,1
#define config_LCD_D11			gpioPortA,2
#define config_LCD_D12			gpioPortA,3
#define config_LCD_D13			gpioPortA,4
#define config_LCD_D14			gpioPortA,5
#define config_LCD_D15			gpioPortA,6

//	SPI module  settings for NOR flash and MicroSD card
/* Don't increase MICROSD_HI_SPI_FREQ beyond 12MHz. Next step will be 24MHz which is out of spec. */
#define MICROSD_HI_SPI_FREQ     12000000
#define MICROSD_LO_SPI_FREQ     100000

#define SPI_USART  				USART2
#define SPI_LOCATION           	USART_ROUTE_LOCATION_LOC0
#define SPI_CMUCLOCK        	cmuClock_USART2

#define SPI_MOSIPIN         	gpioPortC,2
#define SPI_MISOPIN         	gpioPortC,3
#define SPI_CLKPIN          	gpioPortC,4

#define NORFLASH_CSPIN         	gpioPortC,5
#define MICROSD_CSPIN          	gpioPortD,11

//-------------------------------------------------------------------------------------------------

#define config_SENSOR_NUMBER_T  (1)
//#define config_SENSOR_NUMBER_P  (1)
//#define config_SENSOR_NUMBER_H  (1)
//#define config_SENSOR_NUMBER_ACC  (1)
//#define config_SENSOR_NUMBER_MGN  (1)

#define config_SENSOR_NUMBER_ALS (1)

#endif /* CONFIG_HW_CONFIG_H_ */
