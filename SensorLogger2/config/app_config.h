/*
 * app_config.h
 *
 *  Created on: 17 ���. 2016 �.
 *      Author: Yuriy
 */

#ifndef CONFIG_APP_CONFIG_H_
#define CONFIG_APP_CONFIG_H_

#define config_FW_NAME "Sensor Logger BLE+"
#define config_FW_VERSION_STRING "1.1"
#define config_FW_VERSION_MINOR 1
#define config_FW_VERSION_MAJOR 1
#define config_FW_UPDATE_REQUEST 0x5A5A
#define config_FW_GUID {0x95,0xf1,0xd6,0xa0,0x07,0x11,0x14,0x77}
#define config_FW_GUID_SIZE 0x08

#define default_SENSOR_DATA_TIMEOUT (1000)

//--------------- BLE part config -------------------------------------------------

#define config_BLE_CONNECTION_MIN_INTERVAL 16
#define config_BLE_CONNECTION_MAX_INTERVAL 16
#define config_BLE_CONNECTION_LATENCY 10
#define config_BLE_CONNECTION_TIMEOUT 40

#define BLE_UART_BUFFERSIZE 64

/*
#define config_DEFAULT_DATA_TIMEOUT_ACC (100)
#define config_DEFAULT_DATA_TIMEOUT_TPH (5000)
#define config_DEFAULT_DATA_TIMEOUT_ALS (1000)

#define config_DEFAULT_DATA_TIMEOUT_RGB (1000)
#define config_DEFAULT_DATA_TIMEOUT_UV 	(1000)
#define config_DEFAULT_DATA_TIMEOUT_IR 	(1000)
*/

#endif /* CONFIG_APP_CONFIG_H_ */
