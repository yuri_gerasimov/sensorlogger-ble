#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

// --- Include EFM common library files ---
#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_rtc.h"
#include "em_gpio.h"
#include "em_usart.h"
#include "em_timer.h"
#include "em_dma.h"
#include "em_ebi.h"
#include "em_assert.h"
//#include "em_usb.h"

// --- Include configuration files ---
#include "hw_config.h"
#include "app_config.h"
#include "FreeRTOSConfig.h"

// --- FreeRTOS include files ---
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
//#include "croutine.h"
#include "timers.h"

#include "tft/TFT_ILI9341.h"
//#include "tft_driver.h"
#include "dmactrl.h"

// --- Segger RTT include file ---
#include "trace.h"

// --- EFM32 drivers include file ---
#include "rtc_cal.h"

// --- BGM111 drivers include file ---
#include "ble_host.h"
#include "ble_host_config.h"

// --- common include file ---
#include "str.h"

#include "delay.h"

#include "usb_ctrl.h"
#include "hid.h"
#include "cdc.h"

#include "msdd.h"
#include "ff.h"

//#include "NOR-S25FL.h"
#include "microsd.h"

//#include "usb_ctrl.h"

//#include "gui.h"
#include "AppGUI.h"

#include "SensorLogger.h"

// --- Constant definitions ---

#define STACK_SIZE_FOR_TASK    (configMINIMAL_STACK_SIZE + 10)

#define LOWEST_TASK_PRIORITY 	(tskIDLE_PRIORITY)
#define LOW_TASK_PRIORITY 		(tskIDLE_PRIORITY+1)
#define MIDDLE_TASK_PRIORITY 	(tskIDLE_PRIORITY+2)
#define HIGH_TASK_PRIORITY 		(tskIDLE_PRIORITY+3)

uint8_t m_uCurrectBatteryLevel = 100;
int16_t data[48] =
{
	25,	26, 27, 26, 26, 27,	27,  27, 28, 29, 30, 31, 32, 33, 33, 32, 31, 32, 31, 30, 31, 32, 30, 27, 
	25,	26, 27, 26, 26, 27,	27,  27, 28, 29, 30, 31, 32, 33, 33, 32, 31, 32, 31, 30, 29, 28, 27, 25
};


// --- Global variables definition ---
RealTimeClock rtc;

GTempGraphWnd wndTempGraph;
DashboardWnd wndDashboard;
LightWnd wndLight;
TphWnd wndTPH;
TimeWnd wndTime;

SensorLogger device;

TaskHandle_t hTaskTimeKeep = NULL;
TaskHandle_t hTaskGUI = NULL;
TaskHandle_t hTaskIoControl = NULL;
TaskHandle_t hTaskBleControl = NULL;

QueueHandle_t hIoQueue = NULL;

//TaskHandle_t hMsdTask = NULL;
TFT tft(config_TFT_WIDTH, config_TFT_HEIGHT, config_TFT_BPS, (Color_TypeDef *)EBI_BankAddress(RAM_EBI_BANK), 1);

USART_TypeDef *m_pSPI = SPI_USART;

// --- Local variables definition ---

//	--- Extern valiables definitions ---

// --- IRQ handlers definition ---


void RTC_IRQHandler(void)
{
	/* Clear interrupt source */
	RTC_IntClear(RTC_IFC_COMP0);
	rtc.handleTick();
}

// --- Static functions declarations ---

static void initClocks(void)
{
	// Configure for 48MHz HFXO operation of core clock

	/*
	 CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFXO);
	 CMU_ClockDivSet(cmuClock_HF,cmuClkDiv_1);
	 */
	CMU->CTRL = (CMU->CTRL & ~_CMU_CTRL_HFXOMODE_MASK) | CMU_CTRL_HFXOMODE_XTAL;
	CMU->CTRL = (CMU->CTRL & ~_CMU_CTRL_HFXOBOOST_MASK) | CMU_CTRL_HFXOBOOST_50PCENT;

	SystemHFXOClockSet(HFXO_FREQUENCY);
	CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFXO);

	//	Enable RTC with 1 sec interrupt
//	rtcSetup();

	/* Enable clocks */
	CMU_ClockEnable(cmuClock_HFPER, true);
	CMU_ClockEnable(cmuClock_GPIO, true);		//	for LED, BUTTON, SPI's CS pins
	CMU_ClockEnable(cmuClock_EBI, true);		//	for SRAM and TFT driving
	CMU_ClockEnable(cmuClock_USART2, true);		//	for Bluetooth module, NOR flash and microSD card
	CMU_ClockEnable(BLE_CLOCK, true);
}

static void initGPIO(void)
{
	GPIO_PinModeSet(LED_STATUS_PIN, gpioModePushPull, 1);

	GPIO_PinModeSet(RAM_ENABLE_PIN, gpioModePushPull, 1);
	GPIO_PinModeSet(LCD_ENABLE_PIN, gpioModePushPull, 1);
	GPIO_PinModeSet(MIC_ENABLE_PIN, gpioModePushPull, 1);
	GPIO_PinModeSet(MICROSD_ENABLE_PIN, gpioModePushPull, 1);

	GPIO_PinModeSet(BUTTON_PIN, gpioModeInputPull, 1);
	GPIO_IntConfig(BUTTON_PIN, false, true, true);

	/* Enable interrupt in core for even and odd gpio interrupts */
	NVIC_ClearPendingIRQ(GPIO_EVEN_IRQn);
	NVIC_EnableIRQ(GPIO_EVEN_IRQn);
	NVIC_ClearPendingIRQ(GPIO_ODD_IRQn);
	NVIC_EnableIRQ(GPIO_ODD_IRQn);
	NVIC_SetPriority(GPIO_EVEN_IRQn, 6);
	NVIC_SetPriority(GPIO_ODD_IRQn, 6);
}

void GPIO_ODD_IRQHandler(void)
{
	/* Acknowledge interrupt */
	GPIO_IntClear(1 << BUTTON_BIT);

	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	uint32_t uValue = MSG_TYPE_BUTTON;
	xQueueSendFromISR(hIoQueue, &uValue, &xHigherPriorityTaskWoken);
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/**************************************************************************//**
 * @brief GPIO Interrupt handler (PB10)
 *        Sets the minutes
 *****************************************************************************/
void GPIO_EVEN_IRQHandler(void)
{
	/* Acknowledge interrupt */
	GPIO_IntClear(1 << 2);
}

static void initSPI(void)
{
	/* Initialize USART in SPI master mode first. */
	USART_InitSync_TypeDef init = USART_INITSYNC_DEFAULT;

	init.baudrate = MICROSD_LO_SPI_FREQ;
	init.msbf = true;
	USART_InitSync(m_pSPI, &init);

	/* Enabling pins and setting location, SPI CS not enable */
	m_pSPI->ROUTE = USART_ROUTE_TXPEN | USART_ROUTE_RXPEN | USART_ROUTE_CLKPEN | SPI_LOCATION;

#if defined( USART_CTRL_SMSDELAY )
	/* This will allow us to use higher baudrate. */
	m_pSPI->CTRL |= USART_CTRL_SMSDELAY;
#endif

	GPIO_PinModeSet(SPI_MOSIPIN, gpioModePushPull, 0);  	/* MOSI */
	GPIO_PinModeSet(SPI_MISOPIN, gpioModeInputPull, 1); 	/* MISO */
	GPIO_PinModeSet(SPI_CLKPIN, gpioModePushPull, 0);  	/* CLK */
}


//	init USART in UART mode for BGM111 Bluetooth BLE module communication at UART1 location #3
static void initUSART(void)
{
	TRACE("Init UART... ");

	USART_InitAsync_TypeDef initasync = USART_INITASYNC_DEFAULT;

	initasync.baudrate = 115200;
	initasync.databits = usartDatabits8;
	initasync.parity = usartNoParity;
	initasync.stopbits = usartStopbits1;
	initasync.oversampling = usartOVS16;
#if defined( USART_INPUT_RXPRS ) && defined( USART_CTRL_MVDIS )
	initasync.mvdis = 0;
	initasync.prsRxEnable = 0;
	initasync.prsRxCh = usartPrsRxCh0;
#endif

	GPIO_PinModeSet(BLE_TX_PIN, gpioModePushPull, 0);		//	TX as output
	GPIO_PinModeSet(BLE_RX_PIN, gpioModeInput, 0);		//	RX as input

	USART_InitAsync(BLE_USART, &initasync);

	// Prepare UART Rx and Tx interrupts
	USART_IntClear(BLE_USART, _USART_IFC_MASK);
	USART_IntEnable(BLE_USART, USART_IEN_RXDATAV);		// < RX Data Valid Interrupt Enable

	NVIC_ClearPendingIRQ(BLE_RX_IRQ);
	NVIC_ClearPendingIRQ(BLE_TX_IRQ);
	NVIC_EnableIRQ(BLE_RX_IRQ);
	NVIC_EnableIRQ(BLE_TX_IRQ);

	//	set USART I/O location - Enable signals RX, TX at LOCATION #3
	BLE_USART->ROUTE = BLE_USART_LOCATION | UART_ROUTE_RXPEN | UART_ROUTE_TXPEN;
	TRACE("Ok\n");
}

static void initEBI(void)
{
	EBI_Init_TypeDef ebiInit = EBI_INIT_DEFAULT;

	//	GPIO configuration
	GPIO_PinModeSet(gpioPortF, 6, gpioModePushPull, 0);		//	LB - enabled by default
	GPIO_PinModeSet(gpioPortF, 7, gpioModePushPull, 0);		//	UB - enabled by default
	GPIO_PinModeSet(gpioPortF, 8, gpioModePushPull, 1);		//	WE
	GPIO_PinModeSet(gpioPortF, 9, gpioModePushPull, 1);		//	RE
	GPIO_PinModeSet(gpioPortD, 9, gpioModePushPull, 1);		//	RAM CS
	GPIO_PinModeSet(gpioPortD, 10, gpioModePushPull, 1);	//	TFT LCD CS

	GPIO_PinModeSet(gpioPortA, 12, gpioModePushPull, 0);	//	A0
	GPIO_PinModeSet(gpioPortA, 13, gpioModePushPull, 0);	//	A1
	GPIO_PinModeSet(gpioPortA, 14, gpioModePushPull, 0);	//	A2
	GPIO_PinModeSet(gpioPortB, 9, gpioModePushPull, 0);		//	A3
	GPIO_PinModeSet(gpioPortB, 10, gpioModePushPull, 0);	//	A4
	GPIO_PinModeSet(gpioPortC, 6, gpioModePushPull, 0);		//	A5
	GPIO_PinModeSet(gpioPortC, 7, gpioModePushPull, 0);		//	A6
	GPIO_PinModeSet(gpioPortE, 0, gpioModePushPull, 0);		//	A7
	GPIO_PinModeSet(gpioPortE, 1, gpioModePushPull, 0);		//	A8
	GPIO_PinModeSet(gpioPortC, 9, gpioModePushPull, 0);		//	A9
	GPIO_PinModeSet(gpioPortC, 10, gpioModePushPull, 0);	//	A10
	GPIO_PinModeSet(gpioPortE, 4, gpioModePushPull, 0);		//	A11
	GPIO_PinModeSet(gpioPortE, 5, gpioModePushPull, 0);		//	A12
	GPIO_PinModeSet(gpioPortE, 6, gpioModePushPull, 0);		//	A13
	GPIO_PinModeSet(gpioPortE, 7, gpioModePushPull, 0);		//	A14
	GPIO_PinModeSet(gpioPortC, 8, gpioModePushPull, 0);		//	A15
	GPIO_PinModeSet(gpioPortB, 0, gpioModePushPull, 0);		//	A16
	GPIO_PinModeSet(gpioPortB, 1, gpioModePushPull, 0);		//	A17
	GPIO_PinModeSet(gpioPortB, 2, gpioModePushPull, 0);		//	A18 / DC ( 0=command, 1=data)
//	GPIO_PinModeSet(gpioPortB, 3, gpioModePushPull, 0);		//	A19 / DC ( 0=command, 1=data)

	GPIO_PinModeSet(gpioPortE, 8, gpioModePushPull, 0);		//	D0
	GPIO_PinModeSet(gpioPortE, 9, gpioModePushPull, 0);		//	D1
	GPIO_PinModeSet(gpioPortE, 10, gpioModePushPull, 0);	//	D2
	GPIO_PinModeSet(gpioPortE, 11, gpioModePushPull, 0);	//	D3
	GPIO_PinModeSet(gpioPortE, 12, gpioModePushPull, 0);	//	D4
	GPIO_PinModeSet(gpioPortE, 13, gpioModePushPull, 0);	//	D5
	GPIO_PinModeSet(gpioPortE, 14, gpioModePushPull, 0);	//	D6
	GPIO_PinModeSet(gpioPortE, 15, gpioModePushPull, 0);	//	D7
	GPIO_PinModeSet(gpioPortA, 15, gpioModePushPull, 0);	//	D8
	GPIO_PinModeSet(gpioPortA, 0, gpioModePushPull, 0);		//	D9
	GPIO_PinModeSet(gpioPortA, 1, gpioModePushPull, 0);		//	D10
	GPIO_PinModeSet(gpioPortA, 2, gpioModePushPull, 0);		//	D11
	GPIO_PinModeSet(gpioPortA, 3, gpioModePushPull, 0);		//	D12
	GPIO_PinModeSet(gpioPortA, 4, gpioModePushPull, 0);		//	D13
	GPIO_PinModeSet(gpioPortA, 5, gpioModePushPull, 0);		//	D14
	GPIO_PinModeSet(gpioPortA, 6, gpioModePushPull, 0);		//	D15

	/* Start with initializing the base EBI */
	//	Init IS66wV51216DBLL 8Mb Pseudo CMOS Static RAM at bank #0
	//	0x80000000 - base address
	ebiInit.mode = ebiModeD16;
	ebiInit.ardyPolarity = ebiActiveLow;
	ebiInit.alePolarity = ebiActiveLow;
	ebiInit.wePolarity = ebiActiveLow;
	ebiInit.rePolarity = ebiActiveLow;
	ebiInit.csPolarity = ebiActiveLow;
	ebiInit.ardyEnable = 0;

	//	timings for RAM
	//	1 cycle length = 20nS
	ebiInit.ardyDisableTimeout = 1;
	ebiInit.addrSetupCycles = 0;
	ebiInit.addrHoldCycles = 0;

	ebiInit.readSetupCycles = 2;
	ebiInit.readStrobeCycles = 2;
	ebiInit.readHoldCycles = 0;

	ebiInit.writeSetupCycles = 2;
	ebiInit.writeStrobeCycles = 2;
	ebiInit.writeHoldCycles = 0;

	ebiInit.banks = EBI_BANK0;
	ebiInit.csLines = EBI_CS0;
	ebiInit.location = ebiLocation1;

	/* Generating extra config parameters on Giant/Wonder */
	ebiInit.blPolarity = ebiActiveLow;
	ebiInit.blEnable = 0;
	ebiInit.noIdle = 0;
	ebiInit.addrHalfALE = 0;
	ebiInit.readPageMode = 0;
	ebiInit.readPrefetch = 0;
	ebiInit.readHalfRE = 0;
	ebiInit.writeBufferDisable = 0;
	ebiInit.writeHalfWE = 0;
	ebiInit.aLow = ebiALowA0;
	ebiInit.aHigh = ebiAHighA18;

	/* Enable/disable EBI after initialization */
	ebiInit.enable = 1;
	EBI_Init(&ebiInit);

	//	Init TFT LCD controller ILI9341 at bank #1:
	//	0x84000000 - control register access
	//	0x84100000 - data register access
	ebiInit.banks = EBI_BANK1;
	ebiInit.csLines = EBI_CS1;
	ebiInit.aLow = ebiALowA0;
	ebiInit.aHigh = ebiAHighA18;

	//	timings for TFT
	//	1 cycle length = 20nS
	ebiInit.ardyDisableTimeout = 1;
	ebiInit.addrSetupCycles = 0;
	ebiInit.addrHoldCycles = 0;

	ebiInit.readSetupCycles = 0;
	ebiInit.readStrobeCycles = 1;
	ebiInit.readHoldCycles = 0;

	ebiInit.writeSetupCycles = 0;
	ebiInit.writeStrobeCycles = 1;
	ebiInit.writeHoldCycles = 0;

	// Enable/disable EBI after initialization
	ebiInit.enable = 1;
	EBI_Init(&ebiInit);

	EBI->ROUTE = 0;
	/* Module EBI is configured to location 1 */
	EBI->ROUTE = (EBI->ROUTE & ~_EBI_ROUTE_LOCATION_MASK) | EBI_ROUTE_LOCATION_LOC1;
	/* Enable signals APEN_A19, ALB_A0, EBI, CS0, CS1 */
//	EBI->ROUTE |= EBI_ROUTE_APEN_A20 | EBI_ROUTE_ALB_A0 | EBI_ROUTE_EBIPEN | EBI_ROUTE_CS0PEN | EBI_ROUTE_CS1PEN;
	EBI->ROUTE |= EBI_ROUTE_APEN_A19 | EBI_ROUTE_ALB_A0 | EBI_ROUTE_EBIPEN | EBI_ROUTE_CS0PEN | EBI_ROUTE_CS1PEN;
}

void deviceLcdPwmSet(uint16_t uPwmValue)
{
	TIMER_CompareBufSet(LCD_BACKLIGHT_PWM_TIMER, 0, uPwmValue);
}

void initLcdBacklightPWM(void)
{
	/* Select CC channel parameters */
	TIMER_InitCC_TypeDef timerCCInit;
	timerCCInit.eventCtrl = timerEventEveryEdge;
	timerCCInit.edge = timerEdgeBoth;
	timerCCInit.prsSel = timerPRSSELCh0;
	timerCCInit.cufoa = timerOutputActionNone;
	timerCCInit.cofoa = timerOutputActionNone;
	timerCCInit.cmoa = timerOutputActionToggle;
	timerCCInit.mode = timerCCModePWM;
	timerCCInit.filter = false;
	timerCCInit.prsInput = false;
	timerCCInit.coist = false;
	timerCCInit.outInvert = false;

	/* Select timer parameters */
	TIMER_Init_TypeDef timerInit;
	timerInit.enable = true;
	timerInit.debugRun = true;
	timerInit.prescale = timerPrescale8;
	timerInit.clkSel = timerClkSelHFPerClk;
	timerInit.fallAction = timerInputActionNone;
	timerInit.riseAction = timerInputActionNone;
	timerInit.mode = timerModeUp;
	timerInit.dmaClrAct = false;
	timerInit.quadModeX4 = false;
	timerInit.oneShot = false;
	timerInit.sync = false;

	/* Set CC0 location 0 pin (PA8) as output */
	GPIO_PinModeSet(LCD_BACKLIGHT_PIN, gpioModePushPull, 0);//	LCD PWM backlight

	/* Enable clock for TIMER2 module */
	CMU_ClockEnable(LCD_BACKLIGHT_PWM_CLOCK, true);

	/* Configure CC channel 0 */
	TIMER_InitCC(LCD_BACKLIGHT_PWM_TIMER, 0, &timerCCInit);

	/* Route CC0 to location 0 (PA8) and enable pin */
	LCD_BACKLIGHT_PWM_TIMER->ROUTE |= (TIMER_ROUTE_CC0PEN
		| TIMER_ROUTE_LOCATION_LOC0);

	/* Set Top Value */
	TIMER_TopSet(LCD_BACKLIGHT_PWM_TIMER, 0xFFFF);

	/* Set compare value starting at 0 - it will be incremented in the interrupt handler */
	deviceLcdPwmSet(0xFFFE);

	/* Enable overflow interrupt */
	//    TIMER_IntEnable(TIMER3, TIMER_IF_OF);
	/* Enable TIMER0 interrupt vector in NVIC */
	//    NVIC_EnableIRQ(TIMER3_IRQn);
	/* Configure timer */
	TIMER_Init(LCD_BACKLIGHT_PWM_TIMER, &timerInit);
}

static bool testRAM(void)
{
	TRACE_WHITE("\nRAM testing ");
	uint16_t iPasses = 0;
	uint16_t dummy = 0x0;
	volatile uint16_t *pAddr = (uint16_t *)EBI_BankAddress(RAM_EBI_BANK);

	while (iPasses < 2)
	{
		for (uint32_t i = 0; i < RAM_SIZE_WORDS; i++)
		{
			*pAddr = dummy;
			if (*pAddr != dummy)
			{
				TRACE_RED(" Failed at: 0x");
				TRACE(strUtoa((uint32_t)pAddr, 16));
				return false;
			}
			pAddr++;
			if (((uint32_t)pAddr) % 0xFFFF == 0) TRACE(".");
		}
		iPasses++;
		dummy = 0xFFFF;
	}
	TRACE_GREEN(" Ok");
	return true;
}

#ifdef DEBUG
static void delay_ticks(uint32_t base)
{
	volatile uint32_t i = base;
	while (i)
		i = i - 1;
}

extern "C" void debugHardfault(uint32_t *sp)
{
	volatile SCB_Type *p = SCB; (void)(p);

	uint32_t r0 = sp[0]; (void)(r0);
	uint32_t r1 = sp[1]; (void)(r1);
	uint32_t r2 = sp[2]; (void)(r2);
	uint32_t r3 = sp[3]; (void)(r3);
	uint32_t r12 = sp[4]; (void)(r12);
	uint32_t lr = sp[5]; (void)(lr);
	uint32_t pc = sp[6]; (void)(pc);
	uint32_t psr = sp[7]; (void)(psr);
	while (1)
		;
}

__attribute__((naked)) void HardFault_Handler(void)
{
	uint32_t *sp = (uint32_t *)__get_MSP(); 	// Get stack pointer
	uint32_t ia = sp[24 / 4]; 					// Get instruction address from stack

	__asm volatile
	(
		"mrs r0, msp                                   \n"
		"mov r1, #4                                    \n"
		"mov r2, lr                                    \n"
		"tst r2, r1                                    \n"
		"beq jump_debugHardfault                       \n"
		"mrs r0, psp                                   \n"
		"jump_debugHardfault:                          \n"
		"ldr r1, debugHardfault_address                \n"
		"bx r1                                         \n"
		"debugHardfault_address: .word debugHardfault  \n"
		);

	TRACE("HardFault at address: 0x");
	TRACE((const char*)strUtoa((unsigned int)ia, 16));
	TRACE("\r\n");
	delay_ticks(100000);
	while (1)
		;
}

extern "C" void vApplicationStackOverflowHook(TaskHandle_t xTask, signed char *pcTaskName)
{
	TRACE(RTT_CTRL_TEXT_BRIGHT_RED);
	TRACE("Task stack overflow: ");
	TRACE((const char*)pcTaskName);
	TRACE("\n");

	while (1)
	{
		delay_ticks(1000000);
		GPIO_PinOutSet(LED_STATUS_PIN);
		delay_ticks(1000000);
		GPIO_PinOutClear(LED_STATUS_PIN);
	}
}

extern "C" void vApplicationMallocFailedHook(void)
{
	TRACE(RTT_CTRL_TEXT_BRIGHT_RED);
	TRACE("Heap allocation failed.\n");

	while (1)
	{
		delay_ticks(100000);
		GPIO_PinOutSet(LED_STATUS_PIN);
		delay_ticks(100000);
		GPIO_PinOutClear(LED_STATUS_PIN);
	}
}
#endif

static void traceMcuInfo(void)
{
	TRACE("\nMCU: ");

	uint8_t partFamily = (uint8_t)((DEVINFO->PART & _DEVINFO_PART_DEVICE_FAMILY_MASK) >> _DEVINFO_PART_DEVICE_FAMILY_SHIFT);
	if (partFamily == _DEVINFO_PART_DEVICE_FAMILY_EFM32LG)		TRACE("Leapard Gecko");
	else if (partFamily == _DEVINFO_PART_DEVICE_FAMILY_EFM32WG)	TRACE("Wonder Gecko");

	TRACE(", PN:");
	TRACE(strUtoa(((DEVINFO->PART & _DEVINFO_PART_DEVICE_NUMBER_MASK) >> _DEVINFO_PART_DEVICE_NUMBER_SHIFT), 10));
	TRACE(", PRev:");
	TRACE(strUtoa((DEVINFO->PART & _DEVINFO_PART_PROD_REV_MASK) >> _DEVINFO_PART_PROD_REV_SHIFT, 10));

	TRACE(", Mem:");
	TRACE(strUtoa((DEVINFO->MSIZE & _DEVINFO_MSIZE_FLASH_MASK) >> _DEVINFO_MSIZE_FLASH_SHIFT, 10));
	TRACE("/");
	TRACE(strUtoa((DEVINFO->MSIZE & _DEVINFO_MSIZE_SRAM_MASK) >> _DEVINFO_MSIZE_SRAM_SHIFT, 10));

	// Print the chip ID
	TRACE(", ChipID:");
	uint8_t *p = (uint8_t *)&DEVINFO->UNIQUEH;
	for (int i = 0; i < 8; i++)
		TRACE(strUtoa(p[i], 16));
}
#if 0
static void testIO(void)
{
	//	GPIO_PinModeSet(LED_STATUS_PIN, gpioModePushPull, 1);

	GPIO_PinModeSet(config_LCD_RESET_PIN, gpioModePushPull, 1);
	GPIO_PinModeSet(config_LCD_CS_PIN, gpioModePushPull, 1);
	GPIO_PinModeSet(config_LCD_WE_PIN, gpioModePushPull, 1);
	GPIO_PinModeSet(config_LCD_RE_PIN, gpioModePushPull, 1);
	GPIO_PinModeSet(config_LCD_DC_PIN, gpioModePushPull, 1);

	GPIO_PinModeSet(config_LCD_D0, gpioModePushPull, 0);
	GPIO_PinModeSet(config_LCD_D1, gpioModePushPull, 0);
	GPIO_PinModeSet(config_LCD_D2, gpioModePushPull, 0);
	GPIO_PinModeSet(config_LCD_D3, gpioModePushPull, 0);
	GPIO_PinModeSet(config_LCD_D4, gpioModePushPull, 0);
	GPIO_PinModeSet(config_LCD_D5, gpioModePushPull, 0);
	GPIO_PinModeSet(config_LCD_D6, gpioModePushPull, 0);
	GPIO_PinModeSet(config_LCD_D7, gpioModePushPull, 0);

	GPIO_PinModeSet(config_LCD_D8, gpioModePushPull, 0);
	GPIO_PinModeSet(config_LCD_D9, gpioModePushPull, 0);
	GPIO_PinModeSet(config_LCD_D10, gpioModePushPull, 0);
	GPIO_PinModeSet(config_LCD_D11, gpioModePushPull, 0);
	GPIO_PinModeSet(config_LCD_D12, gpioModePushPull, 0);
	GPIO_PinModeSet(config_LCD_D13, gpioModePushPull, 0);
	GPIO_PinModeSet(config_LCD_D14, gpioModePushPull, 0);
	GPIO_PinModeSet(config_LCD_D15, gpioModePushPull, 0);


	while (1)
	{
		GPIO_PinOutToggle(config_LCD_CS_PIN);
		GPIO_PinOutToggle(config_LCD_WE_PIN);
		GPIO_PinOutToggle(config_LCD_RE_PIN);
		GPIO_PinOutToggle(config_LCD_DC_PIN);

		GPIO_PinOutToggle(config_LCD_D0);
		GPIO_PinOutToggle(config_LCD_D1);
		GPIO_PinOutToggle(config_LCD_D2);
		GPIO_PinOutToggle(config_LCD_D3);
		GPIO_PinOutToggle(config_LCD_D4);
		GPIO_PinOutToggle(config_LCD_D5);
		GPIO_PinOutToggle(config_LCD_D6);
		GPIO_PinOutToggle(config_LCD_D7);

		GPIO_PinOutToggle(config_LCD_D8);
		GPIO_PinOutToggle(config_LCD_D9);
		GPIO_PinOutToggle(config_LCD_D10);
		GPIO_PinOutToggle(config_LCD_D11);
		GPIO_PinOutToggle(config_LCD_D12);
		GPIO_PinOutToggle(config_LCD_D13);
		GPIO_PinOutToggle(config_LCD_D14);
		GPIO_PinOutToggle(config_LCD_D15);

		delay_us(4000000);
	}
}
#endif

static void initHW(void)
{
	//	Color_TypeDef *pFrameBufferPtr = (Color_TypeDef *) EBI_BankAddress(EBI_BANK0);
		//	Init buttons and configure IRQ
	TRACE_WHITE("\nInit GPIO ... ");
	initGPIO();
	TRACE_GREEN("Ok");

	//	testIO();

		//	Initialize External Bus Interface

#ifdef TFT_INTERFACE_8080_16BIT_EBI
	TRACE_WHITE("\nInit EBI ... ");
	initEBI();
	TRACE_GREEN("Ok");
#endif

	//	Initialize external RAM
//	testRAM();

	// Initialize DMA for frame buffer transfer
	TRACE_WHITE("\nInit DMA ... ");
	initDMA();
	TRACE_GREEN("Ok");

	//	Init buttons SENSORS
//	deviceInitSensors();
	//	Init flash memory & microSD devices
//	initFlashMemory();

	//	init SPI bus for MicroSD and NOR flash memory
	initSPI();

	//	init UART for BGM111 bluetooth module
	initUSART();

	//	init USB composite device modes
/*	TRACE_WHITE("\nInit USB ... ");
	initUSB(m_pSPI);
//	initUSB();
	TRACE_GREEN("Ok");
*/
// TFT LCD display
	TRACE_WHITE("\nInit LCD ... ");

	tft.init();
	tft.setOrientation(1);
	tft.fill();

	TRACE_GREEN("Ok");

	/*

#ifdef TFT_USE_FB
	TFT_init(config_FB_WIDTH, config_FB_HEIGHT, config_FB_BPP, pFrameBufferPtr, TFT_FB_NUM);
#else
	TFT_init(config_FB_WIDTH, config_FB_HEIGHT, config_FB_BPP);
#endif
	TFT_setOrientation(3);
	TFT_drawFill(RGB(0,0,0));

	TFT_drawFillRect(10,10,100,100,RGB(0,0,255));
	TFT_drawFillRect(50,50,100,100,RGB(0,255,0));
	TFT_drawFillRect(100,100,100,100,RGB(255,0,0));

	TFTFB_update(false);
	*/

	// Initialize RTC clock
	RealTimeClock::initHW();

	//	Init LCD backlight
	initLcdBacklightPWM();
}

void taskIoControl(void *pParams)
{
	uint32_t ulNotificationValue;
	EFM_ASSERT(hIoQueue);
	while (1)
	{
		if (xQueueReceive(hIoQueue, &ulNotificationValue, portMAX_DELAY))
		{
			switch (ulNotificationValue)
			{
			case MSG_TYPE_BUTTON:
			{
				wndMgr.SendMessage(MSG_TYPE_BUTTON, NULL);
				break;
			}
			default:
				break;
			}
		}
	}
}

void taskTimeKeeper(void *pParams)
{
	while (1)
	{
		GPIO_PinOutSet(LED_STATUS_PIN);
		vTaskDelay(50);

		GPIO_PinOutClear(LED_STATUS_PIN);
		vTaskDelay(950);
	}
}

extern "C" void vApplicationIdleHook(void)
{
	MSDD_Handler();
	if (BLE_IsEvent())
		BLE_ProcessEvent();
}

static void taskGui(void *pParameters)
{
	while (1)
	{
		wndTime.create(0, 0, tft.getWidth(), tft.getHeight());
		wndTime.DoModal();

		wndTPH.create(0, 0, tft.getWidth(), tft.getHeight());
		wndTPH.DoModal();

		wndLight.create(0, 0, tft.getWidth(), tft.getHeight());
		wndLight.DoModal();

		wndTempGraph.create(0, 0, tft.getWidth(), tft.getHeight());
		wndTempGraph.SetData(data, 48);
		wndTempGraph.DoModal();

		wndDashboard.create(0, 0, tft.getWidth(), tft.getHeight());
		wndDashboard.DoModal();
	}
	//	Invalidate();
}

//	TODO: check for FreeRTOS heap size
//	TODO: check for FreeRTOS stacks size
//	TODO: check for appliation's common stacks size
//	TODO: check for appliation's common heap size

int main(void)
{
	// 	Chip errata
	CHIP_Init();

	// Initialize clocks first
	initClocks();

	//	Trace output initialization
	TRACE_INIT();

#ifdef DEBUG
	TRACE(RTT_CTRL_CLEAR);
	TRACE(RTT_CTRL_TEXT_BRIGHT_CYAN);
	TRACE(config_FW_NAME);
	TRACE(" firmware: ver. ");
	TRACE(config_FW_VERSION_STRING);
	TRACE(", build: ");
	TRACE(__DATE__);
	TRACE(", ");
	TRACE(__TIME__);
	TRACE(RTT_CTRL_TEXT_BRIGHT_WHITE);
#endif

	traceMcuInfo();

	//	HW initialization
	initHW();

	wndMgr.Init(&tft, MSG_QUEUE_SIZE);

	logger.init();

	BLE_HostInit();

	xTaskCreate(taskTimeKeeper, (const char *) "TIME", STACK_SIZE_FOR_TASK, NULL, MIDDLE_TASK_PRIORITY, &hTaskTimeKeep);
	configASSERT(hTaskTimeKeep);

	xTaskCreate(taskGui, (const char *) "GUI", 2 * STACK_SIZE_FOR_TASK, NULL, LOW_TASK_PRIORITY, &hTaskGUI);
	configASSERT(hTaskGUI);

	xTaskCreate(taskIoControl, (const char *) "IO", STACK_SIZE_FOR_TASK, NULL, HIGH_TASK_PRIORITY, &hTaskIoControl);
	configASSERT(hTaskIoControl);

	xTaskCreate(taskBleControl, (const char *) "BLE", STACK_SIZE_FOR_TASK, NULL, HIGH_TASK_PRIORITY, &hTaskBleControl);
	configASSERT(hTaskBleControl);

	hIoQueue = xQueueCreate(4, sizeof(uint32_t));
	configASSERT(hIoQueue);

	/*	Start FreeRTOS Scheduler	*/
	vTaskStartScheduler();

	EFM_ASSERT(0);
	/*	Should not be here !!!	*/
	return 0;
}
