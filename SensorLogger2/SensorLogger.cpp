/*
 * SensorLogger.cpp
 *
 *  Created on: 28 ���. 2016 �.
 *      Author: Yuriy
 */

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "em_assert.h"
#include "em_timer.h"

#include "i2cspm.h"

#include "MAX44009.h"
#include "BME280.h"
#include "FXOS8700CQ.h"
#include "APDS9250.h"
#include "Si1133.h"

#include "gui.h"
#include "SensorLogger.h"

// --- FreeRTOS include files ---
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "timers.h"

#include "trace.h"
#include "str.h"

#include "app_config.h"

MAX44009 	als_sensor;
BME280		tph_sensor;
FXOS8700CQ 	acc_sensor;
APDS9250 	rgb_sensor;
Si1133		uv_sensor;

SensorLogger logger;

SensorLogger::SensorLogger()
{
	// TODO Auto-generated constructor stub

	m_ulSensorDataMask = (1<<SL_SensorType_Temperature) | (1<<SL_SensorType_Pressure) | (1<<SL_SensorType_Humidity) | (1<<SL_SensorType_ALS) | (1<<SL_SensorType_Accelerometer) | (1<<SL_SensorType_Magnetometer) | (1<<SL_SensorType_RGB) | (1<<SL_SensorType_IR) | (1<<SL_SensorType_UV);
	m_ulSensorDataTimeout = default_SENSOR_DATA_TIMEOUT;
	m_hSensorDataTimer = NULL;

	m_rgbData.r = 0;
	m_rgbData.g = 0;
	m_rgbData.b = 0;

	m_ulIrData = 0;
}

SensorLogger::~SensorLogger()
{
	// TODO Auto-generated destructor stub
}

void SensorLogger::timerHandlerSensorData(TimerHandle_t xTimer)
{
	for(uint8_t sensorType = (uint8_t)SL_SensorType_Temperature; sensorType<=(uint8_t)SL_SensorType_Current; sensorType++)
	{
		if(logger.m_ulSensorDataMask & (1<<sensorType))
		{
			switch(sensorType)
			{
				case SL_SensorType_Temperature:
					logger.updateTemperature(0);
					break;
				case SL_SensorType_Pressure:
					logger.updatePressure(0);
					break;
				case SL_SensorType_Humidity:
					logger.updateHumidity(0);
					break;
				case SL_SensorType_ALS:
					logger.updateALS(0);
					break;
				case SL_SensorType_RGB:
					logger.updateRGB(0);
					break;
				case SL_SensorType_IR:
					logger.updateIR(0);
					break;
				case SL_SensorType_UV:
					logger.updateUV(0);
					break;
				case SL_SensorType_Accelerometer:
					logger.updateACC(0);
					break;
				case SL_SensorType_Magnetometer:
					logger.updateMGN(0);
					break;
				default:
					break;
			}
		}
	}
	wndMgr.SendMessage(MSG_TYPE_UPDATE, NULL);
}
/*
static void SensorLogger::timerHandlerACC(TimerHandle_t xTimer)
{
	AccelerometerData_t acc_data;
	acc_sensor.ReadAccelData(&acc_data);

	TRACE("ACC value: ");
	TRACE(strUtoa(acc_data.x,10));
	TRACE(", ");
	TRACE(strUtoa(acc_data.y,10));
	TRACE(", ");
	TRACE(strUtoa(acc_data.z,10));
	TRACE("\n");
}

static void SensorLogger::timerHandlerTPH(TimerHandle_t xTimer)
{
	TRACE("T value: ");
	TRACE(strUtoa(tph_sensor.ReadTemperature(),10));
	TRACE("; P : ");
	TRACE(strUtoa(tph_sensor.ReadPressure(),10));
	TRACE("; H : ");
	TRACE(strUtoa(tph_sensor.ReadHumidity(),10));
	TRACE("\n");
}
*/

bool SensorLogger::initSensors(void)
{
	I2CSPM_Init_TypeDef i2cInit = I2CSPM_DEFAULT_INIT;

	/* Initialize I2C driver, using fast rate. */
	I2CSPM_Init(&i2cInit);

	TRACE("Sensors init ... ");

	if(als_sensor.Init(i2cInit.port, MAX44009_ADDRESS_A0_LOW))
	{
		if(acc_sensor.Init(i2cInit.port, FXOS8700CQ_ADDRESS_SA00))
		{
			if(tph_sensor.Init(i2cInit.port, BME280_ADDRESS_SDO_GND))
			{
				if(rgb_sensor.init(i2cInit.port, APDS9250_ADDRESS))
				{
				/*	if(uv_sensor.init(i2cInit.port, SI1133_ADDRESS_PULL_UP))
					{
						TRACE("Ok\n");

						TRACE("HW id: 0x");
						TRACE(strUtoa(uv_sensor.readHwID(),16));
						TRACE(", REV id: 0x");
						TRACE(strUtoa(uv_sensor.readRevID(),16));*/
						TRACE("Ok\n");
						return true;
				//	}
				}
			}
		}
	}
	TRACE("Failed\n");
	return false;
}

bool SensorLogger::init(void)
{
	if(initSensors())
	{
		rgb_sensor.enable(1,APDS9250_MODE_CS);

		m_hSensorDataTimer = xTimerCreate("SDT", m_ulSensorDataTimeout , pdTRUE, NULL, SensorLogger::timerHandlerSensorData );
		configASSERT(m_hSensorDataTimer);
		if(m_hSensorDataTimer)
		{
			xTimerStart(m_hSensorDataTimer , 2);
			return true;
		}
	}
	return false;
}

bool SensorLogger::updateTemperature(uint8_t nSensorIndexMask)
{
	m_lTemperature[0] = tph_sensor.ReadTemperature();
	return true;
}

bool SensorLogger::updatePressure(uint8_t nSensorIndexMask)
{
	m_ulPressure = tph_sensor.ReadPressure();
	return true;
}

bool SensorLogger::updateHumidity(uint8_t nSensorIndexMask)
{
	m_ulHumidity = tph_sensor.ReadHumidity();
	return true;
}

bool SensorLogger::updateACC(uint8_t nSensorIndexMask)
{
	acc_sensor.ReadAccelData(&m_accData);
	return true;
}

bool SensorLogger::updateMGN(uint8_t nSensorIndexMask)
{
	acc_sensor.ReadMagnData(&m_mgnData);
	return true;
}

bool SensorLogger::updateALS(uint8_t nSensorIndexMask)
{
	als_sensor.Read(&m_ulAlsData[0]);
	return true;
}

bool SensorLogger::updateRGB(uint8_t nSensorIndexMask)
{
	m_rgbData.r = rgb_sensor.readRed();
	m_rgbData.g = rgb_sensor.readGreen();
	m_rgbData.b = rgb_sensor.readBlue();
	return true;
}

bool SensorLogger::updateUV(uint8_t nSensorIndexMask)
{
//	m_ulUvData = uv_sensor.readUV();
	return true;
}

bool SensorLogger::updateIR(uint8_t nSensorIndexMask)
{
//	m_ulIrData = rgb_sensor.readIR();
//	m_ulIrData = uv_sensor.readIR();
	return true;
}

int32_t SensorLogger::getTemperature(uint8_t uSensorIndex)
{
	return m_lTemperature[uSensorIndex];
}

uint32_t SensorLogger::getPressure(uint8_t uSensorIndex)
{
	return m_ulPressure;
}

uint32_t SensorLogger::getHumidity(uint8_t uSensorIndex)
{
	return m_ulHumidity;
}

AccelerometerData_t * SensorLogger::getACC(void)
{
	return &m_accData;
}

MagnetometerData_t * SensorLogger::getMGN(void)
{
	return &m_mgnData;
}

uint32_t SensorLogger::getALS(uint8_t uSensorIndex)
{
	return m_ulAlsData[uSensorIndex];
}

RGB32_t* SensorLogger::getRGB(uint8_t uSensorIndex)
{
	return &m_rgbData;
}

uint32_t SensorLogger::getUV(uint8_t uSensorIndex)
{
	return m_ulUvData;
}

uint32_t SensorLogger::getIR(uint8_t uSensorIndex)
{
	return m_ulIrData;
}
