/*
 * ble_host.c
 *
 *  Created on: 16.01.2016
 *      Author: Yuriy
 */

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>

#include "em_usart.h"
#include "em_gpio.h"
#include "em_cmu.h"

#include "app_config.h"

#include "ble_host.h"
#include "ble_gatt_config.h"
#include "ble_host_config.h"

#include "gecko_bglib.h"
//#include "ezediesel.h"

// --- FreeRTOS include files ---
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "SensorLogger.h"

// --- RTT debug output trace include files ---
#include "trace.h"


BGLIB_DEFINE();

static QueueHandle_t hBleQueue = NULL;

/* Setup UART/USART in async mode */
static struct gecko_cmd_packet *evt = NULL;
static struct gecko_cmd_packet *rsp = NULL;

static bool m_bIsConnected = false;
/** Connection handle variable. */
static uint8_t m_hConnection = 0;

static BLE_CharList_t m_charList[GATTDB_LIST_SIZE] =
{
	/*	{ 	GATT_DeviceName_RequestHandler, 		gattdb_device_name 		},
		{ 	GATT_DeviceParams_RequestHandler, 		gattdb_device_params 	},
		{ 	GATT_DeviceControl_RequestHandler, 		gattdb_device_control 	},
		{ 	GATT_CoeffParams_RequestHandler, 		gattdb_coeff_params 	},

		{ 	GATT_PressureSensor_RequestHandler, 	gattdb_pressure_params 	},
		{ 	GATT_EngineParams_RequestHandler, 		gattdb_engine_params 	},
		{ 	GATT_AccParams_RequestHandler, 			gattdb_acc_params 		},
		{ 	GATT_TempParams_RequestHandler, 		gattdb_temp_params 		},
		*/
};

#ifdef DEBUG
static const char *m_strCharNames[] =
{
		"DevName",
		"DevParam",
		"DevCtrl",
		"CoeffParam",
		"PsiValue",
		"EngValue",
		"AccValue",
		"TempValue"
};
#endif

volatile struct circularBuffer
{
  uint8_t  data[BLE_UART_BUFFERSIZE];  /* data buffer */
  uint32_t rdI;               /* read index */
  uint32_t wrI;               /* write index */
  uint32_t pendingBytes;      /* count of how many bytes are not yet handled */
  bool     overflow;          /* buffer overflow indicator */
} rxBuf, txBuf = { {0}, 0, 0, 0, false };

//extern char m_strDeviceName[config_DEVICE_NAME_SIZE_MAX];
//extern DeviceParams_t mDeviceParams;

#ifdef DEBUG
const char *getCharNameById(uint16_t idChar)
{
	for(int i = 0; i<GATTDB_LIST_SIZE; i++)
	{
		if(m_charList[i].m_pCharacteristic == idChar)	return m_strCharNames[i];
	}
	return "";
}
#endif

/******************************************************************************
 * @brief  uartPutData function
 *
 *****************************************************************************/
static void uartPutData(uint8_t * dataPtr, uint32_t dataLen)
{
  uint32_t i = 0;

  /* Check if buffer is large enough for data */
  if (dataLen > BLE_UART_BUFFERSIZE)
  {
    /* Buffer can never fit the requested amount of data */
    return;
  }

  /* Check if buffer has room for new data */
  if ((txBuf.pendingBytes + dataLen) > BLE_UART_BUFFERSIZE)
  {
    /* Wait until room */
    while ((txBuf.pendingBytes + dataLen) > BLE_UART_BUFFERSIZE) ;
  }

  /* Fill dataPtr[0:dataLen-1] into txBuffer */
  while (i < dataLen)
  {
    txBuf.data[txBuf.wrI] = *(dataPtr + i);
    txBuf.wrI             = (txBuf.wrI + 1) % BLE_UART_BUFFERSIZE;
    i++;
  }

  /* Increment pending byte counter */
  txBuf.pendingBytes += dataLen;

  /* Enable interrupt on USART TX Buffer*/
  USART_IntEnable(BLE_USART, USART_IEN_TXBL);
}

/******************************************************************************
 * @brief  uartGetData function
 *
 *****************************************************************************/
static uint32_t uartGetData(uint8_t * dataPtr, uint32_t dataLen)
{
  uint32_t i = 0;

  /* Wait until the requested number of bytes are available */
  if (rxBuf.pendingBytes < dataLen)
  {
    while (rxBuf.pendingBytes < dataLen) ;
  }

  if (dataLen == 0)
  {
    dataLen = rxBuf.pendingBytes;
  }

  /* Copy data from Rx buffer to dataPtr */
  while (i < dataLen)
  {
    *(dataPtr + i) = rxBuf.data[rxBuf.rdI];
    rxBuf.rdI = (rxBuf.rdI + 1) % BLE_UART_BUFFERSIZE;
    i++;
  }

  /* Decrement pending byte counter */
  rxBuf.pendingBytes -= dataLen;

  return i;
}


int uart_rx(uint16_t data_length, uint8_t* data)
{
    uartGetData(data,data_length);
    return data_length;
}

int uart_tx(uint16_t data_length, uint8_t* data)
{
    uartPutData(data,data_length);
    return data_length;
}

int32_t uart_peek(void)
{
	return (int32_t)rxBuf.pendingBytes;
}


/**************************************************************************//**
 * @brief UART1 RX IRQ Handler
 * Set up the interrupt prior to use
 * Note that this function handles overflows in a very simple way.
 *****************************************************************************/
void UART1_RX_IRQHandler(void)
{
  /* Check for RX data valid interrupt */
  if (BLE_USART->IF & USART_IF_RXDATAV)
  {
    /* Copy data into RX Buffer */
    uint8_t rxData = USART_Rx(BLE_USART);
    rxBuf.data[rxBuf.wrI] = rxData;
    rxBuf.wrI             = (rxBuf.wrI + 1) % BLE_UART_BUFFERSIZE;
    rxBuf.pendingBytes++;

    /* Flag Rx overflow */
    if (rxBuf.pendingBytes > BLE_UART_BUFFERSIZE)
    {
      rxBuf.overflow = true;
    }
  }
}

/**************************************************************************//**
 * @brief UART1 TX IRQ Handler
 * Set up the interrupt prior to use
 *****************************************************************************/
void UART1_TX_IRQHandler(void)
{
  /* Check TX buffer level status */
  if (BLE_USART->IF & USART_IF_TXBL)
  {
    if (txBuf.pendingBytes > 0)
    {
      /* Transmit pending character */
      USART_Tx(BLE_USART, txBuf.data[txBuf.rdI]);
      txBuf.rdI = (txBuf.rdI + 1) % BLE_UART_BUFFERSIZE;
      txBuf.pendingBytes--;
    }

    /* Disable Tx interrupt if no more bytes in queue */
    if (txBuf.pendingBytes == 0)
    {
      USART_IntDisable(BLE_USART, USART_IEN_TXBL);
    }
  }
}


//************* Public functions ******************************
// Init BLE module, UART/USART module should be configured, IRQ configured and enabled/started before calling this functions
void BLE_HostInit(void)
{
	TRACE("Init BLE host... ");
	BGLIB_INITIALIZE_NONBLOCK(uart_tx, uart_rx, uart_peek);
	rsp = (struct gecko_cmd_packet *)gecko_cmd_system_reset(0);
	TRACE("Ok\n");
}

bool BLE_IsEvent(void)
{
	evt = gecko_peek_event();
	if(evt == NULL)	return false;
	return true;
}

bool BLE_IsConnected(void)
{
	return m_bIsConnected;
}

void BLE_ProcessEvent(void)
{
	switch(BGLIB_MSG_ID(evt -> header))
	{
		// SYSTEM BOOT (power-on/reset)
		case gecko_evt_system_boot_id:
		{
			TRACE("BLE on boot\n");
			char strName[] = {"SensorLogger"};
			//rsp = (struct gecko_cmd_packet *)gecko_cmd_gatt_server_write_attribute_value(gattdb_device_name, 0, strlen(mDeviceParams.m_strBleDeviceName) + 1, (uint8_t*) mDeviceParams.m_strBleDeviceName);
			rsp = (struct gecko_cmd_packet *)gecko_cmd_gatt_server_write_attribute_value(gattdb_device_name, 0, strlen(strName) + 1, (uint8_t*) strName);
			rsp = (struct gecko_cmd_packet *)gecko_cmd_le_gap_set_mode(le_gap_general_discoverable, le_gap_undirected_connectable);
			break;
		}
		// LE CONNECTION OPENED (remote device connected)
	    case gecko_evt_le_connection_opened_id:
	    {
	    	TRACE("BLE connected\n");
	    	m_bIsConnected = true;
	    	m_hConnection = evt->data.evt_le_connection_opened.connection;

	    	gecko_cmd_le_connection_set_parameters(m_hConnection, config_BLE_CONNECTION_MIN_INTERVAL, config_BLE_CONNECTION_MAX_INTERVAL, config_BLE_CONNECTION_LATENCY, config_BLE_CONNECTION_TIMEOUT);
	    	break;
	    }
	    // LE CONNECTION CLOSED (remote device disconnected)
	    case gecko_evt_le_connection_closed_id:
	    {
	    	TRACE("BLE disconnected\n");
	    	m_bIsConnected = false;
	    	m_hConnection = 0;
	    	rsp = (struct gecko_cmd_packet *)gecko_cmd_le_gap_set_mode(le_gap_general_discoverable, le_gap_undirected_connectable);
	    	break;
	    }
	    // GATT SERVER CHARACTERISTIC STATUS (remote GATT client changed subscription status)
		case gecko_evt_gatt_server_characteristic_status_id:
		{
			TRACE("BLE GATT status change\n");
			for(int i = 0; i< GATTDB_LIST_SIZE; i++)
			{
				if(m_charList[i].m_pCharacteristic == evt->data.evt_gatt_server_user_write_request.characteristic/* && m_charList[i].m_pRequestHandler*/)
				{
					// make sure this update corresponds to a change in configuration status
					if (evt->data.evt_gatt_server_characteristic_status.status_flags == gatt_server_client_config)
					{
						// client characteristic configuration status changed for GPIO control
						if (evt->data.evt_gatt_server_characteristic_status.client_config_flags & gatt_indication)
						{
							uint8_t data[GATTDB_DATA_SIZE_MAX];
							uint8_t data_lenght = 0;

					//		m_charList[i].m_pRequestHandler(data, &data_lenght, GATT_RequestTypeRead);
							rsp = (struct gecko_cmd_packet *)gecko_cmd_gatt_server_send_characteristic_notification(evt->data.evt_gatt_server_characteristic_status.connection, evt->data.evt_gatt_server_characteristic_status.characteristic, data_lenght, data);
						}
					}
					return;
				}
			}
			break;
		}
		// GATT SERVER USER READ REQUEST (remote GATT client is reading a value from a user-type characteristic)
		case gecko_evt_gatt_server_user_read_request_id:
		{
			TRACE("BLE GATT read: ");
			for(int i = 0; i< GATTDB_LIST_SIZE; i++)
			{
				if(m_charList[i].m_pCharacteristic == evt->data.evt_gatt_server_user_read_request.characteristic)
				{
					uint8_t data[GATTDB_DATA_SIZE_MAX];
					uint8_t data_lenght = 0;

					TRACE(getCharNameById(evt->data.evt_gatt_server_user_read_request.characteristic));
/*
					if(m_charList[i].m_pRequestHandler)
					{
						m_charList[i].m_pRequestHandler(data, &data_lenght, GATT_RequestTypeRead);
						rsp = (struct gecko_cmd_packet *) gecko_cmd_gatt_server_send_user_read_response(evt->data.evt_gatt_server_user_read_request.connection,
								evt->data.evt_gatt_server_user_read_request.characteristic, 0x00, data_lenght, data);
					}
*/
					TRACE(" -> Ok\n");
					return;
				}
			}
			break;
		}
		// GATT SERVER USER WRITE REQUEST (remote GATT client wrote a new value to a user-type characteristic)
		case gecko_evt_gatt_server_user_write_request_id:
		{
			TRACE("BLE GATT write: ");
			for(int i = 0; i< GATTDB_LIST_SIZE; i++)
			{
				if(m_charList[i].m_pCharacteristic == evt->data.evt_gatt_server_user_write_request.characteristic)
				{
					TRACE(getCharNameById(evt->data.evt_gatt_server_user_read_request.characteristic));
/*
					if(m_charList[i].m_pRequestHandler)
					{
						m_charList[i].m_pRequestHandler(evt->data.evt_gatt_server_user_write_request.value.data, &evt->data.evt_gatt_server_user_write_request.value.len, GATT_RequestTypeWrite);
						// send back "success" response packet manually (GATT structure has `type="user"` set)
						rsp = (struct gecko_cmd_packet *)gecko_cmd_gatt_server_send_user_write_response(evt->data.evt_gatt_server_user_write_request.connection, evt->data.evt_gatt_server_user_write_request.characteristic, 0x00);
					}
				*/
					TRACE(" <- Ok\n");
					return;
				}
			}
			rsp = (struct gecko_cmd_packet *)gecko_cmd_gatt_server_send_user_write_response(evt->data.evt_gatt_server_user_write_request.connection, evt->data.evt_gatt_server_user_write_request.characteristic, 0x80); /* CUSTOM ERROR (0x80-0xFF are user-defined) */
			break;
		}
		default:
			break;
	}
}

bool BLE_NotificationSend(uint16_t uCharacteristic)
{
	if(m_bIsConnected == false)	return false;
	for(int i = 0; i< GATTDB_LIST_SIZE; i++)
	{
		if(m_charList[i].m_pCharacteristic == uCharacteristic /* && m_charList[i].m_pRequestHandler */)
		{
			uint8_t data_lenght = 0;
			uint8_t data[GATTDB_DATA_SIZE_MAX];
/*
			if(m_charList[i].m_pRequestHandler(data, &data_lenght, GATT_RequestTypeRead))
			{
				rsp = (struct gecko_cmd_packet *)gecko_cmd_gatt_server_send_characteristic_notification(m_hConnection, uCharacteristic,	data_lenght, data);
			}
			*/
			return true;
		}
	}
	return false;
}


void taskBleControl(void *pParmas)
{
	int i = 0;
	struct gecko_cmd_packet * rsv = NULL;

//	hBleQueue = xQueueCreate(4, sizeof(rsv));
//	configASSERT(hBleQueue);

//	BLE_HostInit();

	while(1)
	{
	//	if(xQueueReceive(hBleQueue,&rsv,portMAX_DELAY) == pdTRUE)
	//	{
/*			if(BLE_IsEvent())
			{
				if(i==4)
				{
					TRACE(".");
				}
				BLE_ProcessEvent();
				i++;
			}
			*/
			vTaskDelay(10);
	//	}
	}
}

