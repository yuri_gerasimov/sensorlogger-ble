/*
 * AppGUI.h
 *
 *  created on: 28 ���. 2016 �.
 *      Author: Yuriy
 */

#ifndef SRC_APPGUI_H_
#define SRC_APPGUI_H_

#include "gui.h"

class GTempGraphWnd : public GWnd
{
private:
	GGraphCtrl graphCtrl;
	GRadioCtrl radioCtrl;
	GBatteryCtrl batCtrl;
	GBleCtrl bleCtrl;
	GUsbCtrl usbCtrl;
	void UpdateDateTime(void);

public:
//	GTempGraphWnd(void);
	virtual ~GTempGraphWnd() {}
	virtual void OnDraw(void);
	virtual void OnMsgReceive(MSG msg, void *pMsgData);

	void create(int16_t x, int16_t y, int16_t w, int16_t h);
	void SetData(int16_t *pData, uint16_t uDataSize);
	bool SetDataOffset(uint16_t offset)
	{
		return graphCtrl.SetHorizontalOffset(offset);
	}
};

class DashboardWnd : public GWnd
{
private:
	GBatteryCtrl batCtrl;
	GBleCtrl bleCtrl;
	GUsbCtrl usbCtrl;

	void UpdateDateTime(void);

public:
	DashboardWnd(void);
	virtual ~DashboardWnd() {}
	virtual void OnDraw(void);
	virtual void OnMsgReceive(MSG msg, void *pMsgData);
	void create(int16_t x, int16_t y, int16_t w, int16_t h);
};

class LightWnd : public GWnd
{
private:
	int32_t m_iLightLimit;

	GBatteryCtrl batCtrl;
	GBleCtrl bleCtrl; 
	GUsbCtrl usbCtrl;

	GProgressBar redValue;
	GProgressBar greenValue;
	GProgressBar blueValue;

	GGaugeCtrl alsGauge;
	GGaugeCtrl irGauge;
	GGaugeCtrl uvGauge;

	void UpdateDateTime(void);

public:
	LightWnd(void);
	virtual ~LightWnd() {}
	virtual void OnDraw(void);
	virtual void OnMsgReceive(MSG msg, void *pMsgData);
	void create(int16_t x, int16_t y, int16_t w, int16_t h);
};

class TphWnd : public GWnd
{
private:
	int32_t m_iLightLimit;

	GBatteryCtrl batCtrl;
	GBleCtrl bleCtrl;
	GUsbCtrl usbCtrl;

	GGaugeCtrl tGauge;
	GGaugeCtrl pGauge;
	GGaugeCtrl hGauge;

	GGraphicWnd graphWnd;

	GArcProgressCtrl sectorCtrl;

	void UpdateDateTime(void);

public:
	TphWnd(void);
	virtual ~TphWnd() {}
	virtual void OnDraw(void);
	virtual void OnMsgReceive(MSG msg, void *pMsgData);
	void create(int16_t x, int16_t y, int16_t w, int16_t h);
};

class TimeWnd : public GWnd
{
private:
	int16_t cx, cy;
//	int32_t m_iLightLimit;

	GBatteryCtrl batCtrl;
	GBleCtrl bleCtrl;
	GUsbCtrl usbCtrl;

	GArcProgressCtrl sectorHCtrl;
	GArcProgressCtrl sectorMCtrl;
	GArcProgressCtrl sectorSCtrl;

	GArcProgressCtrl tempCtrl;
	GArcProgressCtrl presCtrl;

	void UpdateDateTime(void);

public:
	TimeWnd(void);
	virtual ~TimeWnd() {}
	virtual void OnDraw(void);
	virtual void OnMsgReceive(MSG msg, void *pMsgData);
	void create(int16_t x, int16_t y, int16_t w, int16_t h);
};

#endif /* SRC_APPGUI_H_ */
