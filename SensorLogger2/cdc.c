/*
 * usbcdc.c
 *
 *  Created on: 26.11.2015
 *      Author: Yuriy Gerasimov
 */
#include <stdint.h>

#include "em_usb.h"
#include "cdc.h"
#include "descriptors.h"

STATIC_UBUF(usbRxBuffer0, CDC_USB_RX_BUF_SIZ); /* USB receive buffers.   */
STATIC_UBUF(usbRxBuffer1, CDC_USB_RX_BUF_SIZ);
STATIC_UBUF(uartRxBuffer0, CDC_USB_TX_BUF_SIZ); /* UART receive buffers.  */
STATIC_UBUF(uartRxBuffer1, CDC_USB_TX_BUF_SIZ);

static int usbRxIndex, usbBytesReceived;
static int uartRxIndex, uartRxCount;
static bool usbRxActive = false;
static bool usbTxActive = false;
static bool bIsCdcActive = false;
static const uint8_t *usbRxBuffer[2] = 	{	usbRxBuffer0,	usbRxBuffer1	};
static const uint8_t *uartRxBuffer[2] = {	uartRxBuffer0,	uartRxBuffer1	};

//static void CDC_StateChangeEvent(USBD_State_TypeDef oldState, USBD_State_TypeDef newState);
static int LineCodingReceived(USB_Status_TypeDef status, uint32_t xferred, uint32_t remaining);
static int UsbDataReceived(USB_Status_TypeDef status, uint32_t xferred, uint32_t remaining);
static int UsbDataTransmitted(USB_Status_TypeDef status, uint32_t xferred, uint32_t remaining);

/* The serial port LINE CODING data structure, used to carry information  */
/* about serial port baudrate, parity etc. between host and device.       */
EFM32_PACK_START(1)typedef struct
{
	uint32_t dwDTERate; /** Baudrate                            */
	uint8_t bCharFormat; /** Stop bits, 0=1 1=1.5 2=2            */
	uint8_t bParityType; /** 0=None 1=Odd 2=Even 3=Mark 4=Space  */
	uint8_t bDataBits; /** 5, 6, 7, 8 or 16                    */
	uint8_t dummy; /** To ensure size is a multiple of 4 bytes */
}__attribute__ ((packed)) cdcLineCoding_TypeDef;
EFM32_PACK_END()

/*
 * The LineCoding variable must be 4-byte aligned as it is used as USB
 * transmit and receive buffer.
 */
EFM32_ALIGN(4)EFM32_PACK_START(1)static cdcLineCoding_TypeDef __attribute__ ((aligned(4))) cdcLineCoding =
{
		115200, 0, 0, 8, 0
};
EFM32_PACK_END()


/**************************************************************************//**
 * @brief
 *   Called each time UART Rx timeout period elapses.
 *   Implements UART Rx rate monitoring, i.e. we must behave differently when
 *   UART Rx rate is slow e.g. when a person is typing characters, and when UART
 *   Rx rate is maximum.
 *****************************************************************************/
/*
static void UartRxTimeout(void)
{

}
*/

/**************************************************************************//**
 * @brief
 *   Callback function called each time the USB device state is changed.
 *   Starts keyboard scanning when device has been configured by USB host.
 *
 * @param[in] oldState The device state the device has just left.
 * @param[in] newState The new device state.
 *****************************************************************************/
void CDC_StateChangeEvent(USBD_State_TypeDef oldState, USBD_State_TypeDef newState)
{
	/* Call CDC driver state change event handler. */
	if (newState == USBD_STATE_CONFIGURED)
	{
		/* We have been configured, start CDC functionality ! */
		if (oldState == USBD_STATE_SUSPENDED) /* Resume ?   */
		{
		}

		/* Start receiving data from USB host. */
		bIsCdcActive = true;
		usbRxIndex = 0;
		usbRxActive = true;
		USBD_Read(CDC_EP_DATA_OUT, (void*) usbRxBuffer[usbRxIndex],	CDC_USB_RX_BUF_SIZ, UsbDataReceived);
//		USBTIMER_Start(CDC_TIMER_ID, CDC_RX_TIMEOUT, UartRxTimeout);
	}

	else if ((oldState == USBD_STATE_CONFIGURED) && (newState != USBD_STATE_SUSPENDED))
	{
		/* We have been de-configured, stop CDC functionality. */
//		USBTIMER_Stop(CDC_TIMER_ID);
		bIsCdcActive = false;
	}

	else if (newState == USBD_STATE_SUSPENDED)
	{
		bIsCdcActive = false;
		/* We have been suspended, stop CDC functionality. */
		/* Reduce current consumption to below 2.5 mA.     */
//		USBTIMER_Stop(CDC_TIMER_ID);
	}
}

/**************************************************************************//**
 * @brief
 *   Handle USB setup commands. Implements CDC class specific commands.
 *
 * @param[in] setup Pointer to the setup packet received.
 *
 * @return USB_STATUS_OK if command accepted.
 *         USB_STATUS_REQ_UNHANDLED when command is unknown, the USB device
 *         stack will handle the request.
 *****************************************************************************/
int CDC_SetupCmd(const USB_Setup_TypeDef *setup)
{
	int retVal = USB_STATUS_REQ_UNHANDLED;

	  if ( ( setup->Type      == USB_SETUP_TYPE_CLASS          ) &&
	       ( setup->Recipient == USB_SETUP_RECIPIENT_INTERFACE )    )
	  {
	    switch (setup->bRequest)
	    {
	    case USB_CDC_GETLINECODING:
	      /********************/
	      if ( ( setup->wValue    == 0                     ) &&
	           ( setup->wIndex    == CDC_CTRL_INTERFACE_NO ) && /* Interface no. */
	           ( setup->wLength   == 7                     ) && /* Length of cdcLineCoding. */
	           ( setup->Direction == USB_SETUP_DIR_IN      )    )
	      {
	        /* Send current settings to USB host. */
	        USBD_Write(0, (void*) &cdcLineCoding, 7, NULL);
	        retVal = USB_STATUS_OK;
	      }
	      break;

	    case USB_CDC_SETLINECODING:
	      /********************/
	      if ( ( setup->wValue    == 0                     ) &&
	           ( setup->wIndex    == CDC_CTRL_INTERFACE_NO ) && /* Interface no. */
	           ( setup->wLength   == 7                     ) && /* Length of cdcLineCoding. */
	           ( setup->Direction != USB_SETUP_DIR_IN      )    )
	      {
	        /* Get new settings from USB host. */
	        USBD_Read(0, (void*) &cdcLineCoding, 7, LineCodingReceived);
	        retVal = USB_STATUS_OK;
	      }
	      break;

	    case USB_CDC_SETCTRLLINESTATE:
	      /********************/
	      if ( ( setup->wIndex  == CDC_CTRL_INTERFACE_NO ) &&   /* Interface no.  */
	           ( setup->wLength == 0                     )    ) /* No data.       */
	      {
	        /* Do nothing ( Non compliant behaviour !! ) */
	        retVal = USB_STATUS_OK;
	      }
	      break;
	    }
	  }

	  return retVal;

#if 0
	int retVal = USB_STATUS_REQ_UNHANDLED;

	if ((setup->Type == USB_SETUP_TYPE_CLASS) && (setup->Recipient == USB_SETUP_RECIPIENT_INTERFACE))
	{
		switch (setup->bRequest)
		{
			case USB_CDC_GETLINECODING:
				/********************/
			if ((setup->wValue == 0)
					&& (setup->wIndex == CDC_CTRL_INTERFACE_NO)&& /* Interface no. */
					( setup->wLength == 7 ) && /* Length of cdcLineCoding. */
					( setup->Direction == USB_SETUP_DIR_IN ) ){
					/* Send current settings to USB host. */
					USBD_Write(0, (void*) &cdcLineCoding, 7, NULL);
					retVal = USB_STATUS_OK;
				}
				break;

			case USB_CDC_SETLINECODING:
				/********************/
				if ( ( setup->wValue == 0 ) &&
						( setup->wIndex == CDC_CTRL_INTERFACE_NO ) && /* Interface no. */
						( setup->wLength == 7 ) && /* Length of cdcLineCoding. */
						( setup->Direction != USB_SETUP_DIR_IN ) )
				{
					/* Get new settings from USB host. */
					USBD_Read(0, (void*) &cdcLineCoding, 7, LineCodingReceived);
					retVal = USB_STATUS_OK;
				}
				break;

			case USB_CDC_SETCTRLLINESTATE:
				/********************/
				if ( ( setup->wIndex == CDC_CTRL_INTERFACE_NO ) && /* Interface no.  */
						( setup->wLength == 0 ) ) /* No data.       */
				{
					/* Do nothing ( Non compliant behaviour !! ) */
					retVal = USB_STATUS_OK;
				}
				break;
			}
		}

	return retVal;
#endif
}

/**************************************************************************//**
 * @brief
 *   Callback function called when the data stage of a CDC_SET_LINECODING
 *   setup command has completed.
 *
 * @param[in] status    Transfer status code.
 * @param[in] xferred   Number of bytes transferred.
 * @param[in] remaining Number of bytes not transferred.
 *
 * @return USB_STATUS_OK if data accepted.
 *         USB_STATUS_REQ_ERR if data calls for modes we can not support.
 *****************************************************************************/
static int LineCodingReceived(USB_Status_TypeDef status, uint32_t xferred, uint32_t remaining)
{
	uint32_t frame = 0;
	(void) remaining;

	/* We have received new serial port communication settings from USB host. */
	if ((status == USB_STATUS_OK) && (xferred == 7))
	{
		/* Check bDataBits, valid values are: 5, 6, 7, 8 or 16 bits. */
		if (cdcLineCoding.bDataBits == 5)
			frame |= USART_FRAME_DATABITS_FIVE;
		else if (cdcLineCoding.bDataBits == 6)
			frame |= USART_FRAME_DATABITS_SIX;
		else if (cdcLineCoding.bDataBits == 7)
			frame |= USART_FRAME_DATABITS_SEVEN;
		else if (cdcLineCoding.bDataBits == 8)
			frame |= USART_FRAME_DATABITS_EIGHT;
		else if (cdcLineCoding.bDataBits == 16)
			frame |= USART_FRAME_DATABITS_SIXTEEN;
		else
			return USB_STATUS_REQ_ERR;

		/* Check bParityType, valid values are: 0=None 1=Odd 2=Even 3=Mark 4=Space  */
		if (cdcLineCoding.bParityType == 0)
			frame |= USART_FRAME_PARITY_NONE;
		else if (cdcLineCoding.bParityType == 1)
			frame |= USART_FRAME_PARITY_ODD;
		else if (cdcLineCoding.bParityType == 2)
			frame |= USART_FRAME_PARITY_EVEN;
		else if (cdcLineCoding.bParityType == 3)
			return USB_STATUS_REQ_ERR;
		else if (cdcLineCoding.bParityType == 4)
			return USB_STATUS_REQ_ERR;
		else
			return USB_STATUS_REQ_ERR;

		/* Check bCharFormat, valid values are: 0=1 1=1.5 2=2 stop bits */
		if (cdcLineCoding.bCharFormat == 0)
			frame |= USART_FRAME_STOPBITS_ONE;
		else if (cdcLineCoding.bCharFormat == 1)
			frame |= USART_FRAME_STOPBITS_ONEANDAHALF;
		else if (cdcLineCoding.bCharFormat == 2)
			frame |= USART_FRAME_STOPBITS_TWO;
		else
			return USB_STATUS_REQ_ERR;

		/* Program new UART baudrate etc. */
		//   CDC_UART->FRAME = frame;
		//   USART_BaudrateAsyncSet(CDC_UART, 0, cdcLineCoding.dwDTERate, usartOVS16);
		return USB_STATUS_OK;
	}
	return USB_STATUS_REQ_ERR;
}

/**************************************************************************//**
 * @brief Callback function called whenever a new packet with data is received
 *        on USB.
 *
 * @param[in] status    Transfer status code.
 * @param[in] xferred   Number of bytes transferred.
 * @param[in] remaining Number of bytes not transferred.
 *
 * @return USB_STATUS_OK.
 *****************************************************************************/
static int UsbDataReceived(USB_Status_TypeDef status, uint32_t xferred, uint32_t remaining)
{
	(void) remaining; /* Unused parameter. */

	if ((status == USB_STATUS_OK) && (xferred > 0))
	{
		usbRxIndex ^= 1;
		usbRxActive = false;
		usbBytesReceived = xferred;
		USBD_Read(CDC_EP_DATA_OUT, (void*) usbRxBuffer[ usbRxIndex ], CDC_USB_RX_BUF_SIZ, UsbDataReceived);
	}
	return USB_STATUS_OK;
}

static int UsbDataTransmitted(USB_Status_TypeDef status, uint32_t xferred, uint32_t remaining)
{
	(void) xferred; /* Unused parameter. */
	(void) remaining; /* Unused parameter. */

	if (status == USB_STATUS_OK)
	{
	//	USBTIMER_Start(CDC_TIMER_ID, CDC_RX_TIMEOUT, UartRxTimeout);
	}
	usbTxActive = false;
	return USB_STATUS_OK;
}

int CDC_BytesAvailable(void)
{
	if (!bIsCdcActive || usbRxActive == true)
		return 0;
	return usbBytesReceived;
}

bool CDC_IsTxActive(void)
{
	return usbTxActive;
}

int CDC_Print(const char *strText)
{
	return CDC_Write((void *)strText,strlen(strText));
}

int CDC_Write(void *pBuffer, int uSize)
{
	if(!bIsCdcActive || usbTxActive)	return 0;

	usbTxActive = true;
	memcpy((void*) uartRxBuffer[uartRxIndex ^ 1], pBuffer, uSize);
	uartRxCount = uSize;

	USBD_Write(CDC_EP_DATA_IN, (void*) uartRxBuffer[uartRxIndex ^ 1], uartRxCount, UsbDataTransmitted);
//    USBTIMER_Start(CDC_TIMER_ID, CDC_RX_TIMEOUT, UartRxTimeout);
	return uartRxCount;
}

int CDC_Read(void *pBuffer, int uSize)
{
	if (bIsCdcActive && usbRxActive == false)
	{
		if(uSize > usbBytesReceived) uSize = usbBytesReceived;
		memcpy(pBuffer, (void*) usbRxBuffer[usbRxIndex ^ 1], uSize);
		usbBytesReceived = 0;
		return uSize;
	}
	return 0;
}
