/*
 * hid.c
 *
 *  Created on: Jan 4, 2015
 *      Author: YuvalK
 */

#include "em_usb.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_common.h"
#include "hid.h"

static uint8_t HIDbufin[65];
static uint8_t HIDbufout[65];
void (*m_pOutputReportHandler)(uint8_t uReportId, void *pData) = NULL;
static int HID_OutputReportReceived(USB_Status_TypeDef status, uint32_t xferred, uint32_t remaining);

void HID_Init(void (*pOutputReportHandler)(uint8_t uReportId, void *pData))
{
	m_pOutputReportHandler = pOutputReportHandler;
}

/**************************************************************************//**
 * @brief
 *   Callback function called each time the USB device state is changed.
 *   Starts HID operation when device has been configured by USB host.
 *
 * @param[in] oldState The device state the device has just left.
 * @param[in] newState The new device state.
 *****************************************************************************/
void HID_StateChangeEvent(USBD_State_TypeDef oldState, USBD_State_TypeDef newState)
{
  if (newState == USBD_STATE_CONFIGURED)
  {
    /* We have been configured, start HID functionality ! */
    if (oldState != USBD_STATE_SUSPENDED)   /* Resume ?   */
    {

    }
    USBD_Read(HID_EP_DATA_OUT, (void*) HIDbufin, 1, HID_OutputReportReceived);
  }
  else if ((oldState == USBD_STATE_CONFIGURED) && (newState != USBD_STATE_SUSPENDED))
  {

  }
  else if (newState == USBD_STATE_SUSPENDED)
  {

  }
}

/**************************************************************************//**
 * @brief
 *   Handle USB setup commands. Implements HID class specific commands.
 *
 * @param[in] setup Pointer to the setup packet received.
 *
 * @return USB_STATUS_OK if command accepted.
 *         USB_STATUS_REQ_UNHANDLED when command is unknown, the USB device
 *         stack will handle the request.
 *****************************************************************************/
int HID_SetupCmd(const USB_Setup_TypeDef *setup)
{
  STATIC_UBUF( hidDesc, USB_HID_DESCSIZE );

  int retVal = USB_STATUS_REQ_UNHANDLED;

  if ((setup->Type == USB_SETUP_TYPE_STANDARD) &&
      (setup->Direction == USB_SETUP_DIR_IN) &&
      (setup->Recipient == USB_SETUP_RECIPIENT_INTERFACE))
  {
    /* A HID device must extend the standard GET_DESCRIPTOR command   */
    /* with support for HID descriptors.                              */
    switch (setup->bRequest)
    {
    case GET_DESCRIPTOR:
      /********************/
      if ((setup->wValue >> 8) == USB_HID_REPORT_DESCRIPTOR)
      {
        USBD_Write(0, (void*) ReportDescriptor, EFM32_MIN(sizeof(ReportDescriptor), setup->wLength), NULL);
        retVal = USB_STATUS_OK;
      }
      else if ((setup->wValue >> 8) == USB_HID_DESCRIPTOR)
      {
        /* The HID descriptor might be misaligned ! */
        memcpy( hidDesc, &USBDESC_configDesc[ USB_CONFIG_DESCSIZE + USB_INTERFACE_DESCSIZE ], USB_HID_DESCSIZE );
        USBD_Write(0, hidDesc, EFM32_MIN(USB_HID_DESCSIZE, setup->wLength), NULL);
        retVal = USB_STATUS_OK;
      }
 //     DEBUG_USB_API_PUTS( "In GET_DESCRIPTOR\r\n");
      break;
    }
  }
  else if ((setup->Type == USB_SETUP_TYPE_CLASS) &&
           (setup->Recipient == USB_SETUP_RECIPIENT_INTERFACE))
  {
    /* Implement the necessary HID class specific commands.           */
    switch (setup->bRequest)
    {
    case USB_HID_SET_REPORT:
      /********************/
 //   	DEBUG_USB_API_PUTS( "s");
    	if (((setup->wValue >> 8) == 2) &&          /* Output report */
          ((setup->wValue & 0xFF) == 0xFD) &&          /* Report ID     */
          (setup->wIndex == HID_INTERFACE_NO) &&    /* Interface no. */
          (setup->wLength == 64) &&    				/* Report length */
          (setup->Direction != USB_SETUP_DIR_IN))
      {
        USBD_Read(HID_EP_DATA_OUT, (void*) HIDbufin, 64, HID_OutputReportReceived);
        retVal = USB_STATUS_OK;
      }
      break;

    case USB_HID_GET_REPORT:
      /********************/
//      DEBUG_USB_API_PUTS( "g");
      if (((setup->wValue >> 8) == 1) &&            /* Input report  */
          ((setup->wValue & 0xFF) == 1) &&          /* Report ID     */
          (setup->wIndex == HID_INTERFACE_NO) &&    /* Interface no. */
          (setup->wLength == 64) &&                 /* Report length */
          (setup->Direction == USB_SETUP_DIR_IN))
      {
        retVal = USB_STATUS_OK;
      }
      break;

    case USB_HID_SET_IDLE:
      /********************/
      if (((setup->wValue & 0xFF) == 0) &&          /* Report ID     */
          (setup->wIndex == 0) &&                   /* Interface no. */
          (setup->wLength == 0) &&
          (setup->Direction != USB_SETUP_DIR_IN))
      {
        retVal = USB_STATUS_OK;
      }
      break;

    case USB_HID_GET_IDLE:
      /********************/
      if ((setup->wValue == 0) &&                   /* Report ID     */
          (setup->wIndex == 0) &&                   /* Interface no. */
          (setup->wLength == 1) &&
          (setup->Direction == USB_SETUP_DIR_IN))
      {
        *(uint8_t*)&HIDbufin = POLL_RATE / 4;
        USBD_Write(0, (void*) HIDbufin, 1, NULL);
        retVal = USB_STATUS_OK;
      }
//      DEBUG_USB_API_PUTS( "In USB_HID_GET_IDLE\r\n");
      break;
    }
  }
//  else
//	  DEBUG_USB_API_PUTS( "In HID_SetupCmd\r\n");

  return retVal;
}

/**************************************************************************//**
 * @brief
 *   Callback function called when the data stage of a USB_HID_SET_REPORT
 *   setup command has completed.
 *
 * @param[in] status    Transfer status code.
 * @param[in] xferred   Number of bytes transferred.
 * @param[in] remaining Number of bytes not transferred.
 *
 * @return USB_STATUS_OK.
 *****************************************************************************/
static int HID_OutputReportReceived(USB_Status_TypeDef status, uint32_t xferred, uint32_t remaining)
{
  (void) status;
  (void) xferred;
  (void) remaining;

//  DEBUG_USB_API_PUTS("In OutputReportReceived");
  USBD_Read(HID_EP_DATA_OUT, (void*) HIDbufin, 64, HID_OutputReportReceived);

  //First byte is the report ID
  if(m_pOutputReportHandler!=NULL)
	  m_pOutputReportHandler(*((uint8_t*)HIDbufin), (void*)(&HIDbufin[1]));
  //proccessMsg(&HIDbufin[1]);

  return USB_STATUS_OK;
}

void HID_SendData(const uint8_t* buf, int len, uint8_t uReportId)
{
  memcpy(&HIDbufout[1],buf,len);
  //We must start with the input report ID
  HIDbufout[0] = uReportId;
  //We must send 64 bytes + report ID
  USBD_Write(HID_EP_DATA_IN, (void*) HIDbufout, sizeof(HIDbufout), NULL);
}
