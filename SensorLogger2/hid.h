/*
 * hid.h
 *
 *  Created on: Jan 4, 2015
 *      Author: YuvalK
 */

#ifndef HID_H_
#define HID_H_

/*** Typedef's and defines. ***/

#include "descriptors.h"

#ifdef __cplusplus
extern "C" {
#endif

/*** Function prototypes. ***/
void HID_Init(void (*pOutputReportHandler)(uint8_t uReportId, void *pData));
int HID_SetupCmd(const USB_Setup_TypeDef *setup);
void HID_StateChangeEvent( USBD_State_TypeDef oldState, USBD_State_TypeDef newState);
void HID_SendData(const uint8_t* buf, int len, uint8_t uReportId);

#ifdef __cplusplus
}
#endif

#endif /* HID_H_ */
