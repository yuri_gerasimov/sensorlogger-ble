/*
 * SensorLogger.h
 *
 *  Created on: 28 ���. 2016 �.
 *      Author: Yuriy
 */

#include "stdint.h"
#include "stdbool.h"

#include "hw_config.h"

// --- FreeRTOS include files ---
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"

#include "MAX44009.h"
#include "BME280.h"
#include "FXOS8700CQ.h"

#include "rgb.h"

#ifndef SRC_SENSORLOGGER_H_
#define SRC_SENSORLOGGER_H_

//	------------ TYPES of SENSORS ---------------------------------
typedef enum
{
	SL_SensorType_Undefined,
	SL_SensorType_Temperature,
	SL_SensorType_Pressure,
	SL_SensorType_Humidity,
	SL_SensorType_ALS,
	SL_SensorType_RGB,
	SL_SensorType_IR,
	SL_SensorType_UV,
	SL_SensorType_Accelerometer,
	SL_SensorType_Magnetometer,
	SL_SensorType_Hyroscope,
	SL_SensorType_Voltage,
	SL_SensorType_Current,
	SL_SensorType_Maximum,
} SL_SensorType_t;

class SensorLogger
{
public:
	SensorLogger();
	virtual ~SensorLogger();

	bool init(void);
	bool initSensors(void);

	static void timerHandlerSensorData(TimerHandle_t xTimer);

	bool updateTemperature(uint8_t nSensorIndexMask);
	bool updatePressure(uint8_t nSensorIndexMask);
	bool updateHumidity(uint8_t nSensorIndexMask);
	bool updateACC(uint8_t nSensorIndexMask);
	bool updateMGN(uint8_t nSensorIndexMask);
	bool updateALS(uint8_t nSensorIndexMask);
	bool updateRGB(uint8_t nSensorIndexMask);
	bool updateUV(uint8_t nSensorIndexMask);
	bool updateIR(uint8_t nSensorIndexMask);

	int32_t getTemperature(uint8_t uSensorIndex);
	uint32_t getPressure(uint8_t uSensorIndex);
	uint32_t getHumidity(uint8_t uSensorIndex);
	AccelerometerData_t * getACC(void);
	MagnetometerData_t * getMGN(void);
	uint32_t getALS(uint8_t uSensorIndex);
	RGB32_t* getRGB(uint8_t uSensorIndex);
	uint32_t getUV(uint8_t uSensorIndex);
	uint32_t getIR(uint8_t uSensorIndex);

private:
//	uint32_t m_ulSensorDataMask;
	TimerHandle_t m_hSensorDataTimer;
	uint32_t m_ulSensorDataTimeout;

private:
	uint32_t m_ulSensorDataMask;

	int32_t m_lTemperature[config_SENSOR_NUMBER_T];
	uint32_t m_ulPressure;
	uint32_t m_ulHumidity;

	AccelerometerData_t m_accData;
	MagnetometerData_t 	m_mgnData;

	uint32_t m_ulAlsData[config_SENSOR_NUMBER_ALS];

	RGB32_t m_rgbData;
	uint32_t m_ulIrData;
	uint32_t m_ulUvData;
};

extern SensorLogger logger;

#endif /* SRC_SENSORLOGGER_H_ */
