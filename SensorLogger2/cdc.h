/*
 * usbcdc.h
 *
 *  Created on: 26.11.2015
 *      Author: Yuriy
 */

#ifndef USBCDC_H_
#define USBCDC_H_

#include <stdint.h>
#include "em_usb.h"

#ifdef __cplusplus
extern "C" {
#endif

#define CDC_BULK_EP_SIZE  (USB_FS_BULK_EP_MAXSIZE) /* This is the max. ep size.    */
#define CDC_USB_RX_BUF_SIZ  CDC_BULK_EP_SIZE /* Packet size when receiving on USB. */
#define CDC_USB_TX_BUF_SIZ  127    /* Packet size when transmitting on USB.  */

#define CDC_RX_TIMEOUT    EFM32_MAX(10U, 50000 / (cdcLineCoding.dwDTERate))

//extern const USBD_Init_TypeDef usbInitStruct;

int CDC_SetupCmd(const USB_Setup_TypeDef *setup);
void CDC_StateChangeEvent(USBD_State_TypeDef oldState, USBD_State_TypeDef newState);

int CDC_Read(void *pBuffer, int uSize);
int CDC_Write(void *pBuffer, int uSize);
int CDC_Print(const char *strText);
int CDC_BytesAvailable(void);
bool CDC_IsTxActive(void);

#ifdef __cplusplus
}
#endif

#endif /* USBCDC_H_ */
