/*
 * dmactrl.h
 *
 *  Created on: 05.01.2016
 *      Author: Yuriy
 */

#ifndef DMACTRL_H_
#define DMACTRL_H_

#ifdef __cplusplus
extern "C" {
#endif

void initDMA(void);
bool isDmaBusy(void);
void startDmaTransfer(uint16_t *sourceAddress, uint16_t *destAddress, int numElements);

#ifdef __cplusplus
}
#endif

#endif /* DMACTRL_H_ */
