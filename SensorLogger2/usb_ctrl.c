/*
 * usb_ctrl.c
 *
 *  Created on: 20.04.2016
 *      Author: Yuriy
 */

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "em_usb.h"
#include "rtc.h"

#include "descriptors.h"
#include "usb_ctrl.h"

#include "msdd.h"
#include "msddmedia.h"
#include "cdc.h"
#include "hid.h"

//#include "SensorLogger.h"

//	��� ��� ������ ����� � ��� ���� ������, ��� ������

static const USBD_Callbacks_TypeDef callbacks =
{
		.usbReset = NULL,
		.usbStateChange = StateChangeEvent,
		.setupCmd = SetupCmd,
		.isSelfPowered = NULL,
		.sofInt = NULL
};

static const USBD_Init_TypeDef initstruct =
{
		.deviceDescriptor = &USBDESC_deviceDesc,
		.configDescriptor = USBDESC_configDesc,
		.stringDescriptors = USBDESC_strings,
		.numberOfStrings = sizeof(USBDESC_strings) / sizeof(void*),
		.callbacks = &callbacks,
		.bufferingMultiplier = bufferingMultiplier,
		.reserved = 0
};

int SetupCmd(const USB_Setup_TypeDef *setup)
{
	  int retVal;

	  /* Call SetupCmd handlers for all functions within the composite device. */
	  retVal = MSDD_SetupCmd( setup );

	  if ( retVal == USB_STATUS_REQ_UNHANDLED )
	  {
	    retVal = HID_SetupCmd( setup );
	  }

	  if ( retVal == USB_STATUS_REQ_UNHANDLED )
	  {
	    retVal = CDC_SetupCmd( setup );
	  }
	  return retVal;
}

void StateChangeEvent( USBD_State_TypeDef oldState, USBD_State_TypeDef newState )
{
  /* Call device StateChange event handlers for all relevant functions within the composite device. */
	MSDD_StateChangeEvent(oldState, newState);
	CDC_StateChangeEvent(oldState, newState);
	HID_StateChangeEvent(oldState, newState);
}


bool initUSB(USART_TypeDef *pMicroSD_USART)
{
	/* Initialize the Mass Storage Media. */

	if(!MSDDMEDIA_Init(pMicroSD_USART))
	{
	    EFM_ASSERT( false );
	    for( ;; ){}
	}

//	CDC_Init();                  		/* Initialize the communication class device. */
	MSDD_Init( -1, 0 );           		/* Initialize the Mass Storage Device.  */
	HID_Init(HID_OnReportRecieved);		//	init and set HID receive handler

	/* Initialize and start USB device stack. */
	USBD_Init(&initstruct);

	/*
	* When using a debugger it is practical to uncomment the following three
	* lines to force host to re-enumerate the device.
	*/
	/*
	 USBD_Disconnect();
	 USBTIMER_DelayMs( 1000 );
	 USBD_Connect();
	 */

	return true;
}
/*
void HID_OnReportRecieved(uint8_t uReportId, void *pData)
{
	switch(uReportId)
	{
		case SL_OutReportId_DeviceCmd:		//	start device command (do something or response some data)
		{
			uint8_t *pPayload = (uint8_t *)pData;
			SL_DeviceCmdType_t cmd = *((SL_DeviceCmdType_t*)pData);
			deviceOnCmd(cmd, &pPayload[1]);
			break;
		}
		case SL_OutReportId_DeviceTime:		//	set device time
		{
			datetime_t *dt = (datetime_t *)pData;
			deviceSetTime(dt);
			break;
		}
		case SL_OutReportId_DeviceParams:	//	set device parameters
		{
			deviceSetParams((SL_DeviceParameters_t *)pData);
			break;
		}
		case SL_OutReportId_DeviceModeStatus:	//	set device mode / status
		{
			deviceSetModeStatus((SL_DeviceModeStatus_t*)pData);
			break;
		}
		default:
			break;
	}
}
*/
void HID_OnReportRecieved(uint8_t uReportId, void *pData)
{
	/*
	switch(uReportId)
	{
		case SL_OutReportId_DeviceCmd:			//	start device command (do something or response some data)
		{
			uint8_t *pPayload = (uint8_t *)pData;
			SL_DeviceCmdType_t cmd = *((SL_DeviceCmdType_t*)pData);
		//	deviceOnCmd(cmd, &pPayload[1]);
			break;
		}
		case SL_OutReportId_DeviceParams:		//	set different device parameters
		{
			uint8_t *ptr = (uint8_t *)pData;
			//SL_DeviceCmdType_t cmd = *((SL_DeviceCmdType_t*)pData);
			//deviceSetParams(cmd,&pPayload[1]);
		//	deviceSetParams((uint8_t *)pData);
			break;
		}
		case SK_OutReportId_FlashData:			//	write or erase data from flash
		{
			//SL_FlashCmdInfo_t *flashCmd = *((SL_FlashCmdInfo_t *)pData);
		//	deviceHidFlashCmd((SL_FlashCmdInfo_t*)pData);
			break;
		}
		default:
			break;
	}
	*/
}
