/*
 * dmactrl.c
 *
 *  Created on: 05.01.2016
 * 	Author: Yuriy Gerasimov  (yuri.y.gerasimov@gmail.com)
 */
#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include "hw_config.h"

#include "em_device.h"
#include "em_dma.h"
#include "em_cmu.h"

#include "dmactrl.h"

#if ( ( DMA_CHAN_COUNT > 0 ) && ( DMA_CHAN_COUNT <= 4 ) )
#define DMACTRL_CH_CNT      4
#define DMACTRL_ALIGNMENT   128

#elif ( ( DMA_CHAN_COUNT > 4 ) && ( DMA_CHAN_COUNT <= 8 ) )
#define DMACTRL_CH_CNT      8
#define DMACTRL_ALIGNMENT   256

#elif ( ( DMA_CHAN_COUNT > 8 ) && ( DMA_CHAN_COUNT <= 12 ) )
#define DMACTRL_CH_CNT      16
#define DMACTRL_ALIGNMENT   256

#else
#error "Unsupported DMA channel count (dmactrl.c)."
#endif


/** DMA control block array, requires proper alignment. */
#if defined (__ICCARM__)
#pragma data_alignment=DMACTRL_ALIGNMENT
DMA_DESCRIPTOR_TypeDef dmaControlBlock[DMACTRL_CH_CNT * 2];

#elif defined (__CC_ARM)
DMA_DESCRIPTOR_TypeDef dmaControlBlock[DMACTRL_CH_CNT * 2] __attribute__ ((aligned(DMACTRL_ALIGNMENT)));

#elif defined (__GNUC__)
DMA_DESCRIPTOR_TypeDef dmaControlBlock[DMACTRL_CH_CNT * 2] __attribute__ ((aligned(DMACTRL_ALIGNMENT)));

#else
#error Undefined toolkit, need to define alignment
#endif

/* Use channel 0 */
#define DMA_CHANNEL 0

/* Max number of elements the DMA can transfer in one cycle */
#define DMA_MAX_TRANSFER_SIZE 1024

/* 320 * 240 pixels, 1024 pixels per dma transfer = 75 transfers */
#define NUM_DMA_TRANSFERS 75

/* DMA init structure */
DMA_Init_TypeDef dmaInit;

/* DMA callback structure */
DMA_CB_TypeDef dmaCallback;

/* Flag to check if DMA is currently active */
static volatile bool dmaTransferActive = false;

/* Array containing descriptors which will be sequentially loaded
 * to move the whole frame buffer in one DMA operation */
DMA_DESCRIPTOR_TypeDef dmaScatterGatherDescriptors[NUM_DMA_TRANSFERS];

/* Enable clocks and initialize DMA registers */
void initDMA(void)
{
  CMU_ClockEnable(cmuClock_DMA, true);

  dmaInit.hprot = 0;
  dmaInit.controlBlock = dmaControlBlock;
  DMA_Init(&dmaInit);
}

bool isDmaBusy(void)
{
	return dmaTransferActive;
}


/* Called by DMA when transfer is complete */
void transferComplete(unsigned int channel, bool primary, void *user)
{
  (void) channel;
  (void) primary;
  (void) user;

  /* Reset flag to indicate that transfer is done */
  dmaTransferActive = false;
}


/* Start a DMA transfer and return */
void startDmaTransfer(uint16_t *sourceAddress, uint16_t *destAddress, int numElements)
{
  int i;
  int elementsLeft = numElements;
  DMA_CfgChannel_TypeDef channelConfig;
  DMA_CfgDescrSGAlt_TypeDef    descriptorConfig;

  /* Setting call-back function */
  dmaCallback.cbFunc = transferComplete;
  dmaCallback.userPtr = NULL;

  /* Setting up channel */
  channelConfig.highPri = false;              /* No high priority */
  channelConfig.enableInt = true;             /* Enable interrupt */
  channelConfig.select = 0;                   /* Memory to memory transfer */
  channelConfig.cb = &dmaCallback;            /* Callback routine */
  DMA_CfgChannel(DMA_CHANNEL, &channelConfig);

  /* Common values for all the descriptors */
  descriptorConfig.dstInc = dmaDataIncNone;   /* Do not increase destination */
  descriptorConfig.srcInc = dmaDataInc2;      /* Increase source by 2 bytes */
  descriptorConfig.size = dmaDataSize2;       /* Element size is 2 bytes */
  descriptorConfig.arbRate = dmaArbitrate1;
  descriptorConfig.hprot = 0;
  descriptorConfig.nMinus1 = DMA_MAX_TRANSFER_SIZE-1;
  descriptorConfig.peripheral = false;
  descriptorConfig.dst = (void *)destAddress;

  for(i=0; i<NUM_DMA_TRANSFERS; i++ )
  {

    /* Configure number of elements */
    if ( elementsLeft == 0 )
    {
      break;
    }
    else
    	if(elementsLeft < DMA_MAX_TRANSFER_SIZE )
		{
		  descriptorConfig.nMinus1 = elementsLeft-1;
		  elementsLeft = 0;
		}
		else
		{
		  elementsLeft -= DMA_MAX_TRANSFER_SIZE;
		}

    /* Set up source for all descriptors */
    descriptorConfig.src = (void *) (sourceAddress + i*DMA_MAX_TRANSFER_SIZE);

    /* Create the descriptor */
    DMA_CfgDescrScatterGather(dmaScatterGatherDescriptors, i, &descriptorConfig);
  }

  /* Set transfer active flag */
  dmaTransferActive = true;

  /* Start the transfer */
  DMA_ActivateScatterGather(DMA_CHANNEL,false,dmaScatterGatherDescriptors,NUM_DMA_TRANSFERS);
}
